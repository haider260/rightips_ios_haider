

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

/**
 * The BWBeacon class represents a single hardware iBeacon detected by a device. <br/>
 * An iBeacon is identified by a three part identifier based on the fields <br/>
 * proximityUUID - a string UUID typically identifying the owner of a number of ibeacons <br/>
 * major - a 16 bit integer indicating a group of iBeacons <br/>
 * minor - a 16 bit integer identifying a single iBeacon <br/>
 * An iBeacon sends a Bluetooth Low Energy (BLE) advertisement that contains these three identifiers, along with the calibrated tx power (in RSSI) of the iBeacon's Bluetooth transmitter. This class may only be instantiated from a BLE packet, and an RSSI measurement for the packet. The class parses out the three part identifier, along with the calibrated tx power. It then uses the measured RSSI and calibrated tx power to do a rough distance measurement (the accuracy field) and group it into a more reliable buckets of distance (the proximity field.)
 *
 */
@interface BWBeacon : CLBeacon

/**
 *  Use this method to retrieve proximity UUID of BWBeacon.
 *  This value can also be retrieved from _proximityUUID_ property of BWBeaon class.
 *
 *  @return an NSUUID proximity UUID.
 */
-(NSUUID *)getProximityUuid;

/**
 *  Use this method to retrieve major value of BWBeacon.
 *  This value can also be retrieved from major property of BWBeaon class.
 *
 *    Most significant value associated with the beacon.
 *
 *  @return major value as CLBeaconMajorValue
 */
- (CLBeaconMajorValue)getMajor;

/**
 *  Use this method to retrieve minor value of BWBeacon.
 *  This value can also be retrieved from minor property of BWBeaon class.
 *
 *    Least significant value associated with the beacon.
 *
 *  @return minor value as CLBeaconMinorValue
 */
- (CLBeaconMinorValue)getMinor;

/**
 *  Use this method to retrieve proximity value of BWBeacon.
 *  This value can also be retrieved from proximity property of BWBeaon class.
 *
 *    Proximity of the beacon from the device.
 *
 *  @return proximity value as CLProximity
 */
- (CLProximity)getProximity;

/**
 *  Use this method to retrieve accuracy value of BWBeacon.
 *  This value can also be retrieved from accuracy property of BWBeaon class.
 *
 *    Accuracy is represented as an one sigma horizontal accuracy in meters where the measuring device's location is
 *    referenced at the beaconing device. This value is heavily subject to variations in an RF environment.
 *    A negative accuracy value indicates the proximity is unknown.
 *
 *  @return accuracy value as CLLocationAccuracy
 */
- (CLLocationAccuracy)getAccuracy;

/**
 *  Use this method to retrieve rssi value of BWBeacon.
 *  This value can also be retrieved from rssi property of BWBeaon class.
 *
 *    Rssi is received signal strength in decibels of the specified beacon.
 *    This value is an average of the RSSI samples collected since this beacon was last reported.
 *
 *  @return proximity value as CLProximity
 */
- (NSInteger)getRssi;

/**
 *  This method determines if another beacon is same as this beacon. It compares beacons's proximityUUID, major and minor values.
 *
 *  @param beacon A BWBeacon object to compare with this object.
 *
 *  @return returns YES if beacons match otherwise NO.
 */
- (BOOL)matchesBWBeacon:(BWBeacon *)beacon;

@end
