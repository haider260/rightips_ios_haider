

#import <CoreLocation/CoreLocation.h>

/*
 *  BWBeaconRegion
 *
 *  Discussion:
 *    A region containing similar beacons.
 *
 *    Such a region can be defined by proximityUUID, major and minor values.
 *    proximityUUID must be specified. If proximityUUID is only specified, the major and
 *    minor values will be wildcarded and the region will match any beacons with the same
 *    proximityUUID. Similarly if only proximityUUID and major value are specified, the minor value will be
 *    wildcarded and the region will match against any beacons with the same proximityUUID and major
 *    value.
 *
 */
@interface BWBeaconRegion : CLBeaconRegion

/**
 *  Initialize a BWBeaconRegion with a CLBeaconRegion
 *
 *  @param region a CLBeaconRegion object
 *
 *  @return a BWBeaconRegion instance
 */

- (id) initWithCLBeaconRegion:(CLBeaconRegion *)region;

/**
 *  Use this method to retrieve proximity UUID of BWBeaconRegion.
 *  This value can also be retrieved from proximityUUID property of BWBeaconRegion class.
 *
 *  @return an NSUUID proximity UUID.
 */
-(NSUUID *)getProximityUuid;

/**
 *  Use this method to retrieve major value of BWBeaconRegion.
 *  This value can also be retrieved from major property of BWBeaconRegion class.
 *
 *    Most significant value associated with the beacon.
 *
 *  @return major value as CLBeaconMajorValue
 */
- (CLBeaconMajorValue)getMajor;

/**
 *  Use this method to retrieve minor value of BWBeaconRegion.
 *  This value can also be retrieved from minor property of BWBeaconRegion class.
 *
 *    Least significant value associated with the beacon.
 *
 *  @return minor value as CLBeaconMinorValue
 */
- (CLBeaconMinorValue)getMinor;

/**
 *  This method determines if another region is same as this region.
 *  It compares region's proximityUUID, major value, minor value and
 *  region identifier.
 *
 *  @param region a CLBeaconRegion object
 *
 *  @return YES if regions match otherwise NO.
 */
- (BOOL)matchesBWBeaconRegion:(BWBeaconRegion *)region;

/**
 *  This method returns region string in following format. </br>
 *  "proximityUuid: --- major: ---  minor: ---"
 *
 *  @return an NSString representing region in specified format.
 */
- (NSString *)getRegionString;

@end
