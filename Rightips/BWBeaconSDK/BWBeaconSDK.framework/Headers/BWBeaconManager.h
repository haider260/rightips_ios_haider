
#import <CoreBluetooth/CoreBluetooth.h>

#import <CoreLocation/CoreLocation.h>
#import "BWBeacon.h"
#import "BWBeaconRegion.h"



static CLLocationManager *locationManager;

#define DEFAULT_FOREGROUND_SCAN_PERIOD              1100
#define DEFAULT_FOREGROUND_BETWEEN_SCAN_PERIOD      3000
#define DEFAULT_BACKGROUND_SCAN_PERIOD              5500
#define DEFAULT_BACKGROUND_BETWEEN_SCAN_PERIOD      5000

@class BWBeaconManager;
@protocol BWBeaconManagerDelegate;

/**
 *  This class exposes APIs to implement ranging and monitoring of BWBeaconRegion.
 */
@interface BWBeaconManager : CLLocationManager <CLLocationManagerDelegate, CBCentralManagerDelegate> {
    id <BWBeaconManagerDelegate> _delegate;
    
    //CLLocationManager *locationManager;
    
    CBCentralManager *bluetoothManager;
    
    UIApplicationState appState;
    BOOL shouldScan;
    NSTimer *scanTimer;
    NSTimer *scanBtwTimer;
    long scanIntervalFG;
    long scanBtwIntervalFG;
    long scanIntervalBG;
    long scanBtwIntervalBG;
    
    NSMutableArray *rangedRegions;
    NSMutableArray *monitoredRegions;
}

/** @name Properties */

@property (nonatomic, assign) id delegate;

/** @name Class Methods */

/**
 *  Check if Bluetooth LE is supported by the device. Result is received in
 *  delegate method beaconManager:didCheckAvailability:
 *
 *    This check will only return correct results if bluetooth is enabled on device.
 */
- (void)checkAvailability;

/**
 *  This method informs SDK about the state application that is using this SDK.
 *  You must call this method if application state changes from foreground to backgroud
 *  or vice versa. This method is required in order to correctly function
 *  foreground/background scan periods.
 *
 *  @param appState active or background
 */
- (void)setApplicationState:(UIApplicationState)appState;

/**
 *  Sets the duration in milliseconds of each Bluetooth LE scan cycle to look for iBeacons.
 */
- (void)setForegroundScanPeriod:(long)p;

/**
 *  Sets the duration in milliseconds of each Bluetooth LE scan cycle to look for iBeacons.
 */
- (void)setForegroundBetweenScanPeriod:(long)p;

/**
 *  Sets the duration in milliseconds of each Bluetooth LE scan cycle to look for iBeacons.
 */
- (void)setBackgroundScanPeriod:(long)p;

/**
 *  Sets the duration in milliseconds of each Bluetooth LE scan cycle to look for iBeacons.
 */
- (void)setBackgroundBetweenScanPeriod:(long)p;

/**
 *  Updates an already running scan with scanPeriod/betweenScanPeriod. Change will take effect on the start of the next scan cycle.
 */
- (void)updateScanPeriods;

/**
 *  The list of regions currently being ranged.
 */
- (NSArray *)getRangedRegions;

/**
 *  The list of regions currently being monitored.
 */
- (NSArray *)getMonitoredRegions;

/**
 *  Start calculating ranges for beacons in the specified region.
 *
 *  @param region start ranging on this region.
 */
- (void)startRangingBeaconsInRegion:(BWBeaconRegion *)region;

/**
 *  Stop calculating ranges for the specified region.
 *
 *  @param region stop ranging on this region.
 */
- (void)stopRangingBeaconsInRegion:(BWBeaconRegion *)region;

/**
 *  Start monitoring the specified region.
 *
 *  If a region of the same type with the same identifier is already being monitored for this application,
 *  it will be removed from monitoring. For circular regions, the region monitoring service will prioritize
 *  regions by their size, favoring smaller regions over larger regions.
 *
 *  This is done asynchronously and may not be immediately reflected in monitoredRegions.
 *
 *  @param region start monitoring this region.
 */
- (void)startMonitoringForRegion:(BWBeaconRegion *)region;

/**
 *  Stop monitoring the specified region. It is valid to call stopMonitoringForRegion: for a region that was registered
 *  for monitoring with a different location manager object, during this or previous launches of your application.
 *
 *  This is done asynchronously and may not be immediately reflected in monitoredRegions.
 *
 *  @param region stop monitoring this region.
 */
- (void)stopMonitoringForRegion:(BWBeaconRegion *)region;







/** @name Delegate Protocol */

/**
 *  Invoked when a Bluetooth LE availability check is complete.
 *
 *  @param manager used to access BWBeaconManager object
 *  @param isAvailable  boolean value indicating availability of Bluetooth LE on device.
 */
- (void)beaconManager:(BWBeaconManager *)manager didCheckAvailability:(BOOL)isAvailable;

/**
 *  Invoked when a new set of beacons are available in the specified region.
 *
 *  @param manager used to access BWBeaconManager object
 *  @param beacons an array of BWBeacons
 *  @param region  ranged beacons in this region
 */
- (void)beaconManager:(BWBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(BWBeaconRegion *)region;

/**
 *  Invoked when there's a state transition for a monitored region or in response to a request for state via a
 *    a call to requestStateForRegion:.
 *
 *  @param manager used to access BWBeaconManager object
 *  @param state   state of a beacon in the region
 *  @param region  state deteremined for this region
 */
- (void)beaconManager:(BWBeaconManager *)manager didDetermineState:(CLRegionState)state forRegion:(BWBeaconRegion *)region;

/**
 *  Invoked when a beacon enters a monitored region.
 *
 *  @param manager used to access BWBeaconManager object
 *  @param region  a monitored beacon entered in this region
 */
- (void)beaconManager:(BWBeaconManager *)manager didEnterRegion:(BWBeaconRegion *)region;

/**
 *  Invoked when a beacon exits a monitored region.
 *
 *  @param manager used to access BWBeaconManager object
 *  @param region  a monitored beacon entered in this region
 */
- (void)beaconManager:(BWBeaconManager *)manager didExitRegion:(BWBeaconRegion *)region;










@end

/**
 *  BWBeaconManagerDelegate
 */
@protocol BWBeaconManagerDelegate <NSObject>
@optional

/**
 *  Invoked when a Bluetooth LE availability check is complete.
 *
 *  @param manager used to access BWBeaconManager object
 *  @param isAvailable  boolean value indicating availability of Bluetooth LE on device.
 */
- (void)beaconManager:(BWBeaconManager *)manager didCheckAvailability:(BOOL)isAvailable;

/**
 *  Invoked when a new set of beacons are available in the specified region.
 *
 *  @param manager used to access BWBeaconManager object
 *  @param beacons an array of BWBeacons
 *  @param region  ranged beacons in this region
 */
- (void)beaconManager:(BWBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(BWBeaconRegion *)region;

/**
 *  Invoked when there's a state transition for a monitored region or in response to a request for state via a
 *    a call to requestStateForRegion:.
 *
 *  @param manager used to access BWBeaconManager object
 *  @param state   state of a beacon in the region
 *  @param region  state deteremined for this region
 */
- (void)beaconManager:(BWBeaconManager *)manager didDetermineState:(CLRegionState)state forRegion:(BWBeaconRegion *)region;

/**
 *  Invoked when a beacon enters a monitored region.
 *
 *  @param manager used to access BWBeaconManager object
 *  @param region  a monitored beacon entered in this region
 */
- (void)beaconManager:(BWBeaconManager *)manager didEnterRegion:(BWBeaconRegion *)region;

/**
 *  Invoked when a beacon exits a monitored region.
 *
 *  @param manager used to access BWBeaconManager object
 *  @param region  a monitored beacon entered in this region
 */
- (void)beaconManager:(BWBeaconManager *)manager didExitRegion:(BWBeaconRegion *)region;

@end
