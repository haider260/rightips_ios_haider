//
//  NotificationAnnotation.h
//  Rightips
//
//  Created by Esol on 20/05/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface NotificationAnnotation : NSObject<MKAnnotation>
{
}
@property (copy, nonatomic) NSString *title;
@property (strong, nonatomic) NSMutableDictionary *annotationDict;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

-(id) initWithLocation:(CLLocationCoordinate2D)location AnnotationDictionary:(NSMutableDictionary*) annotationDict;

@end
