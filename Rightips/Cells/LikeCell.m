//
//  LikeCell.m
//  Rightips
//
//  Created by Mac on 11/01/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import "LikeCell.h"

@implementation LikeCell

- (void)awakeFromNib
{
    // Initialization code
    [self.profileImageView.layer setCornerRadius:self.profileImageView.frame.size.width / 2];
    [self.profileImageView setClipsToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
