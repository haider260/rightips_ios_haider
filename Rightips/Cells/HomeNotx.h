//
//  HomeNotx.h
//  Rightips
//
//  Created by Abdul Mannan on 12/30/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeNotx : UIView

@property (strong, nonatomic) IBOutlet AsyncImageView *iconBigIV;


@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *descLabel;
@property (nonatomic, weak) IBOutlet UIImageView *cat1IconIV;
@property (nonatomic, weak) IBOutlet UILabel *cat1Label;
@property (nonatomic, weak) IBOutlet UIImageView *cat2IconIV;
@property (nonatomic, weak) IBOutlet UILabel *cat2Label;

@property (strong, nonatomic) IBOutlet UIView *titleView;

- (void)setupView;

@end
