//
//  SearchCell.h
//  Rightips
//
//  Created by Mac on 16/03/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *backGroundImageView;

@property (strong, nonatomic) IBOutlet UILabel *titleLbl;

@property (strong, nonatomic) IBOutlet UILabel *descLbl;

@property (strong, nonatomic) IBOutlet UILabel *distanceLbl;

@property (strong, nonatomic) IBOutlet UILabel *firstTagLabel;

@property (strong, nonatomic) IBOutlet UILabel *secondTagLabel;

@property (strong, nonatomic) IBOutlet UILabel *thirdTagLabel;




@end
