//
//  ReelChatCell.h
//  Reeln
//
//  Created by Abdul Mannan on 5/22/14.
//  Copyright (c) 2014 Curiologix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface HistoryCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *container;
@property (nonatomic, weak) IBOutlet UIImageView *iconIV;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *descLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateTimeLabel;
@property (nonatomic, weak) IBOutlet UIImageView *notxSourceImageView;

@property (nonatomic, weak) IBOutlet AsyncImageView *profileImageView;

@property (nonatomic, strong) IBOutlet UIButton *imageIcon;



@end
