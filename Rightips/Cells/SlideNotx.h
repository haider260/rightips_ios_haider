//
//  HomeNotx.h
//  Rightips
//
//  Created by Abdul Mannan on 12/30/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideNotx : UIView

@property (nonatomic, weak) IBOutlet UIImageView *notxIV;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *descLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;

@property (nonatomic, weak) IBOutlet UIView *overlayView;

@end
