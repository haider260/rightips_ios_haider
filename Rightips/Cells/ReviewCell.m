//
//  ReviewCell.m
//  Rightips
//
//  Created by Mac on 05/01/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import "ReviewCell.h"

@implementation ReviewCell

- (void)awakeFromNib {
    // Initialization code
    
    [self.profileImageView.layer setCornerRadius:self.profileImageView.frame.size.width / 2];
    [self.profileImageView setClipsToBounds:YES];
    
    self.rateView = [[RatingView alloc] initWithFrame:self.rateView.frame
                                    selectedImageName:@"YellowStar"
                                      unSelectedImage:@"MapGreyStar"
                                             minValue:0
                                             maxValue:5
                                        intervalValue:1
                                           stepByStep:NO];
    self.rateView.value = 3;
    [self.rateView setBackgroundColor:[UIColor clearColor]];
    [self.rateView setUserInteractionEnabled:NO];
//    [self.rateView setDelegate:self];
    [mainView addSubview:self.rateView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
