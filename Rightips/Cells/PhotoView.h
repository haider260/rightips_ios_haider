//
//  PhotoView.h
//  Rightips
//
//  Created by Mac on 26/02/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface PhotoView : UIView


@property (strong, nonatomic) IBOutlet AsyncImageView *commentImageView;


@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityView;


@end
