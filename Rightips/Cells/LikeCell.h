//
//  LikeCell.h
//  Rightips
//
//  Created by Mac on 11/01/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface LikeCell : UITableViewCell


@property (strong, nonatomic) IBOutlet AsyncImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (strong, nonatomic) IBOutlet UILabel *timeLeftLabel;

@property (strong, nonatomic) IBOutlet AsyncImageView *commentImageView;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) IBOutlet UIButton *reportButton;


@end
