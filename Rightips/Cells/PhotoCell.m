//
//  PhotoCell.m
//  Rightips
//
//  Created by Mac on 11/01/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import "PhotoCell.h"
#import "AsyncImageView.h"

@implementation PhotoCell

- (void)awakeFromNib
{
    // Initialization code
    
    [self.profileImageView.layer setCornerRadius:self.profileImageView.frame.size.width / 2];
    [self.profileImageView setClipsToBounds:YES];
//
//    [self.likeButton.layer setBorderWidth:0.5];
//    [self.likeButton.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//    
//    [self.commentButton.layer setBorderWidth:0.5];
//    [self.commentButton.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//    
//    [self.shareButton.layer setBorderWidth:0.5];
//    [self.shareButton.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//    [self.profileImageView setBackgroundColor:[UIColor grayColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
