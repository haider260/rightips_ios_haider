//
//  CommentsCellTableViewCell.h
//  Rightips
//
//  Created by Mac on 18/09/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface CommentsCellTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet AsyncImageView *profileImageView;

@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleButton;




@property (weak, nonatomic) IBOutlet AsyncImageView *commentImageView;
@property (strong, nonatomic) IBOutlet UILabel *createdTime;
@property (strong, nonatomic) IBOutlet UIButton *likeButton;



@end
