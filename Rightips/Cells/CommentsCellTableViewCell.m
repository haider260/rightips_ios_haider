//
//  CommentsCellTableViewCell.m
//  Rightips
//
//  Created by Mac on 18/09/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "CommentsCellTableViewCell.h"

@implementation CommentsCellTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    
    [self.profileImageView.layer setCornerRadius:self.profileImageView.frame.size.width / 2];
    [self.profileImageView setClipsToBounds:YES];
    
    [self.commentImageView setUserInteractionEnabled:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
