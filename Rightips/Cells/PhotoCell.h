//
//  PhotoCell.h
//  Rightips
//
//  Created by Mac on 11/01/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface PhotoCell : UITableViewCell


@property (strong, nonatomic) IBOutlet AsyncImageView *profileImageView;

@property (strong, nonatomic) IBOutlet AsyncImageView *commentImageView;

@property (strong, nonatomic) IBOutlet UILabel *likeCountLabel;

@property (strong, nonatomic) IBOutlet UILabel *userTitleLabel;

@property (strong, nonatomic) IBOutlet UILabel *commentLabel;

@property (strong, nonatomic) IBOutlet UILabel *timeLeftLabel;


@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

@property (strong, nonatomic) IBOutlet UIButton *likeButton;

@property (strong, nonatomic) IBOutlet UIButton *commentButton;

@property (strong, nonatomic) IBOutlet UIButton *shareButton;

@property (strong, nonatomic) IBOutlet UIButton *delButton;


@end
