//
//  ReviewCell.h
//  Rightips
//
//  Created by Mac on 05/01/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingView.h"

@interface ReviewCell : UITableViewCell
{
    
    IBOutlet UIView *mainView;
}

@property (strong, nonatomic) IBOutlet UIButton *userNameButton;


@property (strong, nonatomic) IBOutlet RatingView *rateView;

@property (strong, nonatomic) IBOutlet UILabel *descLabel;

@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;


@property (strong, nonatomic) IBOutlet UILabel *timeLeftLabel;

@property (strong, nonatomic) IBOutlet UILabel *likeLabel;

@end
