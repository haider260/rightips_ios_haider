//
//  HomeFullListCell.h
//  Rightips
//
//  Created by Esol on 19/06/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeFullListCell : UIView

@property (weak, nonatomic) IBOutlet UILabel *fullListLabel;

@end
