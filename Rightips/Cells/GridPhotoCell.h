//
//  GridPhotoCell.h
//  Rightips
//
//  Created by Mac on 19/01/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface GridPhotoCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet AsyncImageView *imgview;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

@end
