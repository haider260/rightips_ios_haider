//
//  ContactTableViewCell.h
//  Rightips
//
//  Created by Esol on 28/06/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *imgSelect;
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *friendImage;

@end
