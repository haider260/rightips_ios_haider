//
//  ReelChatCell.m
//  Reeln
//
//  Created by Abdul Mannan on 5/22/14.
//  Copyright (c) 2014 Curiologix. All rights reserved.
//

#import "HistoryCell.h"

@implementation HistoryCell

@synthesize container, iconIV, nameLabel, descLabel, dateTimeLabel,profileImageView,imageIcon;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    
    [self.profileImageView.layer setCornerRadius:self.profileImageView.frame.size.width / 2];
    [self.profileImageView setClipsToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
