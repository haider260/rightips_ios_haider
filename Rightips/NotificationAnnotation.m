//
//  NotificationAnnotation.m
//  Rightips
//
//  Created by Esol on 20/05/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "NotificationAnnotation.h"

@implementation NotificationAnnotation

-(id) initWithLocation:(CLLocationCoordinate2D)location AnnotationDictionary:(NSMutableDictionary *)annotationDict
{
    self = [super init];
    if (self)
    {
        _coordinate = location;
        self.annotationDict = annotationDict;
        _title = [annotationDict objectForKey:@"notificationSiteName"];
    }
    return self;
}

@end
