//
//  UserProfile.m
//  Rightips
//
//  Created by Abdul Mannan on 11/27/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import "UserProfile.h"

@implementation UserProfile

@synthesize userIdEmail;

- (id)init {
    if (self = [super init]) {
        self.userIdEmail = @"";
        self.firstName = @"";
        self.lastName = @"";
        self.userName = @"";
        self.email = @"";
        self.smId = @"";
        self.provider = @"";
        self.profileImgUrl = @"";
        self.coverImgUrl = @"";
        self.createdAt = @"";
        self.reviewRate = @"";
        self.type = @"";
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.userIdEmail forKey:@"userIdEmail"];
    [encoder encodeObject:self.firstName forKey:@"firstName"];
    [encoder encodeObject:self.lastName forKey:@"lastName"];
    [encoder encodeObject:self.email forKey:@"email"];
    [encoder encodeObject:self.smId forKey:@"smId"];
    [encoder encodeObject:self.provider forKey:@"provider"];
    [encoder encodeObject:self.profileImgUrl forKey:@"profileImgUrl"];
    [encoder encodeObject:self.coverImgUrl forKey:@"coverImgUrl"];
    [encoder encodeObject:self.createdAt forKey:@"createdAt"];
    [encoder encodeObject:self.userName forKey:@"userName"];

}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        self.userIdEmail = [decoder decodeObjectForKey:@"userIdEmail"];
        self.firstName = [decoder decodeObjectForKey:@"firstName"];
        self.lastName = [decoder decodeObjectForKey:@"lastName"];
        self.email = [decoder decodeObjectForKey:@"email"];
        self.smId = [decoder decodeObjectForKey:@"smId"];
        self.provider = [decoder decodeObjectForKey:@"provider"];
        self.profileImgUrl = [decoder decodeObjectForKey:@"profileImgUrl"];
        self.coverImgUrl = [decoder decodeObjectForKey:@"coverImgUrl"];
        self.createdAt = [decoder decodeObjectForKey:@"createdAt"];
        self.userName = [decoder decodeObjectForKey:@"userName"];

    }
    
    return self;
}

- (UserProfile *)copyUserProfile {
    UserProfile *copyUP = [[[self class] alloc] init];
    
    copyUP.userIdEmail = self.userIdEmail;
    copyUP.firstName = self.firstName;
    copyUP.lastName = self.lastName;
    copyUP.userName = self.userName;
    copyUP.email = self.email;
    copyUP.smId = self.smId;
    copyUP.provider = self.provider;
    copyUP.profileImgUrl = self.profileImgUrl;
    copyUP.coverImgUrl = self.coverImgUrl;
    
    return copyUP;
}

@end
