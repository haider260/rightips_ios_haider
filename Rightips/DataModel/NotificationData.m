//
//  NotificationData.m
//  Rightips
//
//  Created by Abdul Mannan on 12/8/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import "NotificationData.h"

@implementation NotificationData

@synthesize msgId;
@synthesize zone;
@synthesize notxId;
@synthesize title;
@synthesize detail;
@synthesize notxTemplate;
@synthesize statusId;
@synthesize siteName;
@synthesize siteAddress;
@synthesize sitePhone;
@synthesize notxStartDate;
@synthesize notxEndDate;
@synthesize discount;
@synthesize dealStartDate;
@synthesize dealEndDate;
@synthesize creationDate;
@synthesize images;
@synthesize cats;
@synthesize likes;
@synthesize noOfLikes;
@synthesize longitude;
@synthesize latitude;
@synthesize catGroupId;
@synthesize catId;
@synthesize fbUrl;
@synthesize notxUrl;
@synthesize instagramUrl;
@synthesize googlePlusUrl;
@synthesize mailUrl;
@synthesize catLink;
@synthesize totalSiteNotifications;
@synthesize source;
@synthesize distance;
@synthesize tagList;
@synthesize videoUrl;

- (id)init {
    if (self = [super init]) {
        self.msgId=@"";
        self.zone = @"";
        self.notxId = @"";
        self.title = @"";
        self.detail = @"";
        self.notxTemplate = @"";
        self.statusId = @"";
        self.siteId = @"";
        self.siteName = @"";
        self.siteAddress = @"";
        self.sitePhone = @"";
        self.notxStartDate = @"";
        self.notxEndDate = @"";
        self.discount = @"";
        self.dealStartDate = @"";
        self.dealEndDate = @"";
        self.creationDate = 0;
        self.noOfLikes=@"";
        self.likes=[NSMutableArray array];
        self.images = [NSMutableArray array];
        self.cats = [NSMutableArray array];
        self.tagList = [NSMutableArray array];
        self.distance = 0.0;
        self.longitude = 0;
        self.latitude = 0;
        self.catGroupId = @"";
        self.catId = @"";
        self.fbUrl = @"";
        self.notxUrl = @"";
        self.instagramUrl = @"";
        self.catLink = @"";
        self.googlePlusUrl = @"";
        self.mailUrl = @"";
        self.totalSiteNotifications = @"";
        self.source = @"";
        self.videoUrl = @"";
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.msgId forKey:@"msgId"];
    [encoder encodeObject:self.zone forKey:@"zone"];
    [encoder encodeObject:self.notxId forKey:@"notxId"];
    [encoder encodeObject:self.title forKey:@"title"];
    [encoder encodeObject:self.detail forKey:@"detail"];
    [encoder encodeObject:self.notxTemplate forKey:@"notxTemplate"];
    [encoder encodeObject:self.statusId forKey:@"statusId"];
    [encoder encodeObject:self.siteId forKey:@"siteId"];
    [encoder encodeObject:self.siteName forKey:@"siteName"];
    [encoder encodeObject:self.siteAddress forKey:@"siteAddress"];
    [encoder encodeObject:self.sitePhone forKey:@"sitePhone"];
    [encoder encodeObject:self.notxStartDate forKey:@"notxStartDate"];
    [encoder encodeObject:self.notxEndDate forKey:@"notxEndDate"];
    [encoder encodeObject:self.discount forKey:@"discount"];
    [encoder encodeObject:self.dealStartDate forKey:@"dealStartDate"];
    [encoder encodeObject:self.dealEndDate forKey:@"dealEndDate"];
    [encoder encodeInteger:self.creationDate forKey:@"creationDate"];
    [encoder encodeObject:self.images forKey:@"images"];
    [encoder encodeObject:self.cats forKey:@"cats"];
    [encoder encodeObject:self.likes forKey:@"likes"];
    [encoder encodeObject:self.noOfLikes forKey:@"noOfLikes"];
    [encoder encodeObject:self.longitude forKey:@"longitude"];
    [encoder encodeObject:self.latitude forKey:@"latitude"];
    [encoder encodeObject:self.catGroupId forKey:@"catGroupId"];
    [encoder encodeObject:self.catId forKey:@"catId"];
    [encoder encodeObject:self.fbUrl forKey:@"fbUrl"];
    [encoder encodeObject:self.notxUrl forKey:@"notxUrl"];
    [encoder encodeObject:self.videoUrl forKey:@"videoUrl"];
    [encoder encodeObject:self.googlePlusUrl forKey:@"googlePlusUrl"];
    [encoder encodeObject:self.totalSiteNotifications forKey:@"totalSiteNotifications"];
    [encoder encodeObject:self.instagramUrl forKey:@"instagramUrl"];
    [encoder encodeObject:self.catLink forKey:@"catLink"];

    [encoder encodeObject:self.mailUrl forKey:@"mailUrl"];
    [encoder encodeObject:self.source forKey:@"source"];
    [encoder encodeObject:self.tagList forKey:@"tagList"];
    [encoder encodeFloat:self.distance forKey:@"distance"];
    
    

}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        self.msgId = [decoder decodeObjectForKey:@"msgId"];
        self.zone = [decoder decodeObjectForKey:@"zone"];
        self.notxId = [decoder decodeObjectForKey:@"notxId"];
        self.title = [decoder decodeObjectForKey:@"title"];
        self.detail = [decoder decodeObjectForKey:@"detail"];
        self.notxTemplate = [decoder decodeObjectForKey:@"notxTemplate"];
        self.statusId = [decoder decodeObjectForKey:@"statusId"];
        self.siteId = [decoder decodeObjectForKey:@"siteId"];
        self.siteName = [decoder decodeObjectForKey:@"siteName"];
        self.siteAddress = [decoder decodeObjectForKey:@"siteAddress"];
        self.sitePhone = [decoder decodeObjectForKey:@"sitePhone"];
        self.notxStartDate = [decoder decodeObjectForKey:@"notxStartDate"];
        self.notxEndDate = [decoder decodeObjectForKey:@"notxEndDate"];
        self.discount = [decoder decodeObjectForKey:@"discount"];
        self.dealStartDate = [decoder decodeObjectForKey:@"dealStartDate"];
        self.dealEndDate = [decoder decodeObjectForKey:@"dealEndDate"];
        self.creationDate = [decoder decodeIntegerForKey:@"creationDate"];
        self.images = [decoder decodeObjectForKey:@"images"];
        self.likes = [decoder decodeObjectForKey:@"likes"];
        self.noOfLikes = [decoder decodeObjectForKey:@"noOfLikes"];
        self.longitude = [decoder decodeObjectForKey:@"longitude"];
        self.latitude = [decoder decodeObjectForKey:@"latitude"];
        self.catGroupId = [decoder decodeObjectForKey:@"catGroupId"];
        self.catId = [decoder decodeObjectForKey:@"catId"];
        self.fbUrl = [decoder decodeObjectForKey:@"fbUrl"];
        self.notxUrl = [decoder decodeObjectForKey:@"notxUrl"];
        self.notxUrl = [decoder decodeObjectForKey:@"videoUrl"];
        self.instagramUrl = [decoder decodeObjectForKey:@"instagramUrl"];
        self.googlePlusUrl = [decoder decodeObjectForKey:@"googlePlusUrl"];
        self.mailUrl = [decoder decodeObjectForKey:@"mailUrl"];
        self.catLink = [decoder decodeObjectForKey:@"catLink"];
        self.source = [decoder decodeObjectForKey:@"source"];
        self.tagList = [decoder decodeObjectForKey:@"tagList"];
        self.distance = [decoder decodeFloatForKey:@"distance"];

        self.totalSiteNotifications = [decoder decodeObjectForKey:@"totalSiteNotifications"];
    }
    return self;
}

/*
- (NotificationData *)copyNotificationData {
    NotificationData *copyND = [[[self class] alloc] init];
    
    return copyND;
}
*/
 
@end
