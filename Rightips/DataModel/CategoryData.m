//
//  CategoryData.m
//  Rightips
//
//  Created by Abdul Mannan on 12/8/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import "CategoryData.h"

@implementation CategoryData

@synthesize catId, catName, catImgUrl;

- (id)init {
    if (self = [super init]) {
        self.catId = @"";
        self.catName = @"";
        self.catImgUrl = @"";
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.catId forKey:@"catId"];
    [encoder encodeObject:self.catName forKey:@"catName"];
    [encoder encodeObject:self.catImgUrl forKey:@"catImgUrl"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        self.catId = [decoder decodeObjectForKey:@"catId"];
        self.catName = [decoder decodeObjectForKey:@"catName"];
        self.catImgUrl = [decoder decodeObjectForKey:@"catImgUrl"];
    }
    return self;
}

- (CategoryData *)copyCategoryData {
    CategoryData *copyCD = [[[self class] alloc] init];
    copyCD.catId = self.catId;
    copyCD.catName = self.catName;
    copyCD.catImgUrl = self.catImgUrl;
    return copyCD;
}

@end
