//
//  AppConstants.h
//  Rightips
//
//  Created by Shahbaz Ali on 7/16/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#ifndef Rightips_AppConstants_h
#define Rightips_AppConstants_h

#define kReferringUsersArray    @"referringUsersArray"

#define kReferringUserId        @"referringUserId"
#define kReferringUsername      @"referringUsername"
#define kReferringUserImage     @"pictureURL"

#endif
