//
//  CategoryGroup.h
//  Rightips
//
//  Created by Esol on 18/05/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryGroup : NSObject

@property (nonatomic, retain) NSString *catId;
@property (nonatomic, retain) NSString *name;

@property (nonatomic, retain) NSString *iconImageName;
@property (nonatomic, retain) NSString *markerImageName;

@property (nonatomic, retain) NSString *selectedIconImageName;
@property (nonatomic, retain) NSString *selectedMarkerImageName;

@end
