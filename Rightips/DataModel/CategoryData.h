//
//  CategoryData.h
//  Rightips
//
//  Created by Abdul Mannan on 12/8/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryData : NSObject

@property (nonatomic, retain) NSString *catId;
@property (nonatomic, retain) NSString *catName;
@property (nonatomic, retain) NSString *catImgUrl;

@end
