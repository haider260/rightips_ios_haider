//
//  MapConstants.h
//  Rightips
//
//  Created by Esol on 23/06/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#ifndef Rightips_MapConstants_h
#define Rightips_MapConstants_h

#define kCategoryGroupId    @"categoryGroupId"
#define kNotificationCatId @"notificationCatId"
#define kNotificationAddress   @"notificationAddress"
#define kIsNotificationSelected   @"isNotificationSelected"

//Category marker Images names
#define kBarMarker  @"map_bar_marker"
#define kBarMarkerSelected  @"map_bar_marker_over"

#define kEntertainmentMarker  @"map_entertainment_marker"
#define kEntertainmentMarkerSelected  @"map_entertainment_marker_over"

#define kEventMarker  @"map_events_marker"
#define kEventMarkerSelected  @"map_events_marker_over"

#define kServicesMarker  @"map_services_marker"
#define kServicesMarkerSelected  @"map_services_marker_over"

#define kShopMarker  @"map_shop_marker"
#define kShopMarkerSelected  @"map_shop_marker_over"

#define kStudentsMarker  @"map_students_marker"
#define kStudentsMarkerSelected  @"map_students_marker_over"

#define kTourismMarker  @"map_tourism_marker"
#define kTourismMarkerSelected  @"map_tourism_marker_over"

// Route Modes
#define kWalkingMode    @"walking"
#define kDrivingMode    @"driving"
#define kTransitMode    @"transit"

// Google Direction Images
#define kDirectionImage   @"icon_google_directions_over"
#define kDirectionSelectedImage   @"icon_google_directions"
#define kDirectionWalkingImage   @"icon_google_directions_walk"
#define kDirectionWalkingSelectedImage   @"icon_google_directions_walk_over"
#define kDirectionDrivingImage   @"icon_google_directions_drive"
#define kDirectionDrivingSelectedImage   @"icon_google_directions_drive_selected"
#define kDirectionTransitImage   @"icon_google_directions_transit"
#define kDirectionTransitSelectedImage   @"icon_google_directions_transit_over"

#endif
