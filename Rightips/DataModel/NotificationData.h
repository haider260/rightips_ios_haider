//
//  NotificationData.h
//  Rightips
//
//  Created by Abdul Mannan on 12/8/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationData : NSObject

@property (nonatomic, retain) NSString *msgId;
@property (nonatomic, retain) NSString *zone;
@property (nonatomic, retain) NSString *notxId;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *detail;
@property (nonatomic, retain) NSString *notxTemplate;
@property (nonatomic, retain) NSString *statusId;
@property (nonatomic, retain) NSString *siteId;
@property (nonatomic, retain) NSString *siteName;
@property (nonatomic, retain) NSString *siteAddress;
@property (nonatomic, retain) NSString *sitePhone;
@property (nonatomic, retain) NSString *notxStartDate;
@property (nonatomic, retain) NSString *notxEndDate;
@property (nonatomic, retain) NSString *discount;
@property (nonatomic, retain) NSString *dealStartDate;
@property (nonatomic, retain) NSString *dealEndDate;
@property (nonatomic, retain) NSArray *likes;
@property (nonatomic, retain) NSString *noOfLikes;
@property (nonatomic, assign) NSTimeInterval creationDate;
@property (nonatomic, retain) NSMutableArray *images;
@property (nonatomic, retain) NSMutableArray *cats;
@property (nonatomic, retain) NSString *catId;
@property (nonatomic, retain) NSNumber *longitude;
@property (nonatomic, retain) NSNumber *latitude;
@property (nonatomic, retain) NSString *catGroupId;
@property (nonatomic,retain) NSString *source;

@property (nonatomic,retain) NSString *notxUrl;
@property (nonatomic,retain) NSString *videoUrl;
@property (nonatomic,retain) NSString *fbUrl;
@property (nonatomic,retain) NSString *instagramUrl;
@property (nonatomic,retain) NSString *googlePlusUrl;
@property (nonatomic,retain) NSString *mailUrl;
@property (nonatomic,retain) NSString *catLink;
@property (nonatomic) float distance;
@property (nonatomic) NSMutableArray *tagList;

@property (nonatomic,retain) NSString *totalSiteNotifications;
@end
