//
//  SearchData.m
//  Rightips
//
//  Created by Mac on 16/03/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import "SearchData.h"

@implementation SearchData
@synthesize titleName,address,imgUrl,distance;

- (id)init {
    if (self = [super init]) {
        self.titleName = @"";
        self.address = @"";
        self.imgUrl = @"";
        self.distance = 0;
        
        
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.titleName forKey:@"titleName"];
    [encoder encodeObject:self.address forKey:@"address"];
    [encoder encodeObject:self.imgUrl forKey:@"imgUrl"];
    [encoder encodeFloat:self.distance forKey:@"distance"];
   
    
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if((self = [super init]))
    {
        self.titleName = [decoder decodeObjectForKey:@"titleName"];
        self.address = [decoder decodeObjectForKey:@"address"];
        self.imgUrl = [decoder decodeObjectForKey:@"imgUrl"];
        self.distance = [decoder decodeFloatForKey:@"distance"];
        
    }
    
    return self;
}



@end
