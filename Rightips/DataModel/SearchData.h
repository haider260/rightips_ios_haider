//
//  SearchData.h
//  Rightips
//
//  Created by Mac on 16/03/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchData : NSObject

@property (nonatomic, retain) NSString *titleName;
@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) NSString *imgUrl;
@property (nonatomic) float distance;
@property (nonatomic) NSArray *tagList;

@end
