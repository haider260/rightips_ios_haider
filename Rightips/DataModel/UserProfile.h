//
//  UserProfile.h
//  Rightips
//
//  Created by Abdul Mannan on 11/27/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserProfile : NSObject

@property (nonatomic, retain) NSString *userIdEmail;
@property (nonatomic, retain) NSString *firstName;
@property (nonatomic, retain) NSString *lastName;
@property (nonatomic, retain) NSString *userName;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *smId; // Social Media Id
@property (nonatomic, retain) NSString *uid;
@property (nonatomic, retain) NSString *profileImgUrl;
@property (nonatomic, retain) NSString *coverImgUrl;
@property (nonatomic, retain) NSString *provider; // Email/Facebook etc
@property (nonatomic, retain) NSString *userReview;
@property (nonatomic, retain) NSString *reviewRate;
@property (nonatomic) NSInteger userLikes;
@property (nonatomic, retain) NSString *createdAt;
@property (nonatomic, retain) NSString *commentImage;
@property (nonatomic, retain) NSString *isLiked;
@property (nonatomic, retain) NSString *commentId;
@property (nonatomic, retain) NSString *messageId;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) UIImage *img;
@property (nonatomic, retain) NSString *type;
@property (nonatomic) NSNumber *height;


@end
