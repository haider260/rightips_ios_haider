//
//  UserLocation.m
//  Rightips
//
//  Created by Abdul Mannan on 1/28/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "UserLocation.h"

@implementation UserLocation

@synthesize userLocationMode;
@synthesize currentLocation, currentCityName;
@synthesize selectedLocation, selectedCityName;

- (id)init {
    if (self = [super init]) {
        self.userLocationMode = UserLocationModeCurrent;
        self.currentLocation = nil;
        self.currentCityName = @"";
        self.selectedLocation = nil;
        self.selectedCityName = @"";
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeInteger:self.userLocationMode forKey:@"userLocationMode"];
    [encoder encodeObject:self.currentLocation forKey:@"currentLocation"];
    [encoder encodeObject:self.currentCityName forKey:@"currentCityName"];
    [encoder encodeObject:self.selectedLocation forKey:@"selectedLocation"];
    [encoder encodeObject:self.selectedCityName forKey:@"selectedCityName"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if((self = [super init]))
    {
        self.userLocationMode = [decoder decodeIntegerForKey:@"userLocationMode"];
        self.currentLocation = [decoder decodeObjectForKey:@"currentLocation"];
        self.currentCityName = [decoder decodeObjectForKey:@"currentCityName"];
        self.selectedLocation = [decoder decodeObjectForKey:@"selectedLocation"];
        self.selectedCityName = [decoder decodeObjectForKey:@"selectedCityName"];
    }
    
    return self;
}

@end
