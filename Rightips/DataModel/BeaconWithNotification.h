//
//  BeaconWithNotification.h
//  Rightips
//
//  Created by Abdul Mannan on 12/8/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BeaconWithNotification : NSObject

@property (nonatomic, retain) NSString *uuid;
@property (nonatomic, retain) NSString *major;
@property (nonatomic, retain) NSString *minor;
@property (nonatomic, retain) NSString *macAddress;

@property (nonatomic, retain) NSMutableArray *notifications;

@end
