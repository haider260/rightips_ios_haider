//
//  CategoryGroup.m
//  Rightips
//
//  Created by Esol on 18/05/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "CategoryGroup.h"

@implementation CategoryGroup

@synthesize catId, name, iconImageName, markerImageName, selectedIconImageName, selectedMarkerImageName;

- (id)init {
    if (self = [super init]) {
        self.catId = @"";
        self.name = @"";
        self.iconImageName = @"";
        self.markerImageName = @"";
        self.selectedIconImageName = @"";
        self.selectedMarkerImageName = @"";
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.catId forKey:@"catId"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.iconImageName forKey:@"iconImageName"];
    [encoder encodeObject:self.markerImageName forKey:@"markerImageName"];
    [encoder encodeObject:self.selectedIconImageName forKey:@"selectedIconImageName"];
    [encoder encodeObject:self.selectedMarkerImageName forKey:@"selectedMarkerImageName"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        self.catId = [decoder decodeObjectForKey:@"catId"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.iconImageName = [decoder decodeObjectForKey:@"iconImageName"];
        self.markerImageName = [decoder decodeObjectForKey:@"markerImageName"];
        self.selectedIconImageName = [decoder decodeObjectForKey:@"selectedIconImageName"];
        self.selectedMarkerImageName = [decoder decodeObjectForKey:@"selectedMarkerImageName"];
    }
    return self;
}

- (CategoryGroup *)copyCategoryData {
    CategoryGroup *copyCD = [[[self class] alloc] init];
    copyCD.catId = self.catId;
    copyCD.name = self.name;
    copyCD.iconImageName = self.iconImageName;
    copyCD.markerImageName = self.markerImageName;
    copyCD.selectedIconImageName = self.selectedIconImageName;
    copyCD.selectedMarkerImageName = self.selectedMarkerImageName;
    
    return copyCD;
}


@end
