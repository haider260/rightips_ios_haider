//
//  BeaconWithNotification.m
//  Rightips
//
//  Created by Abdul Mannan on 12/8/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import "BeaconWithNotification.h"

@implementation BeaconWithNotification

@synthesize uuid, major, minor, macAddress;
@synthesize notifications;

- (id)init {
    if (self = [super init]) {
        self.uuid = @"";
        self.major = @"";
        self.minor = @"";
        self.macAddress = @"";
        self.notifications = [NSMutableArray array];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.uuid forKey:@"uuid"];
    [encoder encodeObject:self.major forKey:@"major"];
    [encoder encodeObject:self.minor forKey:@"minor"];
    [encoder encodeObject:self.macAddress forKey:@"macAddress"];
    [encoder encodeObject:self.notifications forKey:@"notifications"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        self.uuid = [decoder decodeObjectForKey:@"uuid"];
        self.major = [decoder decodeObjectForKey:@"major"];
        self.minor = [decoder decodeObjectForKey:@"minor"];
        self.macAddress = [decoder decodeObjectForKey:@"macAddress"];
        self.notifications = [[decoder decodeObjectForKey:@"notifications"] mutableCopy];
    }
    return self;
}

- (BeaconWithNotification *)copyBeaconWithNotification {
    BeaconWithNotification *copyBWN = [[[self class] alloc] init];
    copyBWN.uuid = self.uuid;
    copyBWN.major = self.major;
    copyBWN.minor = self.minor;
    copyBWN.macAddress = self.macAddress;
    copyBWN.notifications = [self.notifications mutableCopy];
    return copyBWN;
}

@end
