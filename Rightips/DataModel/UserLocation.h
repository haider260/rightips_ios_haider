//
//  UserLocation.h
//  Rightips
//
//  Created by Abdul Mannan on 1/28/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    UserLocationModeCurrent = 0,
    UserLocationModeSelected
} UserLocationMode;

@interface UserLocation : NSObject

@property (nonatomic, assign) NSInteger userLocationMode;

@property (nonatomic, retain) CLLocation *currentLocation;
@property (nonatomic, retain) NSString *currentCityName;
@property (nonatomic, retain) CLLocation *selectedLocation;
@property (nonatomic, retain) NSString *selectedCityName;

@end
