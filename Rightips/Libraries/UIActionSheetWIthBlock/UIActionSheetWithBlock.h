

#import <UIKit/UIKit.h>

@interface UIActionSheetWithBlock : NSObject <UIActionSheetDelegate>

typedef void (^ActionSheetCompletionBlock)(NSInteger buttonIndex);
@property (nonatomic, strong) ActionSheetCompletionBlock callback;

+ (void)showActionSheet:(UIActionSheet *)alertView withCallback:(ActionSheetCompletionBlock)callback;

@end