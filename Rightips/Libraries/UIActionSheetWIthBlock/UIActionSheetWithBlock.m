

#import "UIActionSheetWithBlock.h"

@implementation UIActionSheetWithBlock
@synthesize callback;

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    callback(buttonIndex);
}

+ (void)showActionSheet:(UIActionSheet *)actionSheet withCallback:(ActionSheetCompletionBlock)callback {
    __block UIActionSheetWithBlock *delegate = [[UIActionSheetWithBlock alloc] init];
    actionSheet.delegate = delegate;
    delegate.callback = ^(NSInteger buttonIndex) {
        callback(buttonIndex);
        actionSheet.delegate = nil;
        delegate = nil;
    };
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

@end