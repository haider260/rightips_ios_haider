//
//  ReuseAbleFunctions.m
//  Rightips
//
//  Created by Esol on 01/06/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "ReuseAbleFunctions.h"

@implementation ReuseAbleFunctions

+ (NSString *) convertDictionaryToJSONString:(NSDictionary *) dict
{
    NSString *returnString=@"{";
    for(NSString *key in dict.allKeys)
    {
        returnString = [returnString stringByAppendingString:key];
        returnString = [returnString stringByAppendingString:@":"];
        returnString=[returnString stringByAppendingString:[NSString stringWithFormat:@"%@",[dict objectForKey:key]]];
        returnString=[returnString stringByAppendingString:@","];
    }
    NSInteger stringLenght=[returnString length];
    NSRange temprange=NSMakeRange(stringLenght-1, 1);
    returnString = [returnString stringByReplacingCharactersInRange:temprange withString:@"}"];
    return returnString;
}
#pragma mark - Add Friend
+ (void) addFriends
{
    NSData *userProfileData = [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData];
    UserProfile *userProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userProfileData];
    
    NSMutableArray *friendsIdsArray = [[NSUserDefaults standardUserDefaults] objectForKey:kReferringUsersArray];
    NSString *friendsIdsString = [friendsIdsArray componentsJoinedByString:@","];

    NSString *apiURL=@"http://www.rightips.com/api/index.php?action=addFriend";
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setObject:[StaticData instance].apiKey forKey:@"key"];
    [postParams setObject:userProfile.userIdEmail forKey:@"uid"];
    
    [postParams setObject:friendsIdsString forKey:@"fid"];
    NSURL *url = [[NSURL alloc] initWithString:apiURL];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:apiURL parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];

    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON){
        if([[JSON objectForKey:@"status"] integerValue]==1)
        {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:kReferringUsersArray];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON){
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
    }];
    [operation start];
}


+ (NotificationData*) getNotificationDataFromObject:(id)notxObj
{
    NotificationData *nd = [[NotificationData alloc] init];
    nd.msgId=[notxObj objectForKey:@"msg_id"];
    nd.zone = [notxObj objectForKey:@"nzone_name"];
    nd.notxId = [notxObj objectForKey:@"nt_id"];
    nd.title = [notxObj objectForKey:@"nt_title"];
    nd.detail = [notxObj objectForKey:@"nt_details"];
    if([notxObj objectForKey:@"template"])
    {
        nd.notxTemplate = [notxObj objectForKey:@"template"];
    }
    if([notxObj objectForKey:@"nt_url"])
    {
        nd.notxUrl = [notxObj objectForKey:@"nt_url"];
    }
    
    
    
        nd.videoUrl = [notxObj objectForKey:@"video_url"];
    
    nd.statusId = [notxObj objectForKey:@"status_id"];
    nd.siteId = [notxObj objectForKey:@"site_id"];
    nd.siteName = [notxObj objectForKey:@"site_title"];
    nd.siteAddress = [notxObj objectForKey:@"site_address"];
    nd.sitePhone = [notxObj objectForKey:@"site_phone"];
    nd.notxStartDate = [notxObj objectForKey:@"nt_startdate"];
    nd.notxEndDate = [notxObj objectForKey:@"nt_enddate"];
    nd.discount = [notxObj objectForKey:@"nt_discount"];
    nd.dealStartDate = [notxObj objectForKey:@"nt_deal_start"];
    nd.dealEndDate = [notxObj objectForKey:@"nt_deal_end"];
    
    NSArray *socialLinksArray = [notxObj objectForKey:@"social_links"];
    if ([socialLinksArray isKindOfClass:[NSArray class]])
    {
    for (NSDictionary *linkDict in socialLinksArray)
    {
        if ([[linkDict objectForKey:@"name"] isEqualToString:@"facebook"])
        {
            nd.fbUrl = [linkDict objectForKey:@"url"];
        }
        else if ([[linkDict objectForKey:@"name"] isEqualToString:@"instagram"]) {
            nd.instagramUrl = [linkDict objectForKey:@"url"];
        }
        else if ([[linkDict objectForKey:@"name"] isEqualToString:@"googleplus"]) {
            nd.googlePlusUrl = [linkDict objectForKey:@"url"];
        }
        else if ([[linkDict objectForKey:@"name"] isEqualToString:@"mail"]) {
            nd.mailUrl = [linkDict objectForKey:@"url"];
        }
    }
    }
    if ([notxObj objectForKey:@"nt_website_url"])
    {
        nd.catLink = [notxObj objectForKey:@"nt_website_url"];
    }
    //nd.creationDate = [[notObj objectForKey:@""] longValue];
    if ([notxObj objectForKey:@"site_long"])
    {
        nd.longitude = [notxObj objectForKey:@"site_long"];
    }
    if ([notxObj objectForKey:@"site_lat"])
    {
        nd.latitude = [notxObj objectForKey:@"site_lat"];
    }
    if ([notxObj objectForKey:@"cat_id"])
    {
        nd.catId = [notxObj objectForKey:@"cat_id"];
    }
    nd.catGroupId = [notxObj objectForKey:@"catGroup_id"];
    NSArray *imgsArrObj = [notxObj objectForKey:@"images"];
    for (id imgObj in imgsArrObj) [nd.images addObject:[imgObj objectForKey:@"url"]];
    
    nd.likes=[notxObj objectForKey:@"likes"];
    nd.noOfLikes=[notxObj objectForKey:@"total_likes"];
    if ([notxObj objectForKey:@"site_total_notifications"])
    {
        nd.totalSiteNotifications = [NSString stringWithFormat:@"%@",[notxObj objectForKey:@"site_total_notifications"]];
    }
    
    return nd;
}
@end
