//
//  AppConstatnts.h
//  Rightips
//
//  Created by Esol on 02/06/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#ifndef Rightips_PiwikConstatnts_h
#define Rightips_PiwikConstatnts_h

#define kPiwikServerURL         @"http://kiwip-prod.elasticbeanstalk.com/"
#define kPiwikSiteID            @"4"

//Categories
#define CATEGORY_APPLICATION    @"Application"
#define CATEGORY_USERS          @"User"
#define CATEGORY_VENUE          @"Venue"
#define CATEGORY_NOTIFICATION   @"Notification"
#define CATEGORY_LOCATION       @"Location"
#define CATEGORY_SHARE          @"Share"

////////////////////////////Application////////////////////////////
//Done
//Actions
#define ACTION_START            @"Start"
#define ACTION_DOWNLOAD         @"Download"

//No Name keys

////////////////////////////User////////////////////////////
//No Entry From Android
//Actions
#define ACTION_LOGIN            @"Login"
#define ACTION_SIGNUP           @"Signup"

////////////////////////////Venue////////////////////////////
//Done
//Actions
#define ACTION_ENTER            @"Enter"
#define ACTION_EXIT             @"Exit"

//Name Keys
#define kBeaconMajor            @"major"
#define kBeaconMinor            @"minor"
#define kBeaconSiteId           @"site_id"
#define kBeaconUUID             @"uuid"


////////////////////////////Notification////////////////////////////
//Actions
#define ACTION_OPENED           @"Opened"   //Done In History and HomeVC
#define ACTION_POSTED           @"Posted"   //Done in Post Notification AppDelegate
#define ACTION_LIKE             @"Like"     //Done in PostLike Of NotxDetailVC
#define ACTION_VIEW             @"View"     //Done in viewDidAppear of NotxDetailVC

//Name Keys
#define kNotificationId         @"note_id"
#define kNotificationSiteId     @"note_site_id"
#define kNotificationSiteName   @"note_site_name"
#define kNotificationTitle      @"note_title"
#define kNotificationZone       @"note_zone"
#define kSiteTotalNotifications @"site_total_notifications"
////////////////////////////Location////////////////////////////
//Actions
#define ACTION_LOCATION_SEARCHED        @"Location Searched"   //Done in HomeVC DidiUpdateUserLocation
#define ACTION_CURRENT_LOCATION         @"User Location"       //Done in HomeVC DidiUpdateUserLocation

//Name Keys
#define kLocationAddress                @"_address"
#define kLocationCityName               @"_cityName"
#define kLocationLatitude               @"_latitude"
#define kLocationLongitude              @"_longitude"
#define kLocationSource                 @"_source"

////////////////////////////Share Actions////////////////////////////
//Actions
#define ACTION_WHATSAPP                 @"WhatsApp"     //Done in shareButtonTapped of NotxDetailVC
#define ACTION_FACEBOOK                 @"Facebook"     //Done in faceBookPOSTRequestWithDictionary of NotxDetailVC

//Same Name Keys As Notification Keys
#endif
