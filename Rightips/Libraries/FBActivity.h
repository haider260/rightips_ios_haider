//
//  FBActivity.h
//  Rightips
//
//  Created by Esol on 26/05/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FacBookShareDelegate <NSObject>
@required
- (void) tempShareForFacebook;
@end

@interface FBActivity : UIActivity
{
    id <FacBookShareDelegate> delegate;
}
@property (nonatomic,strong) id delegate;

@property (strong, nonatomic) NSMutableDictionary *fbDict;

@end
