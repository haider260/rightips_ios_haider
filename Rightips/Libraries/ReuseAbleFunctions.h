//
//  ReuseAbleFunctions.h
//  Rightips
//
//  Created by Esol on 01/06/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReuseAbleFunctions : NSObject

+ (NSString *) convertDictionaryToJSONString:(NSDictionary *) dict;
+ (void) addFriends;
+ (NotificationData*) getNotificationDataFromObject:(id)notxObj;

@end
