//
//  BWBeaconSDK.h
//  BWBeaconSDK
//
//  Created by Abdul Mannan on 10/13/14.
//  Copyright (c) 2014 Beacon Watcher. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BWBeaconSDK.
FOUNDATION_EXPORT double BWBeaconSDKVersionNumber;

//! Project version string for BWBeaconSDK.
FOUNDATION_EXPORT const unsigned char BWBeaconSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BWBeaconSDK/PublicHeader.h>

#import "BWBeacon.h"
#import "BWBeaconRegion.h"
#import "BWBeaconManager.h"
#import "BWConnectionManager.h"
