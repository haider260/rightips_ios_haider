

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

//#import "BWCentralManager.h"
//#import "BWPeripheralManager.h"

#import "BWBeacon.h"

#define DEFAULT_PERIPHERAL_SCAN_TIMEOUT         2000        // if device is not discovered again in this period,
                                                            // it will be removed from save peripherals data array
#define DEFAULT_PERIPHERAL_DISCOVERY_PERIOD     1000        // SDK will notify about discovered devices after this
                                                            // period, periodically.

@class BWConnectionManager;
@protocol BWConnectionManagerDelegate;

static CBCentralManager *centralManager;

/**
 *  This class exposes APIs to connect to Bluetooth LE peripheral (iBeacon) and modify its services/charactersitics.
 */
@interface BWConnectionManager : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate> {
    id <BWConnectionManagerDelegate> _delegate;
    
    CBCentralManager *centralManager;
    NSMutableArray *discoveredPeripheralsData; //array of BWPeripheralData objects
    CBPeripheral *connectedPeripheral;
    NSTimer *discoverTimer;
    
    BOOL canScan;
    BOOL shouldScan;
    BOOL shouldNotify;
    
    CBCharacteristic *proximityUuidCharacteristic;
    CBCharacteristic *majorCharacteristic;
    CBCharacteristic *minorCharacteristic;
    CBCharacteristic *softRebootCharacteristic;
}

@property (nonatomic, retain) id delegate;

/**
 *  Start discovering nearby Bluetooth LE Peripherals
 */
- (void)startScanningPeripherals;

/**
 *  Stop discovering nearby Bluetooth LE Peripherals
 */
- (void)stopScanningPeripherals;

/**
 *  Get an array of discovered peripherals.
 *
 *  @return array of discovered peripherals.
 */
- (NSMutableArray *)discoveredPeripherals;

/**
 *  Connect to Bluetooth LE peripheral.
 *
 *  @param peripheral peripheral to be connected to.
 */
- (void)connectToPeripheral:(CBPeripheral *)peripheral;

/**
 *  Disconnect from Bluetooth LE peripheral.
 *
 *  @param peripheral peripheral to be disconnected from
 */
- (void)disconnectFromPeripheral:(CBPeripheral *)peripheral;

/**
 *  Determines whether characteristic is readable or not.
 *
 *  @param characteristic characteristic to be checked for readability.
 *
 *  @return YES if characteristic is readable otherwise NO.
 */
- (BOOL)isCharacterisitcReadable:(CBCharacteristic *)characteristic;

/**
 *  Determines whether characteristic is notifiable or not.
 *
 *  @param characteristic characteristic to be checked for notifiability.
 *
 *  @return YES if characteristic is notifiable otherwise NO.
 */
- (BOOL)isCharacterisiticNotifiable:(CBCharacteristic *)characteristic;

/**
 *  Determines whether characteristic is writable or not.
 *
 *  @param characteristic characteristic to be checked for writability.
 *
 *  @return YES if characteristic is writable otherwise NO.
 */
- (BOOL)isCharacteristicWriteable:(CBCharacteristic *)characteristic;




- (void)writeProximityUuid:(NSString *)uuid;
- (void)writeMajor:(NSString *)major;
- (void)writeMinor:(NSString *)minor;




/**
 *  Write advertising interval to beacon.
 *
 *  @param broadcastRate advertising interval
 *  @param success       success block (callback)
 *  @param failure       failure block (callback)
 */
- (void)writeAdvertisingInterval:(NSString *)broadcastRate success:(void (^)())success failure:(void (^)())failure;

/**
 *  Write name to beacon.
 *
 *  @param name    name of beacon
 *  @param success success block (callback)
 *  @param failure failure block (callback)
 */
- (void)writeBeaconName:(NSString *)name success:(void (^)())success failure:(void (^)())failure;

/**
 *  Write broadcasting power value to beacon.
 *
 *  @param val     broadcast power value
 *  @param success success block (callback)
 *  @param failure failure block (callback)
 */
- (void)writeBroadcastingPowerValue:(NSString *)val success:(void (^)())success failure:(void (^)())failure;

/**
 *  Write major value to beacon.
 *
 *  @param major   major value
 *  @param success success block (callback)
 *  @param failure failure block (callback)
 */
- (void)writeMajor:(NSString *)major success:(void (^)())success failure:(void (^)())failure;

/**
 *  Write minor value to beacon.
 *
 *  @param minor   minor value
 *  @param success success block (callback)
 *  @param failure failure block (callback)
 */
- (void)writeMinor:(NSString *)minor success:(void (^)())success failure:(void (^)())failure;

/**
 *  Write proximity UUID to beacon.
 *
 *  @param uuid    proximity UUID
 *  @param success success block (callback)
 *  @param failure failure block (callback)
 */
- (void)writeProximityUuid:(NSString *)uuid success:(void (^)())success failure:(void (^)())failure;

/**
 *  Write soft reboot to beacon. Soft reboot is required in some cases in order to successfully write a characteristic value.
 *
 *  @param val     soft reboot value
 *  @param success success block (callback)
 *  @param failure failure block (callback)
 */
- (void)writeSoftReboot:(NSString *)val success:(void (^)())success failure:(void (^)())failure;

@end













@protocol BWConnectionManagerDelegate <NSObject>
@optional

/**
 *  Invoked when a new Bluetooth LE peripheral is discovered.
 *
 *  @param manager               manager used to access BWConnectionManager object
 *  @param discoveredPeripherals an array of BWPeripheralData objects
 */
- (void)connectionManager:(BWConnectionManager *)manager didDiscoverPeripherals:(NSArray *)discoveredPeripherals;

- (void)connectionManager:(BWConnectionManager *)manager didConnectPeripheral:(CBPeripheral *)peripheral;
- (void)connectionManager:(BWConnectionManager *)manager didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error;
- (void)connectionManager:(BWConnectionManager *)manager didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error;

- (void)connectionManager:(BWConnectionManager *)manager didReadBeaconInfo:(NSUUID *)proximityUUID major:(NSNumber *)major minor:(NSNumber *)minor;

- (void)connectionManager:(BWConnectionManager *)manager didWriteCharacteristic:(CBCharacteristic *)characteristic;
- (void)connectionManager:(BWConnectionManager *)manager failedToWriteCharacteristic:(CBCharacteristic *)characteristic;

@end

@interface BWPeripheralData : NSObject {
    NSTimeInterval _scanTimeStamp;
}

@property (nonatomic, retain) CBPeripheral *peripheral;
@property (nonatomic, retain) NSDictionary *advertisementData;
@property (nonatomic, retain) NSNumber *RSSI;

- (NSTimeInterval) getCurrentTimeStamp;
- (NSTimeInterval) getScanTimeStamp;
- (void) setScanTimeStamp:(NSTimeInterval)scanTimeStamp;

@end
