//
//  JSQPhotoWithTextMediaItem.m
//  Rightips
//
//  Created by Shahbaz Ali on 8/7/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "JSQPhotoWithTextMediaItem.h"
#import "JSQMessagesMediaPlaceholderView.h"
#import "JSQMessagesMediaViewBubbleImageMasker.h"

@interface JSQPhotoWithTextMediaItem ()

@property (strong, nonatomic) UIView *cachedView;

@end

@implementation JSQPhotoWithTextMediaItem

#pragma mark - Initialization

- (instancetype)initWithImage:(UIImage *)image AndText:(NSString *)text
{
    self = [super init];
    if (self) {
        _text = [text copy];
        _image = [image copy];
        _cachedView = nil;
    }
    return self;
}

- (void)dealloc
{
    _text = nil;
    _image = nil;
    _cachedView = nil;
}

- (void)clearCachedMediaViews
{
    [super clearCachedMediaViews];
    _cachedView = nil;
}

#pragma mark - Setters

- (void)setImage:(UIImage *)image
{
    _image = [image copy];
    _cachedView = nil;
}

- (void)setAppliesMediaViewMaskAsOutgoing:(BOOL)appliesMediaViewMaskAsOutgoing
{
    [super setAppliesMediaViewMaskAsOutgoing:appliesMediaViewMaskAsOutgoing];
    _cachedView = nil;
}

#pragma mark - JSQMessageMediaData protocol

- (UIView *)mediaView
{
    if (self.image == nil) {
        return nil;
    }
    
    if (self.cachedView == nil) {

        CGSize size = [self mediaViewDisplaySizeWithText:self.text];
        CGFloat leftEdgeWidth = 12.0;
        CGFloat spacing = 5.0f;
        CGFloat contentX = leftEdgeWidth + spacing;
        CGFloat contentWidth = size.width - contentX - spacing;
        CGFloat imageViewHeight = 150.0f;
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:self.image];
        imageView.frame = CGRectMake(contentX, spacing, contentWidth , imageViewHeight);
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        CGRect frame1 = imageView.frame;
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(contentX, frame1.origin.y + frame1.size.height + spacing, contentWidth, 50)];
        lbl.text = @"hwuke kajdkxjsj akjjkslj ankdxjnskxn ajdjkxhns dekho dekho kon aya";
        [lbl setTextColor:[UIColor whiteColor]];
        [lbl setNumberOfLines:0];
        [lbl sizeToFit];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, size.width, size.height)];
        [view addSubview:imageView];
        [view addSubview:lbl];
        if (self.appliesMediaViewMaskAsOutgoing) {
            [view setBackgroundColor:[UIColor colorWithRed:255.0/256.0 green:0 blue:61.0/256.0 alpha:1.0]];
        }
        else {
            [view setBackgroundColor:[UIColor colorWithHue:240.0f / 360.0f
                                                saturation:0.02f
                                                brightness:0.92f
                                                     alpha:1.0f]];
        }
        [JSQMessagesMediaViewBubbleImageMasker applyBubbleImageMaskToMediaView:view isOutgoing:self.appliesMediaViewMaskAsOutgoing];
        self.cachedView = view;
    }
    
    return self.cachedView;
}

- (NSUInteger)mediaHash
{
    return self.hash;
}

#pragma mark - NSObject

- (NSUInteger)hash
{
    return super.hash ^ self.image.hash;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: image=%@, text=%@ appliesMediaViewMaskAsOutgoing=%@>",
            [self class], self.image, self.text,  @(self.appliesMediaViewMaskAsOutgoing)];
}

#pragma mark - NSCoding

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _image = [aDecoder decodeObjectForKey:NSStringFromSelector(@selector(image))];
        _text = [aDecoder decodeObjectForKey:NSStringFromSelector(@selector(text))];

    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.image forKey:NSStringFromSelector(@selector(image))];
    [aCoder encodeObject:self.text forKey:NSStringFromSelector(@selector(text))];
}

#pragma mark - NSCopying

- (instancetype)copyWithZone:(NSZone *)zone
{
    JSQPhotoWithTextMediaItem *copy = [[[self class] allocWithZone:zone] initWithImage:self.image AndText:self.text];
    copy.appliesMediaViewMaskAsOutgoing = self.appliesMediaViewMaskAsOutgoing;
    return copy;
}


@end
