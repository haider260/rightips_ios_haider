//
//  FBActivity.m
//  Rightips
//
//  Created by Esol on 26/05/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "FBActivity.h"

@implementation FBActivity
@synthesize delegate;

+ (UIActivityCategory) activityCategory
{
    return UIActivityCategoryShare;
}
- (NSString *)activityType
{
    return @"rightips.Review.App";
}

- (NSString *)activityTitle
{
    return @"Facebook";
}

- (UIImage *)activityImage
{
    // Note: These images need to have a transparent background and I recommend these sizes:
    // iPadShare@2x should be 126 px, iPadShare should be 53 px, iPhoneShare@2x should be 100
    // px, and iPhoneShare should be 50 px. I found these sizes to work for what I was making.
    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
//        return [UIImage imageNamed:@"iPadShare.png"];
//    }
//    else
//    {
//        return [UIImage imageNamed:@"iPhoneShare.png"];
//    }
    return [UIImage imageNamed:@"fb_icon"];

}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems
{
    NSLog(@"%s", __FUNCTION__);
    return YES;
}

- (void)prepareWithActivityItems:(NSArray *)activityItems
{
    NSLog(@"%s",__FUNCTION__);
}

- (UIViewController *)activityViewController
{
    
    NSLog(@"%s",__FUNCTION__);
    return nil;
}

- (void)performActivity
{

   // [self faceBookPOSTRequestWithDictionary:self.fbDict];
    [delegate performSelector:@selector(tempShareForFacebook)];

    // This is where you can do anything you want, and is the whole reason for creating a custom
    // UIActivity
    
  //  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=yourappid"]];
  //  [self activityDidFinish:YES];
}

@end
