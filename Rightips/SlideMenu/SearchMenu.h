//
//  SearchMenu.h
//  Rightips
//
//  Created by Mac on 15/03/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchMenu : UIView <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIAlertViewDelegate>
{
    
   
    
}

@property (strong, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) IBOutlet UIButton *locationbutton;

@property (strong, nonatomic) IBOutlet UITableView *searchResultTableView;

@property (strong, nonatomic) IBOutlet UIButton *searchPlaceButton;


@property (strong, nonatomic) IBOutlet UITextField *searchLocationTextField;
@property (strong, nonatomic) NSMutableArray *searchResults;



- (void)showHideMenu;
- (IBAction)locationButtonPressed:(UIButton *)sender;
- (void)handleSearchForSearchString:(NSString *)searchString;
-(void)setUpMenu;
- (IBAction)searchPlaceButtonPressed:(UIButton *)sender;
-(void)getSearchResults;


@end
