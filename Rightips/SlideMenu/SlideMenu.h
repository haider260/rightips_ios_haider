/**
 * Methods.h - common static utility methods
 * @author  Beaconwatcher iOS Team
 * @version 1.0
 */

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface SlideMenu : UIView <UIGestureRecognizerDelegate>
{
    
    
}

@property (nonatomic, retain) IBOutlet UIView *loginCont;
@property (nonatomic, retain) IBOutlet UIButton *fbButton;
@property (nonatomic, retain) IBOutlet UIButton *gpButton;
@property (nonatomic, retain) IBOutlet UIButton *emButton;

@property (nonatomic, retain) IBOutlet UIView *userImageCont;
@property (nonatomic, retain) IBOutlet UIImageView *userImageView;
@property (strong, nonatomic) IBOutlet UIImageView *coverImageView;


@property (nonatomic, retain) IBOutlet UILabel *userNameLabel;

@property (nonatomic, retain) IBOutlet UIView *signoutButtonCont;
@property (weak, nonatomic) IBOutlet UIView *myWallView;


- (void)setupMenu;
- (void)showHideMenu;

@end
