/**
 * Methods.m - common static utility methods
 * @author  Beaconwatcher iOS Team
 * @version 1.0
 */

#import "SlideMenu.h"
#import "ProfileVC.h"
#import "FBActivity.h"

@implementation SlideMenu
{
    UserProfile *userProfile;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SlideMenu_iPhone" owner:self options:nil];
        self = (SlideMenu *) [subviewArray objectAtIndex:0];
        CGRect frame = self.frame;
        frame.origin.x -= frame.size.width;
        self.frame = frame;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToProfileVC)];
        [tap setDelegate:self];
        [self.userImageView setUserInteractionEnabled:YES];
        [self.userImageView addGestureRecognizer:tap];
        [self setupMenu];
    }
    return self;
}


- (void)setupMenu
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isLoggedIn = [defaults boolForKey:[StaticData instance].kLoggedIn];
    
    [_userImageView setClipsToBounds:YES];
    [[_userImageView layer] setCornerRadius:_userImageView.bounds.size.width/2];
    
    if (isLoggedIn)
    {
        NSData *userProfileData = [defaults objectForKey:[StaticData instance].kUserProfileData];
        userProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userProfileData];
        
        [_loginCont setHidden:YES];
        [_userImageCont setHidden:NO];
        [_signoutButtonCont setHidden:NO];
        [self.myWallView setHidden:NO];
        
        
        if (!userProfile.profileImgUrl ||  [userProfile.profileImgUrl isEqual:[NSNull null]]|| [userProfile.profileImgUrl isEqualToString:@""])
        {
            [_userImageView setHidden:YES];
        }
        else
        {
             [_userImageView setHidden:NO];
            [Methods setImageInImageViewFromUrl:_userImageView urlStr:userProfile.profileImgUrl];
        }
        
        
        if (!userProfile.coverImgUrl ||  [userProfile.coverImgUrl isEqual:[NSNull null]]|| [userProfile.coverImgUrl isEqualToString:@""])
        {
            [self.coverImageView setHidden:YES];
        }
        else
        {
            [self.coverImageView setHidden:NO];
            [Methods setImageInImageViewFromUrl:self.coverImageView urlStr:userProfile.coverImgUrl];
        }
        
        _userNameLabel.text = userProfile.userName;
        if (userProfile.firstName == NULL || userProfile.firstName == nil || userProfile.lastName == NULL || userProfile.lastName == nil)
        {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Login data failure", nil) message:NSLocalizedString(@"Please login aagain", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            [self signoutMenuButtonPressed:nil];
        }
    } else {
//        [[FBSession activeSession] closeAndClearTokenInformation];
//        [[GPPSignIn sharedInstance] signOut];
        [_signoutButtonCont setHidden:YES];
        [self.myWallView setHidden:YES];
    }
    
    UIColor *fbColor = [UIColor colorWithRed:0.2353f green:0.3412f blue:0.6157f alpha:1.0f];
    [self customButton:_fbButton borderColor:fbColor];
    
    UIColor *gpColor = [UIColor colorWithRed:0.8510f green:0.3020f blue:0.1961f alpha:1.0f];
    [self customButton:_gpButton borderColor:gpColor];
    
    UIColor *emColor = [UIColor colorWithRed:0.6510f green:0.7804f blue:0.0f alpha:1.0f];
    [self customButton:_emButton borderColor:emColor];
}

- (IBAction)loginWithFacebookButtonPressed:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *shouldLoginDict = [NSMutableDictionary dictionary];
    [shouldLoginDict setObject:[NSNumber numberWithBool:YES] forKey:@"shouldLogin"];
    [shouldLoginDict setObject:[StaticData instance].kProviderFacebook forKey:@"provider"];
    [defaults setObject:shouldLoginDict forKey:@"shouldLoginDict"];
    [defaults synchronize];
    
    [Methods pushVCinNCwithName:@"LandingVC" popTop:YES];
}

- (IBAction)loginWithGooglePlusButtonPressed:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *shouldLoginDict = [NSMutableDictionary dictionary];
    [shouldLoginDict setObject:[NSNumber numberWithBool:YES] forKey:@"shouldLogin"];
    [shouldLoginDict setObject:[StaticData instance].kProviderGooglePlus forKey:@"provider"];
    [defaults setObject:shouldLoginDict forKey:@"shouldLoginDict"];
    [defaults synchronize];
    
    [Methods pushVCinNCwithName:@"LandingVC" popTop:YES];
}
- (IBAction)cameraButtontapped:(UIButton *)sender
{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TapOnCameraButton"
     object:nil];
    
}
-(void)goToProfileVC
{
    [self showHideMenu];
    ProfileVC *vc = (ProfileVC *)[Methods getVCbyName:@"ProfileVC"];
    [vc setUserProfile:userProfile];
    [Methods pushVCinNCwithObj:vc popTop:NO];
}


- (IBAction)loginWithEmailButtonPressed:(id)sender
{
    [Methods pushVCinNCwithName:@"LoginVC" popTop:YES];
}

- (IBAction)homeMenuButtonPressed:(id)sender
{
    [self showHideMenu];
    [Methods pushVCinNCwithName:@"HomeVC" popTop:YES];
}

- (IBAction)settingsMenuButtonPressed:(id)sender
{
    [self showHideMenu];
    [Methods pushVCinNCwithName:@"SettingsVC" popTop:YES];
}

- (IBAction)historyMenuButtonPressed:(id)sender
{
    [self showHideMenu];
    [Methods pushVCinNCwithName:@"HistoryVC" popTop:YES];
}
- (IBAction)friendsButtonPressed:(id)sender
{
    [self showHideMenu];
    [Methods pushVCinNCwithName:@"FriendsVC" popTop:YES];
}

- (IBAction)activityMenuButtonPressed:(id)sender
{
    [self showHideMenu];
    [Methods pushVCinNCwithName:@"NotxSlideVC" popTop:YES];
}

- (IBAction)muWallButtonTapped:(UIButton *)sender
{
    [self showHideMenu];
    [Methods pushVCinNCwithName:@"MyWallVC" popTop:YES];
}

- (IBAction)signoutMenuButtonPressed:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:[StaticData instance].kLoggedIn];
    [defaults removeObjectForKey:[StaticData instance].kUserProfileData];
    
    NSMutableDictionary *shouldLoginDict = [NSMutableDictionary dictionary];
    [shouldLoginDict setObject:[NSNumber numberWithBool:NO] forKey:@"shouldLogin"];
    [defaults setObject:shouldLoginDict forKey:@"shouldLoginDict"];
    [defaults synchronize];
//    1423122061329996
//    if ([FBSession activeSession] != nil)
//        [FBSession.activeSession closeAndClearTokenInformation];
//    if ([[FBSession activeSession] state] == FBSessionStateOpen)
//        [[FBSession activeSession] closeAndClearTokenInformation];
    
        [[[FBSDKLoginManager alloc] init] logOut];
        [[GIDSignIn sharedInstance] signOut];
    
    [Methods pushVCinNCwithName:@"LandingVC" popTop:YES];
}

- (void)showHideMenu
{
    [self setupMenu];
    
    CGRect menuFrame = self.frame;
    
    if (menuFrame.origin.x < 0)
        menuFrame.origin.x += menuFrame.size.width;
    else
        menuFrame.origin.x -= menuFrame.size.width;
    
    [UIView animateWithDuration:0.5f animations:^{
        self.frame = menuFrame;
    } completion:^(BOOL finished){}];
}

- (void)customButton:(UIButton *)button borderColor:(UIColor *)borderColor {
    UIColor *colorNormal = [UIColor blackColor];
    UIColor *colorPressed = [[UIColor redColor] colorWithAlphaComponent:0.4f];
    CGSize buttonSize = button.bounds.size;
    [button setImage:[Methods imageWithColor:colorNormal size:buttonSize] forState:UIControlStateNormal];
    [button setImage:[Methods imageWithColor:colorPressed size:buttonSize] forState:UIControlStateHighlighted];
    
    [button setClipsToBounds:YES];
    [[button layer] setCornerRadius:button.bounds.size.width/2];
    [[button layer] setBorderWidth:2.0f];
    [[button layer] setBorderColor:borderColor.CGColor];
}



@end
