//
//  SearchMenu.m
//  Rightips
//
//  Created by Mac on 15/03/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import "SearchMenu.h"
#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"
#import "SPGooglePlacesPlaceDetailQuery.h"

@implementation SearchMenu
{
    NSArray *searchResultPlaces;
    SPGooglePlacesAutocompleteQuery *searchQuery;
    BOOL isTextFieldHide;
    NSUserDefaults *defaults;
    UserLocation *userLocation;
    NSString *cityName;
    NSString *specificPlace;
    
    MBProgressHUD *HUD;
    

}


- (instancetype)init
{
    self = [super init];
    if (self)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SearchMenu" owner:self options:nil];
        self = (SearchMenu *) [subviewArray objectAtIndex:0];
        CGRect frame = self.frame;
        frame.origin.y -= frame.size.height;
        self.frame = frame;
        
       
        self.searchResults = [[NSMutableArray alloc] init];
        
        [self.locationbutton.layer setBorderWidth:1.0];
        [self.locationbutton.layer setBorderColor:[UIColor darkGrayColor].CGColor];
        [self.searchResultTableView setDataSource:self];
        [self.searchResultTableView setDelegate:self];
        [self.searchTextField setDelegate:self];
        [self.searchLocationTextField setDelegate:self];
        [self.locationbutton setImage:[UIImage imageNamed:@"IconLocation1"] forState:UIControlStateNormal];
        [self.locationbutton setImage:[UIImage imageNamed:@"IconLocation2"] forState:UIControlStateSelected];
        [Methods setLeftPaddingToTextField:self.searchTextField paddingMargin:10.0];
        [Methods setLeftPaddingToTextField:self.searchLocationTextField paddingMargin:10.0];
        defaults = [NSUserDefaults standardUserDefaults];
        cityName = nil;
        
        
        searchQuery = [[SPGooglePlacesAutocompleteQuery alloc] init];
        searchQuery.radius = 100.0;
       
        specificPlace = [defaults objectForKey:@"specificPlace"];
        [self.searchTextField setText:specificPlace];
        
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToProfileVC)];
//        [tap setDelegate:self];
//        [self.userImageView setUserInteractionEnabled:YES];
//        [self.userImageView addGestureRecognizer:tap];
//        [self setUpMenu];
    }
    return self;
}
-(void)setUpMenu
{
//    UIViewController *vc = self.window.rootViewController;
    HUD = [[MBProgressHUD alloc] initWithView:self.superview];
    [self.superview addSubview:HUD];
    HUD.dimBackground = YES;
    
}
- (void)showHideMenu
{
    
    UserLocation *selectedUserLocation = [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"userLocationSelectedData"]];
    if (!selectedUserLocation)
    {
        selectedUserLocation = [StaticData instance].userLocation;
    }
    if (selectedUserLocation.selectedLocation)
    {
        [self.searchPlaceButton setEnabled:YES];
        cityName = selectedUserLocation.selectedCityName;
        [self.searchPlaceButton setTitle:[NSString stringWithFormat:@"Search in %@",selectedUserLocation.selectedCityName] forState:UIControlStateNormal];
    }
    
    CGRect menuFrame = self.frame;
    
    if (menuFrame.origin.y < 0)
    {
        menuFrame.origin.y += menuFrame.size.height;
       
    }
    else
    {
        menuFrame.origin.y -= menuFrame.size.height;
        
    }
    
    [UIView animateWithDuration:0.5f animations:^{
        self.frame = menuFrame;
    } completion:^(BOOL finished){}];
}

- (IBAction)locationButtonPressed:(UIButton *)sender
{
    if ([sender isSelected])
    {
        [sender setSelected:NO];
        [self.searchLocationTextField setHidden:YES];
        [self.searchLocationTextField resignFirstResponder];
        [self.searchResultTableView setHidden:YES];
        [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 100)];
        
        if (cityName)
        {
            [self.searchPlaceButton setEnabled:YES];
        }
        else
        {
            [self.searchPlaceButton setEnabled:NO];
        }
        
    }
    else
    {
        [sender setSelected:YES];
        [self.searchLocationTextField setHidden:NO];
        [self.searchLocationTextField becomeFirstResponder];
        [self handleSearchForSearchString:self.searchLocationTextField.text];
//        if ([self.specificPlace isEqualToString:@""])
//        {
//             [self.searchPlaceButton setEnabled:NO];
//        }
//        else
//        {
//            [self.searchPlaceButton setEnabled:YES];
//        }
//        searchResultPlaces  = nil;
        
        
        [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 420)];
        [self.searchResultTableView setHidden:NO];
        
        
    }
    
}
- (IBAction)searchPlaceButtonPressed:(UIButton *)sender
{
   
    [self.searchTextField resignFirstResponder];
    [self.searchLocationTextField resignFirstResponder];
    
    [self showHideMenu];
    [self getSearchResults];
    
}
- (void)tapOnCurrentLocation
{
   
    if ([CLLocationManager locationServicesEnabled])
    {
        if ([CLLocationManager authorizationStatus] >= kCLAuthorizationStatusAuthorized)
        {
            
        }
        else
        {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location Access for Rightips app is disabled. Please enable it by going to Settings", nil)
                                                                        message:nil
                                                                       delegate:self
                                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                              otherButtonTitles:NSLocalizedString(@"Settings", nil),nil];
            [alert show];
            [StaticData instance].userLocation.currentLocation = nil;
            [StaticData instance].userLocation.currentCityName = @"";
            
            
            
        }
    }
    
        else
        {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Turn on Location Services to allow Rightips to Determine your Current Location", nil)
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                              otherButtonTitles:NSLocalizedString(@"Settings", nil),nil];
        [alert show];
            
            [StaticData instance].userLocation.currentLocation = nil;
            [StaticData instance].userLocation.currentCityName = @"";
            
        }
    
    UserLocation *currentUserLocation = [StaticData instance].userLocation;
    currentUserLocation.userLocationMode = UserLocationModeCurrent;
    currentUserLocation.selectedLocation = currentUserLocation.currentLocation;
    currentUserLocation.selectedCityName = currentUserLocation.currentCityName;
   
    if (currentUserLocation.selectedLocation && ![currentUserLocation.selectedCityName isEqualToString:@""])
    {
    cityName = currentUserLocation.selectedCityName;
    [self.searchPlaceButton setTitle:[NSString stringWithFormat:@"Search in %@",currentUserLocation.selectedCityName] forState:UIControlStateNormal];
    NSData *userLocationData = [NSKeyedArchiver archivedDataWithRootObject:currentUserLocation];
    [defaults setObject:userLocationData forKey:@"userLocationSelectedData"];
    [defaults synchronize];
    }
    
    
   
    [self.searchResultTableView setHidden:YES];
    isTextFieldHide = NO;
    //    [searchTextfield setText:@""];
    [self.searchTextField resignFirstResponder];
    [self.searchLocationTextField resignFirstResponder];
    //    [scrlVu setAlpha:1.0];
    //    [scrlVu setUserInteractionEnabled:YES];
    [self locationButtonPressed:self.locationbutton];

    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"didUpdateLocations"
     object:nil];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: UIApplicationOpenSettingsURLString]];
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == 2)
    {
        [self handleSearchForSearchString:textField.text];
        
    }
    
    return YES;
}

- (SPGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSIndexPath *)indexPath
{
    
    return [searchResultPlaces objectAtIndex:indexPath.row];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.locationbutton isSelected])
    {
        return [searchResultPlaces count] + 1;
    }
    return [searchResultPlaces count];
}
- (SPGooglePlacesAutocompletePlace *)placeAtIndex:(NSInteger)index
{
    return [searchResultPlaces objectAtIndex:index];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"SPGooglePlacesAutocompleteCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"ArialMT" size:16.0];
    if ([self.locationbutton isSelected])
    {
        [cell.imageView setImage:[UIImage imageNamed:@"IconLocation2"]];
        if (indexPath.row == 0)
        {
            cell.textLabel.text = @"Current Location";
            
        }
        else
        {
            cell.textLabel.text = [self placeAtIndex:indexPath.row - 1].name;
        }
        
    }
    else
    {
        cell.textLabel.text = [self placeAtIndex:indexPath.row].name;
        [cell.imageView setImage:nil];
    }
    
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SPGooglePlacesAutocompletePlace *place;
    if ([self.locationbutton isSelected])
    {
        if (indexPath.row == 0) {
            [self tapOnCurrentLocation];
            return;
        }
        else
        {
            place = [self placeAtIndex:indexPath.row - 1];
        }
        
    }
    else
    {
        place = [self placeAtIndex:indexPath.row];
    }
    
    
    SPGooglePlacesPlaceDetailQuery *query = [[SPGooglePlacesPlaceDetailQuery alloc] init];
    query.reference = place.placeId;
    [query fetchPlaceDetail:^(NSDictionary *placeDictionary, NSError *error)
     {
         if (error)
         {
             SPPresentAlertViewWithErrorAndTitle(error, NSLocalizedString(@"Could not map selected Place", nil));
         }
         else if (placeDictionary)
         {
             NSDictionary *location = [[placeDictionary objectForKey:@"geometry"] objectForKey:@"location"];
             double lng = [[location objectForKey:@"lng"] doubleValue];
             double lat = [[location objectForKey:@"lat"] doubleValue];
             CLLocation *loc = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
             
             
             
             userLocation = [StaticData instance].userLocation;
             userLocation.userLocationMode = UserLocationModeSelected;
             userLocation.selectedLocation = loc;
//             userLocation.selectedCityName = [placeDictionary objectForKey:@"name"];
             
             
             if ([self.locationbutton isSelected])
             {
                 cityName = nil;
                 [self getCityName:lat longitude:lng];
                 return;
             }
             
             isTextFieldHide = YES;
             
             [self.searchTextField resignFirstResponder];
             
             
             
             
             //             [self dismissSearchControllerWhileStayingActive];
             //             [self.searchDisplayController.searchResultsTableView deselectRowAtIndexPath:indexPath animated:NO];
             
         }
     }];
}
-(void)getCityName:(double)lat longitude:(double)lng
{
    NSString *apiUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&key=AIzaSyDyEBsyBpVjBf4A6_PdsufThBYwX0FdD4k",lat,lng];
    //    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"getCategories"]];

    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:apiUrl parameters:nil];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
        
        if ([[JSON objectForKey:@"status"] isEqualToString:@"OK"])
        {
            NSArray *resultArray = [JSON objectForKey:@"results"];
//            NSDictionary *dic = [resultArray objectAtIndex:0];
            
           for (NSDictionary *resultDic in resultArray)
           {
            NSArray *addressComponentsList = [resultDic objectForKey:@"address_components"];
               if (cityName)
               {
                   break;
               }
            for (NSDictionary *dic in addressComponentsList)
            {
                if ([[[dic objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"locality"])
                {
                    cityName = [dic objectForKey:@"long_name"];
                    break;
                }
                else if ([[[dic objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"sublocality_level_1"])
                {
                    cityName = [dic objectForKey:@"long_name"];
                    break;
                }
                else if ([[[dic objectForKey:@"types"] objectAtIndex:0] isEqualToString:@"country"])
                {
                    cityName = [dic objectForKey:@"long_name"];
                    
                }
            }
               
           }
            
            userLocation.selectedCityName = cityName;
            [StaticData instance].userLocation = userLocation;
            [self.searchPlaceButton setTitle:[NSString stringWithFormat:@"Search in %@",cityName] forState:UIControlStateNormal];
            
            NSData *userLocationData = [NSKeyedArchiver archivedDataWithRootObject:[StaticData instance].userLocation];
            [defaults setObject:userLocationData forKey:@"userLocationSelectedData"];
            [defaults synchronize];
            
            isTextFieldHide = NO;
            
            [self.searchLocationTextField resignFirstResponder];
            [self locationButtonPressed:self.locationbutton];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"didUpdateLocations"
             object:nil];
            
        }
    
    }
   failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
   {
                                             
                                             
   [Methods showAlertView:@"" message:[error localizedDescription] buttonText:@"OK" tag:0 delegate:nil];
                                         }];
    [operation start];
   
}
- (void)handleSearchForSearchString:(NSString *)searchString {
    searchQuery.input = searchString;
    [searchQuery fetchPlaces:^(NSArray *places, NSError *error) {
        if (error) {
            SPPresentAlertViewWithErrorAndTitle(error, NSLocalizedString(@"Failed to connect to server, check your internet connection.", nil));
        } else {
            searchResultPlaces = [places copy];
            [self.searchResultTableView reloadData];
        }
    }];
}


-(void)getSearchResults
{
    [HUD show:YES];
    
    specificPlace = self.searchTextField.text;
    [defaults setObject:specificPlace forKey:@"specificPlace"];
    [defaults synchronize];
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    userLocation = [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"userLocationSelectedData"]];
    if (!userLocation.selectedLocation)
    {
        userLocation = [StaticData instance].userLocation;
    }
     
    
    NSString *latStr = [NSNumber numberWithDouble:userLocation.selectedLocation.coordinate.latitude].stringValue;
    NSString *lngStr = [NSNumber numberWithDouble:userLocation.selectedLocation.coordinate.longitude].stringValue;
    NSString *apiUrl =@"http://api.beaconwatcher.com/index.php?action=search";
    
    NSString *devLang = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSArray *stringList = [devLang componentsSeparatedByString:@"-"];
    devLang = [stringList objectAtIndex:0];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setValue:@"MTRfX1JpZ2h0VGlwcw==" forKey:@"key"];
    [postParams setValue:self.searchTextField.text forKey:@"keyword"];
    [postParams setValue:latStr forKey:@"lat"];
    [postParams setValue:lngStr forKey:@"lng"];
    [postParams setValue:devLang forKey:@"lang_code"];
    
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiUrl parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
 {
     [HUD hide:YES];
     if([[JSON objectForKey:@"status"] integerValue] == 1)
     {
         if ([JSON objectForKey:@"data"])
         {
             [self fetchResultData:JSON];
         }
         else if([JSON objectForKey:@"message"])
         {
             [[[UIAlertView alloc] initWithTitle:nil message:[JSON objectForKey:@"message"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil] show];
             [[NSNotificationCenter defaultCenter]
              postNotificationName:@"noResult"
              object:nil];
         }
     }
     
     else if([[JSON objectForKey:@"status"] integerValue] == 2)
     {
         [[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"There is currently no venue in this city", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil] show];
         [[NSNotificationCenter defaultCenter]
          postNotificationName:@"noResult"
          object:nil];
     }
     else if([[JSON objectForKey:@"status"] integerValue] == 0)
     {
         [[[UIAlertView alloc] initWithTitle:nil message:[JSON objectForKey:@"message"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil] show];
         [[NSNotificationCenter defaultCenter]
          postNotificationName:@"noResult"
          object:nil];
     }
     
     
 }
failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
 {
     [HUD hide:YES];
     [[[UIAlertView alloc] initWithTitle:@"Error" message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
     [[NSNotificationCenter defaultCenter]
      postNotificationName:@"noResult"
      object:nil];
 }];
    [operation start];
}

-(void)fetchResultData:(id)json
{
    [self.searchResults removeAllObjects];
    NSArray *dataArray = [json objectForKey:@"data"];
    //    for (NSDictionary *dic in dataArray)
    //    {
    //        SearchData *searchData = [[SearchData alloc] init];
    //        [searchData setTitleName:[dic objectForKey:@"site_name"]];
    //        [searchData setAddress:[dic objectForKey:@"site_address"]];
    //        [searchData setImgUrl:[[[dic objectForKey:@"images"] objectAtIndex:0] objectForKey:@"url"]];
    //        [searchData setDistance:[[dic objectForKey:@"distance"] floatValue]];
    //        [searchData setTagList:[dic objectForKey:@"tags"]];
    //        [searchResults addObject:searchData];
    //        
    //        
    //    }
    
    for (unsigned int i = 0; i < [dataArray count]; i++)
    {
        id notxObj = [dataArray objectAtIndex:i];
        NotificationData *nd = [Methods parseNotificationData:notxObj];
        [self.searchResults addObject:nd];
    }
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"SearchHide"
     object:nil];
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
