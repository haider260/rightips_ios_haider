//
//  ImageCaptureView.h
//  Rightips
//
//  Created by Mac on 03/02/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCaptureView : UIView <UIImagePickerControllerDelegate,UIAlertViewDelegate>

@property (nonatomic,strong) UIImage *chosenImage;
@property (nonatomic) NSInteger ind;

@end
