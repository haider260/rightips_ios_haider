//
//  AppDelegate.m
//  Rightips
//
//  Created by Abdul Mannan on 11/26/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import "AppDelegate.h"
#import "NavController.h"
#import "LandingVC.h"
#import "MapVC.h"
#import "InvitationVC.h"
#import <Branch/Branch.h>

@implementation AppDelegate
{
    NSMutableArray *arrayOfBeaconsExitWithUUIDAndTime;
    NSMutableArray *arrayOfEnteredBeaconsWithUUID;
    NSMutableArray *arrayOfBeacons1;
    NSMutableArray *tempArrayOfBeacons;
    BWBeacon *lastRangedBeacon;
    CBCentralManager *bluetoothManager;
    MBProgressHUD *HUD;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    //Branch Invitation Handling
    Branch *branch = [Branch getInstance];
    [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error)
    {
        if (!error && [params objectForKey:kReferringUsername] && [params objectForKey:kReferringUserId])
        {
            NSMutableArray *friendsIdsArray;
            if([[NSUserDefaults standardUserDefaults] objectForKey:kReferringUsersArray])
            {
                friendsIdsArray = [[[NSUserDefaults standardUserDefaults] objectForKey:kReferringUsersArray] mutableCopy];
            }
            else
            {
                friendsIdsArray = [[NSMutableArray alloc] init];
            }
            [friendsIdsArray addObject:[params objectForKey:kReferringUserId]];
            
            [[NSUserDefaults standardUserDefaults] setObject:friendsIdsArray forKey:kReferringUsersArray];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            BOOL isLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:[StaticData instance].kLoggedIn];
            
            if (isLoggedIn && [[NSUserDefaults standardUserDefaults] objectForKey:kReferringUsersArray])
            {
                if ([[[NSUserDefaults standardUserDefaults] objectForKey:kReferringUsersArray] count]>0)
                {
                    [ReuseAbleFunctions addFriends];
                }
            }
        //    if([[params objectForKey:@"+is_first_session"] boolValue] == 1)
            {
                InvitationVC *vc = (InvitationVC *)[Methods getVCbyName:@"InvitationVC"];
                [vc setParamsDict:params];
                [Methods pushVCinNCwithObj:vc popTop:NO];
            }
        }
    }];
    

    // Piwik tracker instantiation
    [PiwikTracker sharedInstanceWithSiteID:kPiwikSiteID baseURL:[NSURL URLWithString:kPiwikServerURL]];
    [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_APPLICATION action:ACTION_START name:nil value:nil];

    // Checking if app is running first time
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"])
    {
        //Piwik Event on the application start
        [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_APPLICATION action:ACTION_START name:nil value:nil];
    }
    else
    {
        //Piwik Event on the first launch ever
        [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_APPLICATION action:ACTION_DOWNLOAD name:nil value:nil];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
 
    // Override point for customization after application launch.

    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)])
    {
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
//         [application registerForRemoteNotifications];
    } else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    

    /*
     if (launchOptions != nil) {
     NSDictionary *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
     }
     */
    arrayOfBeaconsExitWithUUIDAndTime = [[NSMutableArray alloc] init];
    arrayOfEnteredBeaconsWithUUID=[[NSMutableArray alloc] init];
    [StaticData initStaticData];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = 100.0f;//wkCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;//kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];

    beaconManager = [[BWBeaconManager alloc] init];
    beaconManager.delegate = self;
    
    if ([beaconManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        [beaconManager requestAlwaysAuthorization];
    
    NSString *uuidStr = [StaticData instance].bwUdid;
    NSString *idStr = @"com.beaconwatcher";
    NSUUID *proximityUUID = [[NSUUID alloc] initWithUUIDString:uuidStr];
    beaconRegion = [[BWBeaconRegion alloc] initWithProximityUUID:proximityUUID identifier:idStr];
   // beaconRegion.notifyEntryStateOnDisplay = YES;
    beaconRegion.notifyOnEntry = YES;
    beaconRegion.notifyOnExit = YES;
//    beaconRegion.notifyEntryStateOnDisplay = YES;
    
    [beaconManager startMonitoringForRegion:beaconRegion];
    [beaconManager startRangingBeaconsInRegion:beaconRegion];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
//    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded)
//    {
//        [FBSession openActiveSessionWithReadPermissions:[StaticData instance].fbPermissions allowLoginUI:NO completionHandler:^(FBSession *session, FBSessionState state, NSError *error)
//        {
//              [self sessionStateChanged:session state:state error:error];
//          }];
//    }
//    else
//    {
//    }
    
    if ([launchOptions objectForKey:UIApplicationLaunchOptionsLocationKey])
    {
        anotherLocationManager = [[CLLocationManager alloc]init];
        anotherLocationManager.delegate = self;
        anotherLocationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        anotherLocationManager.activityType = CLActivityTypeOtherNavigation;
        [anotherLocationManager requestAlwaysAuthorization];
        
        [anotherLocationManager startMonitoringSignificantLocationChanges];
        
        [Methods fetchAllNotifications];
//         [self detectBluetoothState];
    }
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"inBackground"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [anotherLocationManager stopMonitoringSignificantLocationChanges];
    
    [anotherLocationManager requestAlwaysAuthorization];

    [anotherLocationManager startMonitoringSignificantLocationChanges];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inBackground"];
    [[NSUserDefaults standardUserDefaults] synchronize];
//    [self detectBluetoothState];
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    // TODO: handle this properly
    /*if ([StaticData instance].reachedThankYouScreen)
        [Methods pushVCinNC:@"NotificationVC" popTop:NO];*/
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"inBackground"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if(anotherLocationManager)
        [anotherLocationManager stopMonitoringSignificantLocationChanges];
    
    anotherLocationManager = [[CLLocationManager alloc]init];
    anotherLocationManager.delegate = self;
    anotherLocationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    anotherLocationManager.activityType = CLActivityTypeOtherNavigation;
    
    [anotherLocationManager requestAlwaysAuthorization];

    [anotherLocationManager startMonitoringSignificantLocationChanges];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [Methods fetchAllNotifications];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setDouble:0 forKey:@"recvNotxTime"];
    [defaults setDouble:MUTE_NOTX_INTERVAL_MINUTES_0 forKey:@"muteNotxInterval"];
    [defaults synchronize];
    
    UserLocation *userLocation = [StaticData instance].userLocation;
    userLocation.userLocationMode = UserLocationModeCurrent;
    userLocation.selectedLocation = userLocation.currentLocation;
    userLocation.selectedCityName = userLocation.currentCityName;
    [Methods saveUserLocationData];

    [FBSDKAppEvents activateApp];
//    [FBAppEvents activateApp];
//    [FBAppCall handleDidBecomeActive];
//    [FBAppCall handleDidBecomeActive];
//    [FBSession openActiveSessionWithAllowLoginUI:NO];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
//    [FBSession.activeSession close];
}

//- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
//{
////    return [FBSession.activeSession handleOpenURL:url];
//    return [FBAppCall han];
//}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([[url scheme] rangeOfString:@"fb"].length != 0)
      return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                       openURL:url
                                             sourceApplication:sourceApplication
                                                         annotation:annotation];
//        return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];// facebook url scheme
//        return [FBSession.activeSession handleOpenURL:url];
//    else if (![[Branch getInstance] handleDeepLink:url])  // google+ url scheme
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:sourceApplication
                                      annotation:annotation];
    return YES;
}
//- (BOOL)application:(UIApplication *)app
//            openURL:(NSURL *)url
//            options:(NSDictionary *)options
//{
//    if ([[url scheme] rangeOfString:@"fb"].length != 0)
//        return [[FBSDKApplicationDelegate sharedInstance] application:app
//                                                              openURL:url
//                                                    sourceApplication:nil
//                                                           annotation:nil];
//                
//    return [[GIDSignIn sharedInstance] handleURL:url
//                               sourceApplication:options[UIApplicationLaunchOptionsSourceApplicationKey]
//                                      annotation:options[UIApplicationLaunchOptionsAnnotationKey]];
//}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    
    if ( application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground  )
    {
        //opened from a push notification when the app was on background
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"isHomeLoaded"]==NO)
        {
            NavController *navController= (NavController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
            [navController checkIfAlreadyrun];
        }
    }
    //[Methods pushVCinNC:@"NotxSlideVC" popTop:YES];
}
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

//For interactive notification only
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [StaticData instance].userLocation.currentLocation = [locations lastObject];
    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:[locations lastObject] completionHandler:^(NSArray *placemarks, NSError *error) {
        dispatch_async(dispatch_get_main_queue(),^{
            NSString *cityName = ([placemarks count] > 0) ? [[placemarks objectAtIndex:0] locality] : @"";
            
            UserLocation *userLocation = [StaticData instance].userLocation;
            userLocation.currentCityName = cityName;
            if (userLocation.userLocationMode == UserLocationModeCurrent)
            {
                userLocation.selectedLocation = userLocation.currentLocation;
                userLocation.selectedCityName = userLocation.currentCityName;
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"didUpdateLocations" object:cityName];
            
            [Methods saveUserLocationData];
        });
    }];
}

#pragma mark-Beacon Manager Delegates

- (void)beaconManager:(BWBeaconManager *)manager didDetermineState:(CLRegionState)state forRegion:(BWBeaconRegion *)region
{
    
    if (state == CLRegionStateInside)
    {
        UIApplicationState appState = [[UIApplication sharedApplication] applicationState];
        if (appState == UIApplicationStateActive)
        {
            /*UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
            [self.navigationController pushViewController:vc animated:YES];*/
        }
        else
        {
            //[Methods generateNotification:@"Touch to view content"];
//            UserLocation *userLocation = [StaticData instance].userLocation;
//            NSMutableDictionary *locationDict=[[NSMutableDictionary alloc] init];
//            [locationDict setObject:[NSString stringWithFormat:@"%f", userLocation.selectedLocation.coordinate.longitude] forKey:@"longitude"];
//            [locationDict setObject:[NSString stringWithFormat:@"%f", userLocation.selectedLocation.coordinate.latitude] forKey:@"latitude"];
//            [[PiwikTracker sharedInstance] sendEventWithCategory:@"Notification" action:@"Posted" name:[ReuseAbleFunctions convertDictionaryToJSONString:locationDict] value:nil];
//            [Methods generateNotificationWithMessage:@"Touch to view content" AndInfo:locationDict];
//            
        }
    }
}

-(void) postNotificationWithData:(NotificationData*)notxData AndBeacon:(BWBeacon*)beacon
{
    //Sending Event To Piwik (Notification Opened)
    NSMutableDictionary *datadictForPiwik = [[NSMutableDictionary alloc] init];
    [datadictForPiwik setObject:notxData.notxId forKey:kNotificationId];
    [datadictForPiwik setObject:notxData.siteId forKey:kNotificationSiteId];
    [datadictForPiwik setObject:notxData.siteName forKey:kNotificationSiteName];
    [datadictForPiwik setObject:notxData.title forKey:kNotificationTitle];
    if (notxData.zone)
    {
        [datadictForPiwik setObject:notxData.zone forKey:kNotificationZone];
    }
    //Piwik Event When Notification Posted
    [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_NOTIFICATION action:ACTION_POSTED name:[ReuseAbleFunctions convertDictionaryToJSONString:datadictForPiwik] value:nil];
    
    //Posting notification
    UserLocation *userLocation = [StaticData instance].userLocation;
    NSMutableDictionary *locationDict=[[NSMutableDictionary alloc] init];
    [locationDict setObject:[NSString stringWithFormat:@"%f", userLocation.selectedLocation.coordinate.longitude] forKey:@"longitude"];
    [locationDict setObject:[NSString stringWithFormat:@"%f", userLocation.selectedLocation.coordinate.latitude] forKey:@"latitude"];
    
    if (![self compareBeacons1:lastRangedBeacon WithBeacon2:beacon])
    {
        [Methods generateNotificationWithMessage:notxData.title AndInfo:locationDict];
    }
    lastRangedBeacon = beacon;
}

//- (void)beaconManager:(BWBeaconManager *)manager didEnterRegion:(BWBeaconRegion *)region {
//    UIApplicationState appState = [[UIApplication sharedApplication] applicationState];
//    if (appState != UIApplicationStateActive) {
//        [Methods generateNotification:@"didEnterRegion"];
//    }
//    
//}


//- (void)beaconManager:(BWBeaconManager *)manager didExitRegion:(BWBeaconRegion *)region {
//    UIApplicationState appState = [[UIApplication sharedApplication] applicationState];
//    if (appState != UIApplicationStateActive) {
//        [Methods generateNotification:@"didExitRegion"];
//    }
//    
//}

- (void) checkForBeaconProximityWithBeacons:(NSArray *)beacons
{
    if([beacons count]>0)
    {
        if(arrayOfBeacons1)
        {
            for(BWBeacon *beacon in beacons)
            {
                if([self isBeaconExistInBeaconsArray:beacon])
                {
                    if([self isBeaconsProximityChanged:beacon])
                    {
                        [self replaceBeaconDataInTempArray:beacon];
                        
                    }
                    else
                    {
                        [self updateBeaconDataTimeInTempArray:beacon];
                        [self replaceBeaconInArrayOfBeaconsWithBeacon:beacon];
                    }
                    if([self checkIfTimerExceedTheLimit:beacon])
                    {
                        [self replaceBeaconInarrayOfBeaconsAndUpdateInTempArray:beacon];

                    }
                }
                else
                {
                    [self addBeaconInArrayOfBeaconsAndTemparray:beacon];
                    [self sortArrayOfBeaconsWRTBeacons:beacons];
                }
                
            }
        }
        else
        {
            tempArrayOfBeacons=[[NSMutableArray alloc] init];
            arrayOfBeacons1=[[NSMutableArray alloc] initWithArray:beacons];
            for(int i=0; i<beacons.count;i++)
            {
                NSMutableDictionary *dataDictForBeacon=[[NSMutableDictionary alloc] init];
                [dataDictForBeacon setObject:[beacons objectAtIndex:i]  forKey:@"beacon"];
                [dataDictForBeacon setObject:[NSDate date] forKey:@"time"];
                [tempArrayOfBeacons addObject:dataDictForBeacon];
            }
        }
    }
    
    for (int i=0; i<arrayOfBeacons1.count; i++)
    {
        BWBeacon *beacon=[arrayOfBeacons1 objectAtIndex:i];
        if([self isBeaconExits:beacon Beacons:beacons])
        {
            [self checkForTimerAndRemoveFromArrayOfBeaconsAndTempArray:beacon];
            [self sortArrayOfBeaconsWRTBeacons:beacons];
        }
    }
}

- (void) checkForTimerAndRemoveFromArrayOfBeaconsAndTempArray: (BWBeacon *) beacon
{
    for (int i=0; i<tempArrayOfBeacons.count; i++)
    {
        NSDictionary *tempBeaconData=[tempArrayOfBeacons objectAtIndex:i];
        if ([[NSDate date] timeIntervalSinceDate:[tempBeaconData objectForKey:@"time"]]>5.0)
        {
            NSString *beaconUMM=[self getBeaconUMM:[tempBeaconData objectForKey:@"beacon"]];
            for (int j=0; j<arrayOfBeacons1.count; j++)
            {
                BWBeacon *tempBeacon=[arrayOfBeacons1 objectAtIndex:j];
                if ([beaconUMM isEqualToString:[self getBeaconUMM:tempBeacon]])
                {
                    [arrayOfBeacons1 removeObjectAtIndex:j];
                    [tempArrayOfBeacons removeObjectAtIndex:i];
                    return;
                }
            }
            
        }
    }
}

- (BOOL) isBeaconExits: (BWBeacon *) beacon Beacons: (NSArray *) beacons
{
    BOOL isExist=NO;
    for (int i=0; i<beacons.count; i++)
    {
        BWBeacon *tempBeacon=[beacons objectAtIndex:i];
        if([[self getBeaconUMM:tempBeacon] isEqualToString:[self getBeaconUMM:beacon]] )
        {
            isExist=YES;
        }
    }
    if(isExist)
        return NO;
    else
        return YES;
}

- (void) sortArrayOfBeaconsWRTBeacons:(NSArray *) beacons
{
    NSMutableArray *tempArrayForSort=[[NSMutableArray alloc] init];
    for(int i=0; i<beacons.count; i++)
    {
        BWBeacon *beacon=[beacons objectAtIndex:i];
        NSString *beaconUMM=[self getBeaconUMM:beacon];
        for(int j=0; j<arrayOfBeacons1.count; j++)
        {
            if([beaconUMM isEqualToString:[self getBeaconUMM:[arrayOfBeacons1 objectAtIndex:j]]])
            {
                BWBeacon *tempBeacon = [arrayOfBeacons1 objectAtIndex:i];
                [arrayOfBeacons1 replaceObjectAtIndex:i withObject:[arrayOfBeacons1 objectAtIndex:j]];
                [arrayOfBeacons1 replaceObjectAtIndex:j withObject:tempBeacon];
                break;
            }
        }
    }
    //arrayOfBeacons=[NSMutableArray arrayWithArray:tempArrayForSort];
}



- (void) addBeaconInArrayOfBeaconsAndTemparray: (BWBeacon *) beacon
{
    BOOL isFound= NO;
    for (int i=0; i<arrayOfBeacons1.count; i++)
    {
        BWBeacon *tempBeacon=[arrayOfBeacons1 objectAtIndex:i];
        if([[self getBeaconUMM:beacon] isEqualToString:[self getBeaconUMM:tempBeacon]])
        {
            isFound=YES;
            [arrayOfBeacons1 replaceObjectAtIndex:i withObject:beacon];
        }
    }
    if(isFound==NO)
        [arrayOfBeacons1 addObject:beacon];
    BOOL isFoundB=NO;
    for (int i=0; i<tempArrayOfBeacons.count; i++)
    {
        NSDictionary *tempdataDict=[tempArrayOfBeacons objectAtIndex:i];
        if([[self getBeaconUMM:beacon] isEqualToString:[self getBeaconUMM:[tempdataDict objectForKey:@"beacon"]]])
        {
            isFoundB=YES;
            NSMutableDictionary *dataDict=[[NSMutableDictionary alloc] init];
            [dataDict setObject:beacon forKey:@"beacon"];
            [dataDict setObject:[NSDate date] forKey:@"time"];
            [tempArrayOfBeacons replaceObjectAtIndex:i withObject:dataDict];
        }
    }
    if(isFoundB==NO)
    {
        NSMutableDictionary *dataDict=[[NSMutableDictionary alloc] init];
        [dataDict setObject:beacon forKey:@"beacon"];
        [dataDict setObject:[NSDate date] forKey:@"time"];
        [tempArrayOfBeacons addObject: dataDict];
    }
    
}


- (void) replaceBeaconInarrayOfBeaconsAndUpdateInTempArray: (BWBeacon *) beacon
{
  //  NSLog(@"%s",__FUNCTION__);

    for(int i=0; i<arrayOfBeacons1.count; i++)
    {
        BWBeacon *tempBeacon=[arrayOfBeacons1 objectAtIndex:i];
        if([[self getBeaconUMM:tempBeacon] isEqualToString:[self getBeaconUMM:beacon]])
        {
            [arrayOfBeacons1 replaceObjectAtIndex:i withObject:beacon];
            [self updateBeaconDataTimeInTempArray:beacon];
            return;
        }
    }
}

- (BOOL) checkIfTimerExceedTheLimit: (BWBeacon *) beacon
{
    for(int i=0; i<tempArrayOfBeacons.count; i++)
    {
        NSDictionary *tempDict=[tempArrayOfBeacons objectAtIndex:i];
        if([[self getBeaconUMM:beacon] isEqualToString:[self getBeaconUMM:[tempDict objectForKey:@"beacon"]]])
        {
            if([[NSDate date] timeIntervalSinceDate:[tempDict objectForKey:@"time"]]>5.0)
            {
                return YES;
            }
        }
    }
    return NO;
}


- (void) replaceBeaconInArrayOfBeaconsWithBeacon: (BWBeacon *) beacon
{
    for(int i=0; i<arrayOfBeacons1.count; i++)
    {
        BWBeacon *tempBeacon=[arrayOfBeacons1 objectAtIndex:i];
        if([[self getBeaconUMM:tempBeacon] isEqualToString:[self getBeaconUMM:beacon]])
        {
            [arrayOfBeacons1 replaceObjectAtIndex:i withObject:beacon];
            return;
        }
    }
}


- (void) updateBeaconDataTimeInTempArray: (BWBeacon *)beacon
{
    for(int i=0; i<tempArrayOfBeacons.count; i++)
    {
        NSDictionary *tempDict=[tempArrayOfBeacons objectAtIndex:i];
        if([[self getBeaconUMM:[tempDict objectForKey:@"beacon"]] isEqualToString:[self getBeaconUMM:beacon]])
        {
            NSMutableDictionary *tempDataDict=[[NSMutableDictionary alloc] init];
            [tempDataDict setObject:beacon forKey:@"beacon"];
            [tempDataDict setObject:[NSDate date] forKey:@"time"];
            [tempArrayOfBeacons replaceObjectAtIndex:i withObject:tempDataDict];
            return;
        }
    }
}

- (void) replaceBeaconDataInTempArray: (BWBeacon *) beacon
{
    for(int i=0; i<tempArrayOfBeacons.count; i++)
    {
        NSDictionary *tempDict=[tempArrayOfBeacons objectAtIndex:i];
        if([[self getBeaconUMM:[tempDict objectForKey:@"beacon"]] isEqualToString:[self getBeaconUMM:beacon]])
        {
            NSMutableDictionary *tempDataDict=[[NSMutableDictionary alloc] init];
            [tempDataDict setObject:beacon forKey:@"beacon"];
            [tempDataDict setObject:[tempDict objectForKey:@"time"] forKey:@"time"];
            [tempArrayOfBeacons replaceObjectAtIndex:i withObject:tempDataDict];
            return;
        }
    }
}

- (BOOL) isBeaconsProximityChanged: (BWBeacon *) beacon
{
    for (BWBeacon * tempBeacon in arrayOfBeacons1)
    {
        if([[self getBeaconUMM:tempBeacon] isEqualToString:[self getBeaconUMM:beacon]])
        {
            if(tempBeacon.proximity == beacon.proximity)
            {
                return NO;
            }
            else
            {
                return YES;
            }
        }
    }
    return NO;
}

- (BOOL) isBeaconExistInBeaconsArray:(BWBeacon *) beacon
{
    for(BWBeacon *tempBeacon in arrayOfBeacons1)
    {
        if([[self getBeaconUMM:tempBeacon] isEqualToString:[self getBeaconUMM:beacon]])
        {
            return YES;
        }
    }
    return NO;
}

- (NSString *)getBeaconUMM:(BWBeacon *)beacon
{
    NSString *ummStr = @"";
    ummStr = [ummStr stringByAppendingString:beacon.proximityUUID.UUIDString];
    ummStr = [ummStr stringByAppendingString:@"_"];
    ummStr = [ummStr stringByAppendingString:beacon.major.stringValue];
    ummStr = [ummStr stringByAppendingString:@"_"];
    ummStr = [ummStr stringByAppendingString:beacon.minor.stringValue];
    return ummStr;
}

- (void) beaconManager:(BWBeaconManager *)manager didRangeBeacons:(NSArray *)myBeacons inRegion:(BWBeaconRegion *)region
{
    //Get Beacons whose data matched with any beacon added on server
    NSMutableArray *rangedBeaconsWithProximity = [[NSMutableArray alloc] init];
    for (BWBeacon *beacon in myBeacons)
    {
        NSArray *notxData = [BwMethods getNotificationsFromRangedBeacons:[NSArray arrayWithObject:beacon]];
        if (notxData.count>0)
        {
            [rangedBeaconsWithProximity addObject:beacon];
        }
    }

    if ([rangedBeaconsWithProximity count] > 0)
    {
        [self checkForBeaconProximityWithBeacons:rangedBeaconsWithProximity];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"didRangeBeacons" object:arrayOfBeacons1];
    }
    NSArray * arrayOfCurrentlyInRangeBeacons=[[NSUserDefaults standardUserDefaults] objectForKey:@"currentlyInRangedBeacons"];
    if([rangedBeaconsWithProximity count]>[arrayOfCurrentlyInRangeBeacons count])
    {
        //Beacon Enters
        //Determine Which beacons Enters the region
        NSArray *arrayOfEnteredBeacons=[self getTheBeaconsWhichEntersTheRegionWithScannedBeacons:rangedBeaconsWithProximity AlreadyInRegion:arrayOfCurrentlyInRangeBeacons];
        
        //Check whether the exited beacon re-enters
        for(int i=0; i<[arrayOfEnteredBeacons count]; i++)
        {
            for(int j=0;j<[arrayOfBeaconsExitWithUUIDAndTime count];j++)
            {
                if([[[arrayOfEnteredBeacons objectAtIndex:i] proximityUUID] isEqual:[[arrayOfBeaconsExitWithUUIDAndTime objectAtIndex:j] objectForKey:@"UUID"]])
                {
                    [arrayOfBeaconsExitWithUUIDAndTime removeObjectAtIndex:j];
                    break;
                }
            }
        }
        //Check in arrayOfEnteredBeaconsWithUUID whether the beacon enters already enterred if not then insert that UUID in arrayOfEnteredBeaconsWithUUID
        for(BWBeacon *currentBeacon in arrayOfEnteredBeacons)
        {
            bool isExists=NO;
            for(NSUUID *enteredUUID in arrayOfEnteredBeaconsWithUUID)
            {
                if([enteredUUID  isEqual:currentBeacon])
                {
                    isExists=YES;
                    break;
                }
            }
            if(isExists==NO)
            {
                NSArray *notxData = [BwMethods getNotificationsFromRangedBeacons:[NSArray arrayWithObject:currentBeacon]];
                if (notxData.count>0)
                {
                    NotificationData *bcn = [notxData objectAtIndex:0];
                    [arrayOfEnteredBeaconsWithUUID addObject:currentBeacon];
                    //Here Acknowledge Piwic about the entry of Beacon.
                    
                    NSMutableDictionary *datadictForPiwik=[[NSMutableDictionary alloc] init];
                    [datadictForPiwik setObject:currentBeacon.major forKey:kBeaconMajor];
                    [datadictForPiwik setObject:currentBeacon.minor forKey:kBeaconMinor];
                    if (bcn.siteId)
                    {
                        [datadictForPiwik setObject:bcn.siteId forKey:kBeaconSiteId];
                    }
                    [datadictForPiwik setObject:currentBeacon.proximityUUID forKey:kBeaconUUID];
                    
                    //Piwik Event When beacon entered in range
                    [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_VENUE action:ACTION_ENTER name:[ReuseAbleFunctions convertDictionaryToJSONString:datadictForPiwik] value:nil];
                    [self postNotificationWithData:[notxData objectAtIndex:0] AndBeacon:currentBeacon];
                }
            }
        }
    }
    else if ([rangedBeaconsWithProximity count]<[arrayOfCurrentlyInRangeBeacons count])
    {
        //Beacon exit
        //Determine which beacon exit the region
        NSArray *arrayOfExitBeacons=[self getTheBeaconsWhichExitTheRegionWithScannedBeacons:rangedBeaconsWithProximity AlreadyInRegion:arrayOfCurrentlyInRangeBeacons];
        for (int i=0;i<[arrayOfExitBeacons count]; i++)
        {
            bool isAlreadyExists=NO;
            for(int j=0;j<[arrayOfBeaconsExitWithUUIDAndTime count];j++)
            {
                if([[[arrayOfExitBeacons objectAtIndex:i] proximityUUID] isEqual:[[arrayOfBeaconsExitWithUUIDAndTime objectAtIndex:j] objectForKey:@"UUID"]])
                {
                    isAlreadyExists= YES;
                    NSArray *notxData = [BwMethods getNotificationsFromRangedBeacons:[NSArray arrayWithObject:[arrayOfExitBeacons objectAtIndex:i]]];
                    NSMutableDictionary *dictForExitBeacon=[[NSMutableDictionary alloc] init];
                    [dictForExitBeacon setObject:[[arrayOfExitBeacons objectAtIndex:i] proximityUUID] forKey:@"UUID"];
                    [dictForExitBeacon setObject:[[arrayOfExitBeacons objectAtIndex:i] major] forKey:@"major"];
                    [dictForExitBeacon setObject:[[arrayOfExitBeacons objectAtIndex:i] minor] forKey:@"minor"];
                    if (notxData.count>0)
                    {
                        NotificationData *data = [notxData objectAtIndex:0];
                        if (data) {
                            [dictForExitBeacon setObject:data.siteId forKey:@"site_id"];
                        }
                    }
                    [dictForExitBeacon setObject:[NSDate date] forKey:@"time"];
                    [arrayOfBeaconsExitWithUUIDAndTime replaceObjectAtIndex:j withObject:dictForExitBeacon];
                    
                }
            }
            if(isAlreadyExists==NO)
            {
                NSArray *notxData = [BwMethods getNotificationsFromRangedBeacons:[NSArray arrayWithObject:[arrayOfExitBeacons objectAtIndex:i]]];
                NSMutableDictionary *dictForExitBeacon=[[NSMutableDictionary alloc] init];
                [dictForExitBeacon setObject:[[arrayOfExitBeacons objectAtIndex:i] proximityUUID] forKey:@"UUID"];
                [dictForExitBeacon setObject:[[arrayOfExitBeacons objectAtIndex:i] major] forKey:@"major"];
                [dictForExitBeacon setObject:[[arrayOfExitBeacons objectAtIndex:i] minor] forKey:@"minor"];
                if (notxData.count>0)
                {
                    NotificationData *data = [notxData objectAtIndex:0];
                    if (data) {
                        [dictForExitBeacon setObject:data.siteId forKey:@"site_id"];
                    }
                }
                [dictForExitBeacon setObject:[NSDate date] forKey:@"time"];
                [arrayOfBeaconsExitWithUUIDAndTime addObject:dictForExitBeacon];
            }
            
        }
    }
    else if ([rangedBeaconsWithProximity count]==[arrayOfCurrentlyInRangeBeacons count])
    {
        //Both counts equal Now have to check all beacons are in arrayOfCurrentlyInRangeBeacons if not then have to check which beacon Enters or Exit the region.
        bool isFound=NO;
        NSMutableArray *arrayOfEnteredBeacons=[[NSMutableArray alloc] init];
        for(int i=0; i<[rangedBeaconsWithProximity count];i++)
        {
            for(int j=0;j<[arrayOfCurrentlyInRangeBeacons count];j++)
            {
                if([[[rangedBeaconsWithProximity objectAtIndex:i] proximityUUID]isEqual:[[NSKeyedUnarchiver unarchiveObjectWithData: [arrayOfCurrentlyInRangeBeacons  objectAtIndex:j]] proximityUUID]])
                {
                    isFound=YES;
                    break;
                }
            }
            if(isFound==NO)
            {
                //beacon enters in the region.
                [arrayOfEnteredBeacons addObject:[rangedBeaconsWithProximity objectAtIndex:i]];
            }
        }
        NSArray *arrayOfUniqueEnteredBeacons=[self uniqueArrayOfBeaconsWRTUUIDInArrayOfBeacons:arrayOfEnteredBeacons];
        
        
        //Check in arrayOfEnteredBeaconsWithUUID whether the beacon enters already enterred if not then insert that UUID in arrayOfEnteredBeaconsWithUUID
        for(BWBeacon *currentBeacon in arrayOfUniqueEnteredBeacons)
        {
            bool isExists=NO;
            for(NSUUID *enteredUUID in arrayOfEnteredBeaconsWithUUID)
            {
                if([enteredUUID  isEqual:currentBeacon])
                {
                    isExists=YES;
                    break;
                }
            }
            if(isExists==NO)
            {
                NSArray *notxData = [BwMethods getNotificationsFromRangedBeacons:[NSArray arrayWithObject:currentBeacon]];

                if (notxData.count>0)
                {
                    NotificationData *bcn = [notxData objectAtIndex:0];
                    [arrayOfEnteredBeaconsWithUUID addObject:currentBeacon];
                    //Here Acknowledge Piwic about the entry of Beacon.
                    
                    NSMutableDictionary *datadictForPiwik=[[NSMutableDictionary alloc] init];
                    [datadictForPiwik setObject:currentBeacon.major forKey:kBeaconMajor];
                    [datadictForPiwik setObject:currentBeacon.minor forKey:kBeaconMinor];
                    if (bcn.siteId)
                    {
                        [datadictForPiwik setObject:bcn.siteId forKey:kBeaconSiteId];
                    }
                    [datadictForPiwik setObject:currentBeacon.proximityUUID forKey:kBeaconUUID];
                    
                    [self postNotificationWithData:[notxData objectAtIndex:0] AndBeacon:currentBeacon];
                    //Piwik Event When beacon entered in range
                    [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_VENUE action:ACTION_ENTER name:[ReuseAbleFunctions convertDictionaryToJSONString:datadictForPiwik] value:nil];
                }

            }
        }
        
        
        for(int i=0; i<[arrayOfUniqueEnteredBeacons count]; i++)
        {
            for(int j=0;j<[arrayOfBeaconsExitWithUUIDAndTime count];j++)
            {
                if([[[arrayOfUniqueEnteredBeacons objectAtIndex:i] proximityUUID] isEqual:[[arrayOfBeaconsExitWithUUIDAndTime objectAtIndex:j] objectForKey:@"UUID"]])
                {
                    [arrayOfBeaconsExitWithUUIDAndTime removeObjectAtIndex:j];
                    break;
                }
            }
        }
        
        NSMutableArray *arrayOfExitBeacons=[[NSMutableArray alloc] init];
        bool isFound2=NO;
        for(int i=0; i<[arrayOfCurrentlyInRangeBeacons count];i++)
        {
            for(int j=0;j<[rangedBeaconsWithProximity count];j++)
            {
                if([[[NSKeyedUnarchiver unarchiveObjectWithData: [arrayOfCurrentlyInRangeBeacons  objectAtIndex:j]] proximityUUID] isEqual:[[rangedBeaconsWithProximity objectAtIndex:j] proximityUUID]])
                {
                    isFound2=YES;
                    break;
                }
            }
            if(isFound2==NO)
            {
                //beacon exit from the region.
                [arrayOfExitBeacons addObject:[NSKeyedUnarchiver unarchiveObjectWithData: [arrayOfCurrentlyInRangeBeacons objectAtIndex:i]]];
            }
        }
        
        // If Exit store to the array and watch it whether it remains exit for specific interval of time if it remain exit then acknowledge Piwik that this has been exited from the region.
        NSArray *arrayOfUniquExitBeacons=[self uniqueArrayOfBeaconsWRTUUIDInArrayOfBeacons:arrayOfExitBeacons];
        for (int i=0;i<[arrayOfUniquExitBeacons count]; i++)
        {
            bool isAlreadyExists=NO;
            for(int j=0;j<[arrayOfBeaconsExitWithUUIDAndTime count];j++)
            {
                if([[[arrayOfUniquExitBeacons objectAtIndex:i] proximityUUID] isEqual:[[arrayOfBeaconsExitWithUUIDAndTime objectAtIndex:j] objectForKey:@"UUID"]])
                {
                    isAlreadyExists= YES;
                    NSMutableDictionary *dictForExitBeacon=[[NSMutableDictionary alloc] init];
                    [dictForExitBeacon setObject:[[arrayOfUniquExitBeacons objectAtIndex:i] proximityUUID] forKey:@"UUID"];
                    [dictForExitBeacon setObject:[[arrayOfUniquExitBeacons objectAtIndex:i] major] forKey:@"major"];
                    [dictForExitBeacon setObject:[[arrayOfUniquExitBeacons objectAtIndex:i] minor] forKey:@"minor"];
                    
                    NSArray *notxData = [BwMethods getNotificationsFromRangedBeacons:[NSArray arrayWithObject:[arrayOfUniquExitBeacons objectAtIndex:i]]];
                    if (notxData.count>0)
                    {
                        NotificationData *data = [notxData objectAtIndex:0];
                        if (data)
                        {
                            [dictForExitBeacon setObject:data.siteId forKey:@"site_id"];
                        }
                    }
                    [dictForExitBeacon setObject:[NSDate date] forKey:@"time"];
                    [arrayOfBeaconsExitWithUUIDAndTime replaceObjectAtIndex:j withObject:dictForExitBeacon];
                    
                }
            }
            if(isAlreadyExists==NO)
            {
                NSMutableDictionary *dictForExitBeacon=[[NSMutableDictionary alloc] init];
                [dictForExitBeacon setObject:[[arrayOfUniquExitBeacons objectAtIndex:i] proximityUUID] forKey:@"UUID"];
                [dictForExitBeacon setObject:[[arrayOfUniquExitBeacons objectAtIndex:i] major] forKey:@"major"];
                [dictForExitBeacon setObject:[[arrayOfUniquExitBeacons objectAtIndex:i] minor] forKey:@"minor"];
                NSArray *notxData = [BwMethods getNotificationsFromRangedBeacons:[NSArray arrayWithObject:[arrayOfUniquExitBeacons objectAtIndex:i]]];
                if (notxData.count>0)
                {
                    NotificationData *data = [notxData objectAtIndex:0];
                    if (data) {
                        [dictForExitBeacon setObject:data.siteId forKey:@"site_id"];
                    }
                }
                [dictForExitBeacon setObject:[NSDate date] forKey:@"time"];
                [arrayOfBeaconsExitWithUUIDAndTime addObject:dictForExitBeacon];
            }
        }

        
        
    }
    
    //NSMutableArray *arrayOfBeaconsUUIDs=[[NSMutableArray alloc] init];
    
    for(NSDictionary *dict in arrayOfBeaconsExitWithUUIDAndTime)
    {
        if([[NSDate date] timeIntervalSinceDate:(NSDate *)[dict objectForKey:@"time"]] >180.0)
        {
            //Acknowledge Piwik about the exit of the beacon
            //Also remove the UUID from the arrayOfBeaconsEnteredwithUUID So next time it enters in the region it can acknowledge the Piwik about its entry.
            NSMutableDictionary *datadictForPiwik=[[NSMutableDictionary alloc] init];
            [datadictForPiwik setObject:[dict objectForKey:@"major"] forKey:kBeaconMajor];
            [datadictForPiwik setObject:[dict objectForKey:@"minor"] forKey:kBeaconMinor];
            if ([dict objectForKey:@"site_id"]) {
                [datadictForPiwik setObject:[dict objectForKey:@"site_id"] forKey:kBeaconSiteId];
            }
            [datadictForPiwik setObject:[dict objectForKey:@"UUID"] forKey:kBeaconUUID];
            
            //Piwik Event When beacon exit from range
            [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_VENUE action:ACTION_EXIT name:[ReuseAbleFunctions convertDictionaryToJSONString:datadictForPiwik] value:nil];
            
            [arrayOfBeaconsExitWithUUIDAndTime removeObject:dict];
            
        }
        
    }
    
    NSMutableArray *archiveArray = [NSMutableArray arrayWithCapacity:rangedBeaconsWithProximity.count];
    for(BWBeacon *beacon in rangedBeaconsWithProximity)
    {
        NSData *beaconEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:beacon];
        [archiveArray addObject:beaconEncodedObject];

    }
    
    [[NSUserDefaults standardUserDefaults] setObject:archiveArray forKey:@"currentlyInRangedBeacons"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (BOOL) compareBeacons1:(BWBeacon*)bwn WithBeacon2:(BWBeacon*)beacon
{
    if ([bwn.proximityUUID.UUIDString caseInsensitiveCompare:beacon.proximityUUID.UUIDString] == NSOrderedSame &&
        [bwn.major.stringValue isEqualToString:beacon.major.stringValue] &&
        [bwn.minor.stringValue isEqualToString:beacon.minor.stringValue])
    {
        if (bwn.proximity == beacon.proximity)
        {
            return YES;
        }
    }
    return NO;
}

#pragma mark-Beacons Entry Exit

- (NSArray *) getTheBeaconsWhichEntersTheRegionWithScannedBeacons:(NSArray *)scannedBeacons AlreadyInRegion: (NSArray *)inRegionBeacons
{
    NSMutableArray *arrayOfBeaconsEntered=[[NSMutableArray alloc] init];
    bool isFound=NO;
    for(int i=0;i<[scannedBeacons count];i++)
    {
        for(int j=0;j<[inRegionBeacons count]; j++)
        {
            if([[[scannedBeacons objectAtIndex:i] proximityUUID] isEqual:[[NSKeyedUnarchiver unarchiveObjectWithData:[inRegionBeacons objectAtIndex:j]] proximityUUID]])
            {
                isFound=YES;
                break;
            }
        }
        if(isFound == NO)
        {
            [arrayOfBeaconsEntered addObject:[scannedBeacons objectAtIndex:i]];
        }
    }
    //make the array of entered beacons unique
    return [self uniqueArrayOfBeaconsWRTUUIDInArrayOfBeacons:arrayOfBeaconsEntered];
}

- (NSArray *) uniqueArrayOfBeaconsWRTUUIDInArrayOfBeacons:(NSArray *) arrayOfBeacons
{
    NSMutableArray * unique = [NSMutableArray array];
    NSMutableArray * processed = [NSMutableArray array];
    for(BWBeacon *beacon in arrayOfBeacons)
    {
        if([processed count]==0)
        {
            [unique addObject:beacon];
            [processed addObject:beacon];
        }
        else
        {
            bool isFound=NO;
            for (BWBeacon *procBeacon in processed)
            {
                if([[beacon proximityUUID] isEqual:[procBeacon proximityUUID]])
                {
                    isFound=YES;
                }
            }
            if(isFound==NO)
            {
                [unique addObject:beacon];
                [processed addObject:beacon];
            }
        }
    }

    if([arrayOfBeacons count]>0)
    {
        return unique;
    }
    else
    {
        return nil;
    }
}

- (NSArray *) getTheBeaconsWhichExitTheRegionWithScannedBeacons:(NSArray *)scannedBeacons AlreadyInRegion: (NSArray *)inRegionBeacons
{
    NSMutableArray *arrayOfBeaconsExit=[[NSMutableArray alloc] init];
    bool isFound=NO;
    for(int i=0;i<[inRegionBeacons count];i++)
    {
        for(int j=0;j<[scannedBeacons count]; j++)
        {
            if([[[NSKeyedUnarchiver unarchiveObjectWithData:[inRegionBeacons objectAtIndex:i]] proximityUUID] isEqual:[[scannedBeacons objectAtIndex:j] proximityUUID]])
            {
                isFound=YES;
                break;
            }
        }
        if(isFound == NO)
        {
            [arrayOfBeaconsExit addObject:[NSKeyedUnarchiver unarchiveObjectWithData:[inRegionBeacons objectAtIndex:i]]];
        }
    }
    
    //make the array of entered beacons unique
    return [self uniqueArrayOfBeaconsWRTUUIDInArrayOfBeacons:arrayOfBeaconsExit];

}

#pragma mark-FB Session State Delegate

//- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState)state error:(NSError *)error
//{
//    if (!error && state == FBSessionStateOpen)
//    {
//        return;
//    }
//    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed)
//    {
//
//    }
//    if (error)
//    {
//        NSString *alertText;
//        NSString *alertTitle;
//        if ([FBErrorUtility shouldNotifyUserForError:error] == YES)
//        {
//            alertTitle = @"Facebook: Something went wrong";
//            alertText = [FBErrorUtility userMessageForError:error];
//            [Methods showAlertView:alertTitle message:alertText buttonText:@"OK" tag:0 delegate:nil];
//        }
//        else
//        {
//            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled)
//            {
//            }
//            else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession)
//            {
//                alertTitle = @"Session Error";
//                alertText = @"Your current session is no longer valid. Please log in again.";
//                [Methods showAlertView:alertTitle message:alertText buttonText:@"OK" tag:0 delegate:nil];
//            }
//            else
//            {
//                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
//                alertTitle = @"Something went wrong";
//                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
//                [Methods showAlertView:alertTitle message:alertText buttonText:@"OK" tag:0 delegate:nil];
//            }
//        }
//        [FBSession.activeSession closeAndClearTokenInformation];
//    }
//}

#pragma mark-Bluetooth
- (void)detectBluetoothState
{
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], CBCentralManagerOptionShowPowerAlertKey, nil];
    bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:options];
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if (central.state == CBCentralManagerStatePoweredOff)
    {
        [Methods pushVCinNCwithName:@"LandingVC" popTop:YES];
    }
}

@end
