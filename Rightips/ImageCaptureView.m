//
//  ImageCaptureView.m
//  Rightips
//
//  Created by Mac on 03/02/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import "ImageCaptureView.h"


@implementation ImageCaptureView
{
    UIButton *selectImageButton,*photoLibraryButton,*takePhotoButton,*selectedBtn;
    UIImageView *transparentImageView;
    UIView *selectedPhotoView;
    MBProgressHUD *HUD;
    NSUserDefaults *defaults;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        // initilize all your UIView components
        
        [self setupView];
    }
    return self;
}

-(void)setupView
{
    
    transparentImageView = [[UIImageView alloc] init];
    [transparentImageView setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [transparentImageView setImage:[UIImage imageNamed:@"TransparentImage"]];
    [transparentImageView setUserInteractionEnabled:YES];
    //    [transparentImgView setBackgroundColor:[UIColor blackColor]];
    //    [transparentImgView setAlpha:0.5];
    
    
    
    UIView *cancelView = [[UIView alloc] init];
    [cancelView setFrame:CGRectMake(20, self.frame.size.height - 100, self.frame.size.width - 40, 50)];
    //    [cancelView setCenter:CGPointMake(self.view.frame.size.width / 2, cancelView.frame.origin.y)];
    [cancelView setBackgroundColor:[UIColor whiteColor]];
    [cancelView setAlpha:1.0];
    [cancelView.layer setCornerRadius:5.0];
    [cancelView setClipsToBounds:YES];
    [transparentImageView addSubview:cancelView];
    //    UIView *overlay = [[UIView alloc] initWithFrame:cancelView.frame];
    //    overlay.backgroundColor = [UIColor blackColor];
    //    overlay.alpha = 0.6;
    //    [cancelView addSubview:overlay];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btn setFrame:cancelView.bounds];
    [btn setTitle:@"Cancel" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [btn setBackgroundColor:[UIColor whiteColor]];
    [btn addTarget:self action:@selector(cancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelView addSubview:btn];
    
    UIView *libraryORTakePhotoView = [[UIView alloc] init];
    [libraryORTakePhotoView setFrame:CGRectMake(20, cancelView.frame.origin.y - 60, self.frame.size.width - 40, 50)];
    //    [libraryORTakePhotoView setCenter:CGPointMake(self.view.frame.size.width / 2, libraryORTakePhotoView.frame.origin.y)];
    [libraryORTakePhotoView setBackgroundColor:[UIColor whiteColor]];
    [libraryORTakePhotoView setAlpha:1.0];
    [libraryORTakePhotoView.layer setCornerRadius:5.0];
    [libraryORTakePhotoView setClipsToBounds:YES];
    [transparentImageView addSubview:libraryORTakePhotoView];
    
    //    [overlay setFrame:libraryORTakePhotoView.frame];
    //    overlay.backgroundColor = [UIColor blackColor];
    //    overlay.alpha = 0.6;
    //    [libraryORTakePhotoView addSubview:overlay];
    
    selectImageButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [selectImageButton setFrame:CGRectMake(0, 0, libraryORTakePhotoView.frame.size.width, libraryORTakePhotoView.frame.size.height)];
    [selectImageButton setTitle:NSLocalizedString(@"Use Selected Image", nil) forState:UIControlStateNormal];
    [selectImageButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [selectImageButton setBackgroundColor:[UIColor whiteColor]];
    [selectImageButton setHidden:YES];
    [selectImageButton setAlpha:1.0];
    [selectImageButton addTarget:self action:@selector(selectedImage) forControlEvents:UIControlEventTouchUpInside];
    [libraryORTakePhotoView addSubview:selectImageButton];
    
    photoLibraryButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [photoLibraryButton setFrame:CGRectMake(0, 0, libraryORTakePhotoView.frame.size.width / 2, libraryORTakePhotoView.frame.size.height)];
    [photoLibraryButton setTitle:NSLocalizedString(@"Photo Library", nil) forState:UIControlStateNormal];
    [photoLibraryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [photoLibraryButton setBackgroundColor:[UIColor whiteColor]];
    [photoLibraryButton addTarget:self action:@selector(libraryPhotoButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [photoLibraryButton.layer setBorderColor:[UIColor blackColor].CGColor];
    [photoLibraryButton.layer setBorderWidth:0.5];
    [photoLibraryButton setAlpha:1.0];
    [libraryORTakePhotoView addSubview:photoLibraryButton];
    
    takePhotoButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [takePhotoButton setFrame:CGRectMake(libraryORTakePhotoView.frame.size.width / 2, 0, libraryORTakePhotoView.frame.size.width / 2, libraryORTakePhotoView.frame.size.height)];
    [takePhotoButton setTitle:NSLocalizedString(@"Take Photo", nil) forState:UIControlStateNormal];
    [takePhotoButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [takePhotoButton setBackgroundColor:[UIColor whiteColor]];
    [takePhotoButton addTarget:self action:@selector(takePhotoButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [takePhotoButton.layer setBorderColor:[UIColor blackColor].CGColor];
    [takePhotoButton.layer setBorderWidth:0.5];
    [takePhotoButton setAlpha:1.0];
    [libraryORTakePhotoView addSubview:takePhotoButton];
    
    selectedPhotoView = [[UIView alloc] init];
    [selectedPhotoView setFrame:CGRectMake(20, libraryORTakePhotoView.frame.origin.y - 130, self.frame.size.width - 40, 120)];
    //    [selectedPhotoView setCenter:CGPointMake(self.view.frame.size.width / 2, selectedPhotoView.frame.origin.y)];
    [selectedPhotoView setBackgroundColor:[UIColor whiteColor]];
    [selectedPhotoView setHidden:YES];
    [selectedPhotoView.layer setCornerRadius:5.0];
    [selectedPhotoView setClipsToBounds:YES];
    [selectedPhotoView setAlpha:1.0];
    [transparentImageView addSubview:selectedPhotoView];
    
    //    selectedImageView = [[UIImageView alloc] init];
    selectedBtn = [[UIButton alloc] init];
    [selectedBtn setFrame:CGRectMake(10, 10, 100, 100)];
    [selectedBtn setCenter:CGPointMake(selectedPhotoView.frame.size.width / 2, selectedPhotoView.frame.size.height / 2)];
    [selectedBtn setHidden:YES];
    //    [selectedBtn setBackgroundImage:[self imageWithColor:[UIColor clearColor]] forState:UIControlStateHighlighted];
    [selectedBtn addTarget:self action:@selector(imageButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [selectedPhotoView addSubview:selectedBtn];
    
    [self addSubview:transparentImageView];
    
    
}
- (void)imageButtonPressed:(UIButton *)sender
{
    
    if ([sender isSelected])
    {
        //        [[sender layer] setBorderColor:[UIColor whiteColor].CGColor];
        //        UIColor *buttonColor = [[UIColor greenColor] colorWithAlphaComponent:0.0f];;
        //        [sender setImage:[Methods imageWithColor:buttonColor size:sender.frame.size] forState:UIControlStateNormal];
        [sender setSelected:NO];
        [takePhotoButton setHidden:NO];
        [photoLibraryButton setHidden:NO];
        
        
        for (UIView *tickView in sender.subviews)
        {
            if (tickView.tag == 7)
            {
                [tickView removeFromSuperview];
            }
        }
        
    } else
    {
        [[sender layer] setBorderColor:[UIColor greenColor].CGColor];
        
        [sender setSelected:YES];
        [takePhotoButton setHidden:YES];
        [photoLibraryButton setHidden:YES];
        
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(70, selectedBtn.frame.size.height - 30, 30, 30)];
        imgView.layer.cornerRadius = imgView.frame.size.height/2;
        imgView.image = [UIImage imageNamed: @"icon_tick"];
        imgView.tag = 7;
        [sender addSubview:imgView];
    }
}
-(void)libraryPhotoButtonPressed
{
    UIViewController *vc = self.window.rootViewController;
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [vc presentViewController:picker animated:YES completion:NULL];
}
-(void)takePhotoButtonPressed
{
    UIViewController *vc = self.window.rootViewController;
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [vc presentViewController:picker animated:YES completion:NULL];
}
-(void)selectedImage
{
    
    UIAlertView *alert;
    if (self.ind == 0)
    {
        
        alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"Do you want to save this picture as profile photo?", nil) delegate:self cancelButtonTitle:@"No" otherButtonTitles: @"Yes",nil];
        [alert show];
        
    }
    else if (self.ind == 1)
    {
        
        alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"Do you want to save this picture as cover photo?", nil) delegate:self cancelButtonTitle:@"No" otherButtonTitles: @"Yes",nil];
        [alert show];
    }
    [self cancelButtonPressed];
    

}

-(void)cancelButtonPressed
{
    //    chosenImage = nil;
    
    
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         [self setFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, self.frame.size.height)];
                     }
                     completion:^(BOOL finished){
                         if (finished)
                             [self removeFromSuperview];
                     }];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        
        [self saveProfileOrCoverPhoto];
    }
}
-(void)saveProfileOrCoverPhoto
{
    
    UIViewController *vc =[[[[UIApplication sharedApplication]delegate] window] rootViewController];
    
    HUD = [[MBProgressHUD alloc] initWithView:vc.view];
    [vc.view addSubview:HUD];
    HUD.dimBackground = YES;
    
    defaults = [NSUserDefaults standardUserDefaults];
    NSData *userProfileData = [defaults objectForKey:[StaticData instance].kUserProfileData];
    UserProfile *userProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userProfileData];
    
    UIImage *scaleImage;
    
    
    NSString *apiUrl;
    if (self.ind == 0)
    {
        apiUrl = @"http://www.rightips.com/api/index.php?action=saveProfilePic";
        scaleImage = [self imageWithImage:self.chosenImage scaledToSize:CGSizeMake(100, 100)];
        
    }
    else
    {
        apiUrl = @"http://www.rightips.com/api/index.php?action=saveCoverPhoto";
        scaleImage = [self imageWithImage:self.chosenImage scaledToSize:CGSizeMake(280, 195)];
    }
    NSData *imageData = UIImagePNGRepresentation(scaleImage);
    
    //    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"getCategories"]];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setValue:userProfile.userIdEmail forKey:@"uid"];
    
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request;
    
    
    request = [client multipartFormRequestWithMethod:@"POST" path:apiUrl parameters:postParams constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFileData: imageData name:@"file" fileName:@"file.png" mimeType:@"image/png"];
    }];
    
    
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             [HUD hide:YES];
                                             [self uploadImage:JSON];
                                             
                                             
                                         }
                                                                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             [HUD hide:YES];
                                             
                                             [Methods showAlertView:@"" message:[error localizedDescription] buttonText:@"OK" tag:0 delegate:nil];
                                         }];
    [operation start];
    [HUD setLabelText:NSLocalizedString(@"Updating", nil)];
    ////    [HUD setDetailsLabelText:NSLocalizedString(@"Fetching data ...", nil)];
    [HUD show:YES];
}
-(void)uploadImage:(id)json
{
    NSArray *dataArray = [json objectForKey:@"data"];
    NSDictionary *dic = [dataArray objectAtIndex:0];
    NSString *imgUrl = [dic objectForKey:@"pic"];
    
    
    NSData *userProfileData = [defaults objectForKey:[StaticData instance].kUserProfileData];
    UserProfile *userProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userProfileData];
    
    if (self.ind == 0)
    {
        
        [userProfile setProfileImgUrl:imgUrl];
    }
    else if (self.ind == 1)
    {
        
        [userProfile setCoverImgUrl:imgUrl];
        
    }
    
    userProfileData = [NSKeyedArchiver archivedDataWithRootObject:userProfile];
    [defaults setObject:userProfileData forKey:[StaticData instance].kUserProfileData];
    [defaults synchronize];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"changeImage"
     object:nil];
    
    
}
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    self.chosenImage = info[UIImagePickerControllerEditedImage];
    
    if (self.chosenImage)
    {
        
        [selectedPhotoView setHidden:NO];
        [selectedBtn setHidden:NO];
        [selectedBtn setSelected:YES];
        //        [selectedBtn setBackgroundImage:chosenImage forState:UIControlStateNormal];
        [selectedBtn setImage:self.chosenImage forState:UIControlStateNormal];
        //        [selectedBtn setBackgroundImage:chosenImage forState:UIControlStateHighlighted];
        [takePhotoButton setHidden:YES];
        [photoLibraryButton setHidden:YES];
        [selectImageButton setHidden:NO];
        
        UIImageView *tickImageView = [[UIImageView alloc] initWithFrame:CGRectMake(70, selectedBtn.frame.size.height - 30, 30, 30)];
        tickImageView.layer.cornerRadius = tickImageView.frame.size.height/2;
        tickImageView.image = [UIImage imageNamed:@"icon_tick"];
        [tickImageView setTag:7];
        [selectedBtn addSubview:tickImageView];
    }
    
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
