//
//  AppDelegate.h
//  Rightips
//
//  Created by Abdul Mannan on 11/26/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <FacebookSDK/FacebookSDK.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate,BWBeaconManagerDelegate,CBCentralManagerDelegate> {
    CLLocationManager *locationManager;
    CLLocationManager *anotherLocationManager;
    BWBeaconManager *beaconManager;
    BWBeaconRegion *beaconRegion;
}

@property (strong, nonatomic) UIWindow *window;

//- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState)state error:(NSError *)error;

@end
