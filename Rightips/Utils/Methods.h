/**
 * Methods.h - common static utility methods
 * @author  Beaconwatcher iOS Team
 * @version 1.0
 */

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "NotificationData.h"
#import "AppDelegate.h"

static UIViewController *menuSelf;
static UIButton *menuToggleButton;

@interface Methods : NSObject

 

+ (BOOL)isPhone;
+ (BOOL)isPad;
+ (void)showAlertView:(NSString *)title message:(NSString *)message buttonText:(NSString *)buttonText tag:(NSInteger)tag delegate:(id)delegate;

+ (void)pushVCinNCwithName:(NSString *)name popTop:(BOOL)popTop;
+ (void)pushVCinNCwithObj:(UIViewController *)vc popTop:(BOOL)popTop;
+ (UIViewController *)getVCbyName:(NSString *)name;
+ (void)setupVC:(UIViewController *)mSelf;

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;

+ (void)hideEmptySeparatorsOfTableView:(UITableView *)tableView;

+ (void)setLeftPaddingToTextField:(UITextField *) tf paddingMargin:(float)paddingMargin;

+ (void)setupKeyboardForTextField:(UITextField *)tf;
+ (void)setupKeyboardForTextView:(UITextView *)tv;

+ (BOOL)validateTextFieldData:(UIViewController *)mSelf tf:(UITextField *)tf tfName:(NSString *)tfName shouldTrim:(BOOL)shouldTrim showMsg:(BOOL)showMsg;

+ (void)setImageInImageViewFromUrl:(UIImageView *)imageView urlStr:(NSString *)urlStr;

+ (NSString *)stringFromDate:(NSDate *)date dateFormat:(NSString *)dateFormat timeZone:(NSTimeZone *)timeZone;
+ (NSDate *)dateFromString:(NSString *)dateStr dateFormat:(NSString *)dateFormat timeZone:(NSTimeZone *)timeZone;

+ (void)saveUserLocationData;
+ (void)showMuteNotxActionSheet;

+ (void)generateNotificationWithMessage:(NSString *)message AndInfo:(NSDictionary*) userInfo ;

+ (void)generateNotification:(NSString *)message;
+ (void)fetchAllNotifications;
+(void)showReportActionSheet:(NSInteger)index;
+(void)showDeleteActionSheet:(NSInteger)index;
//+(void)showChangeImageActionSheet;

+ (NSString *)getDealDateStrFromNotx:(NotificationData *)nd;
+ (NotificationData *)parseNotificationData:(id)notxObj;


@end
