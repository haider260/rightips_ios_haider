//
//  DbHelper.h
//  Rightips
//
//  Created by Abdul Mannan on 1/5/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DbHelper : NSObject
{
    NSUserDefaults *defaults;
    
    /*sqlite3 *_database;
    NSFileManager *fileMgr;*/
}

- (NSMutableArray *)getAllNotxFromDB;
- (void)insertNotxToDB:(NotificationData *)nd;

@end
