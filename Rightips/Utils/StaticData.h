/**
 * StaticData.h - A singleton class to hold static data
 * @author  Beaconwatcher iOS Team
 * @version 1.0
 */

#import <Foundation/Foundation.h>

#import "UserLocation.h"

#define MUTE_NOTX_INTERVAL_MINUTES_0        0
#define MUTE_NOTX_INTERVAL_MINUTES_10       (10*60)
#define MUTE_NOTX_INTERVAL_HOURS_1          (1*60*60)
#define MUTE_NOTX_INTERVAL_HOURS_3          (3*60*60)
#define MUTE_NOTX_INTERVAL_HOURS_24          (24*60*60)

@interface StaticData : NSObject

@property (nonatomic, retain) NSNumber *kProviderEmail;
@property (nonatomic, retain) NSNumber *kProviderFacebook;
@property (nonatomic, retain) NSNumber *kProviderTwitter;
@property (nonatomic, retain) NSNumber *kProviderGooglePlus;

@property (nonatomic, retain) NSString *kHasRunAlready;
@property (nonatomic, retain) NSString *kLoggedIn;
@property (nonatomic, retain) NSString *kUserProfileData;
@property (nonatomic, retain) NSString *kSelCatsIds;
@property (nonatomic, retain) NSString *kNotsJson;
@property (nonatomic, retain) NSString *kCatGroupsJson;

@property (nonatomic, retain) NSString *baseUrl;
@property (nonatomic, retain) NSString *apiKey;
@property (nonatomic, assign) NSInteger serverTimeout;
@property (nonatomic, retain) NSString *noInternetMessage;

@property (nonatomic, retain) NSArray *fbPermissions;

@property (nonatomic, retain) NSString *bwUdid;

@property (nonatomic, retain) UserLocation *userLocation;

@property (nonatomic, retain) NSMutableArray *notifications;
@property (nonatomic, retain) NSMutableArray *categoryGroups;

+ (StaticData *)instance;
+ (void)initStaticData;

@end
