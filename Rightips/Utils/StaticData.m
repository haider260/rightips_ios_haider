/**
 * StaticData.m - A singleton class to hold static data
 * @author  Beaconwatcher iOS Team
 * @version 1.0
 */

#import "StaticData.h"

@implementation StaticData

@synthesize kProviderEmail, kProviderFacebook, kProviderTwitter, kProviderGooglePlus;
@synthesize kHasRunAlready, kLoggedIn, kUserProfileData, kSelCatsIds, kNotsJson, kCatGroupsJson;
@synthesize baseUrl, apiKey, serverTimeout, noInternetMessage;
@synthesize fbPermissions;
@synthesize bwUdid;
@synthesize userLocation;
@synthesize notifications;
@synthesize categoryGroups;

static StaticData *_instance;

- (id) init
{
	if (self = [super init])
    {
        
	}
	return self;
}

+ (StaticData *)instance
{
	if (!_instance)
    {
		_instance = [[StaticData alloc] init];
	}
	return _instance;
}

+ (void)initStaticData
{
    
    [StaticData instance].kProviderEmail = [NSNumber numberWithInt:0];
    [StaticData instance].kProviderFacebook = [NSNumber numberWithInt:1];
    [StaticData instance].kProviderTwitter = [NSNumber numberWithInt:2];
    [StaticData instance].kProviderGooglePlus = [NSNumber numberWithInt:3];
    
    [StaticData instance].kHasRunAlready = @"kHasRunAlready";
    [StaticData instance].kLoggedIn = @"kLoggedIn";
    [StaticData instance].kUserProfileData = @"kUserProfileData";
    [StaticData instance].kSelCatsIds = @"kSelCatsIds";
    [StaticData instance].kNotsJson = @"kNotsJson";
    [StaticData instance].kCatGroupsJson = @"kCatGroupsJson";
    
    [StaticData instance].baseUrl = @"http://www.rightips.com/api/index.php?action=";
    [StaticData instance].apiKey = @"MTRfX1JpZ2h0VGlwcw==";
    [StaticData instance].serverTimeout = 30 * 1000;
    [StaticData instance].noInternetMessage = @"Please check your internt connection";
    
    [StaticData instance].fbPermissions = @[@"public_profile", @"user_about_me", @"email"];
    
    [StaticData instance].bwUdid = @"5144A35F-387A-7AC0-B254-DFB1381C9DFF";
    
    [StaticData instance].userLocation = [[UserLocation alloc] init];
    
    [StaticData instance].notifications = [NSMutableArray array];
    [StaticData instance].categoryGroups = [NSMutableArray array];
}

@end
