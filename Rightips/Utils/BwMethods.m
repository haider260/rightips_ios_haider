//
//  BwMethods.m
//  Rightips
//
//  Created by Abdul Mannan on 1/6/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "BwMethods.h"

@implementation BwMethods

static NSString *lastTrackedBeaconUMM;
static CLProximity lastTrackedZone;


+ (NSMutableArray *)getNotificationsFromRangedBeacons:(NSArray *)beacons
{
    NotificationData *nd = nil;
    NSMutableArray *beaconsArrayWithData = [[NSMutableArray alloc] init];
    
    NSMutableArray *validBeacons = [NSMutableArray array];
    for (unsigned int index = 0; index < [beacons count]; index++) {
        BWBeacon *beacon = [beacons objectAtIndex:index];
        if (beacon.accuracy >= 0.0) //exlude accuracy=-1.0
            [validBeacons addObject:beacon];
    }
    // Sorting is not needed as beacons are already sorted based on accuracy.
    
    for (unsigned int index = 0; index < [validBeacons count]; index++)
    {
        BWBeacon *beacon = [validBeacons objectAtIndex:index];
        nd = [self getNotificationDataByBeacon:beacon];
        if (nd != nil)
        {
            [beaconsArrayWithData addObject:nd];
        }
    }
    return beaconsArrayWithData;
}


+ (NotificationData *)getNotificationDataFromRangedBeacons:(NSArray *)beacons
{
    BWBeacon *selBeacon;
    NSString *beaconUMM = @"";
    NotificationData *nd = nil;
    
    NSMutableArray *validBeacons = [NSMutableArray array];
    for (unsigned int index = 0; index < [beacons count]; index++)
    {
        BWBeacon *beacon = [beacons objectAtIndex:index];
        if (beacon.accuracy >= 0.0) //exlude accuracy=-1.0
            [validBeacons addObject:beacon];
    }
    
    // Sorting is not needed as beacons are already sorted based on accuracy.
    
    for (unsigned int index = 0; index < [validBeacons count]; index++)
    {
        BWBeacon *beacon = [validBeacons objectAtIndex:index];
        nd = [self getNotificationDataByBeacon:beacon];
        if (nd != nil)
        {
            selBeacon = beacon;
            beaconUMM = [self getBeaconUMM:beacon];
            break;
        }
    }
    
    if (nd != nil) {
        if ([lastTrackedBeaconUMM isEqualToString:beaconUMM] == NO) {
            lastTrackedBeaconUMM = beaconUMM;
            lastTrackedZone = selBeacon.proximity;
            return nd;
        } else { //Same beacon
            if (lastTrackedZone != selBeacon.proximity) {
                lastTrackedZone = selBeacon.proximity;
                return nd;
            }
        }
    }
    
    return nil;
}

- (NSArray *)sortBeacons:(NSArray *)beacons
{
    return [beacons sortedArrayUsingComparator:^NSComparisonResult(BWBeacon *beacon1, BWBeacon *beacon2)
    {
        if (beacon1.accuracy > beacon2.accuracy)
            return (NSComparisonResult)NSOrderedDescending;
        if (beacon1.accuracy < beacon2.accuracy)
            return (NSComparisonResult)NSOrderedAscending;
        return (NSComparisonResult)NSOrderedSame;
    }];
}

+ (NotificationData *)getNotificationDataByBeacon:(BWBeacon *)beacon
{
 //   NSLog(@"beacon=%@",beacon);
    NSMutableArray *bwns = [StaticData instance].notifications;
    for (unsigned int index = 0; index < [bwns count]; index++) {
        BeaconWithNotification *bwn = [bwns objectAtIndex:index];
//        NSLog(@"bwn=%@",bwn.major);
        if ([bwn.uuid caseInsensitiveCompare:beacon.proximityUUID.UUIDString] == NSOrderedSame &&
            [bwn.major isEqualToString:beacon.major.stringValue] &&
            [bwn.minor isEqualToString:beacon.minor.stringValue])
        {
            for (unsigned int j = 0; j < [bwn.notifications count]; j++) {
                NotificationData *nd = [bwn.notifications objectAtIndex:j];
                [nd setSource:@"Beacon"];
                NSLog(@"%@",nd.title);
                if ([nd.zone intValue] == beacon.proximity)
                    return nd;
            } break;
        }
    }
    return nil;
}

+ (NSString *)getBeaconUMM:(BWBeacon *)beacon
{
    NSString *ummStr = @"";
    ummStr = [ummStr stringByAppendingString:beacon.proximityUUID.UUIDString];
    ummStr = [ummStr stringByAppendingString:@"_"];
    ummStr = [ummStr stringByAppendingString:beacon.major.stringValue];
    ummStr = [ummStr stringByAppendingString:@"_"];
    ummStr = [ummStr stringByAppendingString:beacon.minor.stringValue];
    return ummStr;
}

@end
