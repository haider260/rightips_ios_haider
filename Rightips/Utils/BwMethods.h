//
//  BwMethods.h
//  Rightips
//
//  Created by Abdul Mannan on 1/6/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NotificationData.h"

@interface BwMethods : NSObject

+ (NSMutableArray *)getNotificationsFromRangedBeacons:(NSArray *)beacons;
+ (NotificationData *)getNotificationDataFromRangedBeacons:(NSArray *)beacons;

@end
