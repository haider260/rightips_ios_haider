/**
 * Methods.m - common static utility methods
 * @author  Beaconwatcher iOS Team
 * @version 1.0
 */

#import "Methods.h"
#import "ImageCaptureView.h"

@implementation Methods


+ (BOOL)isPhone {
    
    if([[[UIDevice currentDevice] model] isEqualToString:@"iPhone"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
//#ifdef UI_USER_INTERFACE_IDIOM
//    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
//#else
//    return NO;
//#endif
}

+ (BOOL)isPad
{
#ifdef UI_USER_INTERFACE_IDIOM
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
#else
    return NO;
#endif
}

+ (void)showAlertView:(NSString *)title message:(NSString *)message buttonText:(NSString *)buttonText
                  tag:(NSInteger)tag delegate:(id)delegate
{
    UIAlertView *aView = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:buttonText
                                          otherButtonTitles:nil];
    aView.tag = tag;
    aView.delegate = delegate;
    [aView show];
}

+ (void)pushVCinNCwithName:(NSString *)vcName popTop:(BOOL)popTop
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navController = (UINavigationController *) appDelegate.window.rootViewController;
    UIViewController *topVC = [navController visibleViewController];
    if ([NSStringFromClass(topVC.class) isEqualToString:vcName]) return;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:vcName];
  //  if (popTop) [navController popViewControllerAnimated:NO];
    [navController pushViewController:vc animated:YES];
    
    if (popTop)
    {
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: navController.viewControllers];
        
        for (int i = 0; i < navigationArray.count; i++)
        {
            if ([[navigationArray objectAtIndex:i] isKindOfClass:[topVC class]])
            {
                [navigationArray removeObjectAtIndex:i];
                break;
            }
        }
        [navController setViewControllers:navigationArray animated:NO];
    }
}

+ (void)pushVCinNCwithObj:(UIViewController *)vcObj popTop:(BOOL)popTop {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navController = (UINavigationController *) appDelegate.window.rootViewController;
    UIViewController *topVC = [navController visibleViewController];
    if ([NSStringFromClass(topVC.class) isEqualToString:NSStringFromClass(vcObj.class)]) return;
 //   if (popTop) [navController popViewControllerAnimated:NO];
    [navController pushViewController:vcObj animated:YES];
    
    if (popTop)
    {
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: navController.viewControllers];
        
        for (int i = 0; i < navigationArray.count; i++)
        {
            if ([[navigationArray objectAtIndex:i] isKindOfClass:[topVC class]])
            {
                [navigationArray removeObjectAtIndex:i];
                break;
            }
        }
        [navController setViewControllers:navigationArray animated:NO];
    }
}

+ (UIViewController *)getVCbyName:(NSString *)name
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:name];
}

//currently not being used in project
+ (void)setupVC:(UIViewController *)mSelf
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        //mSelf.wantsFullScreenLayout = NO;
        mSelf.edgesForExtendedLayout = UIRectEdgeNone;
        [mSelf setNeedsStatusBarAppearanceUpdate];
        
        [mSelf.tabBarController.tabBar setTranslucent:NO];
    }
}

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
+ (void)hideEmptySeparatorsOfTableView:(UITableView *)tableView
{
    UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
    v.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:v];
}

//currently not being used in project
+ (void)setLeftPaddingToTextField:(UITextField *) tf paddingMargin:(float)paddingMargin
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, paddingMargin, tf.frame.size.height)];
    tf.leftView = paddingView;
    tf.leftViewMode = UITextFieldViewModeAlways;
}

+ (void)setupKeyboardForTextField:(UITextField *)tf
{
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 44);
    NSMutableArray *items = [[NSMutableArray alloc] init];
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:tf action:@selector(resignFirstResponder)];
    [items addObject:button];
    [toolbar setItems:items animated:NO];
    [tf setInputAccessoryView:toolbar];
}

+ (void)setupKeyboardForTextView:(UITextView *)tv
{
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 44);
    NSMutableArray *items = [[NSMutableArray alloc] init];
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:tv action:@selector(resignFirstResponder)];
    [items addObject:button];
    [toolbar setItems:items animated:NO];
    [tv setInputAccessoryView:toolbar];
}

+ (BOOL)validateTextFieldData:(UIViewController *)mSelf tf:(UITextField *)tf tfName:(NSString *)tfName shouldTrim:(BOOL)shouldTrim showMsg:(BOOL)showMsg
{
    NSString *tfStr = tf.text;
    
    if (shouldTrim)
    {
        tfStr = [tfStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    
    if ([tfStr isEqualToString:@""] && showMsg)
    {
//        NSString *message = [NSString stringWithFormat:@"%@ text field is empty!", tfName];
        NSString *message = [NSString stringWithFormat:NSLocalizedString(@"Please fill all fields.", nil)];
        //[mSelf.view makeToast:message duration:3.0f position:@"bottom"];
        [Methods showAlertView:@"" message:message buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
        return NO;
    }
    
    return YES;
}

+ (void)setImageInImageViewFromUrl:(UIImageView *)imageView urlStr:(NSString *)urlStr
{

    __block UIActivityIndicatorView *activityIndicator;
    __weak UIImageView *weakImageView = imageView;
    [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite]];
    //activityIndicator.center = weakImageView.center;
    CGRect aiFrame = activityIndicator.frame;
    aiFrame.origin.x = (weakImageView.frame.size.width/2)-(activityIndicator.frame.size.width/2);
    aiFrame.origin.y = (weakImageView.frame.size.height/2)-(activityIndicator.frame.size.height/2);
    activityIndicator.frame = aiFrame;
    NSURL *imgUrl = [NSURL URLWithString:urlStr];
//    dispatch_async(dispatch_get_main_queue(), ^{
        // code here
        [imageView sd_setImageWithURL:imgUrl placeholderImage:nil options:SDWebImageProgressiveDownload
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 if (!activityIndicator)
                                 {
                                     
                                     [activityIndicator startAnimating];
                                 }
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
    {
                                if (error) {
                                    weakImageView.image = [UIImage imageNamed:@"ImageNA"];
                                }
                                [activityIndicator removeFromSuperview];
                                activityIndicator = nil;
                            }];
//    });

    
}

+ (NSString *)stringFromDate:(NSDate *)date dateFormat:(NSString *)dateFormat timeZone:(NSTimeZone *)timeZone
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:dateFormat];
    if (timeZone) [formatter setTimeZone:timeZone];
    return [formatter stringFromDate:date];
}

+ (NSDate *)dateFromString:(NSString *)dateStr dateFormat:(NSString *)dateFormat timeZone:(NSTimeZone *)timeZone
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:dateFormat];
    if (timeZone) [formatter setTimeZone:timeZone];
    return [formatter dateFromString:dateStr];
}

+ (void)saveUserLocationData
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *userLocationData = [NSKeyedArchiver archivedDataWithRootObject:[StaticData instance].userLocation];
    [defaults setObject:userLocationData forKey:@"userLocationData"];
    [defaults synchronize];
}

+ (void)showMuteNotxActionSheet
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Mute notifications for" delegate:nil cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    [actionSheet addButtonWithTitle:@"1 hour"];
    [actionSheet addButtonWithTitle:@"3 hour"];
    [actionSheet addButtonWithTitle:@"8 hour"];
    if ([defaults doubleForKey:@"muteEndTime"] && ([defaults doubleForKey:@"muteEndTime"] != 0))
    {
        [actionSheet addButtonWithTitle:@"Unmute"];
    }
    [actionSheet setCancelButtonIndex:[actionSheet addButtonWithTitle:@"Cancel"]];
    
    [UIActionSheetWithBlock showActionSheet:actionSheet withCallback:^(NSInteger buttonIndex) {
        int hoursSelected;
        if (buttonIndex == 0) hoursSelected = 1 ;
        else if (buttonIndex == 1) hoursSelected = 3;
        else if (buttonIndex == 2) hoursSelected = 8;
        else if (actionSheet.numberOfButtons == 4)
        {
            if (buttonIndex == 3) return; //cancel button
        }
        else
        {
            if (buttonIndex == 3) hoursSelected = 0;
            else if (buttonIndex == 4) return; //cancel button
        }
        
      //  NSTimeInterval muteEndTime = [[NSDate date] timeIntervalSince1970] + (hoursSelected*60*60);
        NSTimeInterval muteEndTime = [[NSDate date] timeIntervalSinceNow] + (hoursSelected*60*60);

        if (muteEndTime > 0)
        {
            [defaults setDouble:muteEndTime forKey:@"muteEndTime"];
        }
        else
        {
            [defaults setDouble:0 forKey:@"muteEndTime"];
        }
        [defaults synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeMuteImage" object:nil];

    }];
}
//+(void)showChangeImageActionSheet
//{
//    UIWindow *mainWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
//    UIViewController *vc = mainWindow.rootViewController;
//    
//    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Edit Photo", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Change Profile Photo", nil),NSLocalizedString(@"Change Cover Photo", nil),nil];
//    
//    [UIActionSheetWithBlock showActionSheet:popup withCallback:^(NSInteger buttonIndex)
//     {
//         if (buttonIndex != 2)
//         {
//             
//            ImageCaptureView *view = [[ImageCaptureView alloc] initWithFrame:CGRectMake(0, vc.view.frame.size.height, vc.view.frame.size.width, vc.view.frame.size.height)];
//             [view setInd:buttonIndex];
//             
//             
//             
//             [UIView animateWithDuration:0.5
//                                   delay:0.1
//                                 options: UIViewAnimationCurveEaseIn
//                              animations:^{
//                                  [view setFrame:vc.view.frame];
//                              }
//                              completion:^(BOOL finished){
//                              }];
//             [vc.view addSubview:view];
//             
//         }
//         }];
//    
//}
+(void)showDeleteActionSheet:(NSInteger)index
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Delete", nil),nil];
    
    [UIActionSheetWithBlock showActionSheet:popup withCallback:^(NSInteger buttonIndex)
     {
         if (buttonIndex == 0)
         {
             [self deleteComment:index];
         }
         
     }];
    
}
+(void)deleteComment:(NSInteger)commentId
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] init];
    [HUD setLabelText:@""];
    [HUD show:YES];
    
    UIWindow *mainWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
    UIViewController *vc = mainWindow.rootViewController;
    [vc.view addSubview:HUD];
    [HUD show:YES];
    
    UserProfile *currentUserProfile = [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData] ];
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setObject:currentUserProfile.userIdEmail forKey:@"uid"];
    [postParams setObject:[NSString stringWithFormat:@"%ld",commentId] forKey:@"com_id"];
    
    
    NSString *apiURL=@"http://www.rightips.com/api/index.php?action=deleteComment";
    NSURL *url = [NSURL URLWithString:apiURL];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiURL parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [HUD hide:YES];
        if([[JSON objectForKey:@"status"] integerValue] == 1)
        {
            
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"deleteComment"
             object:nil];
            
            
            
        }
        else if([JSON objectForKey:@"message"])
        {
            [HUD hide:YES];
            [[[UIAlertView alloc] initWithTitle:@"Error" message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
        }
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             [HUD hide:YES];
                                             [Methods showAlertView:@"" message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
                                         }];
    [operation start];
}


+(void)showReportActionSheet:(NSInteger)index
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Report", nil),nil];
    
    [UIActionSheetWithBlock showActionSheet:popup withCallback:^(NSInteger buttonIndex)
    {
        if (buttonIndex == 0)
        {
        [self reportReview:index];
        }
        
    }];
    
}
+(void)reportReview:(NSInteger)reviewId
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Why are you reporting this comment?", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Spam or Scam", nil),NSLocalizedString(@"Abusive Content", nil),nil];
    [UIActionSheetWithBlock showActionSheet:popup withCallback:^(NSInteger buttonIndex)
     {
         if (buttonIndex == 0)
         {
             [self spamORAbusiveContent:1 reviewId:reviewId];
         }
         else if (buttonIndex == 1)
         {
             [self spamORAbusiveContent:2 reviewId:reviewId];
         }
         
         
     }];
}
+(void)spamORAbusiveContent:(int)statusId reviewId:(NSInteger)reviewId
{
    MBProgressHUD *HUD = [[MBProgressHUD alloc] init];
    [HUD setLabelText:@""];
    [HUD show:YES];
    
    UIWindow *mainWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
    UIViewController *vc = mainWindow.rootViewController;
    [vc.view addSubview:HUD];
    [HUD show:YES];
    
    UserProfile *currentUserProfile = [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData] ];
    //    NSString *apiURL=[NSString stringWithFormat:@"http://www.rightips.com/api/index.php?action=insertComment&uid=%@&msg_id=%@&comment=%@",userProfile.userIdEmail,self.selNotxData.msgId,commentText];
    NSString *apiURL=@"http://www.rightips.com/api/index.php?action=report";
    
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setObject:currentUserProfile.userIdEmail forKey:@"uid"];
    [postParams setObject:@"review" forKey:@"type"];
    [postParams setObject:[NSString stringWithFormat:@"%d",statusId] forKey:@"status_id"];
    [postParams setObject:[NSString stringWithFormat:@"%ld",reviewId] forKey:@"id"];
    //    [postParams setObject:imageData forKey:@"file"];
    
    NSURL *url = [NSURL URLWithString:apiURL];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request;
    request = [client requestWithMethod:@"POST" path:apiURL parameters:postParams];
    
    
    
    
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [HUD hide:YES];
        if([[JSON objectForKey:@"status"] integerValue] == 1)
        {
            
            
            [[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"Thanks", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
            
        }
        else if([JSON objectForKey:@"message"])
        {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
        }
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             [HUD hide:YES];
                                             [Methods showAlertView:@"" message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
                                         }];
    [operation start];
}

+ (void)generateNotification:(NSString *)message
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSTimeInterval muteEndTime = [defaults doubleForKey:@"muteEndTime"];
    NSTimeInterval recvNotxTime = [defaults doubleForKey:@"recvNotxTime"];
    NSTimeInterval muteNotxInterval = [defaults doubleForKey:@"muteNotxInterval"];
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    
    if (muteEndTime > currentTime) return;
    
    if ((currentTime - recvNotxTime) < muteNotxInterval) return;
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate date];
    localNotification.alertBody = message;
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    if (muteNotxInterval == MUTE_NOTX_INTERVAL_MINUTES_0) {
        muteNotxInterval = MUTE_NOTX_INTERVAL_MINUTES_10;
    } else if (muteNotxInterval == MUTE_NOTX_INTERVAL_MINUTES_10) {
        muteNotxInterval = MUTE_NOTX_INTERVAL_HOURS_1;
    } else if (muteNotxInterval == MUTE_NOTX_INTERVAL_HOURS_1) {
        muteNotxInterval = MUTE_NOTX_INTERVAL_HOURS_3;
    } else if (muteNotxInterval == MUTE_NOTX_INTERVAL_HOURS_3) {
        muteNotxInterval = MUTE_NOTX_INTERVAL_HOURS_24;
    } else {
        muteNotxInterval = MUTE_NOTX_INTERVAL_MINUTES_0;
    }
    
    recvNotxTime = currentTime;
    [defaults setDouble:recvNotxTime forKey:@"recvNotxTime"];
    [defaults setDouble:muteNotxInterval forKey:@"muteNotxInterval"];
    [defaults synchronize];
}

+ (void)generateNotificationWithMessage:(NSString *)message AndInfo:(NSDictionary*) userInfo
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSTimeInterval muteEndTime = [defaults doubleForKey:@"muteEndTime"];
    NSTimeInterval recvNotxTime = [defaults doubleForKey:@"recvNotxTime"];
    NSTimeInterval muteNotxInterval = [defaults doubleForKey:@"muteNotxInterval"];
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    
    if (muteEndTime > currentTime) return;
    
    if ((currentTime - recvNotxTime) < muteNotxInterval) return;
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate date];
    localNotification.alertBody = message;
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.userInfo=userInfo;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    if (muteNotxInterval == MUTE_NOTX_INTERVAL_MINUTES_0) {
        muteNotxInterval = MUTE_NOTX_INTERVAL_MINUTES_10;
    } else if (muteNotxInterval == MUTE_NOTX_INTERVAL_MINUTES_10) {
        muteNotxInterval = MUTE_NOTX_INTERVAL_HOURS_1;
    } else if (muteNotxInterval == MUTE_NOTX_INTERVAL_HOURS_1) {
        muteNotxInterval = MUTE_NOTX_INTERVAL_HOURS_3;
    } else if (muteNotxInterval == MUTE_NOTX_INTERVAL_HOURS_3) {
        muteNotxInterval = MUTE_NOTX_INTERVAL_HOURS_24;
    } else {
        muteNotxInterval = MUTE_NOTX_INTERVAL_MINUTES_0;
    }
    
    recvNotxTime = currentTime;
    [defaults setDouble:recvNotxTime forKey:@"recvNotxTime"];
    [defaults setDouble:muteNotxInterval forKey:@"muteNotxInterval"];
    [defaults synchronize];
}

+ (void)fetchAllNotifications
{

    static BOOL isCalling = NO;
    if (isCalling)
        return;
    isCalling = YES;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *catIdsArray = [defaults objectForKey:[StaticData instance].kSelCatsIds];
    NSString *catIdsStr = @"";
    if (catIdsArray)
    catIdsStr = [catIdsArray componentsJoinedByString:@","];
    
    NSString *apiUrl = [NSString stringWithFormat:@"%@", @"http://api.beaconwatcher.com/index.php?action="];
    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"getAllNotifications"]];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setValue:[StaticData instance].apiKey forKey:@"key"];
    [postParams setValue:catIdsStr forKey:@"cat_id"];
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:apiUrl parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        isCalling = NO;
        [self handleAllNotificationsSuccess:JSON];
        NSLog(@"notxs al downloaded");
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        isCalling = NO;
        [Methods handleAllNotificationsFailure];
    }];
    [operation start];
}

+ (void)handleAllNotificationsSuccess:(id)JSON
{
    [[StaticData instance].notifications removeAllObjects];
    
    NSString *status = [JSON objectForKey:@"status"];
    if ([status intValue] == 1) {
        NSArray *dataArray = [JSON objectForKey:@"data"];
        for (unsigned int index = 0; index < [dataArray count]; index++) {
            id bwnObj = [dataArray objectAtIndex:index];
            BeaconWithNotification *bwn = [Methods parseBeaconWithNotification:bwnObj];
            [[StaticData instance].notifications addObject:bwn];
        }
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JSON options:0 error:&error];
        if (jsonData)
        {
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:jsonString forKey:[StaticData instance].kNotsJson];
            [defaults synchronize];
        }
    }
}

+ (void)handleAllNotificationsFailure
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *jsonString = [defaults objectForKey:[StaticData instance].kNotsJson];
    if (jsonString) {
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        if (jsonData) {
            NSError *error;
            id JSON = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
            if (JSON) [Methods handleAllNotificationsSuccess:JSON];
        }
    } else {
        [Methods fetchAllNotifications];
    }
}

+ (BeaconWithNotification *)parseBeaconWithNotification:(id)bwnObj
{
    BeaconWithNotification *bwn = [[BeaconWithNotification alloc] init];
    bwn.uuid = [bwnObj objectForKey:@"msb_uuid"];
    bwn.major = [bwnObj objectForKey:@"msb_major"];
    bwn.minor = [bwnObj objectForKey:@"msb_minor"];
    bwn.macAddress = [bwnObj objectForKey:@"msb_mac_address"];
    id notArray = [bwnObj objectForKey:@"notifications"];
    for (unsigned int j = 0; j < [notArray count]; j++) {
        id notxObj = [notArray objectAtIndex:j];
        NotificationData *nd = [Methods parseNotificationData:notxObj];
        [bwn.notifications addObject:nd];
    }
    return bwn;
}
+ (NotificationData *)parseNotificationData:(id)notxObj
{
    NotificationData *nd = [[NotificationData alloc] init];
    [nd setTagList:[notxObj objectForKey:@"tags"]];
    nd.distance = [[notxObj objectForKey:@"distance"] floatValue];
    nd.msgId=[notxObj objectForKey:@"msg_id"];
    nd.zone = [notxObj objectForKey:@"zone"];
    nd.notxId = [notxObj objectForKey:@"nt_id"];
    nd.title = [notxObj objectForKey:@"nt_title"];
    nd.detail = [notxObj objectForKey:@"nt_details"];
    nd.notxTemplate = [notxObj objectForKey:@"template"];
    nd.siteId = [notxObj objectForKey:@"site_id"];
    nd.siteName = [notxObj objectForKey:@"site_name"];
    nd.siteAddress = [notxObj objectForKey:@"site_address"];
    nd.sitePhone = [notxObj objectForKey:@"site_phone"];
    nd.notxStartDate = [notxObj objectForKey:@"nt_startdate"];
    nd.notxEndDate = [notxObj objectForKey:@"nt_enddate"];
    nd.discount = [notxObj objectForKey:@"nt_discount"];
    nd.dealStartDate = [notxObj objectForKey:@"nt_deal_start"];
    nd.dealEndDate = [notxObj objectForKey:@"nt_deal_end"];
    //nd.creationDate = [[notObj objectForKey:@""] longValue];
    if([notxObj objectForKey:@"nt_url"])
    {
        nd.notxUrl = [notxObj objectForKey:@"nt_url"];
    }
    NSArray *imgsArrObj = [notxObj objectForKey:@"images"];
    
    for (id imgObj in imgsArrObj) [nd.images addObject:[imgObj objectForKey:@"url"]];
    
    
    nd.likes=[notxObj objectForKey:@"likes"];
    nd.noOfLikes=[notxObj objectForKey:@"total_likes"];
    
    if ([notxObj objectForKey:@"site_long"])
    {
        nd.longitude = [notxObj objectForKey:@"site_long"];
    }
    if ([notxObj objectForKey:@"site_lat"])
    {
        nd.latitude = [notxObj objectForKey:@"site_lat"];
    }
    if ([notxObj objectForKey:@"cat_id"])
    {
        nd.catId = [notxObj objectForKey:@"cat_id"];
    }
    NSArray *socialLinksArray = [notxObj objectForKey:@"social_links"];
    for (NSDictionary *linkDict in socialLinksArray)
    {
        if ([[linkDict objectForKey:@"name"] isEqualToString:@"facebook"]) {
            nd.fbUrl = [linkDict objectForKey:@"url"];
        }
        else if ([[linkDict objectForKey:@"name"] isEqualToString:@"instagram"]) {
            nd.instagramUrl = [linkDict objectForKey:@"url"];
        }
        else if ([[linkDict objectForKey:@"name"] isEqualToString:@"googleplus"]) {
            nd.googlePlusUrl = [linkDict objectForKey:@"url"];
        }
        else if ([[linkDict objectForKey:@"name"] isEqualToString:@"mail"]) {
            nd.mailUrl = [linkDict objectForKey:@"url"];
        }
    }
    if ([notxObj objectForKey:@"nt_website_url"])
    {
        nd.catLink = [notxObj objectForKey:@"nt_website_url"];
    }

    if ([notxObj objectForKey:@"site_total_notifications"])
    {
        nd.totalSiteNotifications = [notxObj objectForKey:@"site_total_notifications"];
    }
    
    return nd;
}


+ (NSString *)getDealDateStrFromNotx:(NotificationData *)nd
{
    NSTimeZone *dtz = [NSTimeZone defaultTimeZone];
    
    NSDate *dealStartDate = [Methods dateFromString:nd.dealStartDate dateFormat:@"yyyy-MM-dd HH:mm:ss" timeZone:dtz];
    NSDate *dealEndDate = [Methods dateFromString:nd.dealEndDate dateFormat:@"yyyy-MM-dd HH:mm:ss" timeZone:dtz];
    
    //NSString *dealStartTimeStr = [Methods stringFromDate:dealStartDate dateFormat:@"EEE MMM d" timeZone:dtz];
    //NSString *dealEndTimeStr = [Methods stringFromDate:dealEndDate dateFormat:@"EEE MMM d" timeZone:dtz];
    NSString *dealStartTimeStr = [Methods stringFromDate:dealStartDate dateFormat:@"dd/MM/yyyy" timeZone:dtz];
    NSString *dealEndTimeStr = [Methods stringFromDate:dealEndDate dateFormat:@"dd/MM/yyyy" timeZone:dtz];
    
    if (dealStartTimeStr != nil && dealEndTimeStr != nil)
        return [NSString stringWithFormat:@"%@ - %@", dealStartTimeStr, dealEndTimeStr];
    else
        return @"";
}


@end
