//
//  DbHelper.m
//  Rightips
//
//  Created by Abdul Mannan on 1/5/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "DbHelper.h"
#import "NotificationData.h"

@implementation DbHelper

- (id)init
{
    if ((self = [super init]))
    {
        defaults = [NSUserDefaults standardUserDefaults];
    }
    return self;
}

- (NSMutableArray *)getAllNotxFromDB
{
    NSData *notxsHistoryData = [defaults objectForKey:@"notxsHistoryData"];
    if (notxsHistoryData) {
        NSMutableArray *notxsHistory = [NSKeyedUnarchiver unarchiveObjectWithData:notxsHistoryData];
        if (!notxsHistory) return [NSMutableArray array];
        else return notxsHistory;
    } else return [NSMutableArray array];
}

- (void)insertNotxToDB:(NotificationData *)nd
{
    NSData *notxsHistoryData = [defaults objectForKey:@"notxsHistoryData"];
    NSMutableArray *notxsHistory = nil;
    if (notxsHistoryData)
        notxsHistory = [NSKeyedUnarchiver unarchiveObjectWithData:notxsHistoryData];
    
    if (!notxsHistory)
        notxsHistory = [NSMutableArray array];
    
    for (NotificationData *ndd in notxsHistory)
    {
        if ([ndd.notxId isEqualToString:nd.notxId])
        {
            [notxsHistory removeObject:ndd]; break;
        }
    }
    
    nd.creationDate = [[NSDate date] timeIntervalSince1970];
    [notxsHistory insertObject:nd atIndex:0];
    
    notxsHistoryData = [NSKeyedArchiver archivedDataWithRootObject:notxsHistory];
    [defaults setObject:notxsHistoryData forKey:@"notxsHistoryData"];
    [defaults synchronize];
}



/*
static DbHelper *_database;
 
- (id)init {
    if ((self = [super init])) {
        fileMgr = [NSFileManager defaultManager];
        NSString *dbPathInBundle = [[NSBundle mainBundle] pathForResource:@"history" ofType:@"db"];
        NSString *homeDir = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *dbPathInStorage = [homeDir stringByAppendingPathComponent:@"history.db"];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:dbPathInStorage];
        if (!fileExists) [self copyDbFromBundlePath:dbPathInBundle toStoragePath:dbPathInStorage];
        if (sqlite3_open([dbPathInStorage UTF8String], &_database) != SQLITE_OK) {
            NSLog(@"Failed to open database!");
        }
    }
    return self;
}

- (void)dealloc {
    sqlite3_close(_database);
}

- (void)copyDbFromBundlePath:(NSString *)dbPathInBundle toStoragePath:(NSString *)toStoragePath {
    NSError *err=nil;
    [fileMgr removeItemAtPath:toStoragePath error:&err];
    if(![fileMgr copyItemAtPath:dbPathInBundle toPath:toStoragePath error:&err]) {
        NSLog(@"Failed to copy database!");
    }
}

- (NSMutableArray *)getAllNotxFromDB {
    NSMutableArray *retval = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT note_id, title, detail, zone, template, status, site_name, site_address, start_date, end_date, creation_date, images FROM notifications ORDER BY creation_date DESC";
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            NotificationData *nd = [[NotificationData alloc] init];
            char *retCharPtr;
            
            retCharPtr = (char *) sqlite3_column_text(statement, 0);
            if(retCharPtr != NULL)
                nd.notxId = [[NSString alloc] initWithUTF8String:retCharPtr];
            
            retCharPtr = (char *) sqlite3_column_text(statement, 1);
            if(retCharPtr != NULL)
                nd.title = [[NSString alloc] initWithUTF8String:retCharPtr];
            
            retCharPtr = (char *) sqlite3_column_text(statement, 2);
            if(retCharPtr != NULL)
                nd.detail = [[NSString alloc] initWithUTF8String:retCharPtr];
            
            retCharPtr = (char *) sqlite3_column_text(statement, 3);
            if(retCharPtr != NULL)
                nd.zone = [[NSString alloc] initWithUTF8String:retCharPtr];
            
            retCharPtr = (char *) sqlite3_column_text(statement, 4);
            if(retCharPtr != NULL)
                nd.notxTemplate = [[NSString alloc] initWithUTF8String:retCharPtr];
            
            retCharPtr = (char *) sqlite3_column_text(statement, 5);
            if(retCharPtr != NULL)
                nd.statusId = [[NSString alloc] initWithUTF8String:retCharPtr];
            
            retCharPtr = (char *) sqlite3_column_text(statement, 6);
            if(retCharPtr != NULL)
                nd.siteName = [[NSString alloc] initWithUTF8String:retCharPtr];
            
            retCharPtr = (char *) sqlite3_column_text(statement, 7);
            if(retCharPtr != NULL)
                nd.siteAddress = [[NSString alloc] initWithUTF8String:retCharPtr];
            
            retCharPtr = (char *) sqlite3_column_text(statement, 8);
            if(retCharPtr != NULL)
                nd.notxStartDate = [[NSString alloc] initWithUTF8String:retCharPtr];
            
            retCharPtr = (char *) sqlite3_column_text(statement, 9);
            if(retCharPtr != NULL)
                nd.notxEndDate = [[NSString alloc] initWithUTF8String:retCharPtr];
            
            retCharPtr = (char *) sqlite3_column_text(statement, 10);
            if(retCharPtr != NULL) {
                NSString *cdStr = [[NSString alloc] initWithUTF8String:retCharPtr];
                nd.creationDate = [cdStr doubleValue];
            }
            
            retCharPtr = (char *) sqlite3_column_text(statement, 11);
            if(retCharPtr != NULL) {
                nd.images = [[[[NSString alloc] initWithUTF8String:retCharPtr]
                              componentsSeparatedByString:@","] mutableCopy];
            }
            
            [retval addObject:nd];
        }
        sqlite3_finalize(statement);
    }
    return retval;
}

- (void)insertNotxToDB:(NotificationData *)nd {
    unsigned long long creationDate = (unsigned long long)[[NSDate date] timeIntervalSince1970]*1000;
    //NSString *query = [NSString stringWithFormat:@"INSERT INTO notifications (id, note_id, title, detail, zone, template, status, site_name, site_address, start_date, end_date, creation_date, images) VALUES (null, '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', %llu, '%@')", nd.notxId, nd.title, nd.detail, nd.zone, nd.notxTemplate, nd.statusId, nd.siteName, nd.siteAddress, nd.startDate, nd.endDate, creationDate, [nd.images componentsJoinedByString:@","]];
    NSString *query = [NSString stringWithFormat:@"INSERT INTO notifications (id, note_id, title, detail, zone, template, status, site_name, site_address, start_date, end_date, creation_date, images) VALUES (null, \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\", %llu, \"%@\")", nd.notxId, nd.title, nd.detail, nd.zone, nd.notxTemplate, nd.statusId, nd.siteName, nd.siteAddress, nd.notxStartDate, nd.notxEndDate, creationDate, [nd.images componentsJoinedByString:@","]];
    
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        int code = sqlite3_step(statement);
        if (code == SQLITE_DONE) NSLog(@"Added");
        else NSLog(@"Failed to Add");
    }
    sqlite3_finalize(statement);
}*/

@end
