//
//  WeatherView.m
//  Rightips
//
//  Created by Shahbaz Ali on 8/16/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "MyWeatherView.h"
#import "OWMWeatherAPI.h"
#import "FontHeader.h"


@implementation MyWeatherView

// Setup weather api
OWMWeatherAPI *weatherAPI;
NSArray *dayNames;
UserLocation *userLocation;
NSMutableDictionary *weatherDic;


-(void)initialize
{
    dayNames=@[@"SUN", @"MON", @"TUE", @"WED", @"THU", @"FRI", @"SAT"];
    
    weatherDic = [[NSMutableDictionary alloc] init];
    [weatherDic setObject:@"\uEB28" forKey:@"200"];
    [weatherDic setObject:@"\uEB29" forKey:@"201"];
    [weatherDic setObject:@"\uEB2A" forKey:@"202"];
    [weatherDic setObject:@"\uEB32" forKey:@"210"];
    [weatherDic setObject:@"\uEB33" forKey:@"211"];
    [weatherDic setObject:@"\uEB34" forKey:@"212"];
    [weatherDic setObject:@"\uEB3D" forKey:@"221"];
    [weatherDic setObject:@"\uEB46" forKey:@"230"];
    [weatherDic setObject:@"\uEB47" forKey:@"231"];
    [weatherDic setObject:@"\uEB48" forKey:@"232"];
    
    [weatherDic setObject:@"\uEB8C" forKey:@"300"];
    [weatherDic setObject:@"\uEB8D" forKey:@"301"];
    [weatherDic setObject:@"\uEB8E" forKey:@"302"];
    [weatherDic setObject:@"\uEB96" forKey:@"310"];
    [weatherDic setObject:@"\uEB97" forKey:@"311"];
    [weatherDic setObject:@"\uEB98" forKey:@"312"];
    [weatherDic setObject:@"\uEB99" forKey:@"313"];
    [weatherDic setObject:@"\uEB9A" forKey:@"314"];
    [weatherDic setObject:@"\uEBA1" forKey:@"321"];
    
    [weatherDic setObject:@"\uEC54" forKey:@"500"];
    [weatherDic setObject:@"\uEC55" forKey:@"501"];
    [weatherDic setObject:@"\uEC56" forKey:@"502"];
    [weatherDic setObject:@"\uEC57" forKey:@"503"];
    [weatherDic setObject:@"\uEC58" forKey:@"504"];
    [weatherDic setObject:@"\uEC5F" forKey:@"511"];
    [weatherDic setObject:@"\uEC68" forKey:@"520"];
    [weatherDic setObject:@"\uEC69" forKey:@"521"];
    [weatherDic setObject:@"\uEC6A" forKey:@"522"];
    [weatherDic setObject:@"\uEC73" forKey:@"531"];
    
    [weatherDic setObject:@"\uECB8" forKey:@"600"];
    [weatherDic setObject:@"\uECB9" forKey:@"601"];
    [weatherDic setObject:@"\uECBA" forKey:@"602"];
    [weatherDic setObject:@"\uECC3" forKey:@"611"];
    [weatherDic setObject:@"\uECC4" forKey:@"612"];
    [weatherDic setObject:@"\uECC7" forKey:@"615"];
    [weatherDic setObject:@"\uECC8" forKey:@"616"];
    [weatherDic setObject:@"\uECCC" forKey:@"620"];
    [weatherDic setObject:@"\uECCD" forKey:@"621"];
    [weatherDic setObject:@"\uECCE" forKey:@"622"];
    
    [weatherDic setObject:@"\uED1D" forKey:@"701"];
    [weatherDic setObject:@"\uED27" forKey:@"711"];
    [weatherDic setObject:@"\uED31" forKey:@"721"];
    [weatherDic setObject:@"\uED3B" forKey:@"731"];
    [weatherDic setObject:@"\uED45" forKey:@"741"];
    [weatherDic setObject:@"\uED4F" forKey:@"751"];
    [weatherDic setObject:@"\uED59" forKey:@"761"];
    [weatherDic setObject:@"\uED5A" forKey:@"762"];
    [weatherDic setObject:@"\uED63" forKey:@"771"];
    [weatherDic setObject:@"\uED6D" forKey:@"781"];
    
    [weatherDic setObject:@"\uED80" forKey:@"800"];
    [weatherDic setObject:@"\uED81" forKey:@"801"];
    [weatherDic setObject:@"\uED82" forKey:@"802"];
    [weatherDic setObject:@"\uED83" forKey:@"803"];
    [weatherDic setObject:@"\uED84" forKey:@"804"];
    
    [weatherDic setObject:@"\uEDE4" forKey:@"900"];
    [weatherDic setObject:@"\uEDE5" forKey:@"901"];
    [weatherDic setObject:@"\uEDE6" forKey:@"902"];
    [weatherDic setObject:@"\uEDE7" forKey:@"903"];
    [weatherDic setObject:@"\uEDE8" forKey:@"904"];
    [weatherDic setObject:@"\uEDE9" forKey:@"905"];
    [weatherDic setObject:@"\uEDEA" forKey:@"906"];
    
    [weatherDic setObject:@"\uEE16" forKey:@"950"];
    [weatherDic setObject:@"\uEE18" forKey:@"952"];
    [weatherDic setObject:@"\uEE19" forKey:@"953"];
    [weatherDic setObject:@"\uEE1A" forKey:@"954"];
    [weatherDic setObject:@"\uEE1B" forKey:@"955"];
    [weatherDic setObject:@"\uEE1C" forKey:@"956"];
    [weatherDic setObject:@"\uEE1D" forKey:@"957"];
    [weatherDic setObject:@"\uEE1E" forKey:@"958"];
    [weatherDic setObject:@"\uEE1F" forKey:@"959"];
    [weatherDic setObject:@"\uEE20" forKey:@"960"];
    [weatherDic setObject:@"\uEE21" forKey:@"961"];
    [weatherDic setObject:@"\uEE22" forKey:@"962"];
    
    
    
    
    }

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self == nil) return nil;
    [self initalizeSubviews];
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self == nil) return nil;
    [self initalizeSubviews];
    return self;
}

-(void)initalizeSubviews
{
    //Load the contents of the nib
    NSString *nibName = NSStringFromClass([self class]);
    UINib *nib = [UINib nibWithNibName:nibName bundle:nil];
    [nib instantiateWithOwner:self options:nil];
    //Add the view loaded from the nib into self.
    [self addSubview:self.container];
 
    
    [self initialize];
    
    [self.loader setHidesWhenStopped:YES];
    
//    [self updateCurrentWeather];
}





#pragma mark - Fetching And Populating Notification Data


-(void) updateCurrentWeather
{
    UserLocation *userLocation = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"userLocationSelectedData"]];
    if (!userLocation.selectedLocation)
    {
        userLocation = [StaticData instance].userLocation;
    }
    
    [self.loader startAnimating];
        if (userLocation.selectedLocation && ![userLocation.selectedCityName isEqualToString:@""])
        {
    
//            YWeatherUtils* yweatherUtils = [YWeatherUtils getInstance];
//            [yweatherUtils setMAfterRecieveDataDelegate: self];
//            [yweatherUtils queryYahooWeather:userLocation.selectedCityName];
    
    if(weatherAPI==nil) [self initWeatherAPI];
//    [weatherAPI currentWeatherByCityName:userLocation.selectedCityName withCallback:^(NSError *error, NSDictionary *result) {
        [weatherAPI currentWeatherByCoordinate:userLocation.selectedLocation.coordinate withCallback:^(NSError *error, NSDictionary *result) {
                

        if (error)
        {
            // handle the error
            return;
        }
        
        // The data is ready
        
        [self.loader stopAnimating];
        
        
//        NSString *cityName = result[@"name"];
        NSNumber *currentTemp = result[@"main"][@"temp"];
        
        NSInteger value = [currentTemp integerValue];
   
        
        
        NSString *str=[NSString stringWithFormat:@"%ld%@", (long)value, @"\u00B0C"];
        
        
        [self.lblTemperature1 setText:str];
        [self updateWeatherForecast];
        
    }];
        }
    
            
    
}
//- (void)gotWeatherInfo:(WeatherInfo *)weatherInfo
//{
//    // Add your code here
//}



-(void) updateWeatherForecast
{
    [self.loader setHidden:NO];
    [self.loader startAnimating];
    UserLocation *userLocation = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"userLocationSelectedData"]];
    if (!userLocation.selectedLocation)
    {
        userLocation = [StaticData instance].userLocation;
    }
    if (userLocation.selectedLocation && ![userLocation.selectedCityName isEqualToString:@""])
    {
    if(weatherAPI==nil) [self initWeatherAPI];
    [self.loader stopAnimating];
//    [weatherAPI dailyForecastWeatherByCityName:userLocation.selectedCityName withCount:7 andCallback:
//     ^(NSError *error, NSDictionary *result) {
        [weatherAPI dailyForecastWeatherByCoordinate:userLocation.selectedLocation.coordinate withCount:7 andCallback:^(NSError *error, NSDictionary *result) {
        
        if (error)
        {
            // handle the error
            return;
        }
        
        // The data is ready
        
//        [self.loader stopAnimating];
        
         NSArray *days = result[@"list"];
         
//         NSString *str=@"forecast:";
         
         float xx=self.lblTemperature1.bounds.size.width+self.lblTemperature1.frame.origin.x + 10;
         float ww=self.container.bounds.size.width;
         float hh=self.container.bounds.size.height;
         
         ww=ww-xx;
         
         float dayWidth= (ww - 10) / 7;
         
         
         
         NSCalendar* calender = [NSCalendar currentCalendar];
         
         
         for (id dayData in days)
         {
             NSDate *dt=dayData[@"dt"];
             
             
             NSDateComponents* component = [calender components:NSCalendarUnitWeekday fromDate:dt];
             
             NSString *dayName = dayNames[[component weekday]-1];
//             NSLog(@"%@",dayName);
             
             /*UILabel *lbl1 = [[UILabel alloc] init];
             [lbl1 setFrame:CGRectMake(xx,5,dayWidth, hh-10)];
             lbl1.backgroundColor=[UIColor clearColor];
             lbl1.textColor=[UIColor whiteColor];
             
             [lbl1 setFont:[UIFont systemFontOfSize:8]];
             
             lbl1.userInteractionEnabled=YES;
             [self.container addSubview:lbl1];
             lbl1.text= dayName;*/
             
             
             NSArray *nibView=[[NSBundle mainBundle] loadNibNamed:@"WeatherDay" owner:self options:nil];
             UIView *v=[nibView objectAtIndex:0];
             [self.container addSubview:v];
             v.frame=CGRectMake(xx, 5, dayWidth, hh-20);
             xx=xx+dayWidth;
             
             UILabel *lbl=[v viewWithTag:1];
             lbl.text=dayName;
             
             
               UILabel *lbl2=[v viewWithTag:2];
             lbl2.font = [UIFont fontWithName:@"owf-regular" size:24];
//              lbl2.font = [UIFont fontWithName:@"Pe-icon-7-weather" size:24];
             
           
             //int wid=[dayData[@"weather"] [@"id"] intValue];
             NSDictionary *wdic=dayData[@"weather"][0];
             
             NSNumber *num=wdic[@"id"];
             
             int wid=[num intValue];
                                               
//             wid = 200;
             
//             NSString *iconStr=@"\ue60c";
              NSString *iconStr=[weatherDic objectForKey:[NSString stringWithFormat:@"%d",wid]];
//             [iconStr insertString:@"u" atIndex:1];
             /*
             if(wid==800)
             {
                 iconStr=@"\ue60c";
             }
             else if(wid==741)
             {
                 iconStr=@"\ue625";
             }
             else if(wid > 800 && wid<805)
             {
                 iconStr=@"\ue636";
             }
             else if(wid > 499 && wid<532)
             {
                 iconStr=@"\ue613";
             }
             else if(wid > 599 && wid<623)
             {
                 iconStr=@"\ue610";
             }
             else if(wid > 199 && wid<233)
             {
                 iconStr=@"\ue600";
                 
             }
             else if(wid > 299 && wid<322)
             {
                 iconStr=@"\ue628";
             }*/
             lbl2.text= iconStr;
             }
             [self.loader stopAnimating];
        }];
    }

}



-(void) initWeatherAPI
{
//    weatherAPI= [[OWMWeatherAPI alloc] initWithAPIKey:@"fe728b7b35e73fb245c16301532d8e8d"];
    weatherAPI= [[OWMWeatherAPI alloc] initWithAPIKey:@"9db6ac4af9cfc9a1d25ea221ba7a118f"];
    [weatherAPI setTemperatureFormat:kOWMTempCelcius];
}




@end
