//
//  SearchPlaceVC.m
//  Rightips
//
//  Created by Abdul Mannan on 1/18/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "SearchPlaceVC.h"
#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"
#import "SPGooglePlacesPlaceDetailQuery.h"


@interface SearchPlaceVC () <UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate> {
    NSArray *searchResultPlaces;
    SPGooglePlacesAutocompleteQuery *searchQuery;
    BOOL shouldBeginEditing;
}

@end

@implementation SearchPlaceVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    searchQuery = [[SPGooglePlacesAutocompleteQuery alloc] init];
    searchQuery.radius = 100.0;
    shouldBeginEditing = YES;
    
    self.searchDisplayController.searchBar.placeholder = @"Search or Address";

    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //Sending current screen to Piwic Tracker
   // [[PiwikTracker sharedInstance] sendView:@"Search"];
    
    
    [self.searchDisplayController setActive:NO];
    [self.searchDisplayController.searchBar becomeFirstResponder];
    [self.searchDisplayController.searchBar setText:self.searchText];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [searchResultPlaces count] + 1;
}

- (SPGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSIndexPath *)indexPath
{
    return [searchResultPlaces objectAtIndex:indexPath.row];
}

- (SPGooglePlacesAutocompletePlace *)placeAtIndex:(NSInteger)index {
    return [searchResultPlaces objectAtIndex:index];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"SPGooglePlacesAutocompleteCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"ArialMT" size:16.0];
    if (indexPath.row == 0)
        cell.textLabel.text = @"Current Location";
    else
        cell.textLabel.text = [self placeAtIndex:indexPath.row-1].name;
    
    return cell;
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)dismissSearchControllerWhileStayingActive
{
    // Animate out the table view.
    NSTimeInterval animationDuration = 0.3;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    self.searchDisplayController.searchResultsTableView.alpha = 0.0;
    [UIView commitAnimations];
    
    [self.searchDisplayController.searchBar setShowsCancelButton:NO animated:YES];
    [self.searchDisplayController.searchBar resignFirstResponder];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        [self currentLocationButtonPressed:nil];
        return;
    }
    
    SPGooglePlacesAutocompletePlace *place = [self placeAtIndex:indexPath.row-1];//[self placeAtIndexPath:indexPath];
    
    SPGooglePlacesPlaceDetailQuery *query = [[SPGooglePlacesPlaceDetailQuery alloc] init];
    query.reference = place.placeId;
    [query fetchPlaceDetail:^(NSDictionary *placeDictionary, NSError *error)
    {
        if (error)
        {
            SPPresentAlertViewWithErrorAndTitle(error, NSLocalizedString(@"Could not map selected Place", nil));
        }
        else if (placeDictionary)
        {
            NSDictionary *location = [[placeDictionary objectForKey:@"geometry"] objectForKey:@"location"];
            double lng = [[location objectForKey:@"lng"] doubleValue];
            double lat = [[location objectForKey:@"lat"] doubleValue];
            CLLocation *loc = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
            
            
            
            UserLocation *userLocation = [StaticData instance].userLocation;
            userLocation.userLocationMode = UserLocationModeSelected;
            userLocation.selectedLocation = loc;
            userLocation.selectedCityName = [placeDictionary objectForKey:@"name"];
            [StaticData instance].userLocation = userLocation;
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSData *userLocationData = [NSKeyedArchiver archivedDataWithRootObject:[StaticData instance].userLocation];
            [defaults setObject:userLocationData forKey:@"userLocationSelectedData"];
         
            [self cancelButtonPressed:nil];

            [self dismissSearchControllerWhileStayingActive];
            [self.searchDisplayController.searchResultsTableView deselectRowAtIndexPath:indexPath animated:NO];
            
        }
    }];
}

#pragma mark -
#pragma mark UISearchDisplayDelegate

- (void)handleSearchForSearchString:(NSString *)searchString {
    searchQuery.input = searchString;
    [searchQuery fetchPlaces:^(NSArray *places, NSError *error) {
        if (error) {
            SPPresentAlertViewWithErrorAndTitle(error, NSLocalizedString(@"Failed to connect to server, check your internet connection.", nil));
        } else {
            searchResultPlaces = [places copy];
            [self.searchDisplayController.searchResultsTableView reloadData];
        }
    }];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self handleSearchForSearchString:searchString];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView
{
//    CGRect frame = tableView.frame;
//    frame.origin.y += 50;
//    tableView.frame = frame;
}

#pragma mark -
#pragma mark UISearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (![searchBar isFirstResponder])
    {
        // User tapped the 'clear' button.
        shouldBeginEditing = NO;
        [self.searchDisplayController setActive:NO];
    }
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    if (shouldBeginEditing)
    {
        // Animate in the table view.
        NSTimeInterval animationDuration = 0.3;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:animationDuration];
        self.searchDisplayController.searchResultsTableView.alpha = 1.0;
        [UIView commitAnimations];
        
        [self.searchDisplayController.searchBar setShowsCancelButton:YES animated:YES];
    }
    BOOL boolToReturn = shouldBeginEditing;
    shouldBeginEditing = YES;
    return boolToReturn;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self cancelButtonPressed:nil];
}

- (IBAction)currentLocationButtonPressed:(id)sender
{
    UserLocation *userLocation = [StaticData instance].userLocation;
    userLocation.userLocationMode = UserLocationModeCurrent;
    userLocation.selectedLocation = userLocation.currentLocation;
    userLocation.selectedCityName = userLocation.currentCityName;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *userLocationData = [NSKeyedArchiver archivedDataWithRootObject:[StaticData instance].userLocation];
    [defaults setObject:userLocationData forKey:@"userLocationSelectedData"];
    
    
    [self cancelButtonPressed:nil];
}

- (IBAction)cancelButtonPressed:(id)sender
{
    [self.searchDisplayController setActive:NO];
    [Methods pushVCinNCwithName:@"HomeVC" popTop:YES];
}

@end
