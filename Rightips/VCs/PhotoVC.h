//
//  PhotoVC.h
//  Rightips
//
//  Created by Mac on 04/01/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface PhotoVC : UIViewController <UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UIActionSheetDelegate>
{
    
    IBOutlet UITableView *commentTableView;
    
    IBOutlet UILabel *recentActivityLabel;
    
    IBOutlet UICollectionView *collectionVu;
    
    IBOutlet UIButton *gridButton;
    
    IBOutlet UIButton *verticalButton;
    
    IBOutlet UIButton *locationButton;
    
    IBOutlet UIButton *groupButton;
    
}
@property(nonatomic,strong)UserProfile *userProfile;

@end
