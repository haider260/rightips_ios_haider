//
//  WeatherView.m
//  Rightips
//
//  Created by Shahbaz Ali on 8/16/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "MyWeatherView.h"
#import "OWMWeatherAPI.h"

@implementation MyWeatherView

// Setup weather api
OWMWeatherAPI *weatherAPI;
NSArray *dayNames;



-(void)initialize{
    dayNames=@[@"SUN", @"MON", @"TUE", @"WED", @"THU", @"FRI", @"SAT"];
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self == nil) return nil;
    [self initalizeSubviews];
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self == nil) return nil;
    [self initalizeSubviews];
    return self;
}

-(void)initalizeSubviews
{
    //Load the contents of the nib
    NSString *nibName = NSStringFromClass([self class]);
    UINib *nib = [UINib nibWithNibName:nibName bundle:nil];
    [nib instantiateWithOwner:self options:nil];
    //Add the view loaded from the nib into self.
    [self addSubview:self.container];
 
    
    [self initialize];
    
    [self.loader setHidesWhenStopped:YES];
    [self updateCurrentWeather];
}





#pragma mark - Fetching And Populating Notification Data

-(void) updateCurrentWeather{
    if(weatherAPI==nil) [self initWeatherAPI];
    [weatherAPI currentWeatherByCityName:@"Lahore" withCallback:^(NSError *error, NSDictionary *result) {
        if (error) {
            // handle the error
            return;
        }
        
        // The data is ready
        
        [self.loader stopAnimating];
        
        
        NSString *cityName = result[@"name"];
        NSNumber *currentTemp = result[@"main"][@"temp"];
        
        NSInteger value = [currentTemp integerValue];
   
        
        
        NSString *str=[NSString stringWithFormat:@"%ld%@", (long)value, @"\u00B0C"];
        
        [self.lblTemperature1 setText:str];
        [self updateWeatherForecast];
        
    }];
    
    
}



-(void) updateWeatherForecast{
    if(weatherAPI==nil) [self initWeatherAPI];
    [self.loader stopAnimating];
    [weatherAPI dailyForecastWeatherByCityName:@"Lahore" withCount:7 andCallback:
     ^(NSError *error, NSDictionary *result) {
        if (error) {
            // handle the error
            return;
        }
        
        // The data is ready
        
        [self.loader stopAnimating];
        
         NSArray *days = result[@"list"];
         
         NSString *str=@"forecast:";
         
         float xx=self.lblTemperature1.bounds.size.width+10;
         float ww=self.container.bounds.size.width;
         float hh=self.container.bounds.size.height;
         
         ww=ww-xx;
         
         float dayWidth=(ww/7);
         
         
         
         NSCalendar* calender = [NSCalendar currentCalendar];
         
         
         for (id dayData in days) {
             NSDate *dt=dayData[@"dt"];
             
             
             NSDateComponents* component = [calender components:NSCalendarUnitWeekday fromDate:dt];
             
             NSString *dayName = dayNames[[component weekday]-1];
             
             /*UILabel *lbl1 = [[UILabel alloc] init];
             [lbl1 setFrame:CGRectMake(xx,5,dayWidth, hh-10)];
             lbl1.backgroundColor=[UIColor clearColor];
             lbl1.textColor=[UIColor whiteColor];
             
             [lbl1 setFont:[UIFont systemFontOfSize:8]];
             
             lbl1.userInteractionEnabled=YES;
             [self.container addSubview:lbl1];
             lbl1.text= dayName;*/
             
             
             NSArray *nibView=[[NSBundle mainBundle] loadNibNamed:@"WeatherDay" owner:self options:nil];
             UIView *v=[nibView objectAtIndex:0];
             [self.container addSubview:v];
             v.frame=CGRectMake(xx, 5, dayWidth, hh-20);
             xx=xx+dayWidth+5;
             
             UILabel *lbl=[v viewWithTag:1];
             lbl.text=dayName;
             
             
             
             
             
             
             
             //NSNumber *id=dayData[@"weather"] [@"id"];
             
          }
        }];

}



-(void) initWeatherAPI{
    weatherAPI= [[OWMWeatherAPI alloc] initWithAPIKey:@"fe728b7b35e73fb245c16301532d8e8d"];
    [weatherAPI setTemperatureFormat:kOWMTempCelcius];
}




@end
