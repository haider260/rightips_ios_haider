//
//  NotxCommentImageVC.m
//  Rightips
//
//  Created by Mac on 19/11/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "NotxCommentImageVC.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface NotxCommentImageVC ()
{
     MBProgressHUD *HUD;
}

@end

@implementation NotxCommentImageVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [commentImageView setImage:self.img];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    
}
- (IBAction)downlaodImage:(UIButton *)sender
{
//    NSString * documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//    
//
//    //Save Image to Directory
//    [self saveImage:self.img withFileName:@"My Image" ofType:@"png" inDirectory:documentsDirectoryPath];
    
    [HUD show:YES];
//    UIImageWriteToSavedPhotosAlbum(self.img, nil, nil, nil);
//    [HUD hide:YES];


    UIImageWriteToSavedPhotosAlbum(self.img, nil, nil, nil);
    [HUD hide:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"The picture is saved in photos album", nil) delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
}
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo: (void *)contextInfo
{
    if (!error)
    {
        [HUD hide:NO];
    }
    
}
-(void) saveImage:(UIImage *)image withFileName:(NSString *)imageName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath {
    if ([[extension lowercaseString] isEqualToString:@"png"]) {
        [UIImagePNGRepresentation(image) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", imageName, @"png"]] options:NSAtomicWrite error:nil];
    } else if ([[extension lowercaseString] isEqualToString:@"jpg"] || [[extension lowercaseString] isEqualToString:@"jpeg"]) {
        [UIImageJPEGRepresentation(image, 1.0) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", imageName, @"jpg"]] options:NSAtomicWrite error:nil];
    } else {
        NSLog(@"Image Save Failed\nExtension: (%@) is not recognized, use (PNG/JPG)", extension);
    }
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}


- (IBAction)backToVC:(UIButton *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navController = (UINavigationController *) appDelegate.window.rootViewController;
    [navController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
