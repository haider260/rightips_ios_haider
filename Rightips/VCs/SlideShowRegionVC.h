//
//  SlideShowRegionVC.h
//  Rightips
//
//  Created by Mac on 10/09/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideShowRegionVC : UIViewController <UIScrollViewDelegate>

@property (nonatomic,strong) NSArray *regionImagesList;
@property (nonatomic) NSInteger tag;

@end
