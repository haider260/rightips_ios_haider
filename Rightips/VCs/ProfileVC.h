//
//  ProfileVC.h
//  Rightips
//
//  Created by Mac on 30/12/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"


@interface ProfileVC : UIViewController <UIImagePickerControllerDelegate,UIActionSheetDelegate,UIAlertViewDelegate>
{
    
    
    IBOutlet UILabel *followerLabel;
    
    IBOutlet UILabel *likesLabel;
    
    
    IBOutlet UILabel *photosLabel;
    
    
    IBOutlet UILabel *reviewsLabel;
    IBOutlet UIView *followerView;
    
    
    IBOutlet UIView *likesView;
    
    IBOutlet UIView *photosView;
    
    IBOutlet UIView *reviewsView;
    
    IBOutlet AsyncImageView *profileImageView;
    IBOutlet UILabel *userNameLabel;
    
    
    
    IBOutlet UILabel *subscribeDateLabel;
   
    
    IBOutlet UIButton *settingsButton;
    
    IBOutlet AsyncImageView *coverImageView;
    
    IBOutlet UIView *settingView;
    
    IBOutlet UIButton *cameraButton;
    
    
    IBOutlet UIView *categoryView;
    
    
    IBOutlet UIButton *followButton;
    
    IBOutlet UIButton *unFollowButton;
    
}
@property(nonatomic,strong)UserProfile *userProfile;

@end
