//
//  SearchResultVC.m
//  Rightips
//
//  Created by Mac on 16/03/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import "SearchResultVC.h"
#import "SearchCell.h"
#import "ImageCaptureView.h"
#import "NotxDetailsVC.h"
#import "HomeVC.h"


@interface SearchResultVC ()
{
     MBProgressHUD *HUD;
    SlideMenu *slideMenu;
    SearchMenu *searchMenu;
    DbHelper *dbHelper;
    
    UIImageView *imgView;
}

@end

@implementation SearchResultVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    
    dbHelper = [[DbHelper alloc] init];
    slideMenu = [[SlideMenu alloc] init];
    [self.view addSubview:slideMenu];
//    [self.view bringSubviewToFront:menuToggleButton];
    
    
    UISwipeGestureRecognizer *swipeLeftMenu = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToLeftWithGestureRecognizer:)];
    swipeLeftMenu.direction = UISwipeGestureRecognizerDirectionLeft;
    [slideMenu addGestureRecognizer:swipeLeftMenu];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noResult) name:@"noResult" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchHide) name:@"SearchHide" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeImage) name:@"changeImage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraButtonTapped) name:@"TapOnCameraButton" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeMuteImage:) name:@"changeMuteImage" object:nil];
    [self changeMuteImage:nil];
    
    imgView= [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [imgView setImage:[UIImage imageNamed:@"TransparentImage"]];
    [imgView setHidden:YES];
    [imgView setUserInteractionEnabled:YES];
    [self.view addSubview:imgView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideToUpperWithGestureRecognizer:)];
    [tap setDelegate:self];
    [imgView addGestureRecognizer:tap];
    
    searchMenu = [[SearchMenu alloc] init];
    [self.view addSubview:searchMenu];
    [searchMenu setUpMenu];
    
//    [self getSearchResults];
}
-(void)slideToUpperWithGestureRecognizer:(UITapGestureRecognizer *)gestureRecognizer
{
    //    UIImageView *imgView =  (UIImageView *)gestureRecognizer.view;
    [imgView setHidden:YES];
    [searchMenu showHideMenu];
    
}

-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer
{
    [slideMenu showHideMenu];
}
- (IBAction)slideMenuButtonPressed:(UIButton *)sender
{
    [slideMenu showHideMenu];
}
- (IBAction)muteButtonPressed:(UIButton *)sender
{
    [Methods showMuteNotxActionSheet];
}
- (IBAction)searchButtonPressed:(UIButton *)sender
{
//    [searchResultTableView setAlpha:0.5];
    [imgView setHidden:NO];
    [searchMenu showHideMenu];
}
-(void)searchHide
{
//    [searchResultTableView setAlpha:1.0];
    
    [imgView setHidden:YES];
    [searchResultTableView reloadData];
    
}
-(void)noResult
{
    [imgView setHidden:YES];
}
-(void)changeImage
{
    [slideMenu setupMenu];
}
-(void)cameraButtonTapped
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Edit Photo", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Change Profile Photo", nil),NSLocalizedString(@"Change Cover Photo", nil),nil];
    //    [popup setTag:3];
    [popup showInView:self.view];
    
    
    
}
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
        
        ImageCaptureView *view = [[ImageCaptureView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        [view setInd:buttonIndex];
    
        
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             [view setFrame:self.view.frame];
                         }
                         completion:^(BOOL finished){
                         }];
        [self.view addSubview:view];
    
}
- (void)changeMuteImage:(NSNotification *)notification
{
    if ([[NSUserDefaults standardUserDefaults] doubleForKey:@"muteEndTime"] && ([[NSUserDefaults standardUserDefaults] doubleForKey:@"muteEndTime"] != 0))
    {
        [muteButton setImage:[UIImage imageNamed:@"BtnMute"] forState:UIControlStateNormal];
    }
    else
    {
        [muteButton setImage:[UIImage imageNamed:@"BtnUnmuted"] forState:UIControlStateNormal];
    }
}
//-(void)getSearchResults
//{
//    [HUD show:YES];
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    UserLocation *userLocation = [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"userLocationSelectedData"]];
//    NSString *latStr = [NSString stringWithFormat:@"%f", userLocation.selectedLocation.coordinate.latitude];
//    NSString *lngStr = [NSString stringWithFormat:@"%f", userLocation.selectedLocation.coordinate.longitude];
//    NSString *apiUrl =@"http://api.beaconwatcher.com/index.php?action=search";
//    
//    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
//    
//    [postParams setValue:@"MTRfX1JpZ2h0VGlwcw==" forKey:@"key"];
//    [postParams setValue:self.searchString forKey:@"keyword"];
//    [postParams setValue:latStr forKey:@"lat"];
//    [postParams setValue:lngStr forKey:@"lng"];
//    
//    
//    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
//    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
//    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiUrl parameters:postParams];
//    [request setTimeoutInterval:[StaticData instance].serverTimeout];
//    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
//    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
//    {
//     [HUD hide:YES];
//     if([[JSON objectForKey:@"status"] integerValue] == 1)
//     {
//         [self fetchResultData:JSON];
//         
//     }
//     
//     else if([JSON objectForKey:@"message"])
//     {
//         [[[UIAlertView alloc] initWithTitle:nil message:@"There is currently no venue in this city" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:@"Go Back", nil] show];
//     }
//     
//     
// }
//    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
// {
//     [HUD hide:YES];
//     [[[UIAlertView alloc] initWithTitle:@"Error" message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
// }];
//    [operation start];
//}
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (buttonIndex == 1)
//    {
//
//    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
//        HomeVC *vc = (HomeVC *)[Methods getVCbyName:@"HomeVC"];
//        [Methods pushVCinNCwithObj:vc popTop:YES];
//    }
//}
//-(void)fetchResultData:(id)json
//{
//    [searchResults removeAllObjects];
//    NSArray *dataArray = [json objectForKey:@"data"];
////    for (NSDictionary *dic in dataArray)
////    {
////        SearchData *searchData = [[SearchData alloc] init];
////        [searchData setTitleName:[dic objectForKey:@"site_name"]];
////        [searchData setAddress:[dic objectForKey:@"site_address"]];
////        [searchData setImgUrl:[[[dic objectForKey:@"images"] objectAtIndex:0] objectForKey:@"url"]];
////        [searchData setDistance:[[dic objectForKey:@"distance"] floatValue]];
////        [searchData setTagList:[dic objectForKey:@"tags"]];
////        [searchResults addObject:searchData];
////        
////        
////    }
//    
//    for (unsigned int i = 0; i < [dataArray count]; i++)
//    {
//        id notxObj = [dataArray objectAtIndex:i];
//        NotificationData *nd = [self parseNotificationData:notxObj];
//        [searchResults addObject:nd];
//    }
//    [searchResultTableView reloadData];
//    
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.searchResultsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SearchCell *customCell;
    
    customCell = [[[NSBundle mainBundle] loadNibNamed:@"SearchCell" owner:nil options:nil] objectAtIndex: 0];
    NotificationData *nd = [self.searchResultsList objectAtIndex:indexPath.row];
    if ([nd.images count] > 0)
        [Methods setImageInImageViewFromUrl:customCell.backGroundImageView urlStr:[nd.images objectAtIndex:0]];
    else
        [customCell.backGroundImageView setImage:[UIImage imageNamed:@"ImageNA"]];
   
    [customCell.titleLbl setText:nd.siteName];
    [customCell.descLbl setText:nd.siteAddress];
    [customCell.distanceLbl setText:[NSString stringWithFormat:@"%.2fkm",nd.distance]];
    if ([nd.tagList count] > 0)
    {
        for (int i = 0; i < [nd.tagList count]; i++)
        {
            if (i == 0)
            {
                
        if ([nd.tagList objectAtIndex:0])
        {
        [customCell.firstTagLabel setHidden:NO];
        [customCell.firstTagLabel setText:[[nd.tagList objectAtIndex:0] objectForKey:@"name"]];
        
        }
            }
            if (i == 1)
            {
        if ([nd.tagList objectAtIndex:1])
        {
            [customCell.secondTagLabel setHidden:NO];
            [customCell.secondTagLabel setText:[[nd.tagList objectAtIndex:1] objectForKey:@"name"]];
            
        }
            }
            if (i == 2)
            {
        if ([nd.tagList objectAtIndex:2])
        {
            [customCell.thirdTagLabel setHidden:NO];
            [customCell.thirdTagLabel setText:[[nd.tagList objectAtIndex:2] objectForKey:@"name"]];
            
        }
            }
        }
    }
    

    
    return customCell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotxDetailsVC *vc = (NotxDetailsVC *)[Methods getVCbyName:@"NotxDetailsVC"];
    [vc setSelNotxData:[self.searchResultsList objectAtIndex:indexPath.row]];
    
    [dbHelper insertNotxToDB:[self.searchResultsList objectAtIndex:indexPath.row]];
    [Methods pushVCinNCwithObj:vc popTop:YES];
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
