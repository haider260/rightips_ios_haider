//
//  LikeVC.m
//  Rightips
//
//  Created by Mac on 08/01/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import "LikeVC.h"
#import "LikeCell.h"

@interface LikeVC ()
{
    MBProgressHUD *HUD;
    NSMutableArray *likeList;
    IBOutlet UIView *borderView;
    UIButton *delButton;
    UserProfile *currentuserProfile;
    
}

@end

@implementation LikeVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    
    likeList = [[NSMutableArray alloc] init];
    
    [likeLabel setText:[NSString stringWithFormat:@"%@'s Likes",[[self.userProfile.userName componentsSeparatedByString:@" "] objectAtIndex:0]]];
    currentuserProfile=[NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData]];
    
    [self getLikes];
    
}
-(void)getLikes
{
    [HUD setLabelText:NSLocalizedString(@"Fetching User Likes", nil)];
    [HUD show:YES];
    
    NSString *apiUrl;
    
    apiUrl = [NSString stringWithFormat:@"http://www.rightips.com/api/index.php?action=getUserLikes&uid=%@",self.userProfile.userIdEmail];
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiUrl parameters:nil];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
    [HUD hide:YES];
                                             
    if([[JSON objectForKey:@"status"] integerValue] == 1)
    {
                                                 
    [self fetchLikes:JSON];
    }
    else if([[JSON objectForKey:@"status"] integerValue] == 2)
    {
        
        [[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"There are currently no likes", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
    }
    else if([JSON objectForKey:@"message"])
    {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
    }
        
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        //     Commiting
                                             
    [HUD hide:YES];
    }];
    [operation start];
}
-(void)fetchLikes:(id)json
{
    [likeList removeAllObjects];
    NSArray *commentsArray = [json objectForKey:@"data"];
    for (NSDictionary *dic in commentsArray)
    {
        UserProfile *user = [[UserProfile alloc] init];
        user.title = [dic objectForKey:@"title"];
        user.commentImage = [dic objectForKey:@"img"];
        
        
        user.createdAt = [NSString stringWithFormat:@"%@000",[dic objectForKey:@"created"]];
//          user.createdAt = [dic objectForKey:@"createdDate"];
//        user.userReview = [dic objectForKey:@"comment"];
//        user.commentImage = [dic objectForKey:@"com_img"];
          user.profileImgUrl = [dic objectForKey:@"profile_pic"];
          user.type = [dic objectForKey:@"type"];
//        user.userIdEmail = [dic objectForKey:@"uid_fk"];
//        user.isLiked = [dic objectForKey:@"is_liked"];
//        user.commentId = [dic objectForKey:@"com_id"];
//        user.userName = [dic objectForKey:@"name"];
        
        
        [likeList addObject:user];
        
        
    }
    [likeTableView reloadData];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [likeList count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserProfile *user = [likeList objectAtIndex:indexPath.row];
    
    LikeCell *customCell =  [[[NSBundle mainBundle] loadNibNamed:@"LikeCell" owner:nil options:nil] objectAtIndex: 0];
    
    
//    [customCell.descriptionLabel setText:[NSString stringWithFormat:@"%@ has liked the notification '%@'",[[self.userProfile.userName componentsSeparatedByString:@" "] objectAtIndex:0],user.title]];
    [customCell.descriptionLabel setText:user.title];
//    NSString *timeLeft = [self timeLeftSinceTime:user.createdAt.doubleValue];
//    [customCell.timeLeftLabel setText:timeLeft];
    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
//    NSDate *date = [[NSDate alloc] init];
//    date = [dateFormatter dateFromString:user.createdAt];
//    // converting into our required date format
//    [dateFormatter setDateFormat:@"MMM dd yyyy - hh:mma"];
//    NSString *reqDateString = [dateFormatter stringFromDate:date];
//    [customCell.timeLeftLabel setText:reqDateString];
    
    if (user.profileImgUrl && ![user.profileImgUrl isEqual:[NSNull null]])
    {
        [customCell.profileImageView setImageURL:[NSURL URLWithString:self.userProfile.profileImgUrl]];
    }
    else
    {
        [customCell.profileImageView setImage:[UIImage imageNamed:@"NoProfileImage"]];
    }
    
    if (user.commentImage && ![user.commentImage isEqual:[NSNull null]] && ![user.commentImage isEqualToString:@""])
    {
        [customCell.commentImageView setImageURL:[NSURL URLWithString:user.commentImage]];
    }
    else
    {
        [customCell.commentImageView setHidden:YES];
    }
    
    if ([user.type isEqualToString:@"comment"])
    {
        [customCell.titleLabel setText:[NSString stringWithFormat:@"%@ liked your comment",self.userProfile.userName]];
    }
    else if ([user.type isEqualToString:@"notification"])
    {
        [customCell.titleLabel setText:[NSString stringWithFormat:@"%@ liked notification",self.userProfile.userName]];
        
    }
    NSString *timeLeft = [self timeLeftSinceTime:user.createdAt.doubleValue];
    [customCell.timeLeftLabel setText:timeLeft];
    
    [customCell.reportButton addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [customCell.reportButton setTag:indexPath.row];
    
    
    [customCell.contentView setBackgroundColor:[UIColor clearColor]];
    
    customCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UserProfile *user = [likeList objectAtIndex:indexPath.row];
    
    LikeCell *customCell =  [[[NSBundle mainBundle] loadNibNamed:@"LikeCell" owner:nil options:nil] objectAtIndex: 0];
    
    [customCell.descriptionLabel setText:user.title];
    [customCell.descriptionLabel sizeToFit];
    
//    if ([user.type isEqualToString:@"comment"])
//    {
//        [customCell.descriptionLabel setText:user.title];
//        [customCell.descriptionLabel sizeToFit];
//    }
//    else if ([user.type isEqualToString:@"notification"])
//    {
//        [customCell.titleLabel setText:[NSString stringWithFormat:@"%@ liked notification",self.userProfile.userName]];
//        [customCell.descriptionLabel setHidden:YES];
//        [customCell.titleLabel sizeToFit];
//        return  customCell.frame.size.height + customCell.descriptionLabel.frame.size.height;
//    }
    
    return  customCell.frame.size.height + customCell.descriptionLabel.frame.size.height;
    
}
-(void)deleteButtonPressed:(UIButton *)sender
{
    UserProfile *user = [likeList objectAtIndex:sender.tag];
    [Methods showReportActionSheet:user.commentId.integerValue];
}
-(NSString*)timeLeftSinceTime:(double)timesTamp
{
    NSString *timeLeft;
    
    double currentTimeInMiliSeconds = (double)([[NSDate date] timeIntervalSince1970] * 1000.0);
    
    
    double miliSeconds = currentTimeInMiliSeconds - timesTamp;
    double seconds = miliSeconds / 1000.0;
    
    
    NSInteger months = (int) (floor(seconds / (3600 * 24 * 30)));
    if(months) seconds -= months * 3600 * 24 *30;
    
    NSInteger days = (int) (floor(seconds / (3600 * 24)));
    if(days) seconds -= days * 3600 * 24;
    
    NSInteger hours = (int) (floor(seconds / 3600));
    if(hours) seconds -= hours * 3600;
    
    NSInteger minutes = (int) (floor(seconds / 60));
    if(minutes) seconds -= minutes * 60;
    
    if (months)
    {
        timeLeft = [NSString stringWithFormat:@"%ld months ago", (long)months];
    }
    else if(days)
    {
        timeLeft = [NSString stringWithFormat:@"%ld days ago", (long)days];
    }
    else if(hours) { timeLeft = [NSString stringWithFormat: @"%ld hours ago", (long)hours];
    }
    else if(minutes) { timeLeft = [NSString stringWithFormat: @"%ld minutes ago", (long)minutes];
    }
    else if(seconds)
        timeLeft = [NSString stringWithFormat: @"%ld seconds ago", (long)seconds];
    
    return timeLeft;
}
- (IBAction)backButtonPressed:(UIButton *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navController = (UINavigationController *) appDelegate.window.rootViewController;
    [navController popViewControllerAnimated:YES];
}
- (IBAction)likesButtonPressed:(UIButton *)sender
{
    CGRect likeContFrame = likeTableView.frame;
    CGRect likerContFrame = likerTableView.frame;
    CGRect buttonBottomViewFrame = borderView.frame;
    likeContFrame.origin.x = 0.0f;
    likerContFrame.origin.x = likeContFrame.size.width;
    buttonBottomViewFrame.origin.x = 10.0f;
    [UIView animateWithDuration:0.5f animations:^{
        likeTableView.frame = likeContFrame;
        likerTableView.frame = likeContFrame;
        borderView.frame = buttonBottomViewFrame;
    }];
}
- (IBAction)likersButtonPressed:(UIButton *)sender
{
    CGRect likeContFrame = likeTableView.frame;
    CGRect likerContFrame = likerTableView.frame;
    CGRect buttonBottomViewFrame = borderView.frame;
    likeContFrame.origin.x = likeContFrame.size.width * -1.0f;
    likerContFrame.origin.x = 0.0f;
    buttonBottomViewFrame.origin.x = 160.0f;
    [UIView animateWithDuration:0.5f animations:^{
        likeTableView.frame = likeContFrame;
        likerTableView.frame = likeContFrame;
        borderView.frame = buttonBottomViewFrame;
    }];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
