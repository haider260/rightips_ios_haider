//
//  NavController.h
//  WaveClock
//
//  Created by Abdul Mannan on 11/19/14.
//  Copyright (c) 2014 Curiologix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavController : UINavigationController<CBCentralManagerDelegate>
- (void) checkIfAlreadyrun;

@end
