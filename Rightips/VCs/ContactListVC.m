//
//  ContactListVC.m
//  Rightips
//
//  Created by Esol on 26/06/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "ContactListVC.h"
#import "ContactTableViewCell.h"
#import <Branch/Branch.h>

#define kContactName    @"name"
#define kContactPhone    @"Phone"
#define kContactEmail    @"Email"
#define kContactWhatsapp    @"Whatsapp"
#define kContactImage    @"image"
#define kCheckBoxCheckedImage   @"IconInviteContactSelected"
#define kCheckBoxUncheckedImage   @"AddContact"

#define kMessageTextString  NSLocalizedString(@"Hey! Check out RighTips, the new app i'm using to discover the best people tips",nil)

@interface ContactListVC ()
{
    NSMutableArray *arrayOfLocalAddressBookContacts;
    NSMutableArray *selectedContactsArray;
    NSMutableArray *tempContactsArray;
    MBProgressHUD *HUD;
    NSMutableDictionary *params;
    NSString *branchUrl;
}
@end

@implementation ContactListVC
@synthesize contactListSearchBar;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    arrayOfLocalAddressBookContacts = [[NSMutableArray alloc] init];
    selectedContactsArray = [[NSMutableArray alloc] init];
    tempContactsArray = [[NSMutableArray alloc] init];
    [self setUpAddressBook];
    self.contactTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    BOOL isLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:[StaticData instance].kLoggedIn];
    params = [[NSMutableDictionary alloc] init];
    if (isLoggedIn)
    {
        NSData *userProfileData = [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData];
        UserProfile *userProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userProfileData];
        
        NSString *userName = [NSString stringWithFormat:@"%@ %@",userProfile.firstName,userProfile.lastName];
        [params setObject:userName forKey:kReferringUsername];
        [params setObject:userProfile.userIdEmail forKey:kReferringUserId];
        [params setObject:userProfile.profileImgUrl forKey:kReferringUserImage];
    }
    
    [[Branch getInstance] getShortURLWithParams:params andCallback:^(NSString *url, NSError *error)
    {
        if(!error)
        {
            branchUrl = url;
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayOfLocalAddressBookContacts.count;
}

- (ContactTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *contactName = [[arrayOfLocalAddressBookContacts objectAtIndex:indexPath.row] objectForKey:kContactName];
    
    ContactTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (!cell) {
        cell = [[ContactTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.friendImage.layer.cornerRadius = cell.friendImage.frame.size.height/2.0;
    NSData *imageData = [[arrayOfLocalAddressBookContacts objectAtIndex:indexPath.row] objectForKey:kContactImage];
    if (imageData) {
        cell.friendImage.image = [UIImage imageWithData:imageData];
    }
    else
    {
        cell.friendImage.image = [UIImage imageNamed:@"ImageNA"];
    }
    if ([contactName isEqualToString:@"(null)"] || contactName == nil)
    {
        cell.lblName.text = @"";
    }
    else
    {
        cell.lblName.text = [[arrayOfLocalAddressBookContacts objectAtIndex:indexPath.row] objectForKey:kContactName];
    }

    cell.imgSelect.image = [UIImage imageNamed:kCheckBoxUncheckedImage];

    if ([self.type isEqualToString:kContactPhone])
    {
        for (NSString *contact in selectedContactsArray)
        {
            if ([contact isEqualToString:[[arrayOfLocalAddressBookContacts objectAtIndex:indexPath.row] objectForKey:kContactPhone]])
            {
                cell.imgSelect.image = [UIImage imageNamed:kCheckBoxCheckedImage];
                break;
            }
        }
    }
    else if ([self.type isEqualToString:kContactEmail])
    {
        for (NSString *contact in selectedContactsArray)
        {
            if ([contact isEqualToString:[[arrayOfLocalAddressBookContacts objectAtIndex:indexPath.row] objectForKey:kContactEmail]])
            {
                cell.imgSelect.image = [UIImage imageNamed:kCheckBoxCheckedImage];
                break;
            }
        }
    }
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactTableViewCell *cell = (ContactTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    // Phone Selection Handling
    if ([self.type isEqualToString:kContactPhone])
    {
        for (NSString *contact in selectedContactsArray)
        {
            if ([contact isEqualToString:[[arrayOfLocalAddressBookContacts objectAtIndex:indexPath.row] objectForKey:kContactPhone]])
            {
                cell.imgSelect.image = [UIImage imageNamed:kCheckBoxUncheckedImage];
                [selectedContactsArray removeObject:contact];
                if (selectedContactsArray.count == 0)
                {
                    [self.doneButton setEnabled:NO];
                }
                return;
            }
        }
        [selectedContactsArray addObject:[[arrayOfLocalAddressBookContacts objectAtIndex:indexPath.row] objectForKey:kContactPhone]];
        cell.imgSelect.image = [UIImage imageNamed:kCheckBoxCheckedImage];
    }

    
    
    ////Email selection handling
    else if ([self.type isEqualToString:kContactEmail])
    {
        for (NSString *contact in selectedContactsArray)
        {
            if ([contact isEqualToString:[[arrayOfLocalAddressBookContacts objectAtIndex:indexPath.row] objectForKey:kContactEmail]])
            {
                cell.imgSelect.image = [UIImage imageNamed:kCheckBoxUncheckedImage];
                [selectedContactsArray removeObject:contact];
                if (selectedContactsArray.count == 0)
                {
                    [self.doneButton setEnabled:NO];
                }
                return;
            }
        }
        [selectedContactsArray addObject:[[arrayOfLocalAddressBookContacts objectAtIndex:indexPath.row] objectForKey:kContactEmail]];
        cell.imgSelect.image = [UIImage imageNamed:kCheckBoxCheckedImage];
    }
    
    if (selectedContactsArray.count > 0)
    {
        [self.doneButton setEnabled:YES];
    }
}

#pragma mark - IBActions
- (IBAction)smsButton:(id)sender
{
    if (![self.type isEqualToString:kContactPhone])
    {
        [selectedContactsArray removeAllObjects];
        [self.doneButton setEnabled:NO];
    }
    self.type = kContactPhone;
    [self setUpAddressBook];
    CGRect frame = self.redLineView.frame;
    frame.origin.x = self.smsButton.frame.origin.x;
    self.redLineView.frame = frame;
}
- (IBAction)whatsappButton:(id)sender
{
    if(branchUrl)
    {
        [self showWhatsapp];
    }
    else
    {
        [self getBranchUrlAndSendWithType:kContactWhatsapp];
    }
}
- (IBAction)mailButton:(id)sender
{
    if (![self.type isEqualToString:kContactEmail])
    {
        [selectedContactsArray removeAllObjects];
        [self.doneButton setEnabled:NO];
    }
    self.type = kContactEmail;
    [self setUpAddressBook];
    CGRect frame = self.redLineView.frame;
    frame.origin.x = self.mail.frame.origin.x;
    self.redLineView.frame = frame;
}

- (IBAction)doneButton:(id)sender
{
    if ([self.type isEqualToString:kContactPhone])
    {
        if(branchUrl)
        {
            [self showSMS];
        }
        else
        {
            [self getBranchUrlAndSendWithType:kContactPhone];
        }
    }
    else if ([self.type isEqualToString:kContactEmail])
    {
        if(branchUrl)
        {
            [self ShowEmail];
        }
        else
        {
            [self getBranchUrlAndSendWithType:kContactEmail];
        }
    }
}

- (IBAction)cancelButton:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navController = (UINavigationController *) appDelegate.window.rootViewController;
    [navController popViewControllerAnimated:YES];
}

#pragma mark - Get Branch Url And Send
- (void) getBranchUrlAndSendWithType:(NSString*)type
{
    [HUD setLabelText:@"Generating Link"];
    [HUD show:YES];
    [[Branch getInstance] getShortURLWithParams:params andCallback:^(NSString *url, NSError *error)
     {
         [HUD hide:YES];
         if(!error)
         {
             branchUrl = url;
             if([type isEqualToString:kContactPhone])
             {
                 [self showSMS];
             }
             else if([type isEqualToString:kContactEmail])
             {
                 [self ShowEmail];
             }
             else if([type isEqualToString:kContactWhatsapp])
             {
                 [self showWhatsapp];
             }
         }
         else
         {
             [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
         }
     }];
}

#pragma mark - Message Methods

- (void)showSMS
{
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)  message:NSLocalizedString(@"Your device doesn't support SMS!",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    NSString *messageWithUrl = [NSString stringWithFormat:@"%@ \n %@",kMessageTextString,branchUrl];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:selectedContactsArray];
    [messageController setBody:messageWithUrl];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    [self dismissViewControllerAnimated:YES completion:Nil];
    switch (result)
    {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",nil) message:NSLocalizedString(@"Failed to send SMS!",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
        {
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            UINavigationController *navController = (UINavigationController *) appDelegate.window.rootViewController;
            [navController popViewControllerAnimated:YES];
            break;
        }
        default:
            break;
    }
}

#pragma mark - Email Methods

- (void) ShowEmail
{
    if ([MFMailComposeViewController canSendMail])
    {
        NSString *messageWithUrl = [NSString stringWithFormat:@"%@ \n%@",kMessageTextString,branchUrl];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setToRecipients:selectedContactsArray];
        [mc setSubject:@"RighTips"];
        [mc setMessageBody:messageWithUrl isHTML:NO];
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Oops!", nil) message:NSLocalizedString(@"Can not open email. Please configure you email first.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    [self dismissViewControllerAnimated:YES completion:Nil];
    if (result == MFMailComposeResultSent || result == MFMailComposeResultSaved)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UINavigationController *navController = (UINavigationController *) appDelegate.window.rootViewController;
        [navController popViewControllerAnimated:YES];
    }
    else if (!(result == MFMailComposeResultCancelled))
    {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)  message:@"Action Failed" delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [warningAlert show];
    }
}

#pragma mark - Whatsapp Methods

- (void) showWhatsapp
{
    NSString *messageWithUrl = [NSString stringWithFormat:@"%@ \n%@",kMessageTextString,branchUrl];
    NSString *encodedURLString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)messageWithUrl, NULL,
                                                                                                       (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8 ));
    
    NSString *stringToShare=[@"whatsapp://send?text=" stringByAppendingString:encodedURLString];
    NSURL *whatsappURL = [NSURL URLWithString:stringToShare];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL])
    {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Can not open Whatsapp" message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    }
}

#pragma mark - AddressBook Methods

- (void)setUpAddressBook
{
    
    [HUD show:YES];
    [HUD setLabelText:@"Loading Contacts"];
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {

    });
    __block BOOL accessGranted = NO;
    
    if (&ABAddressBookRequestAccessWithCompletion != NULL) { // We are on iOS 6
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(semaphore);
        });
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    }
    
    else { // We are on iOS 5 or Older
        accessGranted = YES;
        [self getContactsWithAddressBook:addressBook];
    }
    
    if (accessGranted)
    {
        [self getContactsWithAddressBook:addressBook];
    }
    [HUD hide:YES];
}

- (void)getContactsWithAddressBook:(ABAddressBookRef)addressBook
{
    [arrayOfLocalAddressBookContacts removeAllObjects];
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
    
    for (int i = 0; i < nPeople; i++) {
        NSMutableDictionary *dictionaryOfPerson = [NSMutableDictionary dictionary];
        
        ABRecordRef ref = CFArrayGetValueAtIndex(allPeople, i);
        
        //For username and surname
        
        CFStringRef firstName, lastName;
        firstName = ABRecordCopyValue(ref, kABPersonFirstNameProperty);
        lastName  = ABRecordCopyValue(ref, kABPersonLastNameProperty);
        
        if (ABPersonHasImageData(ref))
        {
            CFDataRef imageData = ABPersonCopyImageDataWithFormat(ref, kABPersonImageFormatThumbnail);
            [dictionaryOfPerson setObject:(__bridge NSData*)imageData forKey:kContactImage];
        }
        if (lastName == nil)
        {
            [dictionaryOfPerson setObject:[NSString stringWithFormat:@"%@", firstName] forKey:kContactName];
        }
        else if(firstName !=nil)
            [dictionaryOfPerson setObject:[NSString stringWithFormat:@"%@ %@", firstName, lastName] forKey:kContactName];
        else if (firstName ==nil)
            [dictionaryOfPerson setObject:[NSString stringWithFormat:@"%@", lastName] forKey:kContactName];
        
        //For Email ids
        ABMutableMultiValueRef eMail  = ABRecordCopyValue(ref, kABPersonEmailProperty);
        if (ABMultiValueGetCount(eMail) > 0) {
            [dictionaryOfPerson setObject:(__bridge NSString *)ABMultiValueCopyValueAtIndex(eMail, 0) forKey:kContactEmail];
        }
        
        //For Phone number
        ABMultiValueRef phones = ABRecordCopyValue(ref, kABPersonPhoneProperty);
        CFArrayRef phoneRef = ABMultiValueCopyArrayOfAllValues(phones);
        NSArray *phoneArray = (__bridge NSArray*)phoneRef;
        if (phoneArray.count > 0)
        {
            [dictionaryOfPerson setObject:(NSString*)[phoneArray objectAtIndex:0] forKey:kContactPhone];
        }
        if ([self.type isEqualToString:kContactPhone] && [dictionaryOfPerson objectForKey:kContactPhone])
        {
            [arrayOfLocalAddressBookContacts addObject:dictionaryOfPerson];
        }
        else if ([self.type isEqualToString:kContactEmail] && [dictionaryOfPerson objectForKey:kContactEmail])
        {
            [arrayOfLocalAddressBookContacts addObject:dictionaryOfPerson];
        }
    }
    [self sortArrayOfLocalAddressBookContacts];
    [self.contactTableView reloadData];
}

- (void) sortArrayOfLocalAddressBookContacts
{
    NSSortDescriptor *alphaNumSD = [NSSortDescriptor sortDescriptorWithKey:kContactName ascending:YES comparator:^(NSString *string1, NSString *string2) {
        return [string1 compare:string2 options:NSCaseInsensitiveSearch];
    }];
    
    NSArray *sortedArray = [arrayOfLocalAddressBookContacts sortedArrayUsingDescriptors:@[alphaNumSD]];
    arrayOfLocalAddressBookContacts = [sortedArray mutableCopy];
    tempContactsArray = [arrayOfLocalAddressBookContacts copy];
}


#pragma mark - SearchBar Methods

- (void) searchBarBookmarkButtonClicked:(UISearchBar *)searchBar
{
    [contactListSearchBar resignFirstResponder];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [contactListSearchBar resignFirstResponder];
    if (searchBar.text.length == 0)
    {
        arrayOfLocalAddressBookContacts = tempContactsArray;
        [searchBar resignFirstResponder];
    }
    else
    {
        NSMutableArray *searchedContacts = [[NSMutableArray alloc] init];
        for (int i = 0; i < tempContactsArray.count; i++)
        {
            NSString *contactName = [[tempContactsArray objectAtIndex:i] objectForKey:kContactName];

            if ([contactName.lowercaseString rangeOfString:searchBar.text.lowercaseString].location != NSNotFound) {
                [searchedContacts addObject:[tempContactsArray objectAtIndex:i]];
            }
        }
        arrayOfLocalAddressBookContacts = searchedContacts;
    }
    [self.contactTableView reloadData];
}



- (BOOL) searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}
- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchBar.text.length == 0)
    {
        arrayOfLocalAddressBookContacts = tempContactsArray;
        [searchBar performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0.001];
        
        //[searchBar resignFirstResponder];
    }
    else
    {
        NSMutableArray *searchedContacts = [[NSMutableArray alloc] init];
        for (int i = 0; i < tempContactsArray.count; i++)
        {
            NSString *contactName = [[tempContactsArray objectAtIndex:i] objectForKey:kContactName];
            if ([contactName.lowercaseString rangeOfString:searchBar.text.lowercaseString].location != NSNotFound)
            {
                [searchedContacts addObject:[tempContactsArray objectAtIndex:i]];
            }
        }
        arrayOfLocalAddressBookContacts = searchedContacts;
    }
    [self.contactTableView reloadData];
}


@end
