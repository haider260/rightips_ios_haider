//
//  FriendsVC.h
//  Rightips
//
//  Created by Esol on 29/06/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsVC : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain) IBOutlet UIButton *menuToggleButton;
@property (nonatomic, retain) IBOutlet UITableView *friendsTableView;
- (IBAction)inviteByMessageButton:(id)sender;
- (IBAction)slideMenuButtonPressed:(id)sender;

@end
