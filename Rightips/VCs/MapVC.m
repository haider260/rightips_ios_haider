//
//  MapVC.m
//  Rightips
//
//  Created by Esol on 12/05/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "MapVC.h"
#import "RatingView.h"
#import "NotificationAnnotation.h"
#import "MapConstants.h"
#import "NotxSlideVC.h"
#import "NotxDetailsVC.h"
#import "HomeVC.h"
#import "ImageCaptureView.h"

@interface MapVC ()<RatingViewDelegate>
{
    SlideMenu *slideMenu;
    NSMutableArray *catGroups;
    MBProgressHUD *HUD;
    float selectedVenueLongitude;
    float selectedVenueLatitude;
    NSString *selectedVenueId;
    NSString *selectedMode;
    BOOL isRouteExist;
    NSString *latStr;
    NSString *lngStr;
   BOOL isSiteInfoViewHide;
    int selected;
    NSInteger ind;
    ImageCaptureView *view;
    NSUserDefaults *defaults;
}

@end

@implementation MapVC

@synthesize ratingView;
@synthesize selNotxData;
@synthesize menuToggleButton;

#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    latStr = [NSString stringWithFormat:@"%f", self.userLoc.selectedLocation.coordinate.latitude];
    lngStr = [NSString stringWithFormat:@"%f", self.userLoc.selectedLocation.coordinate.longitude];
    
    selectedMode = [[NSString alloc] init];
    selectedMode = kWalkingMode;
    isRouteExist = NO;
    catGroups = [[NSMutableArray alloc] init];
    [self populateCatGroupData];

    slideMenu = [[SlideMenu alloc] init];
    [self.mapView setShowsUserLocation:YES];
    
    // Category Group Data
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];

    HUD.dimBackground = YES;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeImage) name:@"changeImage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraButtonTapped) name:@"TapOnCameraButton" object:nil];
    
    if(!selNotxData)
    {
        [self.siteInfoView setHidden:YES];
        isSiteInfoViewHide = YES;
        [self.lblTitle setHidden:YES];
    }
    else
    {
        isSiteInfoViewHide = NO;
        [self.lblTitle setHidden:NO];
    }
    
    [self reload];
    

}
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //rating View
    self.ratingView = [[RatingView alloc] initWithFrame:ratingView.frame
                                 selectedImageName:@"RedStarIcon"
                                   unSelectedImage:@"GreyStarIcon"
                                          minValue:0
                                          maxValue:5
                                     intervalValue:1
                                        stepByStep:NO];
    self.ratingView.value = 3;
    self.ratingView.delegate = self;
    [self.siteInfoView addSubview:self.ratingView];
}
- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   // [[PiwikTracker sharedInstance] sendView:@"Notification Location"];
}

#pragma mark - memory Warnings
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SwipeGesture Methods
-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer
{
    [slideMenu showHideMenu];
}
#pragma mark - Setting View Data

- (void) adjustViews
{
    [self.view bringSubviewToFront:self.btnDrawRoute];
    [self.view bringSubviewToFront:self.topView];
    [self.view bringSubviewToFront:slideMenu];
    [self.view bringSubviewToFront:menuToggleButton];
    [self.view bringSubviewToFront:HUD];
}
- (void) reload
{
    [self setButtonsAndAnnotationImages];
    // Remove All previously added annotations
    for (NotificationAnnotation *annotation in self.mapView.annotations)
    {
        if ([annotation isKindOfClass:[NotificationAnnotation class]])
        {
            [self.mapView removeAnnotation:annotation];
        }
    }
    // Add annotation to location of beacon
    
    if(selNotxData)
    {
    [self setMapViewCenterWithLatititude:[selNotxData.latitude floatValue] Longitude:[selNotxData.longitude floatValue] AndDelta:0.005];

    NSMutableDictionary *annotationDict = [[NSMutableDictionary alloc] init];
    [annotationDict setObject:selNotxData.catGroupId forKey:kCategoryGroupId];
    [annotationDict setObject:selNotxData.siteName forKey:kNotificationSiteName];
    [annotationDict setObject:selNotxData.siteId forKey:kNotificationSiteId];

    [annotationDict setObject:selNotxData.siteAddress forKey:kNotificationAddress];
    [annotationDict setObject:selNotxData.catId forKey:kNotificationCatId];
    if (selNotxData.totalSiteNotifications)
    {
        [annotationDict setObject:selNotxData.totalSiteNotifications forKey:kSiteTotalNotifications];
    }

    [annotationDict setValue:[NSNumber numberWithBool:NO] forKey:kIsNotificationSelected];
    
    CLLocationCoordinate2D locationCoordinate = CLLocationCoordinate2DMake([selNotxData.latitude floatValue], [selNotxData.longitude floatValue]);
    NotificationAnnotation *annotation = [[NotificationAnnotation alloc] initWithLocation:locationCoordinate AnnotationDictionary:annotationDict];
    [self.mapView addAnnotation:annotation];
    selectedVenueLatitude = [selNotxData.latitude floatValue];
    selectedVenueLongitude = [selNotxData.longitude floatValue];
    selectedVenueId = selNotxData.siteId;
    //Add Title and Name etc
    self.lblName.text = selNotxData.siteName;
    self.lblTitle.text = selNotxData.title;
    self.lblDate.text = selNotxData.siteAddress;
    if (selNotxData.totalSiteNotifications)
    {
        self.lblNotifications.text = [NSString stringWithFormat:@"%@",selNotxData.totalSiteNotifications];
    }

    
    }
    self.lblNotifications.layer.cornerRadius = self.lblNotifications.bounds.size.height/2.0;
//    else
//    {
//        latStr = [NSString stringWithFormat:@"%f", self.userLoc.selectedLocation.coordinate.latitude];
//        lngStr = [NSString stringWithFormat:@"%f", self.userLoc.selectedLocation.coordinate.longitude];
//        [self setMapViewCenterWithLatititude:[latStr floatValue] Longitude:[lngStr floatValue] AndDelta:0.005];
//    }
    
    // Adjust Views
    [self.view addSubview:slideMenu];
    
    UISwipeGestureRecognizer *swipeLeftMenu = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToLeftWithGestureRecognizer:)];
    swipeLeftMenu.direction = UISwipeGestureRecognizerDirectionLeft;
    [slideMenu addGestureRecognizer:swipeLeftMenu];
    
    [self adjustViews];
}

- (void) setButtonsAndAnnotationImages
{
    float btnWidth = (self.view.frame.size.width - [catGroups count] + 1)/catGroups.count;
    float btnHeight = 45;
    
    for (int i = 0; i < catGroups.count; i++)
    {
        CategoryGroup *catGroup = [catGroups objectAtIndex:i];
        
        for (UIView *subview in self.view.subviews)
        {
            if ([subview isKindOfClass:[UIButton class]])
            {
                UIButton *button = (UIButton*) subview;

                if (button.tag == [catGroup.catId integerValue])
                {
                    [button removeFromSuperview];
                }
            }
        }
        UIButton *catButton;
        
        if(isSiteInfoViewHide)
        {
        
//            [self.mapView setFrame:CGRectMake(self.mapView.frame.origin.x, self.mapView.frame.origin.y , self.mapView.frame.size.width, self.view.frame.size.height - btnHeight)];
        catButton = [[UIButton alloc] initWithFrame:CGRectMake(i * btnWidth + i , self.view.frame.size.height - btnHeight, btnWidth, btnHeight)];
            catButton.tag = [catGroup.catId integerValue];
            
            if(!selNotxData)        {
                if(catButton.tag == 1)
                {
                    [self categorySelected:catButton];
                }
            }
            
        }
        else
        {
//            [self.mapView setFrame:CGRectMake(self.mapView.frame.origin.x, self.mapView.frame.origin.y , self.mapView.frame.size.width, self.view.frame.size.height -(self.topView.frame.size.height + btnHeight + self.siteInfoView.frame.size.height))];
            catButton = [[UIButton alloc] initWithFrame:CGRectMake(i * btnWidth + i , self.view.frame.size.height - (self.siteInfoView.frame.size.height + btnHeight), btnWidth, btnHeight)];
            catButton.tag = [catGroup.catId integerValue];
            
            if(catButton.tag == selected)
            {
                [self categorySelected:catButton];
            }
        }
        
        
        UIImage *catIconImage = [UIImage imageNamed:catGroup.iconImageName];

        [catButton setImage:catIconImage forState:UIControlStateNormal];
        [catButton setImage:[UIImage imageNamed:catGroup.selectedIconImageName] forState:UIControlStateSelected];
        
        [catButton addTarget:self action:@selector(categorySelected:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:catButton];
    }
}

- (void) setMapViewCenterWithLatititude: (float)latitude Longitude:(float) longitude AndDelta:(CLLocationDegrees)delta
{
    CLLocationCoordinate2D locationCoordinate = CLLocationCoordinate2DMake(latitude, longitude);
    MKCoordinateRegion adjustedRegion;
    adjustedRegion.center = locationCoordinate;
    adjustedRegion.span.longitudeDelta  = delta;
    adjustedRegion.span.latitudeDelta  = delta;
    [self.mapView setRegion:adjustedRegion animated:YES];
}

#pragma mark - IBActions
- (IBAction)btnNotifications:(id)sender
{
    if ([self.lblNotifications.text isEqualToString:@""] || [self.lblNotifications.text isEqualToString:@"0"])
    {
        [[[UIAlertView alloc] initWithTitle:@"No notifications" message:@"There is no notification on this venue" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return;
    }
    [HUD setLabelText:@"..."];
    [HUD show:YES];
    
    NSString *apiURL=@"http://beaconwatcher.com/api/index.php?action=getSpecificNotifications";
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setObject:[StaticData instance].apiKey forKey:@"key"];
    [postParams setObject:selectedVenueId forKey:@"site_id"];
    NSURL *url = [[NSURL alloc] initWithString:apiURL];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiURL parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
        [HUD hide:YES];
        NSMutableArray *venueNotifications = [[NSMutableArray alloc] init];
        if([[JSON objectForKey:@"status"] integerValue]==1)
        {
            for (id notxObj in [JSON objectForKey:@"data"])
            {
                NotificationData *nd = [ReuseAbleFunctions getNotificationDataFromObject:notxObj];
                [venueNotifications addObject:nd];
            }
            NotxDetailsVC *vc = (NotxDetailsVC *)[Methods getVCbyName:@"NotxDetailsVC"];
            [vc setSelNotxData:[venueNotifications objectAtIndex:0]];
//            [dbHelper insertNotxToDB:[homeNotxs objectAtIndex:index]];
            [Methods pushVCinNCwithObj:vc popTop:YES];
//            NotxSlideVC *vc = (NotxSlideVC *)[Methods getVCbyName:@"NotxSlideVC"];
//            [vc setSelNotxData:[venueNotifications objectAtIndex:0]];
//            [vc setArrayOfNotifications:venueNotifications];
//            [Methods pushVCinNCwithObj:vc popTop:YES];
        }
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        [HUD hide:YES];
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
    }];
    [operation start];
}

- (IBAction)btnBack:(id)sender
{
    if(selNotxData)
    {
    NotxDetailsVC *vc = (NotxDetailsVC *)[Methods getVCbyName:@"NotxDetailsVC"];
    [vc setSelNotxData:selNotxData];
    [Methods pushVCinNCwithObj:vc popTop:YES];
    }
    else
    {
        HomeVC *vc = (HomeVC *)[Methods getVCbyName:@"HomeVC"];
        [Methods pushVCinNCwithObj:vc popTop:YES];
        
    }
}

- (IBAction)btnDrawRoute:(id)sender;
{
    [self.view bringSubviewToFront:self.routeDetailView];

    [self removeRoute];
    [self addRouteWithMode:kWalkingMode];
    [self.routeDetailView setHidden:NO];

    [self.btnDrawWalkingRoute setSelected:YES];
    [self.btnDrawDriveRoute setSelected:NO];
    [self.btnDrawTransitRoute setSelected:NO];
    CGRect frame = self.selectedRouteLineView.frame;
    frame.origin.x = self.btnDrawWalkingRoute.frame.origin.x;
    self.selectedRouteLineView.frame = frame;

}
- (IBAction)btnDrawWalkingRoute:(id)sender;
{
    selectedMode = kWalkingMode;
    [self removeRoute];
    [self addRouteWithMode:kWalkingMode];
    
    [self.btnDrawWalkingRoute setSelected:YES];
    [self.btnDrawDriveRoute setSelected:NO];
    [self.btnDrawTransitRoute setSelected:NO];
    CGRect frame = self.selectedRouteLineView.frame;
    frame.origin.x = self.btnDrawWalkingRoute.frame.origin.x;
    self.selectedRouteLineView.frame = frame;
}
- (IBAction)btnDrawDriveRoute:(id)sender;
{
    selectedMode = kDrivingMode;
    [self removeRoute];
    [self addRouteWithMode:kDrivingMode];
    
    [self.btnDrawWalkingRoute setSelected:NO];
    [self.btnDrawDriveRoute setSelected:YES];
    [self.btnDrawTransitRoute setSelected:NO];
    
    CGRect frame = self.selectedRouteLineView.frame;
    frame.origin.x = self.btnDrawDriveRoute.frame.origin.x;
    self.selectedRouteLineView.frame = frame;
}
- (IBAction)btnDrawTransitRoute:(id)sender;
{
    selectedMode = kTransitMode;
    [self removeRoute];
    [self addRouteWithMode:kTransitMode];
    
    [self.btnDrawWalkingRoute setSelected:NO];
    [self.btnDrawDriveRoute setSelected:NO];
    [self.btnDrawTransitRoute setSelected:YES];
    
    CGRect frame = self.selectedRouteLineView.frame;
    frame.origin.x = self.btnDrawTransitRoute.frame.origin.x;
    self.selectedRouteLineView.frame = frame;
}
- (IBAction)slideMenuButtonPressed:(UIButton*)sender
{
    [self adjustViews];
    [slideMenu showHideMenu];
}


- (IBAction)btnUdateLocation:(id)sender
{
    [self setMapViewCenterWithLatititude:self.mapView.userLocation.coordinate.latitude Longitude:self.mapView.userLocation.coordinate.longitude AndDelta:0.005];
}

- (IBAction)categorySelected:(UIButton*)sender
{
    [self removeRoute];
    [self.routeDetailView setHidden:YES];
    
    selected = (int)sender.tag;
    

    NSString *selectedGroupId = [NSString stringWithFormat:@"%ld",(long)sender.tag];

    for (CategoryGroup *catGroup in catGroups)
    {
        if ([catGroup.catId isEqualToString:selectedGroupId])
        {
            self.lblTitle.text = [NSString stringWithFormat:@"%@ %@ %@",catGroup.name, NSLocalizedString(@"near" , nil), selNotxData.title];
        }
    }
    
    //Deselect All Button and Select pressed button
    for (UIView *subview in self.view.subviews)
    {
        if ([subview isKindOfClass:[UIButton class]])
        {
            UIButton *button = (UIButton*) subview;
            if (button.tag< 10 && button.tag>=1)
            {
                [button setSelected:NO];
            }
        }
    }
    [sender setSelected:YES];
    if(selNotxData)
    {
    [self setMapViewCenterWithLatititude:[selNotxData.latitude floatValue] Longitude:[selNotxData.longitude floatValue] AndDelta:0.06];
    }
    else
    {
        [self setMapViewCenterWithLatititude:[latStr floatValue] Longitude:[lngStr floatValue] AndDelta:0.06];
    }
    
    // Remove All previously added annotations except selected one
    
    for (NotificationAnnotation *annotation in self.mapView.annotations)
    {
        if ([annotation isKindOfClass:[NotificationAnnotation class]])
        {
            if (([[annotation.annotationDict valueForKey:kIsNotificationSelected] boolValue] == NO))
            {
                [self.mapView removeAnnotation:annotation];
            }
        }
    }
    // To show all annotation related to Selected group
    [self FetchSelectedGroupDataAndShowOnMap:selectedGroupId];
}

#pragma mark - Fetch Selected Group data and add annotations

- (void) FetchSelectedGroupDataAndShowOnMap: (NSString*)selectedGroupId
{
    NSString *apiURL=@"http://www.beaconwatcher.com/api/index.php?action=getLocSites";
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    
    [postParams setObject:[StaticData instance].apiKey forKey:@"key"];
    [postParams setObject:selectedGroupId forKey:@"catGroup_id"];
    if(selNotxData)
    {
    [postParams setObject:selNotxData.latitude forKey:@"lat"];
    [postParams setObject:selNotxData.longitude forKey:@"lng"];
    }
    else
    {
        [postParams setObject:[NSNumber numberWithFloat:latStr.floatValue] forKey:@"lat"];
        [postParams setObject:[NSNumber numberWithFloat:lngStr.floatValue] forKey:@"lng"];
    }
    
    NSURL *url = [[NSURL alloc] initWithString:apiURL];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiURL parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    
    [HUD show:YES];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
        [HUD hide:YES];
        if([[JSON objectForKey:@"status"] integerValue]==1)
        {
            NSMutableArray *selGroupNotifications = [[NSMutableArray alloc] init];
            selGroupNotifications = [JSON objectForKey:@"data"];
            
            for (NSDictionary *notificationDict in selGroupNotifications)
            {
                if ([[notificationDict objectForKey:@"catGroup_id"] isEqualToString:selectedGroupId])
                {
                    NSMutableDictionary *annotationDict = [[NSMutableDictionary alloc] init];
                    [annotationDict setObject:[notificationDict objectForKey:@"catGroup_id"] forKey:kCategoryGroupId];
                    [annotationDict setObject:[notificationDict objectForKey:@"site_name"] forKey:kNotificationSiteName];
                    [annotationDict setObject:[notificationDict objectForKey:@"site_id"] forKey:kNotificationSiteId];
                    [annotationDict setObject:[notificationDict objectForKey:@"site_address"] forKey:kNotificationAddress];
                    [annotationDict setObject:[notificationDict objectForKey:@"cat_id"] forKey:kNotificationCatId];
                    [annotationDict setObject:[NSString stringWithFormat:@"%@",[notificationDict objectForKey:kSiteTotalNotifications]] forKey:kSiteTotalNotifications];
                    [annotationDict setObject:[NSNumber numberWithBool:NO] forKey:kIsNotificationSelected];
                    
                    CLLocationCoordinate2D locationCoordinate = CLLocationCoordinate2DMake([[notificationDict objectForKey:@"site_lat"] floatValue], [[notificationDict objectForKey:@"site_long"] floatValue]);
                    NotificationAnnotation *annotation = [[NotificationAnnotation alloc] initWithLocation:locationCoordinate AnnotationDictionary:[annotationDict mutableCopy]];
                    [self.mapView addAnnotation:annotation];
                }
            }
        }
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        [HUD hide:YES];
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
    }];
    [operation start];
}

#pragma mark - Category Groups Data

- (void) populateCatGroupData
{
    for (int i = 1; i <= 3; i++)
    {
        CategoryGroup *group = [[CategoryGroup alloc] init];
        group.catId = [NSString stringWithFormat:@"%d",i];
        if (i == 1)
        {
            group.name = @"Stay";
            group.iconImageName = @"Building";
            group.selectedIconImageName = @"BuildingSelected";
        }
        if (i == 2)
        {
            group.name = @"Eat";
            group.iconImageName = @"Food";
            group.selectedIconImageName = @"FoodSelected";
        }
        if (i == 3)
        {
            group.name = @"Play";
            group.iconImageName = @"Category";
            group.selectedIconImageName = @"CategorySelected";
        }
        [catGroups addObject:group];
    }
}

- (NSString*) getAnnotationImageNameWithCatId:(NSString*)catId
{
    NSString *imageName = [[NSString alloc] init];
    switch ([catId integerValue])
    {
        case 1:
            imageName = kBarMarker;
            break;
        case 2:
            imageName = kShopMarker;
            break;
        case 3:
            imageName = kServicesMarker;
            break;
        case 4:
            imageName = kEntertainmentMarker;
            break;
        case 5:
            imageName = kTourismMarker;
            break;
        case 6:
            imageName = kEventMarker;
            break;
        case 7:
            imageName = kStudentsMarker;
            break;
            
        default:
            break;
            
    }
    return imageName;
}

- (NSString*) getSelectedAnnotationImageNameWithCatId:(NSString*)catId
{
    NSString *imageName = [[NSString alloc] init];
    switch ([catId integerValue])
    {
        case 1:
            imageName = kBarMarkerSelected;
            break;
        case 2:
            imageName = kShopMarkerSelected;
            break;
        case 3:
            imageName = kServicesMarkerSelected;
            break;
        case 4:
            imageName = kEntertainmentMarkerSelected;
            break;
        case 5:
            imageName = kTourismMarkerSelected;
            break;
        case 6:
            imageName = kEventMarkerSelected;
            break;
        case 7:
            imageName = kStudentsMarkerSelected;
            break;
            
        default:
            break;
            
    }
    return imageName;
}

#pragma mark - Draw And remove Root

- (void) removeRoute
{
    for (id<MKOverlay> overlayToRemove in self.mapView.overlays)
    {
        if ([overlayToRemove isKindOfClass:[MKPolyline class]])
        {
            [self.mapView removeOverlay:overlayToRemove];
        }
    }
    isRouteExist = NO;
}
-(void) addRouteWithMode:(NSString*)travelMode
{
    self.lblRouteDistance.text = @"";
    self.lblRouteTime.text = @"";
    // [self.mapViewOutlet setCenterCoordinate:self.mapViewOutlet.userLocation.coordinate animated:YES];
    
    NSString *baseUrl = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=true&mode=%@", self.mapView.userLocation.location.coordinate.latitude,  self.mapView.userLocation.location.coordinate.longitude, selectedVenueLatitude,selectedVenueLongitude,travelMode];//31.6,74.4
    
    NSURL *url = [NSURL URLWithString:[baseUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [HUD show:YES];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (isRouteExist == NO)
        {
            [HUD hide:YES];
        }
        if(! connectionError)
        {
            NSError *error = nil;
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            NSArray *routes = [result objectForKey:@"routes"];

            if (routes.count>0)
            {
                isRouteExist = YES;

                [self adjustViews];
                NSDictionary *firstRoute = [routes objectAtIndex:0];
                NSDictionary *leg =  [[firstRoute objectForKey:@"legs"] objectAtIndex:0];
                NSArray *steps = [leg objectForKey:@"steps"];
                
                NSString *routeDistance = [[leg objectForKey:@"distance"] objectForKey:@"text"];
                NSString *routeTime = [[leg objectForKey:@"duration"] objectForKey:@"text"];
                self.lblRouteDistance.text = routeDistance;
                self.lblRouteTime.text = routeTime;
                
                int stepIndex = 0;
                
                CLLocationCoordinate2D stepCoordinates[1  + [steps count] + 1];
                
                stepCoordinates[stepIndex] = self.mapView.userLocation.coordinate;
                
                for (NSDictionary *step in steps) {
                    
                    NSDictionary *start_location = [step objectForKey:@"start_location"];
                    stepCoordinates[++stepIndex] = [self coordinateWithLocation:start_location];
                    
                    if ([steps count] == stepIndex){
                        NSDictionary *end_location = [step objectForKey:@"end_location"];
                        stepCoordinates[++stepIndex] = [self coordinateWithLocation:end_location];
                    }
                }
                [self removeRoute];

                MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:stepCoordinates count:1 + stepIndex];
                [self.mapView addOverlay:polyLine];
            }
            else{
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"No Route Found", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
            }
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:connectionError.description message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
        }
        
    }];
}

- (CLLocationCoordinate2D)coordinateWithLocation:(NSDictionary*)location
{
    double latitude = [[location objectForKey:@"lat"] doubleValue];
    double longitude = [[location objectForKey:@"lng"] doubleValue];
    
    return CLLocationCoordinate2DMake(latitude, longitude);
}
#pragma mark - MapView Delegate Methods

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation;
{
    if ([annotation isKindOfClass:[NotificationAnnotation class]])
    {
        NotificationAnnotation *notxAnnotation = (NotificationAnnotation*) annotation;
        MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"annotation"];
        
        if (annotationView == nil)
        {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:notxAnnotation reuseIdentifier:@"annotation"];
        }
        
        if (([[notxAnnotation.annotationDict valueForKey:kIsNotificationSelected] boolValue] == YES))
        {
            NSString *annotationImageName = [self getSelectedAnnotationImageNameWithCatId:[notxAnnotation.annotationDict objectForKey:kNotificationCatId]];
            
            UIImage *image = [UIImage imageNamed:annotationImageName];
            CGRect rect = CGRectMake(0,0,image.size.width*1.25,image.size.height*1.25);
            
            UIGraphicsBeginImageContext( rect.size );
            [image drawInRect:rect];
            UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            NSData *imageData = UIImagePNGRepresentation(picture1);
            UIImage *img=[UIImage imageWithData:imageData];
            annotationView.image = img;
        }
        else
        {
            NSString *annotationImageName = [self getAnnotationImageNameWithCatId:[notxAnnotation.annotationDict objectForKey:kNotificationCatId]];
            annotationView.image = [UIImage imageNamed:annotationImageName];
        }

        annotationView.canShowCallout = NO;
        
        return annotationView;
    }
    else
    {
        return nil;
    }
}


- (void) mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    for (NotificationAnnotation *annotation in self.mapView.annotations)
    {
        if ([annotation isKindOfClass:[NotificationAnnotation class]])
        {
            MKAnnotationView* anView = [mapView viewForAnnotation: annotation];
            
            if (anView)
            {
                NSString *annotationImageName = [self getAnnotationImageNameWithCatId:[annotation.annotationDict objectForKey:kNotificationCatId]];
                anView.image = [UIImage imageNamed:annotationImageName];
            }
        }
    }
    
    
    
    
    if ([view.annotation isKindOfClass:[NotificationAnnotation class]])
    {
        //Add Title and Name etc
        NotificationAnnotation *notifAnnotation = (NotificationAnnotation*)view.annotation;
        self.lblName.text = [notifAnnotation.annotationDict objectForKey:kNotificationSiteName];
        self.lblDate.text = [notifAnnotation.annotationDict objectForKey:kNotificationAddress];
        [self.mapView setCenterCoordinate:CLLocationCoordinate2DMake(notifAnnotation.coordinate.latitude, notifAnnotation.coordinate.longitude)];
        if ([notifAnnotation.annotationDict objectForKey:kSiteTotalNotifications])
        {
            self.lblNotifications.text = [NSString stringWithFormat:@"%@",[notifAnnotation.annotationDict objectForKey:kSiteTotalNotifications]];
        }

    //    [self setMapViewCenterWithLatititude:notifAnnotation.coordinate.latitude Longitude:notifAnnotation.coordinate.longitude AndDelta:0.005];

        [self removeRoute];
        [self.routeDetailView setHidden:YES];

        selectedVenueLatitude = notifAnnotation.coordinate.latitude;
        selectedVenueLongitude = notifAnnotation.coordinate.longitude;
        selectedVenueId = [notifAnnotation.annotationDict objectForKey:kNotificationSiteId];
        
        NSString *annotationImageName = [self getSelectedAnnotationImageNameWithCatId:[notifAnnotation.annotationDict objectForKey:kNotificationCatId]];
        UIImage *image = [UIImage imageNamed:annotationImageName];
        CGRect rect = CGRectMake(0,0,image.size.width*1.25,image.size.height*1.25);
        
        UIGraphicsBeginImageContext( rect.size );
        [image drawInRect:rect];
        UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData *imageData = UIImagePNGRepresentation(picture1);
        UIImage *img=[UIImage imageWithData:imageData];
        view.image = img;
    }
    
    if(isSiteInfoViewHide)
    {
        [self.siteInfoView setHidden:NO];
        isSiteInfoViewHide = NO;
        [self setButtonsAndAnnotationImages];
    }
    
}
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if(isRouteExist)
    {
        [self addRouteWithMode:selectedMode];
    }
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    MKPolylineView *polylineView = [[MKPolylineView alloc] initWithPolyline:overlay];
    polylineView.strokeColor = [UIColor colorWithRed:11.0/255.0 green:101.0/255.0 blue:255.070/255.0 alpha:1.0];
    polylineView.lineWidth = 8.0;
    
    return polylineView;
}


-(void)cameraButtonTapped
{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Edit Photo", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Change Profile Photo", nil),NSLocalizedString(@"Change Cover Photo", nil),nil];
    //    [popup setTag:3];
    [popup showInView:self.view];
    
    
    
}
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ind = buttonIndex;
    
    if (ind != 2)
    {
        
        view = [[ImageCaptureView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        [view setInd:ind];
        
        
        
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             [view setFrame:self.view.frame];
                         }
                         completion:^(BOOL finished){
                         }];
        [self.view addSubview:view];
        
    }
    
    
}
-(void)changeImage
{
    [slideMenu setupMenu];
}





@end
