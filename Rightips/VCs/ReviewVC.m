//
//  ReviewVC.m
//  Rightips
//
//  Created by Mac on 05/01/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import "ReviewVC.h"
#import "CommentsCellTableViewCell.h"

@interface ReviewVC ()
{
    NSMutableArray *allReviewList;
    MBProgressHUD *HUD;
    UIButton *delButton;
    UserProfile *currentuserProfile,*specificUser;
    BOOL isLikedReview;
}

@end

@implementation ReviewVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    allReviewList = [[NSMutableArray alloc] init];
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    currentuserProfile=[NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData]];
    
    [reviewLabel setText:[NSString stringWithFormat:@"%@'s reviews",[[self.userProfile.userName componentsSeparatedByString:@" "] objectAtIndex:0]]];
    
    [self getReviews];
    
}
-(void)getReviews
{
    [HUD setLabelText:NSLocalizedString(@"Fetching Reviews", nil)];
    [HUD show:YES];
    
    NSString *apiUrl;
    
    apiUrl = [NSString stringWithFormat:@"http://www.rightips.com/api/index.php?action=getReviews&uid=%@",self.userProfile.userIdEmail];
    
    
    
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiUrl parameters:nil];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
    [HUD hide:YES];
                                             
    if([[JSON objectForKey:@"status"] integerValue] == 1)
    {
                                                 
    [self fetchReviews:JSON];
    }
    else if([[JSON objectForKey:@"status"] integerValue] == 2)
    {
            
    [[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"There are currently no reviews", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
    }
    else if([JSON objectForKey:@"message"])
    {
                                                 
    [[[UIAlertView alloc] initWithTitle:nil message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
    }
                                             
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
                                             
    [HUD hide:YES];
    }];
    [operation start];
}
-(void)fetchReviews:(id)json
{
    [allReviewList removeAllObjects];
    NSArray *commentsArray = [json objectForKey:@"data"];
    for (NSDictionary *dic in commentsArray)
    {
        UserProfile *user = [[UserProfile alloc] init];
        user.userName = [dic objectForKey:@"site_title"];
        
        
        //        user.createdAt = [NSString stringWithFormat:@"%@000",[dic objectForKey:@"rev_datemili"]];
        user.createdAt = [dic objectForKey:@"createdDate"];
        user.userReview = [dic objectForKey:@"text"];
        user.reviewRate = [dic objectForKey:@"rev_rate"];
                user.commentImage = [dic objectForKey:@"img"];
                user.profileImgUrl = [dic objectForKey:@"profile_pic"];
        user.userIdEmail = [dic objectForKey:@"uid"];
        user.userLikes = [[dic objectForKey:@"likes"] integerValue];
        user.isLiked = [dic objectForKey:@"is_liked"];
        user.commentId = [dic objectForKey:@"id"];
        
        
        
        [allReviewList addObject:user];
        
        
    }
    
    
    [userReviewTableView reloadData];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [allReviewList count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserProfile *user = [allReviewList objectAtIndex:indexPath.row];
    
    CommentsCellTableViewCell *customCell;
    if  (![user.commentImage isEqual:[NSNull null]] && ![user.commentImage isEqualToString:@""])
    {
        customCell = [[[NSBundle mainBundle] loadNibNamed:@"CommentsCellTableViewCell" owner:nil options:nil] objectAtIndex: 1];
        [customCell.commentImageView setImageURL:[NSURL URLWithString:user.commentImage]];
        
        //        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
        //        [tap addTarget:self action:@selector(imageTapped:)];
        //        [customCell.commentImageView addGestureRecognizer:tap];
        
        
    }
    else
    {
        customCell = [[[NSBundle mainBundle] loadNibNamed:@"CommentsCellTableViewCell" owner:nil options:nil] objectAtIndex: 0];
        
    }
    [customCell.userNameLabel setText:user.userName];
    //    [customCell.userNameButton setTag:indexPath.row];
    //    [customCell.userNameButton addTarget:self action:@selector(userNameButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [customCell.descriptionLabel setText:user.userReview];
    
    //    [customCell.rateView setValue:user.reviewRate.floatValue];
    
    [customCell.likeButton setTitle:[NSString stringWithFormat:@"%ld Likes",(long)user.userLikes] forState:UIControlStateNormal];
    
    if (user.profileImgUrl && ![user.profileImgUrl isEqual:[NSNull null]])
    {
        [customCell.profileImageView setImageURL:[NSURL URLWithString:user.profileImgUrl]];
    }
    else
    {
        [customCell.profileImageView setImage:[UIImage imageNamed:@"NoProfileImage"]];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *date = [[NSDate alloc] init];
    date = [dateFormatter dateFromString:user.createdAt];
    // converting into our required date format
    [dateFormatter setDateFormat:@"MMM dd yyyy - hh:mma"];
    NSString *reqDateString = [dateFormatter stringFromDate:date];
    [customCell.createdTime setText:reqDateString];
    
//    [customCell.profileImageView setTag:indexPath.row];
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
//    [customCell.profileImageView addGestureRecognizer:tap];
    
    [customCell.deleButton addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [customCell.likeButton setTag:indexPath.row];
    
    
    
    [customCell.likeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [customCell.likeButton setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
    
    
    if ([user.isLiked isEqualToString:@"0"])
    {
        [customCell.likeButton setSelected:NO];
        if (user.userLikes == 0)
        {
            [customCell.likeButton setTitle:@"Like" forState:UIControlStateNormal];
        }
        else
        {
            [customCell.likeButton setTitle:[NSString stringWithFormat:@"Likes(%ld)",(long)user.userLikes] forState:UIControlStateNormal];
        }
        
        
    }
    else
    {
        [customCell.likeButton setSelected:YES];
        [customCell.likeButton setTitle:[NSString stringWithFormat:@"Likes(%ld)",(long)user.userLikes] forState:UIControlStateSelected];
    }
    
    [customCell.likeButton addTarget:self action:@selector(likeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [customCell.contentView setBackgroundColor:[UIColor clearColor]];
    
    customCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UserProfile *user = [allReviewList objectAtIndex:indexPath.row];
    
    
    CommentsCellTableViewCell *customCell;
    if  (![user.commentImage isEqual:[NSNull null]] && ![user.commentImage isEqualToString:@""])
    {
        customCell = [[[NSBundle mainBundle] loadNibNamed:@"CommentsCellTableViewCell" owner:nil options:nil] objectAtIndex: 1];
        [customCell.commentImageView setImageURL:[NSURL URLWithString:user.commentImage]];
        
        //        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
        //        [tap addTarget:self action:@selector(imageTapped:)];
        //        [customCell.commentImageView addGestureRecognizer:tap];
        
        
    }
    else
    {
        customCell = [[[NSBundle mainBundle] loadNibNamed:@"CommentsCellTableViewCell" owner:nil options:nil] objectAtIndex: 0];
        
    }
    [customCell.userNameLabel setText:user.userName];
    //    [customCell.userNameButton setTag:indexPath.row];
    //    [customCell.userNameButton addTarget:self action:@selector(userNameButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [customCell.descriptionLabel setText:user.userReview];
    [customCell.descriptionLabel sizeToFit];
    
    return  customCell.frame.size.height + customCell.descriptionLabel.frame.size.height;
    
}
-(void)likeButtonPressed:(UIButton *)sender
{
    [self likeReview:sender];
}
-(void)likeReview:(UIButton *)btn
{
    [HUD setLabelText:@""];
    [HUD show:YES];
    specificUser = [allReviewList objectAtIndex:btn.tag];
    
    NSString *apiURL=@"http://www.rightips.com/api/index.php?action=like";
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setObject:currentuserProfile.userIdEmail forKey:@"uid"];
    [postParams setObject:[NSString stringWithFormat:@"%ld",specificUser.commentId.integerValue] forKey:@"id"];
    [postParams setObject:@"review" forKey:@"type"];
    if ([specificUser.isLiked isEqualToString:@"0"])
    {
        [postParams setObject:@"Like" forKey:@"rel"];
        isLikedReview = YES;
    }
    else
    {
        [postParams setObject:@"Unlike" forKey:@"rel"];
        isLikedReview = NO;
    }
    
    NSURL *url = [NSURL URLWithString:apiURL];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiURL parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [HUD hide:YES];
        if([[JSON objectForKey:@"status"] integerValue] == 1)
        {
            if (isLikedReview)
            {
                [btn setSelected:YES];
                specificUser.userLikes++;
                specificUser.isLiked = @"1";
                [allReviewList replaceObjectAtIndex:btn.tag withObject:specificUser];
                [btn setTitle:[NSString stringWithFormat:@"Likes(%ld)",(long)specificUser.userLikes] forState:UIControlStateSelected];
            }
            else
            {
                [btn setSelected:NO];
                specificUser.userLikes--;
                specificUser.isLiked = @"0";
                [allReviewList replaceObjectAtIndex:btn.tag withObject:specificUser];
                if (specificUser.userLikes == 0)
                {
                    [btn setTitle:[NSString stringWithFormat:@"Like"] forState:UIControlStateNormal];
                }
                else
                {
                    [btn setTitle:[NSString stringWithFormat:@"Likes(%ld)",(long)specificUser.userLikes] forState:UIControlStateNormal];
                }
            }
        }
        else if([JSON objectForKey:@"message"])
        {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
        }
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             [HUD hide:YES];
                                             [Methods showAlertView:@"" message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
                                         }];
    [operation start];
}
-(void)deleteButtonPressed:(UIButton *)sender
{
    UserProfile *user = [allReviewList objectAtIndex:sender.tag];
    [Methods showReportActionSheet:user.commentId.integerValue];
}

-(NSString*)timeLeftSinceTime:(double)timesTamp
{
    NSString *timeLeft;
    
    double currentTimeInMiliSeconds = (double)([[NSDate date] timeIntervalSince1970] * 1000.0);
    
    
    double miliSeconds = currentTimeInMiliSeconds - timesTamp;
    double seconds = miliSeconds / 1000.0;
    
    
    NSInteger months = (int) (floor(seconds / (3600 * 24 * 30)));
    if(months) seconds -= months * 3600 * 24 *30;
    
    NSInteger days = (int) (floor(seconds / (3600 * 24)));
    if(days) seconds -= days * 3600 * 24;
    
    NSInteger hours = (int) (floor(seconds / 3600));
    if(hours) seconds -= hours * 3600;
    
    NSInteger minutes = (int) (floor(seconds / 60));
    if(minutes) seconds -= minutes * 60;
    
    if (months)
    {
        timeLeft = [NSString stringWithFormat:@"%ld months ago", (long)months];
    }
    else if(days)
    {
        timeLeft = [NSString stringWithFormat:@"%ld days ago", (long)days];
    }
    else if(hours) { timeLeft = [NSString stringWithFormat: @"%ld hours ago", (long)hours];
    }
    else if(minutes) { timeLeft = [NSString stringWithFormat: @"%ld minutes ago", (long)minutes];
    }
    else if(seconds)
        timeLeft = [NSString stringWithFormat: @"%ld seconds ago", (long)seconds];
    
    return timeLeft;
}
- (IBAction)backButtonPressed:(UIButton *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navController = (UINavigationController *) appDelegate.window.rootViewController;
    [navController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
