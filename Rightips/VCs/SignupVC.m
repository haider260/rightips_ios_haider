//
//  SignupVC.m
//  Rightips
//
//  Created by Abdul Mannan on 11/27/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import "SignupVC.h"
#import "SettingsVC.h"

#import <QuartzCore/QuartzCore.h>

@interface SignupVC () <UITextFieldDelegate>
{
    MBProgressHUD *HUD;
    NSUserDefaults *defaults;
    UITextField *focusedTF;
    
    UserProfile *userProfile;
}

@property (nonatomic, retain) IBOutlet UITextField *firstNameTF;
@property (nonatomic, retain) IBOutlet UITextField *lastNameTF;
@property (nonatomic, retain) IBOutlet UITextField *emailTF;
@property (nonatomic, retain) IBOutlet UITextField *passwordTF;
@property (nonatomic, retain) IBOutlet UITextField *password2TF;
@property (nonatomic, retain) IBOutlet UIButton *signupButton;
@property (nonatomic, retain) IBOutlet UIButton *alreadyButton;

@end

@implementation SignupVC

@synthesize firstNameTF, lastNameTF, emailTF, passwordTF, password2TF;
@synthesize signupButton, alreadyButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    
    defaults = [NSUserDefaults standardUserDefaults];
    
    userProfile = [[UserProfile alloc] init];
    
    signupButton.layer.cornerRadius = 5.0f;
    signupButton.clipsToBounds = YES;
    alreadyButton.layer.cornerRadius = 5.0f;
    alreadyButton.clipsToBounds = YES;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //Sending current screen to Piwic Tracker
    //[[PiwikTracker sharedInstance] sendView:@"SignUp"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    focusedTF = textField;
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    if (textField == firstNameTF)
    {
        [lastNameTF becomeFirstResponder];
    }
    else if (textField == lastNameTF)
    {
        [emailTF becomeFirstResponder];
    }
    else if (textField == emailTF)
    {
        [passwordTF becomeFirstResponder];
    }
    else if (textField == passwordTF)
    {
        [password2TF becomeFirstResponder];
    }
    else if (textField == password2TF)
    {
        [password2TF resignFirstResponder];
    }
    
    return NO;
}

- (IBAction)signupButtonPressed:(id)sender
{
    if ([self validateInputData] == YES)
    {
        [self verifyEmail:emailTF.text];
    }
}

- (IBAction)alreadyButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)pushNextVC
{
    id object = [defaults objectForKey:[StaticData instance].kSelCatsIds];
    if (!object) [Methods pushVCinNCwithName:@"SettingsVC" popTop:YES];
    else [Methods pushVCinNCwithName:@"HomeVC" popTop:YES];
}

- (BOOL)validateInputData
{
    if ([Methods validateTextFieldData:self tf:firstNameTF tfName:@"First Name" shouldTrim:YES showMsg:YES]) {
        if([Methods validateTextFieldData:self tf:lastNameTF tfName:@"Last Name" shouldTrim:YES showMsg:YES]) {
            if([Methods validateTextFieldData:self tf:emailTF tfName:@"Email" shouldTrim:YES showMsg:YES]) {
                if([Methods validateTextFieldData:self tf:passwordTF tfName:@"Password" shouldTrim:YES showMsg:YES]) {
                    if([Methods validateTextFieldData:self tf:password2TF tfName:@"Repeat Password" shouldTrim:YES showMsg:YES]) {
                        if (![passwordTF.text isEqualToString:password2TF.text]) {
                            [Methods showAlertView:@"" message:NSLocalizedString(@"Passwords do not match!", nil) buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
                            return NO;
                        } else {
                            [focusedTF resignFirstResponder];
                            return YES;
                        }
                    } else return NO;
                } else return NO;
            } else return NO;
        } else return NO;
    }
    
    return NO;
}
-(void)verifyEmail:(NSString *)email
{
    NSString *apiUrl = @"https://bpi.briteverify.com/emails.json?";
    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"login"]];

    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setValue:email forKey:@"address"];
    [postParams setValue:@"031b8dbb-2485-4916-923f-e348b9075ca2" forKey:@"apikey"];


    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiUrl parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
     {
         [HUD hide:YES];
         if ([[JSON objectForKey:@"status"] isEqualToString:@"invalid"])
         {
             [Methods showAlertView:@"" message:[JSON objectForKey:@"error"] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
         }
         else
         {
             [self performRegister:firstNameTF.text lastName:lastNameTF.text email:emailTF.text password:passwordTF.text];
         }

     }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
     {
         [HUD hide:YES];
         [Methods showAlertView:@"" message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
     }];
    [operation start];
    [HUD setLabelText:NSLocalizedString(@"Please Wait.", nil)];
    [HUD setDetailsLabelText:NSLocalizedString(@"Verifying the Email", nil)];
    [HUD show:YES];
}

- (void)performRegister:(NSString *)firstName lastName:(NSString *)lastName email:(NSString *)email password:(NSString *)password
{
    NSString *apiUrl = [NSString stringWithFormat:@"%@", [StaticData instance].baseUrl];
    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"login"]];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setValue:firstName forKey:@"fname"];
    [postParams setValue:lastName forKey:@"lname"];
    [postParams setValue:email forKey:@"email"];
    [postParams setValue:password forKey:@"pass"];
    [postParams setValue:@"0" forKey:@"provider"];
    [postParams setValue:@"1" forKey:@"signup"];
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:apiUrl parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [HUD hide:YES];
        [self handleRegisterSuccess:JSON];
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [HUD hide:YES];
        [Methods showAlertView:@"" message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }];
    [operation start];
    [HUD setLabelText:NSLocalizedString(@"Please Wait.", nil)];
    [HUD setDetailsLabelText:NSLocalizedString(@"Signing up ...", nil)];
    [HUD show:YES];
}

- (void)handleRegisterSuccess:(id)JSON
{
    NSString *status = [JSON objectForKey:@"status"];
    NSString *message = [JSON objectForKey:@"message"];
    if ([status intValue] == 1) {
        NSDictionary *dataDict = [[JSON objectForKey:@"data"] objectAtIndex:0];
        
        userProfile.userIdEmail = [dataDict objectForKey:@"uid"];
//        userProfile.firstName = [dataDict objectForKey:@"first_name"];
//        userProfile.lastName = [dataDict objectForKey:@"last_name"];
        [userProfile setUserName:[dataDict objectForKey:@"user_name"]];
        userProfile.email = [emailTF.text copy];
        //userProfile.smId = @"0";
        userProfile.provider = @"1";
        
        NSData *userProfileData = [NSKeyedArchiver archivedDataWithRootObject:userProfile];
        [defaults setObject:userProfileData forKey:[StaticData instance].kUserProfileData];
        [defaults setBool:YES forKey:[StaticData instance].kLoggedIn];
        [defaults synchronize];
        
        //NSString *toastMessage = [NSString stringWithFormat:@"Welcom %@ %@. You are now signed in.", userProfile.fistName, userProfile.lastName];
        //[self.view makeToast:toastMessage duration:3.0f position:@"bottom"];
        [PiwikTracker sharedInstance].userID = [dataDict objectForKey:@"uid"];
        [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_USERS action:ACTION_SIGNUP name:[ReuseAbleFunctions convertDictionaryToJSONString:dataDict] value:nil];
        [self pushNextVC];
    } else {
        [Methods showAlertView:@"" message:message buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }
}

@end
