//
//  FriendsVC.m
//  Rightips
//
//  Created by Esol on 29/06/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "FriendsVC.h"
#import "ContactTableViewCell.h"
#import "ContactListVC.h"
#import "ImageCaptureView.h"


@interface FriendsVC ()
{
    MBProgressHUD *HUD;
    SlideMenu *slideMenu;
    NSMutableArray *friendsArray;
    NSInteger ind;
    ImageCaptureView *view;
    NSUserDefaults *defaults;
}
@end

@implementation FriendsVC
@synthesize menuToggleButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    friendsArray = [[NSMutableArray alloc] init];
    //HUD
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraButtonTapped) name:@"TapOnCameraButton" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageSelected) name:@"ImageSelected" object:nil];
    
    // Slide Menu
    slideMenu = [[SlideMenu alloc] init];
    [self.view addSubview:slideMenu];
    [self.view bringSubviewToFront:menuToggleButton];
    
     defaults = [NSUserDefaults standardUserDefaults];

    UISwipeGestureRecognizer *swipeLeftMenu = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToLeftWithGestureRecognizer:)];
    swipeLeftMenu.direction = UISwipeGestureRecognizerDirectionLeft;
    [slideMenu addGestureRecognizer:swipeLeftMenu];
    
    [self FetchFriendsData];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.friendsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return friendsArray.count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

- (ContactTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ContactTableViewCell" owner:self options:nil];
    ContactTableViewCell *cell = [nib objectAtIndex:0];
    cell.friendImage.layer.cornerRadius = cell.friendImage.bounds.size.height/2.0;
    cell.lblName.text = [[friendsArray objectAtIndex:indexPath.row] objectForKey:@"username"];
    if([[friendsArray objectAtIndex:indexPath.row] objectForKey:@"image"])
    {
        [Methods setImageInImageViewFromUrl:cell.friendImage urlStr:[[friendsArray objectAtIndex:indexPath.row] objectForKey:@"image"]];
    }
    else
    {
        [cell.friendImage setImage:[UIImage imageNamed:@"ImageNA"]];
    }
    [cell.imgSelect setHidden:YES];
    return cell;
}

#pragma mark - IBActions
- (IBAction)slideMenuButtonPressed:(id)sender
{
    [slideMenu showHideMenu];
}

- (IBAction)inviteByMessageButton:(id)sender
{
    ContactListVC *vc = (ContactListVC *)[Methods getVCbyName:@"ContactListVC"];
    [vc setType:@"Phone"];
    [Methods pushVCinNCwithObj:vc popTop:NO];
}

#pragma mark - SwipeGesture Methods
-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer
{
    [slideMenu showHideMenu];
}

#pragma mark - Get Freinds
- (void) FetchFriendsData
{
    NSString *apiURL=@"http://www.rightips.com/api/index.php?action=friendsList";
   
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    
    [postParams setObject:[StaticData instance].apiKey forKey:@"key"];
    BOOL isLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:[StaticData instance].kLoggedIn];
    
    if (isLoggedIn)
    {
        NSData *userProfileData = [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData];
        UserProfile *userProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userProfileData];
        [postParams setObject:userProfile.userIdEmail forKey:@"uid"];
//        NSString *apiURL=[NSString stringWithFormat:@"http://www.rightips.com/api/index.php?action=friendsList&key=%@&uid=%@",[StaticData instance].apiKey,userProfile.userIdEmail];
        
        NSURL *url = [[NSURL alloc] initWithString:apiURL];
        AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
        NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiURL parameters:postParams];
        [request setTimeoutInterval:[StaticData instance].serverTimeout];
        [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
        
        [HUD setLabelText:@"Loading Friends..."];
        [HUD show:YES];
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
        {
            [HUD hide:YES];
            if([[JSON objectForKey:@"status"] integerValue]==1)
            {
                if([[JSON objectForKey:@"data"] count] > 0)
                {
                    friendsArray = [[JSON objectForKey:@"data"] mutableCopy];
                    [self.friendsTableView reloadData];
                }
                else
                {
                    [[[UIAlertView alloc] initWithTitle:@"No friend found" message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];

                }
            }
        }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
        {
            [HUD hide:YES];
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
        }];
        [operation start];
    }
}

-(void)imageSelected
{
    UIAlertView *alert;
    if (ind == 0)
    {
        
        alert = [[UIAlertView alloc] initWithTitle:nil message:@"Do you want to save this picture as profile photo" delegate:self cancelButtonTitle:@"No" otherButtonTitles: @"Yes",nil];
        
    }
    else if (ind == 1)
    {
        
        alert = [[UIAlertView alloc] initWithTitle:nil message:@"Do you want to save this picture as cover photo" delegate:self cancelButtonTitle:@"No" otherButtonTitles: @"Yes",nil];
    }
    
    
    [alert show];
}
-(void)cameraButtonTapped
{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Edit Photo", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Change Profile Photo", nil),NSLocalizedString(@"Change Cover Photo", nil),nil];
    //    [popup setTag:3];
    [popup showInView:self.view];
    
    
    
}
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ind = buttonIndex;
    
    view = [[ImageCaptureView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    [view setBackgroundColor:[UIColor clearColor]];
    
    
    
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         [view setFrame:self.view.frame];
                     }
                     completion:^(BOOL finished){
                     }];
    [self.view addSubview:view];
    
    
    
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        
        [self saveProfileOrCoverPhoto];
    }
}

-(void)saveProfileOrCoverPhoto
{
    
    NSData *userProfileData = [defaults objectForKey:[StaticData instance].kUserProfileData];
    UserProfile *userProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userProfileData];
    
    UIImage *scaleImage;
    
    
    NSString *apiUrl;
    if (ind == 0)
    {
        apiUrl = @"http://www.rightips.com/api/index.php?action=saveProfilePic";
        scaleImage = [self imageWithImage:view.chosenImage scaledToSize:CGSizeMake(100, 100)];
        
    }
    else
    {
        apiUrl = @"http://www.rightips.com/api/index.php?action=saveCoverPhoto";
        scaleImage = [self imageWithImage:view.chosenImage scaledToSize:CGSizeMake(280, 195)];
    }
    NSData *imageData = UIImagePNGRepresentation(scaleImage);
    
    //    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"getCategories"]];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setValue:userProfile.userIdEmail forKey:@"uid"];
    
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request;
    
    
    request = [client multipartFormRequestWithMethod:@"POST" path:apiUrl parameters:postParams constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFileData: imageData name:@"file" fileName:@"file.png" mimeType:@"image/png"];
    }];
    
    
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             [HUD hide:YES];
                                             [self uploadImage:JSON];
                                             
                                             
                                         }
                                                                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             [HUD hide:YES];
                                             
                                             [Methods showAlertView:@"" message:[error localizedDescription] buttonText:@"OK" tag:0 delegate:nil];
                                         }];
    [operation start];
    [HUD setLabelText:NSLocalizedString(@"Updating", nil)];
    ////    [HUD setDetailsLabelText:NSLocalizedString(@"Fetching data ...", nil)];
    [HUD show:YES];
}
-(void)uploadImage:(id)json
{
    NSArray *dataArray = [json objectForKey:@"data"];
    NSDictionary *dic = [dataArray objectAtIndex:0];
    NSString *imgUrl = [dic objectForKey:@"pic"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *userProfileData = [defaults objectForKey:[StaticData instance].kUserProfileData];
    UserProfile *userProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userProfileData];
    
    if (ind == 0)
    {
        
        [userProfile setProfileImgUrl:imgUrl];
    }
    else if (ind == 1)
    {
        
        [userProfile setCoverImgUrl:imgUrl];
        
    }
    
    userProfileData = [NSKeyedArchiver archivedDataWithRootObject:userProfile];
    [defaults setObject:userProfileData forKey:[StaticData instance].kUserProfileData];
    [defaults synchronize];
    
    [slideMenu setupMenu];
}
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


@end
