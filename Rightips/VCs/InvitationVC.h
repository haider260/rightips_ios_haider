//
//  InvitationVC.h
//  Rightips
//
//  Created by Shahbaz Ali on 7/15/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InvitationVC : UIViewController

@property (nonatomic, retain) NSDictionary *paramsDict;
@property (weak, nonatomic) IBOutlet UIImageView *friendImgView;
@property (weak, nonatomic) IBOutlet UILabel *inviteLabel;
@property (weak, nonatomic) IBOutlet UILabel *discoverPlacesLabel;

@property (weak, nonatomic) IBOutlet UIButton *closeInviteButton;
@property (weak, nonatomic) IBOutlet UIButton *fbButton;
- (IBAction)fbButton:(id)sender;

- (IBAction)closeInviteButton:(id)sender;

@end
