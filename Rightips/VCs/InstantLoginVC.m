//
//  InstantLoginVC.m
//  Rightips
//
//  Created by Esol on 25/03/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "InstantLoginVC.h"

@interface InstantLoginVC () <UITextFieldDelegate,GIDSignInUIDelegate,GIDSignInDelegate>
{
    MBProgressHUD *HUD;
    NSUserDefaults *defaults;
    UITextField *focusedTF;
    
    UserProfile *userProfile;
}
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *fbButton;
@property (weak, nonatomic) IBOutlet UIButton *gpButton;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;
@end

@implementation InstantLoginVC
@synthesize loginButton,signUpButton,passwordTF,emailTF,gpButton,fbButton;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    
    defaults = [NSUserDefaults standardUserDefaults];
    
    userProfile = [[UserProfile alloc] init];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signUpDone) name:@"signUpDone" object:nil];
    
    UIColor *fbColor = [UIColor colorWithRed:0.2353f green:0.3412f blue:0.6157f alpha:1.0f];
    [self customButton:fbButton borderColor:fbColor];
    UIColor *gpColor = [UIColor colorWithRed:0.8510f green:0.3020f blue:0.1961f alpha:1.0f];
    [self customButton:gpButton borderColor:gpColor];
    
    loginButton.layer.cornerRadius = 5.0f;
    loginButton.clipsToBounds = YES;
    signUpButton.layer.cornerRadius = 5.0f;
    signUpButton.clipsToBounds = YES;
}

- (void) signUpDone
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //Sending current screen to Piwic Tracker
    //[[PiwikTracker sharedInstance] sendView:@"Login"];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark Actions
- (IBAction)needAnAccountTapped:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
   // [self presentViewController:[storyboard instantiateViewControllerWithIdentifier:@"InstantSignupVC" ] animated:YES completion:NULL];
    [self showViewController:[storyboard instantiateViewControllerWithIdentifier:@"InstantSignupVC" ] sender:self];
    //[self dismissViewControllerAnimated:NO completion:NULL];

}
- (IBAction)cancelButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)fbButtonTapped:(id)sender
{
    
//    [FBSession openActiveSessionWithReadPermissions:[StaticData instance].fbPermissions allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
//        AppDelegate* appDelegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
//        [appDelegate sessionStateChanged:session state:state error:error];
//        
//        if(!error)
//        {
//            switch (state)
//            {
//                case FBSessionStateOpen:
//                    [FBSession setActiveSession:session];
//                    [self retrieveFbUserData];
//                    break;
//                case FBSessionStateClosedLoginFailed:
//                    NSLog(@"Failed to log in");
//                    break;
//                default:
//                    break;
//            }
//
//        } else {
//            NSLog(@"fbsession error: %@", [error localizedDescription]);
//            //[Methods showAlertView:@"Oops!" message:@"Login with facebook failed!" buttonText:@"OK" tag:0 delegate:nil]; // commented as error popups are being shown in sessionStateChanged method.
//        }
//    }];
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: [StaticData instance].fbPermissions fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            NSLog(@"Process error");
        } else if (result.isCancelled) {
            NSLog(@"Cancelled");
        } else
        {
            if ([result.grantedPermissions containsObject:@"email"])
            {
//                NSLog(@"result is:%@",result);
                [self retrieveFbUserData];
            }
            
        }
    }];
    
}

- (IBAction)gpButtonTapped:(id)sender
{
    
    static NSString * const kClientId = @"462207644497-cb7ha38i5ra4lkp0c25crj4eooeda5v6.apps.googleusercontent.com";
    
    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    signIn.allowsSignInWithBrowser = NO;
    signIn.allowsSignInWithWebView = YES;
    signIn.clientID = kClientId;
    //    signIn.scopes = @[ kGTLAuthScopePlusLogin ];
    signIn.scopes = @[ @"profile" ];
    signIn.uiDelegate = self;
    signIn.delegate = self;
    
    [signIn signIn];
    
    [HUD setLabelText:NSLocalizedString(@"Please Wait.", nil)];
    [HUD setDetailsLabelText:NSLocalizedString(@"Signing in ...", nil)];
    [HUD show:YES];
}

- (IBAction)loginButtonTapped:(id)sender
{
    if ([self validateInputData] == YES)
    {
        [self performLogin:emailTF.text password:passwordTF.text];
    }
}

#pragma  -mark TextField Delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    focusedTF = textField;
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField {
    if (textField == emailTF) {
        [passwordTF becomeFirstResponder];
    } else if (textField == passwordTF) {
        [textField resignFirstResponder];
        [self loginButtonTapped:loginButton];
    }
    
    return NO;
}

#pragma mark -Google Plus Delegate

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error
{
    if (!error)
    {
        [self retrieveGpUserData:user];
    }
    else
    {
        [HUD hide:YES];
        NSLog(@"gp sign in error: %@", [error localizedDescription]);
        [Methods showAlertView:NSLocalizedString(@"Oops!", nil) message:NSLocalizedString(@"Login with google+ failed!", nil) buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }
    
}
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error
{
    if (!error)
    {
        [self retrieveGpUserData:signIn.currentUser];
    }
    else
    {
        [HUD hide:YES];
        NSLog(@"gp sign in error: %@", [error localizedDescription]);
        [Methods showAlertView:NSLocalizedString(@"Oops!", nil) message:NSLocalizedString(@"Login with google+ failed!", nil) buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }
}


#pragma mark -Custom Methods

- (void)retrieveGpUserData: (GIDGoogleUser *)user
{
    if (user.authentication == nil)
    {
        return;
    }
    
    //    GTLPlusPerson *person = [GPPSignIn sharedInstance].googlePlusUser;
    if (user == nil)
    {
        return;
    }
    
    NSString *smId =  user.userID;
    NSString *email =  user.profile.email;
    NSArray *name = [user.profile.name componentsSeparatedByString:@" "];
    NSString *firstName = (name.count > 0) ? name[0] : @"";
    NSString *lastName = (name.count > 1) ? name[1] : @"";
    
    //    userProfile.profileImgUrl = person.image.url;
    
    // TODO: change provider to of google+.
    [self performLogin:[StaticData instance].kProviderFacebook.stringValue smId:smId email:email firstName:firstName lastName:lastName];
}

- (void)retrieveFbUserData
{
//    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//        if (!error) {
//            NSString *smId =  [result objectForKey:@"id"] != nil ? [result objectForKey:@"id"] : @"";
//            NSString *email =  [result objectForKey:@"email"] != nil ? [result objectForKey:@"email"] : @"";
//            NSString *firstName = [result objectForKey:@"first_name"] != nil ? [result objectForKey:@"first_name"] : @"";
//            NSString *lastName = [result objectForKey:@"last_name"] != nil ? [result objectForKey:@"last_name"] : @"";
//            
//            userProfile.profileImgUrl = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", smId];
//
//            
//            [self performLogin:[StaticData instance].kProviderFacebook.stringValue smId:smId email:email firstName:firstName lastName:lastName];
//        } else {
//            [HUD hide:YES];
//            NSLog(@"fbrequestconnection error: %@", [error localizedDescription]);
//            [Methods showAlertView:NSLocalizedString(@"Oops!", nil) message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
//        }
//    }];
//    [HUD show:YES];
    
    if ([FBSDKAccessToken currentAccessToken])
    {
        //        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email, birthday, bio ,location ,friends ,hometown , friendlists"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 NSString *smId =  [result objectForKey:@"id"] != nil ? [result objectForKey:@"id"] : @"";
                 NSString *email =  [result objectForKey:@"email"] != nil ? [result objectForKey:@"email"] : @"";
                 NSString *firstName = [result objectForKey:@"first_name"] != nil ? [result objectForKey:@"first_name"] : @"";
                 NSString *lastName = [result objectForKey:@"last_name"] != nil ? [result objectForKey:@"last_name"] : @"";
                 
                 //            userProfile.profileImgUrl = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", smId];
                 
                 if ([email isEqualToString:@""])
                 {
                     [HUD hide:YES];
                     //                                 [[FBSDKAccessToken clo];
                     [Methods showAlertView:NSLocalizedString(@"Oops!", nil) message:@"Your facebook email address is not accessible" buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
                 }
                 else
                 {
                     
                     [self performLogin:[StaticData instance].kProviderFacebook.stringValue smId:smId email:email firstName:firstName lastName:lastName];
                 }
                 
                 
             }
             else
             {
                 [HUD hide:YES];
                 NSLog(@"fbrequestconnection error: %@", [error localizedDescription]);
                 [Methods showAlertView:NSLocalizedString(@"Oops!", nil) message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
             }
         }];
        
    }
    
    [HUD show:YES];
}

- (void)performLogin:(NSString *)provider smId:(NSString *)smId  email:(NSString *)email firstName:(NSString *)firstName lastName:(NSString *)lastName {
    NSString *apiUrl = [NSString stringWithFormat:@"%@", [StaticData instance].baseUrl];
    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"login"]];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setValue:firstName forKey:@"fname"];
    [postParams setValue:lastName forKey:@"lname"];
    [postParams setValue:email forKey:@"email"];
    [postParams setValue:@"" forKey:@"pass"];
    [postParams setValue:smId forKey:@"sm_id"];
    [postParams setValue:provider forKey:@"provider"];
    [postParams setValue:@"0" forKey:@"signup"];
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:apiUrl parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [HUD hide:YES];
        userProfile.provider = provider;
        userProfile.smId = smId;
        userProfile.email = email;
//        [userProfile setFirstName:firstName];
//        [userProfile setLastName:lastName];
        [self handleLoginSuccess:JSON];
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [HUD hide:YES];
        [Methods showAlertView:@"" message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }];
    [operation start];
    [HUD setLabelText:NSLocalizedString(@"Please Wait.", nil)];
    [HUD setDetailsLabelText:NSLocalizedString(@"Signing in ...", nil)];
    [HUD show:YES];
}



- (void)customButton:(UIButton *)button borderColor:(UIColor *)borderColor {
    UIColor *colorNormal = [UIColor clearColor];
    UIColor *colorPressed = [[UIColor redColor] colorWithAlphaComponent:0.4f];
    CGSize buttonSize = button.bounds.size;
    [button setImage:[Methods imageWithColor:colorNormal size:buttonSize] forState:UIControlStateNormal];
    [button setImage:[Methods imageWithColor:colorPressed size:buttonSize] forState:UIControlStateHighlighted];
    
    [button setClipsToBounds:YES];
    [[button layer] setCornerRadius:button.bounds.size.width/2];
    [[button layer] setBorderWidth:2.0f];
    [[button layer] setBorderColor:borderColor.CGColor];
}



- (BOOL)validateInputData {
    if ([Methods validateTextFieldData:self tf:emailTF tfName:@"Email" shouldTrim:YES showMsg:YES]) {
        if([Methods validateTextFieldData:self tf:passwordTF tfName:@"Password" shouldTrim:YES showMsg:YES]) {
            [focusedTF resignFirstResponder];
            return YES;
        }
        else
            return NO;
    }
    
    return NO;
}

- (void)performLogin:(NSString *)username password:(NSString *)password {
    NSString *apiUrl = [NSString stringWithFormat:@"%@", [StaticData instance].baseUrl];
    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"login"]];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setValue:username forKey:@"email"];
    [postParams setValue:password forKey:@"pass"];
    [postParams setValue:@"0" forKey:@"sm_id"];
    [postParams setValue:@"0" forKey:@"provider"];
    [postParams setValue:@"0" forKey:@"signup"];
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:apiUrl parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [HUD hide:YES];
        [self handleLoginSuccess:JSON];
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [HUD hide:YES];
        [Methods showAlertView:@"" message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }];
    [operation start];
    [HUD setLabelText:NSLocalizedString(@"Please Wait.", nil)];
    [HUD setDetailsLabelText:NSLocalizedString(@"Signing in ...", nil)];
    [HUD show:YES];
}

- (void)handleLoginSuccess:(id)JSON
{
    NSString *status = [JSON objectForKey:@"status"];
    NSString *message = [JSON objectForKey:@"message"];
    if ([status intValue] == 1) {
        NSArray *dataArray = [JSON objectForKey:@"data"];
        NSDictionary *dataDict = [dataArray objectAtIndex:0];
        
        userProfile.userIdEmail = [dataDict objectForKey:@"uid"];
        userProfile.userName = [dataDict objectForKey:@"user_name"];
        //        userProfile.lastName = [dataDict objectForKey:@"last_name"];
        userProfile.profileImgUrl = [dataDict objectForKey:@"profile_pic"];
        userProfile.coverImgUrl = [dataDict objectForKey:@"cover_photo"];
        userProfile.email = [emailTF.text copy];
        userProfile.smId = @"0";
        userProfile.provider = @"0";
        
        NSData *userProfileData = [NSKeyedArchiver archivedDataWithRootObject:userProfile];
        [defaults setObject:userProfileData forKey:[StaticData instance].kUserProfileData];
        [defaults setBool:YES forKey:[StaticData instance].kLoggedIn];
        [defaults synchronize];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:kReferringUsersArray])
        {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:kReferringUsersArray] count]>0)
            {
                [ReuseAbleFunctions addFriends];
            }
        }
        
        //NSString *toastMessage = [NSString stringWithFormat:@"Welcom %@ %@. You are now signed in.", userProfile.fistName, userProfile.lastName];
        //[self.view makeToast:toastMessage duration:3.0f position:@"bottom"];
        [PiwikTracker sharedInstance].userID = [dataDict objectForKey:@"uid"];
        // Piwik Event (Action Login)
        [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_USERS action:ACTION_LOGIN name:[ReuseAbleFunctions convertDictionaryToJSONString:dataDict] value:nil];
        [self popVC];
    } else {
        [Methods showAlertView:@"" message:message buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }
}

- (void) popVC
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
