//
//  LikeVC.h
//  Rightips
//
//  Created by Mac on 08/01/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LikeVC : UIViewController <UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate>
{
    
    IBOutlet UITableView *likeTableView;
    
    IBOutlet UITableView *likerTableView;
    
    IBOutlet UILabel *likeLabel;
    
}
@property(nonatomic,strong)UserProfile *userProfile;

@end
