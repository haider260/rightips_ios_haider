//
//  MapVC.h
//  Rightips
//
//  Created by Esol on 12/05/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "RatingView.h"

@interface MapVC : UIViewController <MKMapViewDelegate,RatingViewDelegate,UIActionSheetDelegate>

@property (nonatomic, retain) NotificationData *selNotxData;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *topView;
@property (strong, nonatomic) IBOutlet RatingView *ratingView;
@property (strong, nonatomic) IBOutlet UIView *routeDetailView;
@property (strong, nonatomic) IBOutlet UIView *selectedRouteLineView;

@property (nonatomic, retain) IBOutlet UILabel *lblName;
@property (nonatomic, retain) IBOutlet UILabel *lblTitle;
@property (nonatomic, retain) IBOutlet UILabel *lblDate;
@property (nonatomic, retain) IBOutlet UILabel *lblRouteDistance;
@property (nonatomic, retain) IBOutlet UILabel *lblRouteTime;
@property (nonatomic, retain) IBOutlet UILabel *lblNotifications;

@property (nonatomic, retain) IBOutlet UIButton *menuToggleButton;
@property (nonatomic, retain) IBOutlet UIButton *btnUdateLocation;
@property (nonatomic, retain) IBOutlet UIButton *btnDrawRoute;

@property (nonatomic, retain) IBOutlet UIButton *btnDrawWalkingRoute;
@property (nonatomic, retain) IBOutlet UIButton *btnDrawDriveRoute;
@property (nonatomic, retain) IBOutlet UIButton *btnDrawTransitRoute;
@property (nonatomic, retain) IBOutlet UIButton *btnBack;
@property (nonatomic, retain) IBOutlet UIButton *btnNotifications;
//@property (nonatomic,strong) NSNumber *latitude;
//@property (nonatomic,strong) NSNumber *longitude;
@property(nonatomic,strong)UserLocation *userLoc;

@property (strong, nonatomic) IBOutlet UIView *siteInfoView;



- (IBAction)btnUdateLocation:(id)sender;
- (IBAction)btnDrawRoute:(id)sender;
- (IBAction)btnDrawWalkingRoute:(id)sender;
- (IBAction)btnDrawDriveRoute:(id)sender;
- (IBAction)btnDrawTransitRoute:(id)sender;
- (IBAction)btnBack:(id)sender;
- (IBAction)btnNotifications:(id)sender;

@end
