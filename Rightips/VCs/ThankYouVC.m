//
//  ThankYouVC.m
//  Rightips
//
//  Created by Abdul Mannan on 12/8/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import "ThankYouVC.h"

@interface ThankYouVC () <BWBeaconManagerDelegate>
{
    BWBeaconManager *_beaconManager;
    BWBeaconRegion *_beaconRegion;
}

@end

@implementation ThankYouVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    
    //[StaticData instance].reachedThankYouScreen = YES;
    [self.view makeToast:NSLocalizedString(@"Settings saved successfuly.", nil) duration:3.0f position:@"bottom"];
    
    _beaconManager = [[BWBeaconManager alloc] init];
    _beaconManager.delegate = self;
    
    if ([_beaconManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        [_beaconManager requestAlwaysAuthorization];
    
    NSString *uuidStr = @"5144A35F-387A-7AC0-B254-DFB1381C9DFF";
    NSString *idStr = @"com.beaconwatcher";
    NSUUID *proximityUUID = [[NSUUID alloc] initWithUUIDString:uuidStr];
    _beaconRegion = [[BWBeaconRegion alloc] initWithProximityUUID:proximityUUID identifier:idStr];
    //_beaconRegion.notifyEntryStateOnDisplay = YES;
    _beaconRegion.notifyOnEntry = YES;
    _beaconRegion.notifyOnExit = YES;
    
    [_beaconManager startMonitoringForRegion:_beaconRegion];
    [_beaconManager startRangingBeaconsInRegion:_beaconRegion];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //Sending current screen to Piwic Tracker
   // [[PiwikTracker sharedInstance] sendView:@"Thankyou"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)beaconManager:(BWBeaconManager *)manager didDetermineState:(CLRegionState)state forRegion:(BWBeaconRegion *)region {
    if (state == CLRegionStateInside) {
        UIApplicationState appState = [[UIApplication sharedApplication] applicationState];
        if (appState == UIApplicationStateActive) {
            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            [Methods generateNotification:NSLocalizedString(@"Touch to view content", nil)];
        }
    }
}

@end
