//
//  HomeVC.h
//  Rightips
//
//  Created by Abdul Mannan on 12/30/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyWeatherView.h"

@interface HomeVC : UIViewController <UIImagePickerControllerDelegate,UIActionSheetDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,UIAlertViewDelegate>
{
    
    IBOutlet UILabel *noNotificationLabel;
    
    
    IBOutlet UITextField *searchTextfield;
    
    
    IBOutlet UIView *textFieldView;
    
    
    IBOutlet UIScrollView *scrlVu;
    
    
    IBOutlet UIButton *searchBtn;
    
    IBOutlet UITableView *searchResultTableView;
    
    
    IBOutlet UIImageView *backGroundImageView;
    
   
    
    IBOutlet UIButton *locationButton;
    
    
    IBOutlet UIButton *searchPlaceButton;
    
//    IBOutlet UIView *mainView;
    
    
}

@property (nonatomic,assign) IBOutlet MyWeatherView *myWeatherView;

@end
