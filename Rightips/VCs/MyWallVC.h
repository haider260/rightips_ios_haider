//
//  MyWallVC.h
//  Rightips
//
//  Created by Mac on 09/12/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyWallVC : UIViewController <UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate>
{
   
    IBOutlet UITableView *myWallTableView;
    
    IBOutlet UIButton *menuToggleButton;
    
    IBOutlet UIButton *muteButton;
    
    IBOutlet UIView *myWallView;
    
}

@end
