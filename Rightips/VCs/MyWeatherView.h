//
//  WeatherView.h
//  Rightips
//
//  Created by Shahbaz Ali on 8/16/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyWeatherView : UIView
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (strong, nonatomic) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet UILabel *lblTemperature1;
-(void) updateCurrentWeather;
-(void) updateWeatherForecast;

@end
