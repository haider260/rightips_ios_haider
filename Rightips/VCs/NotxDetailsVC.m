//
//  NotxDetailsVC.m
//  Rightips
//
//  Created by Abdul Mannan on 1/5/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//
#import "NotxDetailsVC.h"
#import "MMMWhatsAppActivity.h"
#import <Social/Social.h>
#import "MapVC.h"
#import "NotxCommentsVC.h"
#import "JBWhatsAppActivity.h"
#import "FBActivity.h"
#import <MessageUI/MessageUI.h>
#import <CoreText/CoreText.h>
#import "CommentsCellTableViewCell.h"
#import "ImageCaptureView.h"
#import "ImageCaptureView.h"
#import "YTPlayerView.h"


@interface NotxDetailsVC () <SwipeViewDelegate, SwipeViewDataSource, MFMailComposeViewControllerDelegate>
{
    SlideMenu *slideMenu;
    DbHelper *dbHelper;
    MBProgressHUD *HUD;
    UILabel *descLabel;
    UIButton *phoneButton,*btnFacebook,*btnInstagram,*btnGooglePlus,*btnCatLink,*btnEmail,*delButton;
    UIView *socialMediaContainer;
    BOOL isLoggedIn;
    NSString *tempString;
    NSMutableArray *allReviewList;
    NSInteger ind;
    ImageCaptureView *view;
    NSUserDefaults *defaults;
    UserProfile *currentuserProfile,*specificUser;
    BOOL isLiked,isLikedReview;

    
    IBOutlet YTPlayerView *playerView;
   
}
@property (weak, nonatomic) IBOutlet UILabel *likesBadge;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;

@property (nonatomic, retain) IBOutlet UIButton *menuToggleButton;
@property (nonatomic, retain) IBOutlet UIButton *muteButton;
@property (strong, nonatomic) IBOutlet UIView *infoContainer;
@property (strong, nonatomic) IBOutlet UIView *infoSubCont1;


@property (nonatomic, retain) IBOutlet UIScrollView *notxTextSV;
@property (nonatomic, strong) IBOutlet SwipeView *swipeView;
@property (nonatomic, strong) IBOutlet UILabel *discountLabel;
@property (nonatomic, retain) IBOutlet UIView *buttonBottomView;
@property (strong, nonatomic) IBOutlet UIView *infoSubCont2;

//@property (nonatomic, retain) IBOutlet UIView *socialMediaContainer;

@property (strong, nonatomic) IBOutlet UIButton *mapButton;

@property (strong, nonatomic) IBOutlet UIButton *commentsButton;

@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
//@property (nonatomic, retain) IBOutlet UILabel *descLabel;
@property (nonatomic, retain) IBOutlet UILabel *timeLabel;
@property (nonatomic, retain) IBOutlet UILabel *addrLabel;
//@property (nonatomic, retain) IBOutlet UIButton *phoneButton;

//@property (nonatomic, retain) IBOutlet UIButton *threeDotsButton;
@property (nonatomic, retain) IBOutlet UIImageView *tempFriendIV;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@property (nonatomic, retain) IBOutlet UIView *reviewContainer;
@property (nonatomic, retain) IBOutlet UITextView *reviewTextView;

@end

@implementation NotxDetailsVC

@synthesize selNotxData;

@synthesize menuToggleButton;
@synthesize notxTextSV;
@synthesize discountLabel, buttonBottomView;
@synthesize infoContainer, infoSubCont1, infoSubCont2, titleLabel, timeLabel, addrLabel, mapButton, tempFriendIV,likeButton,shareButton,likesBadge;
@synthesize reviewContainer, reviewTextView;


#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    slideMenu = [[SlideMenu alloc] init];
    [self.view addSubview:slideMenu];
    
    defaults = [NSUserDefaults standardUserDefaults];
    allReviewList = [[NSMutableArray alloc] init];
    
    currentuserProfile=[NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData]];

    UISwipeGestureRecognizer *swipeLeftMenu = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToLeftWithGestureRecognizer:)];
    swipeLeftMenu.direction = UISwipeGestureRecognizerDirectionLeft;
    [slideMenu addGestureRecognizer:swipeLeftMenu];
   
    
    [self.view bringSubviewToFront:menuToggleButton];
    [self.view bringSubviewToFront:self.muteButton];
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    [likesBadge setText:selNotxData.noOfLikes];
    
    [likesBadge sizeToFit];
    if(likesBadge.frame.size.width<16.0)
    {
        [likesBadge setFrame:CGRectMake(likesBadge.frame.origin.x, likesBadge.frame.origin.y, 16.0f, 16.0f)];
    }
    [likesBadge.layer setCornerRadius:8.0f];
    [likesBadge setClipsToBounds:YES];
    dbHelper = [[DbHelper alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraButtonTapped) name:@"TapOnCameraButton" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeImage) name:@"changeImage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRangeBeacons:) name:@"didRangeBeacons" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeMuteImage:) name:@"changeMuteImage" object:nil];
    [self changeMuteImage:nil];
    
    _swipeView.alignment = SwipeViewAlignmentCenter;
    _swipeView.pagingEnabled = YES;
    _swipeView.itemsPerPage = 1;
    _swipeView.truncateFinalPage = YES;
    
    [discountLabel setClipsToBounds:YES];
    [[discountLabel layer] setCornerRadius:5.0f];
    
    [tempFriendIV setClipsToBounds:YES];
    [[tempFriendIV layer] setCornerRadius:tempFriendIV.bounds.size.width/2];
    
 /*   UIColor *borderColor = [UIColor colorWithRed:0.5059f green:0.5059f blue:0.5059f alpha:1.0f];
    phoneButton.layer.borderWidth = 1.0f;
    phoneButton.layer.borderColor = borderColor.CGColor;
    mapButton.layer.borderWidth = 1.0f;
    mapButton.layer.borderColor = borderColor.CGColor;
  */
//    threeDotsButton.layer.borderWidth = 1.0f;
//    threeDotsButton.layer.borderColor = borderColor.CGColor;
    
    reviewTextView.layer.borderColor = [UIColor grayColor].CGColor;
    reviewTextView.layer.borderWidth = 0.5f;
    [Methods setupKeyboardForTextView:reviewTextView];
    
    descLabel = [[UILabel alloc] init];
    [descLabel setFrame:CGRectMake(0, 0, notxTextSV.frame.size.width,140)];
    [descLabel setNumberOfLines:0];
    [descLabel setLineBreakMode:UILineBreakModeWordWrap];
    [descLabel setTextColor:[UIColor darkGrayColor]];
    [notxTextSV addSubview:descLabel];
    
    socialMediaContainer = [[UIView alloc] initWithFrame:CGRectMake(0, descLabel.bounds.size.height, notxTextSV.frame.size.width, 30)];
    [socialMediaContainer setBackgroundColor:[UIColor whiteColor]];
    
    phoneButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [phoneButton setFrame:CGRectMake(0, 0, 30, 30)];
    [phoneButton setBackgroundImage: [UIImage imageNamed:@"PhoneIcon"] forState:UIControlStateNormal];
    [phoneButton setBackgroundImage: [UIImage imageNamed:@"call_inactive"] forState:UIControlStateDisabled];
    [phoneButton addTarget:self action:@selector(phoneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [socialMediaContainer addSubview:phoneButton];
    
    btnFacebook = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btnFacebook setFrame:CGRectMake(50, 0, 30, 30)];
    [btnFacebook setBackgroundImage:[UIImage imageNamed:@"sm_fb_active"] forState:UIControlStateNormal];
    [btnFacebook setBackgroundImage:[UIImage imageNamed:@"sm_fb_inactive"] forState:UIControlStateDisabled];
    [btnFacebook addTarget:self action:@selector(btnFacebookPressed:) forControlEvents:UIControlEventTouchUpInside];
    [socialMediaContainer addSubview:btnFacebook];
    
    
    btnInstagram = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btnInstagram setFrame:CGRectMake(100, 0, 30, 30)];
    [btnInstagram setBackgroundImage:[UIImage imageNamed:@"sm_instagram_active"] forState:UIControlStateNormal];
    [btnInstagram setBackgroundImage:[UIImage imageNamed:@"sm_instagram_inactive"] forState:UIControlStateDisabled];
    [btnInstagram addTarget:self action:@selector(btnInstgramPressed:) forControlEvents:UIControlEventTouchUpInside];
    [socialMediaContainer addSubview:btnInstagram];
    
    btnGooglePlus = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btnGooglePlus setFrame:CGRectMake(150, 0, 30, 30)];
    [btnGooglePlus setBackgroundImage:[UIImage imageNamed:@"sm_gp_active"] forState:UIControlStateNormal];
    [btnGooglePlus setBackgroundImage:[UIImage imageNamed:@"sm_gp_inactive"] forState:UIControlStateDisabled];
    [btnGooglePlus addTarget:self action:@selector(btnGooglePlusPressed:) forControlEvents:UIControlEventTouchUpInside];
    [socialMediaContainer addSubview:btnGooglePlus];
    
    btnEmail = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btnEmail setFrame:CGRectMake(200, 0, 30, 30)];
    [btnEmail setBackgroundImage:[UIImage imageNamed:@"sm_mail_active"] forState:UIControlStateNormal];
    [btnEmail setBackgroundImage:[UIImage imageNamed:@"sm_mail_inactive"] forState:UIControlStateDisabled];
    [btnEmail addTarget:self action:@selector(btnEmailPressed:) forControlEvents:UIControlEventTouchUpInside];
    [socialMediaContainer addSubview:btnEmail];
    
    btnCatLink = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btnCatLink setFrame:CGRectMake(250, 0, 30, 30)];
    [btnCatLink setBackgroundImage:[UIImage imageNamed:@"home_active"] forState:UIControlStateNormal];
    [btnCatLink setBackgroundImage:[UIImage imageNamed:@"home_inactive"] forState:UIControlStateDisabled];
    [btnCatLink addTarget:self action:@selector(btnCatLink:) forControlEvents:UIControlEventTouchUpInside];
    [socialMediaContainer addSubview:btnCatLink];
    
    [notxTextSV addSubview:socialMediaContainer];
    
    
    
    
    [self populateNotificationData];
    [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
    
}
- (void)scrollingTimer
{
    CGFloat contentOffset = self.swipeView.scrollView.contentOffset.x;
    // calculate next page to display
    int nextPage = (int)(contentOffset/self.swipeView.scrollView.frame.size.width) + 1 ;
    if( nextPage < self.selNotxData.images.count)
    {
        [self.swipeView.scrollView scrollRectToVisible:CGRectMake(nextPage*self.swipeView.scrollView.frame.size.width, 0, self.swipeView.scrollView.frame.size.width, self.swipeView.scrollView.frame.size.height) animated:YES];
//        self.swipeView.currentPage=nextPage;
        // else start sliding form 1 :)
    } else {
        [self.swipeView.scrollView scrollRectToVisible:CGRectMake(0, 0, self.swipeView.scrollView.frame.size.width, self.swipeView.scrollView.frame.size.height) animated:YES];
//        self.swipeView.currentPage=0;
    }
}
// Rating view delegate method
- (void)rateChanged:(RatingView *)sender
{

    AddReviewVC *vc = (AddReviewVC *)[Methods getVCbyName:@"AddReviewVC"];
    [vc setRatingValue:sender.value];
    [vc setNotificationdata:self.selNotxData];
    [self presentViewController:vc animated:YES completion:nil];
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self fetchDataFromServer];
    
    self.rateview = [[RatingView alloc] initWithFrame:self.rateview.frame
                                    selectedImageName:@"GreyStarIcon"
                                      unSelectedImage:@"GreyStarIcon"
                                             minValue:0
                                             maxValue:5
                                        intervalValue:1
                                           stepByStep:NO];
    self.rateview.value = 0;
    [self.rateview setDelegate:self];
    [rateInfoView addSubview:self.rateview];
    
    
    rateView = [[RatingView alloc] initWithFrame:rateView.frame
                                    selectedImageName:@"RedStarIcon"
                                      unSelectedImage:@"GreyStarIcon"
                                             minValue:0
                                             maxValue:5
                                        intervalValue:1
                                           stepByStep:NO];
    rateView.value = 3;
    [rateView setUserInteractionEnabled:NO];
    [infoSubCont1 addSubview:rateView];

    if ([selNotxData.sitePhone isEqualToString:@""] || selNotxData.sitePhone == nil)
    {
        phoneButton.enabled = NO;
    }
    else
    {
        phoneButton.enabled = YES;
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    isLoggedIn = [defaults boolForKey:[StaticData instance].kLoggedIn];
    
    if (tempString && isLoggedIn)
    {
        if ([tempString isEqualToString:@"Like"])
        {
            tempString = nil;
            [self likeButtonTapped:nil];
        }
        else if ([tempString isEqualToString:@"Share"])
        {
            tempString = nil;
            [self shareButtonTapped:nil];
        }
        else if ([tempString isEqualToString:@"Comment"])
        {
            tempString = nil;
            [self goToCommentVC];
        }
    }
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    if (notxTextSV.frame.size.height < descLabel.bounds.size.height+self.socialMediaContainer.bounds.size.height)
//    {
    
   
    
//    CGSize expectedLabelSize = [descLabel.text sizeWithFont:descLabel.font
//                                      constrainedToSize:maximumLabelSize
//                                          lineBreakMode:descLabel.lineBreakMode];
//    
//    //adjust the label the the new height.
//    CGRect newFrame = descLabel.frame;
//    newFrame.size.height = expectedLabelSize.height;
//    descLabel.frame = newFrame;
    
//     notxTextSV.contentSize = CGSizeMake(notxTextSV.frame.size.width, 250);
//        CGRect smFrame = self.socialMediaContainer.frame;
//        smFrame.origin.y = descLabel.bounds.size.height;
//        self.socialMediaContainer.frame = smFrame;
//        notxTextSV.contentSize = CGSizeMake(notxTextSV.frame.size.width, descLabel.bounds.size.height+self.socialMediaContainer.bounds.size.height+10);
    
    
//
//    }

    //Sending current screen to Piwic Tracker
    //[[PiwikTracker sharedInstance] sendView:@"Notification Detail"];
    
    //Sending Event To Piwik (Notification Opened)
    NSMutableDictionary *dataDictForPiwik = [self getNotificationDictForPiwik];
    [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_NOTIFICATION action:ACTION_VIEW name:[ReuseAbleFunctions convertDictionaryToJSONString:dataDictForPiwik] value:nil];

}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
{
    
}
-(void)goToCommentVC
{
    NotxCommentsVC *vc = (NotxCommentsVC *)[Methods getVCbyName:@"NotxCommentsVC"];
    [vc setSelNotxData:selNotxData];
    [Methods pushVCinNCwithObj:vc popTop:NO];
}

#pragma mark - Memory Warnings
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SwipeGesture Methods
-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer
{
    [slideMenu showHideMenu];
}

#pragma mark - Fetching And Populating Notification Data

-(void) fetchDataFromServer
{
    [HUD setLabelText:@"..."];
    [HUD show:YES];
    
    NSString *apiURL=@"http://beaconwatcher.com/api/index.php?action=getSpecificNotifications";
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setObject:[StaticData instance].apiKey forKey:@"key"];
    [postParams setObject:selNotxData.notxId forKey:@"nt_id"];
    NSURL *url = [[NSURL alloc] initWithString:apiURL];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiURL parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [HUD hide:YES];
        if([[JSON objectForKey:@"status"] integerValue]==1)
        {
            id notxObj=[[JSON objectForKey:@"data"] objectAtIndex:0];
            NotificationData *nd = [ReuseAbleFunctions getNotificationDataFromObject:notxObj];
            selNotxData = nd;
            [self populateNotificationData];
        }
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        [HUD hide:YES];
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
    }];
    [operation start];
}

- (void)populateNotificationData
{
    [self setSocialMediaButtons];
    
    if (![selNotxData.videoUrl isEqualToString:@""] && selNotxData.videoUrl)
    {
        NSLog(@"video url =%@",selNotxData.videoUrl);
        [self.swipeView setHidden:YES];
//        [playerView loadWithVideoId:@"raQq5TNEvvQ"];
        [playerView loadWithVideoId:[self.selNotxData.videoUrl substringFromIndex:[self.selNotxData.videoUrl length] - 11]];
    }
    else
    {
        [self.swipeView setHidden:NO];
        [self.swipeView reloadData];
    }
    [_swipeView reloadData];
    isLiked=NO;
    

    if ([[NSUserDefaults standardUserDefaults] boolForKey:[StaticData instance].kLoggedIn] == YES)
    {
        if (![selNotxData.likes isEqual:@""])
        {
        
        for(NSDictionary *likes in selNotxData.likes)
        {
            if([[likes objectForKey:@"uid"] isEqualToString:currentuserProfile.userIdEmail])
            {
                isLiked=YES;
            }
        }
        }
    }
    if(isLiked)
    {
        [likeButton setImage:[UIImage imageNamed:@"BtnLike"] forState:UIControlStateNormal];
//        [likeButton setUserInteractionEnabled:NO];
    }
    else
    {
        [likeButton setImage:[UIImage imageNamed:@"Liked"] forState:UIControlStateNormal];
    }
    
    [likesBadge setText:selNotxData.noOfLikes];
    
    [likesBadge sizeToFit];
    if(likesBadge.frame.size.width<16.0)
    {
        [likesBadge setFrame:CGRectMake(likesBadge.frame.origin.x, likesBadge.frame.origin.y, 16.0f, 16.0f)];
    }
    [likesBadge.layer setCornerRadius:8.0f];
    [likesBadge setClipsToBounds:YES];
    
    if (selNotxData.discount != nil && ![selNotxData.discount isEqualToString:@""] && ![selNotxData.discount isEqualToString:@"0"])
    {
        discountLabel.text = [NSString stringWithFormat:@"%@", selNotxData.discount];

        [discountLabel sizeToFit];
        
        float discountLabelMaximumWidth = shareButton.frame.origin.x - likeButton.frame.origin.x - shareButton.frame.size.width - 30;
        
        if (discountLabel.frame.size.width > discountLabelMaximumWidth)
        {
            discountLabel.frame = CGRectMake(discountLabel.frame.origin.x, discountLabel.frame.origin.y, discountLabelMaximumWidth, discountLabel.frame.size.height + 15);
            discountLabel.adjustsFontSizeToFitWidth = YES;
        }
        else
        {
            discountLabel.frame = CGRectMake(discountLabel.frame.origin.x, discountLabel.frame.origin.y, discountLabel.frame.size.width + 15, discountLabel.frame.size.height + 15);
        }
        [discountLabel setCenter:CGPointMake(self.view.center.x, discountLabel.frame.origin.y + discountLabel.frame.size.height/2)];
    }
    else
        [discountLabel setHidden:YES];
   
    titleLabel.text = selNotxData.title;
//    [self.descriptionTextView setText:selNotxData.detail];
    
    descLabel.text = selNotxData.detail;
    [descLabel sizeToFit];
    
    [socialMediaContainer setFrame:CGRectMake(0, descLabel.bounds.size.height + 10, notxTextSV.frame.size.width, 30)];
    
     notxTextSV.contentSize = CGSizeMake(notxTextSV.frame.size.width, descLabel.bounds.size.height + socialMediaContainer.bounds.size.height + 10);
    
    
    timeLabel.text = [Methods getDealDateStrFromNotx:selNotxData];
   // addrLabel.text = [NSString stringWithFormat:@"%@\n%@\n%@", selNotxData.siteName, selNotxData.siteAddress, selNotxData.sitePhone];
    self.lblSiteName.text = selNotxData.siteName;
    addrLabel.text = [NSString stringWithFormat:@"%@\n%@", selNotxData.siteAddress, selNotxData.sitePhone];
    
    
  /*  CGRect infoSubCont1Frame = infoSubCont1.frame;
    infoSubCont1Frame.size.height = 30+descLabel.frame.size.height;
    infoSubCont1.frame = infoSubCont1Frame;
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    if (screenRect.size.height == 480 && infoSubCont1.frame.size.height < 80) {
        CGRect frame = infoSubCont1.frame;
        frame.size.height = 80;
        infoSubCont1.frame = frame;
    } else if (screenRect.size.height == 568 && infoSubCont1.frame.size.height < 170) {
        CGRect frame = infoSubCont1.frame;
        frame.size.height = 170;
        infoSubCont1.frame = frame;
    }
    
    CGRect infoSubCont2Frame = infoSubCont2.frame;
    infoSubCont2Frame.origin.y = infoSubCont1.frame.size.height+10;
    infoSubCont2.frame = infoSubCont2Frame;
    
    CGRect infoContFrame = infoContainer.frame;
    infoContFrame.size.height = infoSubCont1.frame.size.height+infoSubCont2.frame.size.height;
    infoContainer.frame = infoContFrame;
    */
    [self infoButtonPressed:nil];
}

- (void) setSocialMediaButtons
{
    btnFacebook.enabled = YES;
    btnInstagram.enabled = YES;
    btnGooglePlus.enabled = YES;
    btnEmail.enabled = YES;
    btnCatLink.enabled = YES;
    
    if ([selNotxData.fbUrl isEqualToString:@""] || selNotxData.fbUrl == nil)
    {
        btnFacebook.enabled = NO;
    }
    
    if ([selNotxData.instagramUrl isEqualToString:@""] || selNotxData.instagramUrl == nil)
    {
        btnInstagram.enabled = NO;
    }
    
    if ([selNotxData.googlePlusUrl isEqualToString:@""] || selNotxData.googlePlusUrl == nil)
    {
        btnGooglePlus.enabled = NO;
    }
    
    if ([selNotxData.mailUrl isEqualToString:@""] || selNotxData.mailUrl == nil)
    {
        btnEmail.enabled = NO;
    }
    if ([selNotxData.catLink isEqualToString:@""] || selNotxData.catLink == nil) {
        btnCatLink.enabled = NO;
    }
}

#pragma mark - Did Range Beacons
- (void)didRangeBeacons:(NSNotification *)notification
{
    NotificationData *nnd = [BwMethods getNotificationDataFromRangedBeacons:[notification object]];
    if (nnd != nil)
    {
        [dbHelper insertNotxToDB:nnd];
        [self setSelNotxData:nnd];
        [self populateNotificationData];
    }
}

#pragma mark - Swipe View Methods
- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeVie
{
    return selNotxData.images.count;
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    AsyncImageView *imageView = [[AsyncImageView alloc] initWithFrame:swipeView.bounds];
    [imageView setActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [imageView setContentMode:UIViewContentModeScaleAspectFill];

    //imageView.image = [UIImage imageNamed:@"ImageNA"];
    NSString *imgUrl = [selNotxData.images objectAtIndex:index];
    [imageView setImageURL:[NSURL URLWithString:imgUrl]];
//    [Methods setImageInImageViewFromUrl:imageView urlStr:imgUrl];
    

    return imageView;
}


#pragma mark - Change Mute Image
- (void)changeMuteImage:(NSNotification *)notification
{
    if ([[NSUserDefaults standardUserDefaults] doubleForKey:@"muteEndTime"] && ([[NSUserDefaults standardUserDefaults] doubleForKey:@"muteEndTime"] != 0))
    {
        [self.muteButton setImage:[UIImage imageNamed:@"BtnMute"] forState:UIControlStateNormal];
    }
    else
    {
        [self.muteButton setImage:[UIImage imageNamed:@"BtnUnmuted"] forState:UIControlStateNormal];
    }
}

#pragma mark - IBActions
- (IBAction)btnComments:(id)sender
{
   
    if (isLoggedIn)
    {
        [self goToCommentVC];
    }
    else
    {
        
        UIAlertView  *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Oops!", nil) message:NSLocalizedString(@"You are not logged in Kindly login to continue.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:NSLocalizedString(@"Login", nil), nil];
        [alert setTag:3];
        [alert show];
    }
   
}
- (IBAction)slideMenuButtonPressed:(id)sender
{
    [slideMenu showHideMenu];
}

- (IBAction)muteButtonPressed:(id)sender
{
    [Methods showMuteNotxActionSheet];
}

- (IBAction)arrowLeftButtonPressed:(id)sender
{
    [_swipeView scrollToItemAtIndex:_swipeView.currentItemIndex-1 duration:0.5f];
}

- (IBAction)arrowRightButtonPressed:(id)sender
{
    [_swipeView scrollToItemAtIndex:_swipeView.currentItemIndex+1 duration:0.5f];
}

- (IBAction)infoButtonPressed:(id)sender
{
    CGRect infoContFrame = infoSubCont1.frame;
    CGRect reviewContFrame = reviewContainer.frame;
    CGRect buttonBottomViewFrame = buttonBottomView.frame;
    infoContFrame.origin.x = 0.0f;
    reviewContFrame.origin.x = infoContFrame.size.width;
    buttonBottomViewFrame.origin.x = 10.0f;
    [UIView animateWithDuration:0.5f animations:^{
        infoSubCont1.frame = infoContFrame;
        reviewContainer.frame = reviewContFrame;
        buttonBottomView.frame = buttonBottomViewFrame;
    }];
    
//    notxDetailsSV.contentSize = CGSizeMake(notxDetailsSV.frame.size.width, 270+infoContFrame.size.height);
}

- (IBAction)reviewButtonPressed:(id)sender
{
    CGRect infoContFrame = infoSubCont1.frame;
    CGRect reviewContFrame = reviewContainer.frame;
    CGRect buttonBottomViewFrame = buttonBottomView.frame;
    infoContFrame.origin.x = infoContFrame.size.width * -1.0f;
    reviewContFrame.origin.x = 0.0f;
    buttonBottomViewFrame.origin.x = 160.0f;
    [UIView animateWithDuration:0.5f animations:^{
        infoSubCont1.frame = infoContFrame;
        reviewContainer.frame = reviewContFrame;
        buttonBottomView.frame = buttonBottomViewFrame;
    }];
    [self getReviews];
    
//    notxDetailsSV.contentSize = CGSizeMake(notxDetailsSV.frame.size.width, 250+reviewContFrame.size.height);
}
-(void)getReviews
{
    [HUD setLabelText:NSLocalizedString(@"Fetching Reviews", nil)];
    [HUD show:YES];
    
    NSString *apiUrl;
    
    apiUrl = [NSString stringWithFormat:@"http://www.rightips.com/api/index.php?action=getReviews&site_id=%@&uid=%@",self.selNotxData.siteId,currentuserProfile.userIdEmail];
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiUrl parameters:nil];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
    [HUD hide:YES];
                                             
    if([[JSON objectForKey:@"status"] integerValue] == 1)
    {
                                                 
        [self fetchReviews:JSON];
    }
    else if([[JSON objectForKey:@"status"] integerValue] == 2)
    {
        
        [[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"There are currently no reviews", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
    }
    else if([JSON objectForKey:@"message"])
    {
        
        [[[UIAlertView alloc] initWithTitle:nil message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
    }
        
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
                                             
    [HUD hide:YES];
    }];
    [operation start];
}
-(void)fetchReviews:(id)json
{
    [allReviewList removeAllObjects];
    NSArray *commentsArray = [json objectForKey:@"data"];
    for (NSDictionary *dic in commentsArray)
    {
        UserProfile *user = [[UserProfile alloc] init];
        if ([dic objectForKey:@"name"] != [NSNull null])
        {
            user.userName = [dic objectForKey:@"name"];
        }
        
        user.userIdEmail = [dic objectForKey:@"uid"];
        if ([user.userIdEmail isEqualToString:currentuserProfile.userIdEmail])
        {
            [rateInfoView setHidden:YES];
        }
        
        
//        user.createdAt = [NSString stringWithFormat:@"%@000",[dic objectForKey:@"rev_datemili"]];
                user.createdAt = [dic objectForKey:@"createdDate"];
        user.userReview = [dic objectForKey:@"text"];
        user.reviewRate = [dic objectForKey:@"rev_rate"];
        user.commentImage = [dic objectForKey:@"img"];
        user.profileImgUrl = [dic objectForKey:@"profile_pic"];
        user.userIdEmail = [dic objectForKey:@"uid"];
        user.userLikes = [[dic objectForKey:@"likes"] integerValue];
        user.isLiked = [dic objectForKey:@"is_liked"];
        user.commentId = [dic objectForKey:@"id"];

        
        
        [allReviewList addObject:user];
        
        
    }
    
   
    [notificationReviewTableView reloadData];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [allReviewList count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserProfile *user = [allReviewList objectAtIndex:indexPath.row];
    
     CommentsCellTableViewCell *customCell;
    if  (![user.commentImage isEqual:[NSNull null]] && ![user.commentImage isEqualToString:@""])
    {
        customCell = [[[NSBundle mainBundle] loadNibNamed:@"CommentsCellTableViewCell" owner:nil options:nil] objectAtIndex: 1];
        [customCell.commentImageView setImageURL:[NSURL URLWithString:user.commentImage]];
        
        //        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
        //        [tap addTarget:self action:@selector(imageTapped:)];
        //        [customCell.commentImageView addGestureRecognizer:tap];
        
        
    }
    else
    {
        customCell = [[[NSBundle mainBundle] loadNibNamed:@"CommentsCellTableViewCell" owner:nil options:nil] objectAtIndex: 0];
        
    }
    [customCell.userNameLabel setText:user.userName];
//    [customCell.userNameButton setTag:indexPath.row];
//    [customCell.userNameButton addTarget:self action:@selector(userNameButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [customCell.descriptionLabel setText:user.userReview];

//    [customCell.rateView setValue:user.reviewRate.floatValue];
    
    [customCell.likeButton setTitle:[NSString stringWithFormat:@"%ld Likes",(long)user.userLikes] forState:UIControlStateNormal];
    
    if (user.profileImgUrl && ![user.profileImgUrl isEqual:[NSNull null]])
    {
        [customCell.profileImageView setImageURL:[NSURL URLWithString:user.profileImgUrl]];
    }
    else
    {
        [customCell.profileImageView setImage:[UIImage imageNamed:@"NoProfileImage"]];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *date = [[NSDate alloc] init];
    date = [dateFormatter dateFromString:user.createdAt];
    // converting into our required date format
    [dateFormatter setDateFormat:@"MMM dd yyyy - hh:mma"];
    NSString *reqDateString = [dateFormatter stringFromDate:date];
    [customCell.createdTime setText:reqDateString];
    
//    NSString *timeLeft = [self timeLeftSinceTime:user.createdAt.doubleValue];
//    [customCell.timeLeftLabel setText:timeLeft];
    
//    [customCell.profileImageView setTag:indexPath.row];
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
//    [customCell.profileImageView addGestureRecognizer:tap];
    
    [customCell.deleButton setTag:indexPath.row];
    [customCell.deleButton addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [customCell.likeButton setTag:indexPath.row];
    
    
    
    [customCell.likeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [customCell.likeButton setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
    
    
    if ([user.isLiked isEqualToString:@"0"])
    {
        [customCell.likeButton setSelected:NO];
        if (user.userLikes == 0)
        {
            [customCell.likeButton setTitle:@"Like" forState:UIControlStateNormal];
        }
        else
        {
            [customCell.likeButton setTitle:[NSString stringWithFormat:@"Likes(%ld)",(long)user.userLikes] forState:UIControlStateNormal];
        }
        
        
    }
    else
    {
        [customCell.likeButton setSelected:YES];
        [customCell.likeButton setTitle:[NSString stringWithFormat:@"Likes(%ld)",(long)user.userLikes] forState:UIControlStateSelected];
    }
    
    [customCell.likeButton addTarget:self action:@selector(likeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [customCell.contentView setBackgroundColor:[UIColor clearColor]];
    
    customCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customCell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserProfile *user = [allReviewList objectAtIndex:indexPath.row];
    
    
    ProfileVC *vc = (ProfileVC *)[Methods getVCbyName:@"ProfileVC"];
    [vc setUserProfile:user];
    [Methods pushVCinNCwithObj:vc popTop:NO];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UserProfile *user = [allReviewList objectAtIndex:indexPath.row];
    
    CommentsCellTableViewCell *customCell;
    if  (![user.commentImage isEqual:[NSNull null]] && ![user.commentImage isEqualToString:@""])
    {
        customCell = [[[NSBundle mainBundle] loadNibNamed:@"CommentsCellTableViewCell" owner:nil options:nil] objectAtIndex: 1];
        [customCell.commentImageView setImageURL:[NSURL URLWithString:user.commentImage]];
        
        //        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
        //        [tap addTarget:self action:@selector(imageTapped:)];
        //        [customCell.commentImageView addGestureRecognizer:tap];
        
        
    }
    else
    {
        customCell = [[[NSBundle mainBundle] loadNibNamed:@"CommentsCellTableViewCell" owner:nil options:nil] objectAtIndex: 0];
        
    }
    NSLog(@"%@",user.userName);
    [customCell.userNameLabel setText:user.userName];
    //    [customCell.userNameButton setTag:indexPath.row];
    //    [customCell.userNameButton addTarget:self action:@selector(userNameButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [customCell.descriptionLabel setText:user.userReview];
    [customCell.descriptionLabel sizeToFit];
    
    return  customCell.frame.size.height + customCell.descriptionLabel.frame.size.height;
    
}
-(void)likeButtonPressed:(UIButton *)sender
{
    
    
    [self likeComment:sender];
}
-(void)likeComment:(UIButton *)btn
{
    [HUD setLabelText:@""];
    [HUD show:YES];
    specificUser = [allReviewList objectAtIndex:btn.tag];
    
    NSString *apiURL=@"http://www.rightips.com/api/index.php?action=like";
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setObject:currentuserProfile.userIdEmail forKey:@"uid"];
    [postParams setObject:[NSString stringWithFormat:@"%ld",specificUser.commentId.integerValue] forKey:@"id"];
    [postParams setObject:@"review" forKey:@"type"];
    if ([specificUser.isLiked isEqualToString:@"0"])
    {
        [postParams setObject:@"Like" forKey:@"rel"];
        isLikedReview = YES;
    }
    else
    {
        [postParams setObject:@"Unlike" forKey:@"rel"];
        isLikedReview = NO;
    }
    
    NSURL *url = [NSURL URLWithString:apiURL];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiURL parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [HUD hide:YES];
        if([[JSON objectForKey:@"status"] integerValue] == 1)
        {
            if (isLikedReview)
            {
                [btn setSelected:YES];
                specificUser.userLikes++;
                specificUser.isLiked = @"1";
                [allReviewList replaceObjectAtIndex:btn.tag withObject:specificUser];
                [btn setTitle:[NSString stringWithFormat:@"Likes(%ld)",(long)specificUser.userLikes] forState:UIControlStateSelected];
            }
            else
            {
                [btn setSelected:NO];
                specificUser.userLikes--;
                specificUser.isLiked = @"0";
                [allReviewList replaceObjectAtIndex:btn.tag withObject:specificUser];
                if (specificUser.userLikes == 0)
                {
                    [btn setTitle:[NSString stringWithFormat:@"Like"] forState:UIControlStateNormal];
                }
                else
                {
                    [btn setTitle:[NSString stringWithFormat:@"Likes(%ld)",(long)specificUser.userLikes] forState:UIControlStateNormal];
                }
            }
        }
        else if([JSON objectForKey:@"message"])
        {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
        }
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             [HUD hide:YES];
                                             [Methods showAlertView:@"" message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
                                         }];
    [operation start];
}
-(void)deleteButtonPressed:(UIButton *)sender
{
    UserProfile *user = [allReviewList objectAtIndex:sender.tag];
    [Methods showReportActionSheet:user.commentId.integerValue];

}
-(void)userNameButtonTapped:(UIButton *)sender
{
    UserProfile *user = [allReviewList objectAtIndex:sender.tag];
    
    
    ProfileVC *vc = (ProfileVC *)[Methods getVCbyName:@"ProfileVC"];
    [vc setUserProfile:user];
    [Methods pushVCinNCwithObj:vc popTop:NO];
}
-(void)handleTap:(UITapGestureRecognizer *)recognizer
{
    UserProfile *user = [allReviewList objectAtIndex:recognizer.view.tag];
    
    
    ProfileVC *vc = (ProfileVC *)[Methods getVCbyName:@"ProfileVC"];
    [vc setUserProfile:user];
    [Methods pushVCinNCwithObj:vc popTop:NO];
}
-(NSString*)timeLeftSinceTime:(double)timesTamp
{
    NSString *timeLeft;
    
    double currentTimeInMiliSeconds = (double)([[NSDate date] timeIntervalSince1970] * 1000.0);
    
    
    double miliSeconds = currentTimeInMiliSeconds - timesTamp;
    double seconds = miliSeconds / 1000.0;
    
    
    NSInteger months = (int) (floor(seconds / (3600 * 24 * 30)));
    if(months) seconds -= months * 3600 * 24 *30;
    
    NSInteger days = (int) (floor(seconds / (3600 * 24)));
    if(days) seconds -= days * 3600 * 24;
    
    NSInteger hours = (int) (floor(seconds / 3600));
    if(hours) seconds -= hours * 3600;
    
    NSInteger minutes = (int) (floor(seconds / 60));
    if(minutes) seconds -= minutes * 60;
    
    if (months)
    {
        timeLeft = [NSString stringWithFormat:@"%ld months ago", (long)months];
    }
    else if(days)
    {
        timeLeft = [NSString stringWithFormat:@"%ld days ago", (long)days];
    }
    else if(hours) { timeLeft = [NSString stringWithFormat: @"%ld hours ago", (long)hours];
    }
    else if(minutes) { timeLeft = [NSString stringWithFormat: @"%ld minutes ago", (long)minutes];
    }
    else if(seconds)
        timeLeft = [NSString stringWithFormat: @"%ld seconds ago", (long)seconds];
    
    return timeLeft;
}

- (IBAction)phoneButtonPressed:(id)sender
{
    if ([selNotxData.sitePhone isEqualToString:@""] || selNotxData.sitePhone == nil)
    {
        [Methods showAlertView:NSLocalizedString(@"Oops!", nil) message: NSLocalizedString(@"This shop has not provided a phone number!", nil) buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
        return;
    }
    
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] )
    {
//        NSString *phoneNumber = [@"tel://" stringByAppendingString:selNotxData.sitePhone];
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", selNotxData.sitePhone]]];
    } else {
        [Methods showAlertView:NSLocalizedString(@"Oops!", nil) message: NSLocalizedString(@"Your device does not support this feature.", nil)  buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }
}

- (IBAction)mapButtonPressed:(id)sender
{
    if (selNotxData.catGroupId != nil && ![selNotxData.catGroupId isEqualToString:@""])
    {
        MapVC *vc = (MapVC *)[Methods getVCbyName:@"MapVC"];
        [vc setSelNotxData:selNotxData];
        [Methods pushVCinNCwithObj:vc popTop:YES];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Map data missing" message:@"Please check your internet connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }

}

- (IBAction)likeButtonTapped:(id)sender
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    isLoggedIn = [defaults boolForKey:[StaticData instance].kLoggedIn];
    if(isLoggedIn)
    {
        [self postLike];
//        [AddThisSDK setFacebookAPIKey:@"1422791911358653"];
//        [AddThisSDK shareImage:[UIImage imageNamed:@"BG2"] withService:@"facebook" title:@"Test Image" description:@"This is Test image"];
    }
    else
    {
        
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Oops!", nil) message: NSLocalizedString(@"You are not logged in Kindly login to continue.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: NSLocalizedString(@"Login", nil), nil];
        [alert setTag:1];
        [alert show];
        
    }

}

#pragma mark - Social Media Buttons
- (IBAction)shareButtonTapped:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    isLoggedIn = [defaults boolForKey:[StaticData instance].kLoggedIn];
    if(isLoggedIn)
    {
//        [self postShare];
//        NSString *urlString = [self convertToTinyUrl:self.selNotxData.notxUrl];
//        NSURL *URL;
//        if(urlString)
//        {
//            URL = [NSURL URLWithString:urlString];
//        }
//        else
//        {
//            URL = [NSURL URLWithString:@""];
//        }
       // NSString *urlString=[@"http://rightips.com/notifications.php?tpl_id=3&nt_id=" stringByAppendingString:selNotxData.notxId];
//        NSString *stringToPost=[NSString stringWithFormat:@"Check out\n%@\non",selNotxData.title];
//        NSString *stringToPost= selNotxData.title;

//        NSArray *activityItems = @[[UIImage imageNamed:@"Default"],@"Check out",[NSString stringWithFormat:@"%@",selNotxData.title],[NSString stringWithFormat:@"on %@",urlString]];
        
//        NSString *title = [NSString stringWithFormat:@"Check out %@ on %@",self.selNotxData.title,urlString];
//        NSArray *activityItems = @[title];
//        FBActivity *faceBookActivity = [[FBActivity alloc]init];
//        faceBookActivity.delegate = self;
//        NSArray *applicationActivities = @[[[JBWhatsAppActivity alloc] init], faceBookActivity];
//        NSArray *excludedActivities    = [[NSArray alloc] initWithObjects:UIActivityTypeAddToReadingList,UIActivityTypeAirDrop, UIActivityTypeAssignToContact, UIActivityTypeCopyToPasteboard, UIActivityTypeMail, UIActivityTypeMessage, UIActivityTypePostToFlickr, UIActivityTypePostToTencentWeibo, UIActivityTypePostToTwitter, UIActivityTypePostToVimeo, UIActivityTypePostToWeibo, UIActivityTypePrint,UIActivityTypePostToFacebook, nil];
//        
//        UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:applicationActivities];
//        activityViewController.excludedActivityTypes = excludedActivities;
//        
//        activityViewController.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
//            
//            if (completed)
//            {
//                if ([activityType isEqualToString:@"net.whatsapp.WhatsApp.ShareExtension"])
//                {
//                    NSMutableDictionary *dataDictForPiwik = [self getNotificationDictForPiwik];
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_SHARE action:ACTION_WHATSAPP name:[ReuseAbleFunctions convertDictionaryToJSONString:dataDictForPiwik] value:nil];
//                    });
//                }
//            }
//            else
//            {
//                NSLog(@"The Activity: %@ was NOT completed", activityType);
//            }
//                           
//         };
//                [self presentViewController:activityViewController animated:YES completion:^{
//            
//        }];
        
        
        
        //     }];
         UIActionSheet *actionSheet=[[UIActionSheet alloc] initWithTitle:@"Share via:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook",@"Whatsapp", nil];
        [actionSheet setTag:2];
         [actionSheet showInView:self.view];
        
    }
    else
    {
        
       UIAlertView  *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Oops!", nil) message:NSLocalizedString(@"You are not logged in Kindly login to continue.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:NSLocalizedString(@"Login", nil), nil];
        [alert setTag:2];
        [alert show];
    }
}
-(void)shareWhatsapp
{
     NSString *urlString = [self convertToTinyUrl:self.selNotxData.notxUrl];
     NSString *title = [NSString stringWithFormat:@"Check out %@ on %@",self.selNotxData.title,urlString];
    NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?text=%@",title];
    NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
}
- (IBAction)btnFacebookPressed:(id)sender
{
    // https://www.facebook.com/pages/Rightips/1467836776822059?ref=settings
   // NSString *fbAppUrl = @"fb://profile/";
    //   NSString *content = [NSString stringWithContentsOfURL:[NSURL URLWithString:selNotxData.fbUrl] encoding:NSUTF8StringEncoding error:nil];
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.google.com"]];

    [HUD show:YES];
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:selNotxData.fbUrl]];
    [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        [HUD hide:YES];
        if (error)
        {
            [self openURLWithString:selNotxData.fbUrl];
            return;
        }
        NSString *content = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSString *contentIdKey = @"content=\"fb://page/?id=";
        NSArray *componentsSeparatedById = [content componentsSeparatedByString:contentIdKey];
        NSArray *componentsOfIdString;
        if (componentsSeparatedById.count>=2)
        {
            componentsOfIdString = [[componentsSeparatedById lastObject] componentsSeparatedByString:@"\""];
        }
        if (componentsOfIdString.count>=2)
        {
            NSString *fbID = [componentsOfIdString firstObject];
            NSString  *faceBookUrlString = [NSString stringWithFormat:@"fb://profile/%@",fbID];
            NSURL *facebookURL = [NSURL URLWithString:faceBookUrlString];
            if ([[UIApplication sharedApplication] canOpenURL:facebookURL] && fbID.length < 50)
            {
                [[UIApplication sharedApplication] openURL:facebookURL];
            }
            else
            {
                [self openURLWithString:selNotxData.fbUrl];
            }
        }
        else
        {
            [self openURLWithString:selNotxData.fbUrl];
        }
    }];
}

- (IBAction)btnInstgramPressed:(id)sender
{
    [self openURLWithString:selNotxData.instagramUrl];
}

- (IBAction)btnGooglePlusPressed:(id)sender
{
    [self openURLWithString:selNotxData.googlePlusUrl];
}
- (void) openURLWithString: (NSString*) string
{
    NSString *urlString;
    if ([string rangeOfString:@"http"].location == 0)
    {
        urlString = string;
    } else
    {
        urlString = [NSString stringWithFormat:@"http://%@",string];
    }
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:urlString]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Can not open url", nil) message:string delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}


- (IBAction)btnEmailPressed:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        NSArray *toRecipents = [NSArray arrayWithObject:selNotxData.mailUrl];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Oops!", nil) message:NSLocalizedString(@"Can not open email. Please configure you email first.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
    }
}

- (IBAction)btnCatLink:(id)sender
{
    [self openURLWithString:selNotxData.catLink];
}

#pragma mark - MFMailComposer Delegate Methods

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    [self dismissViewControllerAnimated:YES completion:Nil];
}

#pragma mark - Social Media Functions

- (void) tempShareForFacebook
{
    [HUD setLabelText:NSLocalizedString(@"Sharing on Facebook...", nil)];
    [HUD show:YES];
    
    [self downloadImageWithURL:[NSURL URLWithString:[selNotxData.images objectAtIndex:0]] completionBlock:^(BOOL succeeded, UIImage *image)
    {
        if(succeeded)
        {
            NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
           
            
            
            NSString *urlString = [self convertToTinyUrl:self.selNotxData.notxUrl];
//            NSString *urlString=[@"http://rightips.com/notifications.php?tpl_id=3&nt_id=" stringByAppendingString:selNotxData.notxId];
//            NSString *stringToPostFB=[[[[selNotxData.title stringByAppendingString:@":"] stringByAppendingString:selNotxData.detail] stringByAppendingString:@" "]stringByAppendingString:urlString] ;
            NSString *stringToPostFB=[NSString stringWithFormat:@"Check out\n%@\non %@",selNotxData.title,urlString];
            [params setObject:stringToPostFB forKey:@"message"];
            
            if (image)
            {
                [params setObject:image forKey:@"picture"];
                [self faceBookPOSTRequestWithDictionary:params];
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Image not found" , nil)message:NSLocalizedString(@"No image found to share", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK" , nil) otherButtonTitles:nil, nil] show];
            }
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Oops!" , nil) message:NSLocalizedString(@"Failed to connect to server, check your internet connection." , nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK" , nil) otherButtonTitles:nil, nil] show];
            [HUD hide:YES];
        }
    }];
}
-(NSString *)convertToTinyUrl:(NSString *)urlString
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://tinyurl.com/api-create.php?url=%@", urlString]];
    NSURLRequest *request = [ NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:10.0];
                             
    NSError *error;
    NSURLResponse *response;
    NSData *myUrlData = [ NSURLConnection sendSynchronousRequest:request
                                               returningResponse:&response
                                                           error:&error];
    NSString *myTinyUrl = [[NSString alloc] initWithData:myUrlData encoding:NSUTF8StringEncoding];
    return myTinyUrl;
}

- (void) faceBookPOSTRequestWithDictionary: (NSDictionary *)dataDict
{
//    if (!FBSession.activeSession.isOpen) {
//        // if the session is closed, then we open it here, and establish a handler for state changes
//        [FBSession openActiveSessionWithReadPermissions:nil
//                                           allowLoginUI:YES
//                                      completionHandler:^(FBSession *session,
//                                                          FBSessionState state,
//                                                          NSError *error) {
//                                          
//                                          if (error)
//                                          {
//                                              [HUD hide:YES];
//                                              UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                                                                  message:error.localizedDescription
//                                                                                                 delegate:nil
//                                                                                        cancelButtonTitle:@"OK"
//                                                                                        otherButtonTitles:nil];
//                                              [alertView show];
//                                          } else if (session.isOpen) {
//                                              //run your user info request here
//                                              [self postToFb:dataDict];
//                                          }
//                                      }];
//    }
//    else
//    {
//     [self postToFb:dataDict];
//    }
    if (![FBSDKAccessToken currentAccessToken])
    {
        
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: [StaticData instance].fbPermissions fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error)
        {
            [HUD hide:YES];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
        } else if (result.isCancelled) {
            NSLog(@"Cancelled");
        } else
        {
            if ([result.grantedPermissions containsObject:@"email"])
            {
                [self postToFb:dataDict];
                
            }
            
        }
    }];
    }else
    {
        [self postToFb:dataDict];
    }
    
    
}
-(void)postToFb: (NSDictionary *)dataDict
{
//    [FBRequestConnection startWithGraphPath:@"me/photos"
//                                 parameters:dataDict
//                                 HTTPMethod:@"POST"
//                          completionHandler:^(FBRequestConnection *connection,
//                                              id result,
//                                              NSError *error)
//     {
//         [HUD hide:YES];
//         if (error)
//         {
//             [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Oops!" , nil) message:error.description delegate:nil cancelButtonTitle:NSLocalizedString(@"OK" , nil) otherButtonTitles:nil, nil] show];
//             //showing an alert for failure
//         }
//         else
//         {
//             NSMutableDictionary *dataDictForPiwik = [self getNotificationDictForPiwik];
//             dispatch_async(dispatch_get_main_queue(), ^{
//                 [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_SHARE action:ACTION_FACEBOOK name:[ReuseAbleFunctions convertDictionaryToJSONString:dataDictForPiwik] value:nil];
//             });
//         }
//     }];
    
    
//    FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
//    photo.image = [dataDict objectForKey:@"picture"];
//    photo.userGenerated = YES;
//    photo.caption = [dataDict objectForKey:@"message"];
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    [content setImageURL:[NSURL URLWithString:[selNotxData.images objectAtIndex:0]]];
    [content setContentDescription:[NSString stringWithFormat:@"Check out\n%@\non",selNotxData.title]];
    [content setContentURL:[NSURL URLWithString:[self convertToTinyUrl:self.selNotxData.notxUrl]]];
    [FBSDKShareDialog showFromViewController:self withContent:content delegate:self];
}
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    NSLog(@"%@",results);
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}
- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    
}


- (void)postLike
{
    [HUD setLabelText:@"..."];
    [HUD show:YES];
    
    UserProfile *currentUserProfile = [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData] ];
    
    NSString *apiURL=@"http://rightips.com/api/index.php?action=like";
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setObject:currentUserProfile.userIdEmail forKey:@"uid"];
    [postParams setObject:selNotxData.msgId forKey:@"id"];
    [postParams setObject:@"post" forKey:@"type"];
    
    if (isLiked)
    {
        [postParams setObject:@"Unlike" forKey:@"rel"];
    }
    else
    {
        [postParams setObject:@"Like" forKey:@"rel"];
    }
    
    NSURL *url = [[NSURL alloc] initWithString:apiURL];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiURL parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [HUD hide:YES];
        if([[JSON objectForKey:@"status"] integerValue] == 1)
        {
            
            

            [self fetchDataFromServer];
            // Post like to piwik
            NSMutableDictionary *dataDictForPiwik = [self getNotificationDictForPiwik];

            [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_NOTIFICATION action:ACTION_LIKE name:[ReuseAbleFunctions convertDictionaryToJSONString:dataDictForPiwik] value:nil];
            
        }
        else if([JSON objectForKey:@"message"])
        {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:[JSON objectForKey:@"message"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
        }
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        [HUD hide:YES];
    }];
    [operation start];

}

-(void) postShare
{
    NSString *apiURL=@"http://rightips.com/api/index.php?action=sharePost";
//     NSString *apiURL= self.selNotxData.notxUrl;
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setObject:selNotxData.notxId forKey:@"uid"];
    [postParams setObject:selNotxData.msgId forKey:@"msg_id"];
    [postParams setObject:@"Share" forKey:@"rel"];
    
    NSURL *url = [[NSURL alloc] initWithString:apiURL];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiURL parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
    
    
        if([[JSON objectForKey:@"status"] integerValue]==1)
        {

        }
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        
    }];

    [operation start];
}

#pragma mark - UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
        
    
    if([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"Login", nil)])
    {
        if (alertView.tag == 1)
        {
            tempString = @"Like";
        }
        else if (alertView.tag == 2)
        {
            tempString = @"Share";
        }
        else if (alertView.tag == 3)
        {
            tempString = @"Comment";
        }
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        [self.navigationController presentViewController:[storyboard instantiateViewControllerWithIdentifier:@"InstantLoginVC" ] animated:YES completion:NULL];
    }
    else
    {
        
    }
   
    
    
}

#pragma mark - NotificationDictForPiwik

 - (NSMutableDictionary*) getNotificationDictForPiwik
{
    NSMutableDictionary *dataDictForPiwik = [[NSMutableDictionary alloc] init];
    if (selNotxData.notxId)
    {
        [dataDictForPiwik setObject:selNotxData.notxId forKey:kNotificationId];
    }
    if (selNotxData.siteId)
    {
        [dataDictForPiwik setObject:selNotxData.siteId forKey:kNotificationSiteId];
    }
    if (selNotxData.siteName)
    {
        [dataDictForPiwik setObject:selNotxData.siteName forKey:kNotificationSiteName];
    }
    if (selNotxData.title)
    {
        [dataDictForPiwik setObject:selNotxData.title forKey:kNotificationTitle];
    }
    if (selNotxData.zone)
    {
        [dataDictForPiwik setObject:selNotxData.zone forKey:kNotificationZone];
    }

    return dataDictForPiwik;
}




-(void)cameraButtonTapped
{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Edit Photo", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Change Profile Photo", nil),NSLocalizedString(@"Change Cover Photo", nil),nil];
        [popup setTag:1];
    [popup showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (popup.tag == 1)
    {
    ind = buttonIndex;
    
    if (buttonIndex != 2)
    {
    view = [[ImageCaptureView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    [view setInd:buttonIndex];
    
    
    
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         [view setFrame:self.view.frame];
                     }
                     completion:^(BOOL finished){
                     }];
    [self.view addSubview:view];
    }
    }
    else if (popup.tag == 2)
    {
        if (buttonIndex == 0)
        {
            [self tempShareForFacebook];
        }
        else if (buttonIndex == 1)
        {
            [self shareWhatsapp];
        }
        
        
    }
    
}


-(void)changeImage
{
    [slideMenu setupMenu];
}







@end
