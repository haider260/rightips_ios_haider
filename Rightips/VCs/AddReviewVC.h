//
//  AddReviewVC.h
//  Rightips
//
//  Created by Mac on 05/01/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingView.h"

@interface AddReviewVC : UIViewController <RatingViewDelegate,UITextViewDelegate>
{
    
    IBOutlet RatingView *ratingView;
    
    IBOutlet UITextView *reviewTxtView;
    
    IBOutlet UILabel *ratingLabel;
    IBOutlet UILabel *reviewLabel;
    
    IBOutlet UIButton *cameraButton;
    
    IBOutlet UIButton *postItbutton;
    
    IBOutlet UIScrollView *scrlView;
    
}
@property(nonatomic)CGFloat ratingValue;
@property(nonatomic,strong)NotificationData *notificationdata;

@end
