//
//  NotxSlideVC.m
//  Rightips
//
//  Created by Abdul Mannan on 1/1/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "NotxSlideVC.h"
#import "NotxDetailsVC.h"
#import "SlideNotx.h"
#import <CoreText/CoreText.h>
#import "ImageCaptureView.h"

@interface NotxSlideVC () <iCarouselDataSource, iCarouselDelegate>
{
    SlideMenu *slideMenu;
    
    DbHelper *dbHelper;
    NSMutableArray *sliderNotxs;
    NSInteger ind;
    ImageCaptureView *view;
    NSUserDefaults *defaults;
     MBProgressHUD *HUD;
}

@property (nonatomic, retain) IBOutlet UIButton *menuToggleButton;
@property (nonatomic, retain) IBOutlet UIButton *muteButton;

@property (nonatomic, strong) IBOutlet iCarousel *carousel;

@end

@implementation NotxSlideVC

@synthesize selNotxData;
@synthesize menuToggleButton;
@synthesize arrayOfNotifications;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    slideMenu = [[SlideMenu alloc] init];
    [self.view addSubview:slideMenu];
    
    UISwipeGestureRecognizer *swipeLeftMenu = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToLeftWithGestureRecognizer:)];
    swipeLeftMenu.direction = UISwipeGestureRecognizerDirectionLeft;
    [slideMenu addGestureRecognizer:swipeLeftMenu];
    
    [self.view bringSubviewToFront:menuToggleButton];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraButtonTapped) name:@"TapOnCameraButton" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeImage) name:@"changeImage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRangeBeacons:) name:@"didRangeBeacons" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeMuteImage:) name:@"changeMuteImage" object:nil];
    [self changeMuteImage:nil];
    
    _carousel.type = iCarouselTypeRotary;
    
    dbHelper = [[DbHelper alloc] init];
    sliderNotxs = [NSMutableArray array];
    
    //sliderNotxs = [dbHelper getAllNotxFromDB];
    //sliderNotxs=[StaticData instance].notifications;
    sliderNotxs=[arrayOfNotifications mutableCopy];
    [_carousel reloadData];
    
    if (selNotxData)
    {
        for (int index = 0; index < sliderNotxs.count; index++)
        {
            NotificationData *nd = [sliderNotxs objectAtIndex:index];
            if ([nd.notxId isEqualToString:selNotxData.notxId])
            {
                [_carousel setCurrentItemIndex:index];
                break;
            }
        }
    }
}

- (void) viewDidAppear:(BOOL)animated
{


    [super viewDidAppear:animated];
    //{"note_id":"46","note_site_id":"33","note_site_name":"PIZZA MOLTES","note_title":"PIZZA MOLTES","note_zone":"1"}

    //Sending current screen to Piwic Tracker
    //[[PiwikTracker sharedInstance] sendView:@"Notification Slider"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - SwipeGesture Methods
-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer
{
    [slideMenu showHideMenu];
}

#pragma mark - Change Mute Image
- (void)changeMuteImage:(NSNotification *)notification
{
    if ([[NSUserDefaults standardUserDefaults] doubleForKey:@"muteEndTime"] && ([[NSUserDefaults standardUserDefaults] doubleForKey:@"muteEndTime"] != 0))
    {
        [self.muteButton setImage:[UIImage imageNamed:@"BtnMute"] forState:UIControlStateNormal];
    }
    else
    {
        [self.muteButton setImage:[UIImage imageNamed:@"BtnUnmuted"] forState:UIControlStateNormal];
    }
}
#pragma mark - IBActions

- (IBAction)slideMenuButtonPressed:(id)sender
{
    [slideMenu showHideMenu];
}

- (IBAction)muteButtonPressed:(id)sender
{
    [Methods showMuteNotxActionSheet];
}

#pragma mark - Carousel Methods
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    if (sliderNotxs.count <= 1)
        _carousel.scrollEnabled = NO;
    else
        _carousel.scrollEnabled = YES;
    return [sliderNotxs count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    NotificationData *nd = [sliderNotxs objectAtIndex:index];
    
    SlideNotx *slideNotxView;
    if ([Methods isPhone])
    {
        int devHeight = [[UIScreen mainScreen] bounds].size.height;
        if (devHeight == 480)
        {
            slideNotxView = [[NSBundle mainBundle] loadNibNamed:@"SlideNotx_iPhone" owner:self options:nil][0];
            if (index == 0)
            [slideNotxView.overlayView setAlpha:0.0f];
            
            slideNotxView.titleLabel.text = nd.title;
            slideNotxView.descLabel.text = nd.detail;
            //slideNotxView.addressLabel.text = [NSString stringWithFormat:@"%@\n%@\n%@", nd.siteName, nd.siteAddress, [Methods getDealDateStrFromNotx:nd]];
             slideNotxView.addressLabel.text = nd.siteName;

            [slideNotxView.descLabel sizeToFit];
            CGRect descFrame = slideNotxView.descLabel.frame;
            if (descFrame.size.height > 130)
            {
                descFrame.size.height = 130;
                slideNotxView.descLabel.frame = descFrame;
            }
            CGRect addrFrame = slideNotxView.addressLabel.frame;
            addrFrame.origin.y = descFrame.origin.y + descFrame.size.height + 5;
            //slideNotxView.addressLabel.frame = addrFrame;
        }
        else
        {
            slideNotxView = [[NSBundle mainBundle] loadNibNamed:@"SlideNotx_iPhone" owner:self options:nil][1];
            if (index == 0)
            [slideNotxView.overlayView setAlpha:0.0f];
            
            slideNotxView.titleLabel.text = nd.title;
            slideNotxView.descLabel.text = nd.detail;
           // slideNotxView.addressLabel.text = [NSString stringWithFormat:@"%@\n%@\n%@", nd.siteName, nd.siteAddress, [Methods getDealDateStrFromNotx:nd]];
            slideNotxView.addressLabel.text = nd.siteName;

            [slideNotxView.descLabel sizeToFit];
            CGRect descFrame = slideNotxView.descLabel.frame;
            if (descFrame.size.height > 200) {
                descFrame.size.height = 200;
                slideNotxView.descLabel.frame = descFrame;
            }
            CGRect addrFrame = slideNotxView.addressLabel.frame;
            addrFrame.origin.y = descFrame.origin.y + descFrame.size.height + 10;
            //slideNotxView.addressLabel.frame = addrFrame;
        }
    }
    
    if ([nd.images count] > 0)
        [Methods setImageInImageViewFromUrl:slideNotxView.notxIV urlStr:[nd.images objectAtIndex:0]];
    else
        slideNotxView.notxIV.image = [UIImage imageNamed:@"ImageNA"];
    
    return slideNotxView;
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel
{
    SlideNotx *view = (SlideNotx *) carousel.currentItemView;
    [UIView animateWithDuration:0.3f animations:^{
        [view.overlayView setAlpha:0.0f];
    }];
    
    SlideNotx *prevView = (SlideNotx *) [carousel itemViewAtIndex:carousel.currentItemIndex-1];
    if (prevView) {
        [UIView animateWithDuration:0.3f animations:^{
            [prevView.overlayView setAlpha:0.5f];
        }];
    }
    
    SlideNotx *nextView = (SlideNotx *) [carousel itemViewAtIndex:carousel.currentItemIndex+1];
    if (nextView) {
        [UIView animateWithDuration:0.3f animations:^{
            [nextView.overlayView setAlpha:0.5f];
        }];
    }
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    NotxDetailsVC *vc = (NotxDetailsVC *)[Methods getVCbyName:@"NotxDetailsVC"];
    [vc setSelNotxData:[sliderNotxs objectAtIndex:index]];
    
    [dbHelper insertNotxToDB:[sliderNotxs objectAtIndex:index]];
    [Methods pushVCinNCwithObj:vc popTop:YES];
}

#pragma mark - Did Range Beacons
- (void)didRangeBeacons:(NSNotification *)notification
{
    NotificationData *nd = [BwMethods getNotificationDataFromRangedBeacons:[notification object]];
    if (nd != nil)
    {
        [dbHelper insertNotxToDB:nd];
        sliderNotxs = [dbHelper getAllNotxFromDB];
        [_carousel reloadData];
    }
}

-(void)cameraButtonTapped
{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Edit Photo", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Change Profile Photo", nil),NSLocalizedString(@"Change Cover Photo", nil),nil];
    //    [popup setTag:3];
    [popup showInView:self.view];
    
    
    
}
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ind = buttonIndex;
    
    if (ind != 2)
    {
        
        view = [[ImageCaptureView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        [view setInd:ind];
        
        
        
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             [view setFrame:self.view.frame];
                         }
                         completion:^(BOOL finished){
                         }];
        [self.view addSubview:view];
        
    }
    
    
}
-(void)changeImage
{
    [slideMenu setupMenu];
}





@end
