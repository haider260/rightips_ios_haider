//
//  HistoryVC.m
//  Rightips
//
//  Created by Abdul Mannan on 1/5/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "HistoryVC.h"
#import "HistoryCell.h"
#import "NotxDetailsVC.h"
#import "ImageCaptureView.h"

@interface HistoryVC ()
{
    SlideMenu *slideMenu;
    
     MBProgressHUD *HUD;
    DbHelper *dbHelper;
    NSMutableArray *sectionsArray;
    NSInteger ind;
    ImageCaptureView *view;
    NSUserDefaults *defaults;
}

@property (nonatomic, retain) IBOutlet UIButton *muteButton;
@property (nonatomic, retain) IBOutlet UIButton *menuToggleButton;
@property (nonatomic, retain) IBOutlet UITableView *historyTableView;
@property (strong, nonatomic) IBOutlet UIView *historyView;


@end

@implementation HistoryVC

@synthesize menuToggleButton;
@synthesize historyTableView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    defaults = [NSUserDefaults standardUserDefaults];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,historyTableView.frame.origin.y-7, historyTableView.frame.size.width, 7)];
    headerView.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:0 blue:57.0/255.0 alpha:1.0f];;
    [self.historyView addSubview:headerView];
    slideMenu = [[SlideMenu alloc] init];
    [self.view addSubview:slideMenu];
    [self.view bringSubviewToFront:menuToggleButton];
    
    UISwipeGestureRecognizer *swipeLeftMenu = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToLeftWithGestureRecognizer:)];
    swipeLeftMenu.direction = UISwipeGestureRecognizerDirectionLeft;
    [slideMenu addGestureRecognizer:swipeLeftMenu];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraButtonTapped) name:@"TapOnCameraButton" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageSelected) name:@"ImageSelected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRangeBeacons:) name:@"didRangeBeacons" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeMuteImage:) name:@"changeMuteImage" object:nil];
    [self changeMuteImage:nil];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    
    dbHelper = [[DbHelper alloc] init];
    sectionsArray = [NSMutableArray array];
    
    [self fetchNotxFromDB];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //Sending current screen to Piwic Tracker
    //[[PiwikTracker sharedInstance] sendView:@"History"];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - SwipeGesture Methods
-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer
{
    [slideMenu showHideMenu];
}
#pragma mark - Table View Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [sectionsArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[sectionsArray objectAtIndex:section] objectForKey:@"sectionArray"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NotificationData *nd = [[[sectionsArray objectAtIndex:indexPath.section] objectForKey:@"sectionArray"] objectAtIndex:indexPath.row];
    NSDate *notxCDate = [NSDate dateWithTimeIntervalSince1970:nd.creationDate];
    NSString *dateTimeStr = [Methods stringFromDate:notxCDate dateFormat:@"dd.MM.yyyy - hh:mm a" timeZone:[NSTimeZone defaultTimeZone]];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HistoryCell_iPhone" owner:self options:nil];
    HistoryCell *cell = [nib objectAtIndex:0];
    if ([nd.images count] > 0)
        [Methods setImageInImageViewFromUrl:cell.iconIV urlStr:[nd.images objectAtIndex:0]];
    else
        cell.iconIV.image = [UIImage imageNamed:@"ImageNA"];
    cell.nameLabel.text = nd.title;
    cell.descLabel.text = [nd.detail uppercaseString];
    cell.dateTimeLabel.text = dateTimeStr;
    
    if ([nd.source isEqualToString:@"location"])
    {
        [cell.notxSourceImageView setImage:[UIImage imageNamed:@"IconLocation2"]];
    }
    else
    {
        [cell.notxSourceImageView setImage:[UIImage imageNamed:@"BeaconIcon"]];
    }
    
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NotificationData *nd = [[[sectionsArray objectAtIndex:indexPath.section] objectForKey:@"sectionArray"] objectAtIndex:indexPath.row];
    
//    NotxSlideVC *vc = (NotxSlideVC *)[Methods getVCbyName:@"NotxSlideVC"];
//    [vc setSelNotxData:nd];
//    [vc setArrayOfNotifications:[[sectionsArray objectAtIndex:indexPath.section] objectForKey:@"sectionArray"]];
//    [Methods pushVCinNCwithObj:vc popTop:YES];
    NotxDetailsVC *vc = (NotxDetailsVC *)[Methods getVCbyName:@"NotxDetailsVC"];
    [vc setSelNotxData:nd];
//    [dbHelper insertNotxToDB:[homeNotxs objectAtIndex:index]];
    [Methods pushVCinNCwithObj:vc popTop:YES];

}

- (void)didRangeBeacons:(NSNotification *)notification
{
    NotificationData *nd = [BwMethods getNotificationDataFromRangedBeacons:[notification object]];
    if (nd != nil)
    {
        [dbHelper insertNotxToDB:nd];
        [self fetchNotxFromDB];
        [historyTableView reloadData];
    }
}


#pragma mark - Change Mute Image
- (void)changeMuteImage:(NSNotification *)notification
{
    if ([[NSUserDefaults standardUserDefaults] doubleForKey:@"muteEndTime"] && ([[NSUserDefaults standardUserDefaults] doubleForKey:@"muteEndTime"] != 0))
    {
        [self.muteButton setImage:[UIImage imageNamed:@"BtnMute"] forState:UIControlStateNormal];
    }
    else
    {
        [self.muteButton setImage:[UIImage imageNamed:@"BtnUnmuted"] forState:UIControlStateNormal];
    }
}
#pragma mark - IBActions
- (IBAction)slideMenuButtonPressed:(id)sender
{
    [slideMenu showHideMenu];
}

- (IBAction)muteButtonPressed:(id)sender
{
    [Methods showMuteNotxActionSheet];
}


- (void)fetchNotxFromDB
{
    
    [sectionsArray removeAllObjects];
    
    NSMutableArray *historyNotxs = [dbHelper getAllNotxFromDB];
    
    NSMutableArray *recentArray = [NSMutableArray array];
    NSMutableArray *last7DaysArray = [NSMutableArray array];
    NSMutableArray *olderArray = [NSMutableArray array];
    
    for (unsigned int index = 0; index < [historyNotxs count]; index++)
    {
        NotificationData *nd = [historyNotxs objectAtIndex:index];
        NSTimeInterval ndcd = nd.creationDate;
        NSTimeInterval ct = [[NSDate date] timeIntervalSinceNow];
        
        if ((ct-ndcd) < 86400) { // last 24 hours
            [recentArray addObject:nd];
        } else if ((ct-ndcd) < 604800) { // last 7 days
            [last7DaysArray addObject:nd];
        } else { // all older
            [olderArray addObject:nd];
        }
    }
    
    if ([recentArray count] > 0)
    {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:recentArray forKey:@"sectionArray"];
        [dict setObject:@"Recent" forKey:@"sectionLabel"];
        [sectionsArray addObject:dict];
    }
    if ([last7DaysArray count] > 0)
    {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:last7DaysArray forKey:@"sectionArray"];
        [dict setObject:@"Last 7 Days" forKey:@"sectionLabel"];
        [sectionsArray addObject:dict];
    }
    if ([olderArray count] > 0)
    {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:olderArray forKey:@"sectionArray"];
        [dict setObject:@"Older" forKey:@"sectionLabel"];
        [sectionsArray addObject:dict];
    }
}

-(void)imageSelected
{
    UIAlertView *alert;
    if (ind == 0)
    {
        
        alert = [[UIAlertView alloc] initWithTitle:nil message:@"Do you want to save this picture as profile photo" delegate:self cancelButtonTitle:@"No" otherButtonTitles: @"Yes",nil];
        
    }
    else if (ind == 1)
    {
        
        alert = [[UIAlertView alloc] initWithTitle:nil message:@"Do you want to save this picture as cover photo" delegate:self cancelButtonTitle:@"No" otherButtonTitles: @"Yes",nil];
    }
    
    
    [alert show];
}
-(void)cameraButtonTapped
{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Edit Photo", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Change Profile Photo", nil),NSLocalizedString(@"Change Cover Photo", nil),nil];
    [popup showInView:self.view];
    
    
    
}
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ind = buttonIndex;
    
    if (ind != 2)
    {
        
        view = [[ImageCaptureView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        [view setInd:ind];
        
        
        
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             [view setFrame:self.view.frame];
                         }
                         completion:^(BOOL finished){
                         }];
        [self.view addSubview:view];
        
    }
    
    
}
-(void)changeImage
{
    [slideMenu setupMenu];
}





@end
