
//  NotxCommentsVC.m
//  Rightips
//
//  Created by Shahbaz Ali on 8/4/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "NotxCommentsVC.h"
#import "CommentsCellTableViewCell.h"
#import <ImageIO/ImageIO.h>
#import "NotxCommentImageVC.h"



@interface NotxCommentsVC ()
{
    NSMutableArray *tempArray,*allCommentsList;
    UIView *headerView;
    NSDictionary *users;
    NSDictionary *avatars;
    MBProgressHUD *HUD;
    UIImage *chosenImage,*tempImage;
    UIView *selectedPhotoView;
    UIImageView *transparentImgView,*smallImageView;
//    HPGrowingTextView *txtView;
    CGSize keyboardSize;
    UserProfile *userProfile,*specificUser;
    BOOL autoScroll,timer;
    NSTimer *autoTimer;
    NSInteger index;
    int p;
    BOOL isKeyboardHide,isLiked;
    
    
    
    
    UIButton *selectedBtn,*photoLibraryButton,*takePhotoButton,*sendButton,*delButton,*sendBtn;
}
@end

@implementation NotxCommentsVC
@synthesize selNotxData;

- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
    
    isKeyboardHide = YES;
    keyboardSize.height = 0;
    autoScroll = YES;
    timer = YES;
    
    
//    NSData *userProfileData = [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData];
//    UserProfile *userProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userProfileData];
//    userList = [[NSMutableArray alloc] init];
//    for (int i = 0; i < 3; i++)
//    {
//        UserProfile *user = [[UserProfile alloc] init];
//        user.firstName = @"Nat";
//        user.lastName = @"";
//        user.userReview = @"bla bla bla bla";
//        [userList addObject:user];
        
//    }
    p = 0;
    allCommentsList = [[NSMutableArray alloc] init];
    
    NSData *userProfileData = [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData];
    userProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userProfileData];
    
    [likeButton setTitle:[NSString stringWithFormat:@"Like(%@)",self.selNotxData.noOfLikes] forState:UIControlStateNormal];
//    [shareButton setTitle:[NSString stringWithFormat:@"Share(%@)",self.selNotxData.noOfLikes] forState:UIControlStateNormal];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteComment) name:@"deleteComment" object:nil];
    
    txtFieldView = [[UIView alloc] init];
    [txtFieldView setFrame:CGRectMake(0,self.view.frame.size.height - 43, self.view.frame.size.width, 43)];
    [txtFieldView setBackgroundColor:[UIColor whiteColor]];
    
    UIView *redBorderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, txtFieldView.frame.size.width, 5)];
    [redBorderView setBackgroundColor:[UIColor redColor]];
    [txtFieldView addSubview:redBorderView];
    
    UIButton *cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cameraButton setFrame:CGRectMake(0, 5, 32, 38)];
    [cameraButton setImage:[UIImage imageNamed:@"cameraImage"] forState:UIControlStateNormal];
    [cameraButton addTarget:self action:@selector(cameraButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [txtFieldView addSubview:cameraButton];
    
    smallImageView = [[UIImageView alloc] initWithFrame:CGRectMake(35, 10, 30, 30)];
//    [smallImageView setBackgroundColor:[UIColor redColor]];
    [txtFieldView addSubview:smallImageView];
   
    txtView = [[UITextView alloc] init];
    [txtView setFrame:CGRectMake(40, 10, txtFieldView.frame.size.width - 85, txtFieldView.frame.size.height - 15)];
    [txtView setDelegate:self];
    [txtView setEditable:YES];
    [txtView.layer setBackgroundColor: [[UIColor whiteColor] CGColor]];
    [txtView.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [txtView.layer setBorderWidth: 1.0];
    [txtView.layer setCornerRadius:5.0f];
    [txtView setClipsToBounds:YES];
    [txtView setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtFieldView addSubview:txtView];

    
//    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [backBtn setFrame:CGRectMake(txtFieldView.frame.size.width - 80, 5, 50, 38)];
//    [backBtn setImage:[UIImage imageNamed:@"HideKeyboardImage"] forState:UIControlStateNormal];
//    [backBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [backBtn addTarget:self action:@selector(hideKeyBoard) forControlEvents:UIControlEventTouchUpInside];
//    [txtFieldView addSubview:backBtn];
    
    sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [sendBtn setFrame:CGRectMake(txtFieldView.frame.size.width - 38, 5, 38, 38)];
    [sendBtn setImage:[UIImage imageNamed:@"sendImage"] forState:UIControlStateNormal];
    [sendBtn setImage:[UIImage imageNamed:@"sendImageDisable"] forState:UIControlStateDisabled];
    [sendBtn addTarget:self action:@selector(sendButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    sendBtn.enabled = NO;
    [txtFieldView addSubview:sendBtn];
    
    
//    [txtFieldView sizeToFit];
    
    
    tableVu = [[UITableView alloc] init];
    [tableVu setFrame:CGRectMake(0, borderView.frame.origin.y + borderView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - (borderView.frame.origin.y + borderView.frame.size.height +txtFieldView.frame.size.height))];
    [tableVu setDelegate:self];
    [tableVu setDataSource:self];
    [self.view addSubview:tableVu];
    
    headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    [headerView setBackgroundColor:[UIColor clearColor]];
    //    [view setCenter:CGPointMake(self.view.frame.size.width / 2, view.frame.origin.y)];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, headerView.frame.size.width, headerView.frame.size.height)];
    [btn setBackgroundColor:[UIColor blackColor]];
    [btn setAlpha:0.5];
    [btn setTitle:NSLocalizedString(@"LOAD EARLIER MESSAGES", nil) forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(loadEarlierComments) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:btn];
    [tableVu setTableHeaderView:headerView];
    
    [self.view addSubview:txtFieldView];
    
    
    
    HUD  = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    
    
    
//    [notificationName setNumberOfLines:0];
    [notificationName setText:self.selNotxData.title];
//    [notificationName sizeToFit];
    
//    [siteName setNumberOfLines:0];
    [siteName setText:self.selNotxData.siteName];
//    [siteName sizeToFit];
    
//    NSString *urlString = [NSString stringWithFormat:@"http://www.rightips.com/wall/custom/wall_co                                                   mments.php?uid=%@&msg_id=%@",userProfile.userIdEmail,self.selNotxData.msgId];
//    UIWebView *webVu = [[UIWebView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height - 50)];
//    [webVu setDelegate:self];
//    [webVu loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
//    [self.view addSubview:webVu];
//    
//    HUD = [[MBProgressHUD alloc] initWithView:self.view];
//    [self.view addSubview:HUD];
//    HUD.dimBackground = YES;
    
    
//    self.lblNotificationName.text = selNotxData.title;
    //avatars
    p = 0;
   
    [self getComments:p];
    
    
    }
-(void)loadEarlierComments
{
    p++;
    autoScroll = NO;
    [self getComments:p];
}

//- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
//{
//    CGPoint offset = aScrollView.contentOffset;
////    CGRect bounds = aScrollView.bounds;
//    CGSize size = aScrollView.contentSize;
////    UIEdgeInsets inset = aScrollView.contentInset;
//    float y = offset.y;
//    float h = size.height;
//    // NSLog(@"offset: %f", offset.y);
//    // NSLog(@"content.height: %f", size.height);
//    // NSLog(@"bounds.height: %f", bounds.size.height);
//    // NSLog(@"inset.top: %f", inset.top);
//    // NSLog(@"inset.bottom: %f", inset.bottom);
//    // NSLog(@"pos: %f of %f", y, h);
//    
//    float reload_distance = 10;
//    if(y > h + reload_distance)
//    {
//        NSLog(@"load more rows");
//        [ac startAnimating];
//    }
//}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat yOffset = 0.0;
    if (scrollView.contentOffset.y < yOffset)
    {
        
    
    }
//    else
//    {
//        [tableVu.tableHeaderView setHidden:YES];
//    }


}
-(NSString*)timeLeftSinceTime:(double)timesTamp
{
    NSString *timeLeft;
    
    double currentTimeInMiliSeconds = (double)([[NSDate date] timeIntervalSince1970] * 1000.0);

    
    double miliSeconds = currentTimeInMiliSeconds - timesTamp;
    double seconds = miliSeconds / 1000.0;
    
    
    NSInteger days = (int) (floor(seconds / (3600 * 24)));
    if(days) seconds -= days * 3600 * 24;
    
    NSInteger hours = (int) (floor(seconds / 3600));
    if(hours) seconds -= hours * 3600;
    
    NSInteger minutes = (int) (floor(seconds / 60));
    if(minutes) seconds -= minutes * 60;
    
    if(days) {
        timeLeft = [NSString stringWithFormat:@"%ld days ago", (long)days];
    }
    else if(hours) { timeLeft = [NSString stringWithFormat: @"%ld hours ago", (long)hours];
    }
    else if(minutes) { timeLeft = [NSString stringWithFormat: @"%ld minutes ago", (long)minutes];
    }
    else if(seconds)
        timeLeft = [NSString stringWithFormat: @"%ld seconds ago", (long)seconds];
    
    return timeLeft;
}
//-(NSString *)timeSinceNow:(float)timestamp
//{
//    NSString *time;
//    NSTimeInterval ti = [[NSDate date] timeIntervalSinceNow];
//    float diff = ti - timestamp;
//    //days
//    if (diff<60*60*24*365)
//    {
//        time = [NSString stringWithFormat:@"%d days", (int)floor(diff/(60*60*24))];
//    }
//    //hours
//    else if (diff<60*60*24)
//    {
//        time = [NSString stringWithFormat:@"%d hours", (int)floor(diff/(60*60))];
//    }
//    //minutes
//    else if (diff<60*60) {
//        time = [NSString stringWithFormat:@"%d minutes", (int)floor(diff/(60))];
//    }
//    //seconds
//    else if (diff<60) {
//        time = [NSString stringWithFormat:@"%d seconds", (int)floor(diff)];
//    }
//    //years
//    else if (diff>=60*60*24*365) {
//        time = [NSString stringWithFormat:@"%d years", (int)floor(diff/(60*60*24*365))];
//    }
////    //check if its not singular (plural) - add 's' if so
////    else if (![[time substringToIndex:2] isEqualToString:@"1 "]) {
////        [time appendString:@"s"];
////    }
//    return time;
//}

-(void)getComments:(int)index
{
    if (!timer)
    {
        return;
    }
    
    [HUD setLabelText:NSLocalizedString(@"Fetching Comments", nil)];
    [HUD show:YES];
    
    NSString *apiUrl;
    //    NSString *apiUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%f,%f&radius=500&key=AIzaSyAuCgahZyspOpErsbYmlXGx0JCq3LQIz1M",self.userLoc.selectedLocation.coordinate.latitude,self.userLoc.selectedLocation.coordinate.longitude];
    
         apiUrl = [NSString stringWithFormat:@"http://www.rightips.com/api/index.php?action=comments_limit&msg_id=%@&p=%d&uid=%@",self.selNotxData.msgId,index,userProfile.userIdEmail];
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiUrl parameters:nil];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
        [HUD hide:YES];
        
        if([[JSON objectForKey:@"status"] integerValue] == 1)
        {
            
        [self fetchComments:JSON];
        }
        else if([JSON objectForKey:@"message"])
        {
            
            [[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"there is currently no comments", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
        }
        
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {

        [HUD hide:YES];
    }];
    [operation start];
}
-(void)fetchComments:(NSDictionary *)json
{
//    if (p == 0)
//    {
//        [allCommentsList removeAllObjects];
//    }
    [allCommentsList removeAllObjects];
    NSArray *commentsArray = [json objectForKey:@"data"];
    for (NSDictionary *dic in commentsArray)
    {
        UserProfile *user = [[UserProfile alloc] init];
        user.userName = [dic objectForKey:@"username"];
        
        user.userLikes = [[dic objectForKey:@"like_count"] integerValue];
        user.createdAt = [NSString stringWithFormat:@"%@000",[dic objectForKey:@"created"]];
//        user.createdAt = [dic objectForKey:@"createdDate"];
        user.userReview = [dic objectForKey:@"comment"];
        user.commentImage = [dic objectForKey:@"com_img"];
        user.profileImgUrl = [dic objectForKey:@"profile_pic"];
        user.userIdEmail = [dic objectForKey:@"uid_fk"];
        user.isLiked = [dic objectForKey:@"is_liked"];
        user.commentId = [dic objectForKey:@"com_id"];
        user.userName = [dic objectForKey:@"name"];
        
            
            [allCommentsList addObject:user];
        
        
    }
    
//    if ([tempArray count] > 0)
//    {
//        NSMutableArray *arr = [NSMutableArray arrayWithArray:allCommentsList];
//        allCommentsList = [[NSMutableArray alloc] init];
//        NSArray *newArray = [tempArray arrayByAddingObjectsFromArray:arr];
//        [allCommentsList addObjectsFromArray:newArray];
//        
//    }
    [tableVu reloadData];
    if (autoScroll)
    {
        autoScroll = NO;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[allCommentsList count]-1 inSection:0];
        [tableVu scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
   
   
}

- (void)imageButtonPressed:(UIButton *)sender
{
    
    if ([sender isSelected])
    {
//        [[sender layer] setBorderColor:[UIColor whiteColor].CGColor];
//        UIColor *buttonColor = [[UIColor greenColor] colorWithAlphaComponent:0.0f];;
//        [sender setImage:[Methods imageWithColor:buttonColor size:sender.frame.size] forState:UIControlStateNormal];
        [sender setSelected:NO];
        [takePhotoButton setHidden:NO];
        [photoLibraryButton setHidden:NO];
        [sendButton setHidden:YES];
    
        for (UIView *tickView in sender.subviews)
        {
            if (tickView.tag == 7)
            {
                [tickView removeFromSuperview];
            }
        }
        
    } else
    {
        [[sender layer] setBorderColor:[UIColor greenColor].CGColor];
        
        [sender setSelected:YES];
        [takePhotoButton setHidden:YES];
        [photoLibraryButton setHidden:YES];
        [sendButton setHidden:NO];
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(70, selectedBtn.frame.size.height - 30, 30, 30)];
        imgView.layer.cornerRadius = imgView.frame.size.height/2;
        imgView.image = [UIImage imageNamed: @"icon_tick"];
        imgView.tag = 7;
        [sender addSubview:imgView];
    }
}
-(void)selectedImage
{
    timer = YES;
    sendBtn.enabled = YES;
    [smallImageView setImage:chosenImage];
    [txtView setFrame:CGRectMake(70, 10, txtFieldView.frame.size.width - 115, txtFieldView.frame.size.height - 15)];
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         [transparentImgView setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
                     }
                     completion:^(BOOL finished){
                         if (finished)
                             [transparentImgView removeFromSuperview];
                     }];
}

-(void)cancelButtonPressed
{
    chosenImage = nil;
    [smallImageView setImage:chosenImage];
    [txtView setFrame:CGRectMake(40, 10, txtFieldView.frame.size.width - 85, txtFieldView.frame.size.height - 15)];
    if (![txtView.text isEqualToString:@""])
    {
        sendBtn.enabled = YES;
    }
    else
    {
        sendBtn.enabled = NO;
    }
    timer = YES;
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         [transparentImgView setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
                     }
                     completion:^(BOOL finished){
                         if (finished)
                             [transparentImgView removeFromSuperview];
                     }];
}
-(void)libraryPhotoButtonPressed
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}
-(void)takePhotoButtonPressed
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:NULL];
}
-(void)setupView
{
    transparentImgView = [[UIImageView alloc] init];
    [transparentImgView setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    [transparentImgView setImage:[UIImage imageNamed:@"TransparentImage"]];
    [transparentImgView setUserInteractionEnabled:YES];
//    [transparentImgView setBackgroundColor:[UIColor blackColor]];
//    [transparentImgView setAlpha:0.5];
    
    
    
    UIView *cancelView = [[UIView alloc] init];
    [cancelView setFrame:CGRectMake(20, self.view.frame.size.height - 100, self.view.frame.size.width - 40, 50)];
//    [cancelView setCenter:CGPointMake(self.view.frame.size.width / 2, cancelView.frame.origin.y)];
    [cancelView setBackgroundColor:[UIColor whiteColor]];
    [cancelView setAlpha:1.0];
    [cancelView.layer setCornerRadius:5.0];
    [cancelView setClipsToBounds:YES];
    [transparentImgView addSubview:cancelView];
//    UIView *overlay = [[UIView alloc] initWithFrame:cancelView.frame];
//    overlay.backgroundColor = [UIColor blackColor];
//    overlay.alpha = 0.6;
//    [cancelView addSubview:overlay];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btn setFrame:cancelView.bounds];
    [btn setTitle:@"Cancel" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [btn setBackgroundColor:[UIColor whiteColor]];
    [btn addTarget:self action:@selector(cancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelView addSubview:btn];
    
    UIView *libraryORTakePhotoView = [[UIView alloc] init];
    [libraryORTakePhotoView setFrame:CGRectMake(20, cancelView.frame.origin.y - 60, self.view.frame.size.width - 40, 50)];
//    [libraryORTakePhotoView setCenter:CGPointMake(self.view.frame.size.width / 2, libraryORTakePhotoView.frame.origin.y)];
    [libraryORTakePhotoView setBackgroundColor:[UIColor whiteColor]];
    [libraryORTakePhotoView setAlpha:1.0];
    [libraryORTakePhotoView.layer setCornerRadius:5.0];
    [libraryORTakePhotoView setClipsToBounds:YES];
    [transparentImgView addSubview:libraryORTakePhotoView];
    
//    [overlay setFrame:libraryORTakePhotoView.frame];
//    overlay.backgroundColor = [UIColor blackColor];
//    overlay.alpha = 0.6;
//    [libraryORTakePhotoView addSubview:overlay];
    
    sendButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [sendButton setFrame:CGRectMake(0, 0, libraryORTakePhotoView.frame.size.width, libraryORTakePhotoView.frame.size.height)];
    [sendButton setTitle:NSLocalizedString(@"Use Selected Image", nil) forState:UIControlStateNormal];
    [sendButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [sendButton setBackgroundColor:[UIColor whiteColor]];
    [sendButton setHidden:YES];
    [sendButton setAlpha:1.0];
    [sendButton addTarget:self action:@selector(selectedImage) forControlEvents:UIControlEventTouchUpInside];
    [libraryORTakePhotoView addSubview:sendButton];
    
    photoLibraryButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [photoLibraryButton setFrame:CGRectMake(0, 0, libraryORTakePhotoView.frame.size.width / 2, libraryORTakePhotoView.frame.size.height)];
    [photoLibraryButton setTitle:NSLocalizedString(@"Photo Library", nil) forState:UIControlStateNormal];
    [photoLibraryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [photoLibraryButton setBackgroundColor:[UIColor whiteColor]];
    [photoLibraryButton addTarget:self action:@selector(libraryPhotoButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [photoLibraryButton.layer setBorderColor:[UIColor blackColor].CGColor];
    [photoLibraryButton.layer setBorderWidth:0.5];
    [photoLibraryButton setAlpha:1.0];
    [libraryORTakePhotoView addSubview:photoLibraryButton];
    
    takePhotoButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [takePhotoButton setFrame:CGRectMake(libraryORTakePhotoView.frame.size.width / 2, 0, libraryORTakePhotoView.frame.size.width / 2, libraryORTakePhotoView.frame.size.height)];
    [takePhotoButton setTitle:NSLocalizedString(@"Take Photo", nil) forState:UIControlStateNormal];
    [takePhotoButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [takePhotoButton setBackgroundColor:[UIColor whiteColor]];
    [takePhotoButton addTarget:self action:@selector(takePhotoButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [takePhotoButton.layer setBorderColor:[UIColor blackColor].CGColor];
    [takePhotoButton.layer setBorderWidth:0.5];
    [takePhotoButton setAlpha:1.0];
    [libraryORTakePhotoView addSubview:takePhotoButton];
    
    selectedPhotoView = [[UIView alloc] init];
    [selectedPhotoView setFrame:CGRectMake(20, libraryORTakePhotoView.frame.origin.y - 130, self.view.frame.size.width - 40, 120)];
//    [selectedPhotoView setCenter:CGPointMake(self.view.frame.size.width / 2, selectedPhotoView.frame.origin.y)];
    [selectedPhotoView setBackgroundColor:[UIColor whiteColor]];
    [selectedPhotoView setHidden:YES];
    [selectedPhotoView.layer setCornerRadius:5.0];
    [selectedPhotoView setClipsToBounds:YES];
    [selectedPhotoView setAlpha:1.0];
    [transparentImgView addSubview:selectedPhotoView];
    
    //    selectedImageView = [[UIImageView alloc] init];
    selectedBtn = [[UIButton alloc] init];
    [selectedBtn setFrame:CGRectMake(10, 10, 100, 100)];
    [selectedBtn setCenter:CGPointMake(selectedPhotoView.frame.size.width / 2, selectedPhotoView.frame.size.height / 2)];
    [selectedBtn setHidden:YES];
//    [selectedBtn setBackgroundImage:[self imageWithColor:[UIColor clearColor]] forState:UIControlStateHighlighted];
    [selectedBtn addTarget:self action:@selector(imageButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [selectedPhotoView addSubview:selectedBtn];
    
    
}
- (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)cameraButtonTapped:(UIButton *)sender
{
    
    [txtView resignFirstResponder];
    [self setupView];
    timer = NO;
    
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         [transparentImgView setFrame:self.view.frame];
                     }
                     completion:^(BOOL finished){
                     }];
    [self.view addSubview:transparentImgView];
}


- (void)sendButtonTapped:(UIButton *)sender
{
    [txtView resignFirstResponder];

    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         [transparentImgView setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
                     }
                     completion:^(BOOL finished){
                         if (finished)
                             [transparentImgView removeFromSuperview];
                     }];
    
    
//    UserProfile *user = [[UserProfile alloc] init];
//    user.firstName = @"Nat";
//    user.lastName = @"";
    if (!txtView.text || [txtView.text isEqualToString:@""])
    {
        if (chosenImage)
        {
            
            [self insertComment:@""];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter some text." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
    else
    {
        [self insertComment:txtView.text];
        
        [self resizeTextView:43];
        
        
        
    }
   
    
    
//    [userList addObject:user];
    [txtView setText:@""];
//    [tableVu reloadData];
//    [tableVu scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:([userList count] - 1) inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}


-(void)insertComment:(NSString *)commentText
{
    [HUD setLabelText:@""];
    [HUD show:YES];
//    NSString *apiURL=[NSString stringWithFormat:@"http://www.rightips.com/api/index.php?action=insertComment&uid=%@&msg_id=%@&comment=%@",userProfile.userIdEmail,self.selNotxData.msgId,commentText];
    NSString *apiURL=@"http://www.rightips.com/api/index.php?action=insertComment";
//     NSString *apiURL=@"http://esol-tech.com/comment/test.php?action=insertComment";
    CGRect rect = CGRectMake(0,0,500,500);
    UIGraphicsBeginImageContext( rect.size );
    [chosenImage drawInRect:rect];
    UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSData *imageData = UIImagePNGRepresentation(picture1);
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setObject:userProfile.userIdEmail forKey:@"uid"];
    [postParams setObject:self.selNotxData.msgId forKey:@"msg_id"];
    [postParams setObject:commentText forKey:@"comment"];
//    [postParams setObject:imageData forKey:@"file"];
    
    NSURL *url = [NSURL URLWithString:apiURL];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request;
    if (!chosenImage)
    {
        request = [client requestWithMethod:@"POST" path:apiURL parameters:postParams];
    }
    else
    {
        request = [client multipartFormRequestWithMethod:@"POST" path:apiURL parameters:postParams constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
            [formData appendPartWithFileData: imageData name:@"file" fileName:@"file.png" mimeType:@"image/png"];
        }];
    }
    
    
    
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [HUD hide:YES];
        if([[JSON objectForKey:@"status"] integerValue] == 1)
        {
//            chosenImage = nil;
//            [smallImageView setImage:chosenImage];
//            [txtView setFrame:CGRectMake(40, 10, txtFieldView.frame.size.width - 85, txtFieldView.frame.size.height - 15)];
//            [self getComments:0];
            NSDictionary *commentsDic = [JSON objectForKey:@"data"];
            
            
            UserProfile *user = [[UserProfile alloc] init];
            user.userName = [commentsDic objectForKey:@"username"];
                
//                user.userLikes = [[commentsDic objectForKey:@"like_count"] integerValue];
            user.createdAt = [NSString stringWithFormat:@"%@000",[commentsDic objectForKey:@"created"]];
//            user.createdAt = [commentsDic objectForKey:@"createdDate"];
            user.userReview = [commentsDic objectForKey:@"comment"];
            user.commentImage = [commentsDic objectForKey:@"com_img"];
            user.profileImgUrl = [commentsDic objectForKey:@"profile_pic"];
            user.userIdEmail = [commentsDic objectForKey:@"uid_fk"];
            user.isLiked = [commentsDic objectForKey:@"is_liked"];
            user.commentId = [commentsDic objectForKey:@"com_id"];
            user.userName = [commentsDic objectForKey:@"name"];
            [allCommentsList addObject:user];
            [tableVu reloadData];
            
            if ([allCommentsList count] > 0)
            {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[allCommentsList count]-1 inSection:0];
                [tableVu scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            }
           
            
        }
        else if([JSON objectForKey:@"message"])
        {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
        }
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        [HUD hide:YES];
        [Methods showAlertView:@"" message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }];
    [operation start];
    
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [allCommentsList count];
}

- (CommentsCellTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     UserProfile *user = [allCommentsList objectAtIndex:indexPath.row];
    
    CommentsCellTableViewCell *customCell;
    if  (![user.commentImage isEqual:[NSNull null]] && ![user.commentImage isEqualToString:@""])
    {
        customCell = [[[NSBundle mainBundle] loadNibNamed:@"CommentsCellTableViewCell" owner:nil options:nil] objectAtIndex: 1];
        [customCell.commentImageView setImageURL:[NSURL URLWithString:user.commentImage]];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
        [tap addTarget:self action:@selector(imageTapped:)];
        [customCell.commentImageView addGestureRecognizer:tap];
        
        
    }
    else
    {
        customCell = [[[NSBundle mainBundle] loadNibNamed:@"CommentsCellTableViewCell" owner:nil options:nil] objectAtIndex: 0];
    
    }
    [customCell.contentView setBackgroundColor:[UIColor clearColor]];
    
    
    
    
    
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSDate *date = [dateFormat dateFromString:user.createdAt];
    NSString *timeLeft = [self timeLeftSinceTime:user.createdAt.doubleValue];
    [customCell.createdTime setText:timeLeft];
    
    
    if (user.profileImgUrl && ![user.profileImgUrl isEqual:[NSNull null]])
    {
        [customCell.profileImageView setImageURL:[NSURL URLWithString:user.profileImgUrl]];
    }
    else
    {
        [customCell.profileImageView setImage:[UIImage imageNamed:@"NoProfileImage"]];
    }
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initwitwit];
    
    [customCell.likeButton setTag:indexPath.row];
    

    
    [customCell.likeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

    [customCell.likeButton setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
    
    
    [customCell.deleButton addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [customCell.deleButton setTag:indexPath.row];
    
   
    
    
    
    if ([user.isLiked isEqualToString:@"0"])
    {
        [customCell.likeButton setSelected:NO];
        if (user.userLikes == 0)
        {
            [customCell.likeButton setTitle:@"Like" forState:UIControlStateNormal];
        }
        else
        {
            [customCell.likeButton setTitle:[NSString stringWithFormat:@"Likes(%ld)",(long)user.userLikes] forState:UIControlStateNormal];
        }
        

    }
    else
    {
        [customCell.likeButton setSelected:YES];
        [customCell.likeButton setTitle:[NSString stringWithFormat:@"Likes(%ld)",(long)user.userLikes] forState:UIControlStateSelected];
    }
    
    [customCell.likeButton addTarget:self action:@selector(likeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [customCell.userNameLabel setText:user.userName];
    customCell.descriptionLabel.text = user.userReview;
    [customCell.descriptionLabel sizeToFit];
    
    
    
//    NSDate *commentCreateDate = [NSDate dateWithTimeIntervalSince1970:*(user.createdAt)];
//    NSString *dateTimeStr = [Methods stringFromDate:commentCreateDate dateFormat:@"dd.MM.yyyy - hh:mm a" timeZone:[NSTimeZone defaultTimeZone]];
//    [cell.createdTime setText:dateTimeStr];
    
    
//    cell.profileImageView.imageURL = [NSURL URLWithString:user.profileImgUrl];
//    cell.userNameLabel.text = [NSString stringWithFormat:@"%@ %@",user.firstName,user.lastName];
//    if (user.commentImage)
//    {
//        UIImageView *imgView = [[UIImageView alloc] init];
//        [imgView setFrame:CGRectMake(cell.descriptionLabel.frame.origin.x, cell.descriptionLabel.frame.origin.y + cell.descriptionLabel.frame.size.height, 100, 100)];
//        [imgView setImage:user.commentImage];
//        [imgView setContentMode:UIViewContentModeScaleAspectFit];
//        [cell.contentView addSubview:imgView];
//        
//    }
    
    [customCell sizeToFit];
    customCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return customCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UserProfile *user = [allCommentsList objectAtIndex:indexPath.row];
    CommentsCellTableViewCell *customCell;
    if  (![user.commentImage isEqual:[NSNull null]] && ![user.commentImage isEqualToString:@""])
    {
        customCell = [[[NSBundle mainBundle] loadNibNamed:@"CommentsCellTableViewCell" owner:nil options:nil] objectAtIndex: 1];
                [customCell.commentImageView setImageURL:[NSURL URLWithString:user.commentImage]];
        
//        [customCell.commentImageView setImage:[UIImage imageNamed:@"category_icon_shop"]];
        
        
    }
    else
    {
        customCell = [[[NSBundle mainBundle] loadNibNamed:@"CommentsCellTableViewCell" owner:nil options:nil] objectAtIndex: 0];
        
    }
    
    [customCell.userNameLabel setText:user.userName];
    customCell.descriptionLabel.text = user.userReview;
    [customCell.descriptionLabel sizeToFit];
//    [customCell sizeToFit];
    
//    return [self calculateHeightForConfiguredSizingCell:customCell];
    return  customCell.frame.size.height + customCell.descriptionLabel.frame.size.height;
//    return customCell.frame.size.height;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserProfile *user = [allCommentsList objectAtIndex:indexPath.row];
    
    
    ProfileVC *vc = (ProfileVC *)[Methods getVCbyName:@"ProfileVC"];
    [vc setUserProfile:user];
    [Methods pushVCinNCwithObj:vc popTop:NO];
}
- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell
{
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    if (editingStyle == UITableViewCellEditingStyleDelete)
//    {
//        int index = indexPath.row;
//        [allCommentsList removeObjectAtIndex:index];
//        
//        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
//                         withRowAnimation:UITableViewRowAnimationBottom];
//    }
//}

-(void)imageTapped:(UITapGestureRecognizer *)recognizer
{
    UIImageView *imgView = (UIImageView *)recognizer.view;
    
    NotxCommentImageVC *vc = (NotxCommentImageVC *)[Methods getVCbyName:@"NotxCommentImageVC"];
    [vc setImg:imgView.image];
    [Methods pushVCinNCwithObj:vc popTop:NO];
    
}

-(void)deleteButtonPressed:(UIButton *)sender
{
    delButton = sender;
    UserProfile *user = [allCommentsList objectAtIndex:sender.tag];
    
    if ([user.userIdEmail isEqualToString:userProfile.userIdEmail])
    {
        
        [Methods showDeleteActionSheet:user.commentId.integerValue];
        
    }
    else
    {
        
        [Methods showReportActionSheet:user.commentId.integerValue];
    }
    
    
    
}
-(void)deleteComment
{
    CommentsCellTableViewCell *cell = (CommentsCellTableViewCell*) delButton.superview.superview;
    NSIndexPath *indexPath = [tableVu indexPathForCell:cell];
    
    //            NSUInteger ind = [allCommentsList indexOfObject:specificUser];
    [allCommentsList removeObjectAtIndex:delButton.tag];
    [tableVu beginUpdates];
    [tableVu deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [tableVu endUpdates];
    [tableVu reloadData];
}




-(void)likeButtonPressed:(UIButton *)sender
{
    
    [self likeComment:sender];
//    if ([sender isSelected])
//    {
//        
//        [self unlikeComment:sender];
//    }
//    else
//    {
//        
//        
//        [self likeComment:sender];
//    }
}
-(void)likeComment:(UIButton *)btn
{
    [HUD setLabelText:@""];
    [HUD show:YES];
    specificUser = [allCommentsList objectAtIndex:btn.tag];
    
    NSString *apiURL=@"http://www.rightips.com/api/index.php?action=like";
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setObject:userProfile.userIdEmail forKey:@"uid"];
    [postParams setObject:[NSString stringWithFormat:@"%ld",specificUser.commentId.integerValue] forKey:@"id"];
    [postParams setObject:@"comment" forKey:@"type"];
    if ([specificUser.isLiked isEqualToString:@"0"])
    {
        [postParams setObject:@"Like" forKey:@"rel"];
        isLiked = YES;
    }
    else
    {
        [postParams setObject:@"Unlike" forKey:@"rel"];
        isLiked = NO;
    }
    
    NSURL *url = [NSURL URLWithString:apiURL];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiURL parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [HUD hide:YES];
        if([[JSON objectForKey:@"status"] integerValue] == 1)
        {
            if (isLiked)
            {
            [btn setSelected:YES];
            specificUser.userLikes++;
            specificUser.isLiked = @"1";
            [allCommentsList replaceObjectAtIndex:btn.tag withObject:specificUser];
            [btn setTitle:[NSString stringWithFormat:@"Likes(%ld)",(long)specificUser.userLikes] forState:UIControlStateSelected];
            }
            else
            {
                [btn setSelected:NO];
                specificUser.userLikes--;
                specificUser.isLiked = @"0";
                [allCommentsList replaceObjectAtIndex:btn.tag withObject:specificUser];
                if (specificUser.userLikes == 0)
                {
                    [btn setTitle:[NSString stringWithFormat:@"Like"] forState:UIControlStateNormal];
                }
                else
                {
                    [btn setTitle:[NSString stringWithFormat:@"Likes(%ld)",(long)specificUser.userLikes] forState:UIControlStateNormal];
                }
            }
        }
        else if([JSON objectForKey:@"message"])
        {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
        }
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        [HUD hide:YES];
        [Methods showAlertView:@"" message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }];
    [operation start];
}

-(void)unlikeComment:(UIButton *)btn
{
    [HUD setLabelText:@""];
    [HUD show:YES];
    specificUser = [allCommentsList objectAtIndex:btn.tag];
    
    NSString *apiURL=@"http://www.rightips.com/api/index.php?action=like";
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setObject:userProfile.userIdEmail forKey:@"uid"];
    [postParams setObject:[NSString stringWithFormat:@"%ld",specificUser.commentId.integerValue] forKey:@"id"];
    [postParams setObject:@"Unlike" forKey:@"rel"];
    [postParams setObject:@"comment" forKey:@"type"];
    
    
    NSURL *url = [NSURL URLWithString:apiURL];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiURL parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [HUD hide:YES];
        if([[JSON objectForKey:@"status"] integerValue] == 1)
        {
            [btn setSelected:NO];
            specificUser.userLikes--;
             specificUser.isLiked = @"0";
            [allCommentsList replaceObjectAtIndex:btn.tag withObject:specificUser];
            if (specificUser.userLikes == 0)
            {
                [btn setTitle:[NSString stringWithFormat:@"Like"] forState:UIControlStateNormal];
            }
            else
            {
                [btn setTitle:[NSString stringWithFormat:@"Likes(%ld)",(long)specificUser.userLikes] forState:UIControlStateNormal];
            }
            
            
        }
        else if([JSON objectForKey:@"message"])
        {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
        }
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        [HUD hide:YES];
        [Methods showAlertView:@"" message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }];
    [operation start];
}


-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(void)hideKeyBoard
{
    [txtView resignFirstResponder];
    [self resizeTextView:43.0];
}
- (IBAction)backButtonPressed:(UIButton *)sender
{
    if (isKeyboardHide)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UINavigationController *navController = (UINavigationController *) appDelegate.window.rootViewController;
        [navController popViewControllerAnimated:YES];
    }
    else
    {
        [self hideKeyBoard];
    }
    
}
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0 && popup.tag == 1)
    {
        [self deleteComment];
    }
    
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    chosenImage = info[UIImagePickerControllerEditedImage];
    
    if (chosenImage)
    {
    
        [selectedPhotoView setHidden:NO];
        [selectedBtn setHidden:NO];
        [selectedBtn setSelected:YES];
//        [selectedBtn setBackgroundImage:chosenImage forState:UIControlStateNormal];
        [selectedBtn setImage:chosenImage forState:UIControlStateNormal];
//        [selectedBtn setBackgroundImage:chosenImage forState:UIControlStateHighlighted];
        [takePhotoButton setHidden:YES];
        [photoLibraryButton setHidden:YES];
        [sendButton setHidden:NO];
        
        UIImageView *tickImageView = [[UIImageView alloc] initWithFrame:CGRectMake(70, selectedBtn.frame.size.height - 30, 30, 30)];
        tickImageView.layer.cornerRadius = tickImageView.frame.size.height/2;
        tickImageView.image = [UIImage imageNamed:@"icon_tick"];
        [tickImageView setTag:7];
        [selectedBtn addSubview:tickImageView];
    }
    
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
//- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
//{
//    
//    if([text isEqualToString:@"\n"]) {
//        [textView resignFirstResponder];
//        return NO;
//    }
//
//    return YES;
//}
//-(void)growingTextView:(HPGrowingTextView *)growingTextView didChangeHeight:(float)height
//{
////    [txtFieldView setFrame:CGRectMake(0,self.view.frame.size.height - height, self.view.frame.size.width, height)];
//    CGRect frame = txtFieldView.frame;
//    frame.size.height = height;
//    txtFieldView.frame = frame;
//}
- (void)textViewDidChange:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""] && !chosenImage)
    {
        sendBtn.enabled = NO;
    }
    else
    {
        sendBtn.enabled = YES;
    }
    CGFloat height = textView.contentSize.height  + keyboardSize.height;
    [self resizeTextView:height];
//    CGRect frame = txtView.frame;
//    frame.size.height = textView.contentSize.height + 5;
//    txtView.frame = frame;
    
//    CGRect f = txtFieldView.frame;
//    f.size.height = textView.contentSize.height +5;
//    f.origin.y = self.view.frame.size.height - (txtView.contentSize.height + keyboardSize.height);
//    txtFieldView.frame = f;
    
    
}
-(void)resizeTextView:(CGFloat)height
{
    CGRect frame = txtView.frame;
    frame.size.height = height - 15;
    txtView.frame = frame;
    
    CGRect f = txtFieldView.frame;
    f.size.height = height + 5;
    f.origin.y = self.view.frame.size.height - (height + 5);
    txtFieldView.frame = f;
    
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDelegate:self];
//    [UIView setAnimationDuration:0.5];
//    [UIView setAnimationBeginsFromCurrentState:YES];
//    txtFieldView.frame = CGRectMake(txtFieldView.frame.origin.x, (txtFieldView.frame.origin.y - 100.0), txtFieldView.frame.size.width, txtFieldView.frame.size.height);
//    [UIView commitAnimations];

    

}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDelegate:self];
//    [UIView setAnimationDuration:0.5];
//    [UIView setAnimationBeginsFromCurrentState:YES];
//     .frame = CGRectMake(txtFieldView.frame.origin.x, (txtFieldView.frame.origin.y + 100.0), txtFieldView.frame.size.width, txtFieldView.frame.size.height);
//    [UIView commitAnimations];
    
    
}
- (void)keyboardWillShow:(NSNotification *)notification
{
    isKeyboardHide = NO;
    timer = NO;
    keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = txtFieldView.frame;
        f.origin.y = f.origin.y - keyboardSize.height;
        txtFieldView.frame = f;
        
        
//        f = tableVu.frame;
//        f.size.height = f.size.height - keyboardSize.height;
//        tableVu.frame = f;
        
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    isKeyboardHide = YES;
    timer = YES;
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = txtFieldView.frame;
        f.origin.y = f.origin.y + keyboardSize.height;
        txtFieldView.frame = f;
        keyboardSize.height = 0;
        
//        f = tableVu.frame;
//        f.size.height = f.size.height + keyboardSize.height;
//        tableVu.frame = f;
    }];
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    autoTimer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(refresh) userInfo:nil repeats:YES];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)refresh
{
    [self getComments:p];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    /**
     *  Enable/disable springy bubbles, default is NO.
     *  You must set this from `viewDidAppear:`
     *  Note: this feature is mostly stable, but still experimental
     */
   
}
- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    [autoTimer invalidate];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions



@end
