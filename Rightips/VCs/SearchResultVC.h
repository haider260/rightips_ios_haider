//
//  SearchResultVC.h
//  Rightips
//
//  Created by Mac on 16/03/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultVC : UIViewController <UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,UIGestureRecognizerDelegate>
{
    
    IBOutlet UITableView *searchResultTableView;
    
    IBOutlet UIButton *muteButton;
    
}
@property (nonatomic,strong) NSString *searchString;
@property (nonatomic, strong) NSMutableArray *searchResultsList;

@end
