//
//  InvitationVC.m
//  Rightips
//
//  Created by Shahbaz Ali on 7/15/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "InvitationVC.h"
#import <CoreText/CoreText.h>
@interface InvitationVC ()
{
    UserProfile *userProfile;
    MBProgressHUD *HUD;
}
@end

@implementation InvitationVC
@synthesize paramsDict, inviteLabel, discoverPlacesLabel,friendImgView;

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:[StaticData instance].kLoggedIn])
    {
        [self.fbButton setHidden:YES];
    }
    userProfile = [[UserProfile alloc] init];
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    [[friendImgView layer] setCornerRadius:friendImgView.bounds.size.width/2];

    if ([paramsDict objectForKey:@"pictureURL"])
    {
        [Methods setImageInImageViewFromUrl:friendImgView urlStr:[paramsDict objectForKey:@"pictureURL"]];
    }
    
    NSMutableAttributedString *nameString = [self getBoldAttributedStringWithString:[paramsDict objectForKey:kReferringUsername]];
    NSMutableAttributedString *fNameString = [self getBoldAttributedStringWithString:[[[paramsDict objectForKey:kReferringUsername] componentsSeparatedByString:@" "] firstObject]];
    
    NSAttributedString *tempString = [[NSAttributedString alloc] initWithString:@" wants you to join them on RighTips."];
    [nameString appendAttributedString:tempString];
    NSMutableAttributedString *tempString2 = [[NSMutableAttributedString alloc] initWithString:@"Join "];
    NSAttributedString *tempString3 = [[NSMutableAttributedString alloc] initWithString:@" and discover amazing places"];

    [tempString2 appendAttributedString:fNameString];
    [tempString2 appendAttributedString:tempString3];
    inviteLabel.attributedText = nameString;
    discoverPlacesLabel.attributedText = tempString2;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSMutableAttributedString*) getBoldAttributedStringWithString: (NSString*)string
{
    NSRange boldedRange = NSMakeRange(0, string.length);
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:string];
    [attrString beginEditing];
    [attrString addAttribute:(NSString*)kCTFontAttributeName
                           value:[UIFont boldSystemFontOfSize:17]
                           range:boldedRange];
    
    [attrString endEditing];
    return attrString;
}
#pragma mark - IBActions
- (IBAction)closeInviteButton:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navController = (UINavigationController *) appDelegate.window.rootViewController;
    [navController popViewControllerAnimated:YES];
}

- (IBAction)fbButton:(id)sender
{
//    [FBSession openActiveSessionWithReadPermissions:[StaticData instance].fbPermissions allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
//        AppDelegate* appDelegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
//        [appDelegate sessionStateChanged:session state:state error:error];
//        
//        if(!error)
//        {
//            switch (state)
//            {
//                case FBSessionStateOpen:
//                    [FBSession setActiveSession:session];
//                    [self retrieveFbUserData];
//                    break;
//                case FBSessionStateClosedLoginFailed:
//                    NSLog(@"Failed to log in");
//                    break;
//                default:
//                    break;
//            }
//            
//        } else
//        {
//            NSLog(@"fbsession error: %@", [error localizedDescription]);
//            //[Methods showAlertView:@"Oops!" message:@"Login with facebook failed!" buttonText:@"OK" tag:0 delegate:nil]; // commented as error popups are being shown in sessionStateChanged method.
//        }
//    }];
}

#pragma mark - Facebook Handling Methods
- (void)retrieveFbUserData
{
//    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
//    {
//        if (!error)
//        {
//            NSString *smId =  [result objectForKey:@"id"] != nil ? [result objectForKey:@"id"] : @"";
//            NSString *email =  [result objectForKey:@"email"] != nil ? [result objectForKey:@"email"] : @"";
//            NSString *firstName = [result objectForKey:@"first_name"] != nil ? [result objectForKey:@"first_name"] : @"";
//            NSString *lastName = [result objectForKey:@"last_name"] != nil ? [result objectForKey:@"last_name"] : @"";
//            
//            userProfile.profileImgUrl = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", smId];
//            
//            
//            [self performLogin:[StaticData instance].kProviderFacebook.stringValue smId:smId email:email firstName:firstName lastName:lastName];
//        } else {
//            [HUD hide:YES];
//            NSLog(@"fbrequestconnection error: %@", [error localizedDescription]);
//            [Methods showAlertView:NSLocalizedString(@"Oops!", nil) message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
//        }
//    }];
//    [HUD show:YES];
}

- (void)performLogin:(NSString *)provider smId:(NSString *)smId  email:(NSString *)email firstName:(NSString *)firstName lastName:(NSString *)lastName {
    NSString *apiUrl = [NSString stringWithFormat:@"%@", [StaticData instance].baseUrl];
    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"login"]];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setValue:firstName forKey:@"fname"];
    [postParams setValue:lastName forKey:@"lname"];
    [postParams setValue:email forKey:@"email"];
    [postParams setValue:@"" forKey:@"pass"];
    [postParams setValue:smId forKey:@"sm_id"];
    [postParams setValue:provider forKey:@"provider"];
    [postParams setValue:@"0" forKey:@"signup"];
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:apiUrl parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [HUD hide:YES];
        userProfile.provider = provider;
        userProfile.smId = smId;
        userProfile.email = email;
        [self handleLoginSuccess:JSON];
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        [HUD hide:YES];
        [Methods showAlertView:@"" message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }];
    [operation start];
    [HUD setLabelText:NSLocalizedString(@"Please Wait.", nil)];
    [HUD setDetailsLabelText:NSLocalizedString(@"Signing in ...", nil)];
    [HUD show:YES];
}

- (void)handleLoginSuccess:(id)JSON {
    NSString *status = [JSON objectForKey:@"status"];
    NSString *message = [JSON objectForKey:@"message"];
    if ([status intValue] == 1) {
        NSDictionary *dataDict = [JSON objectForKey:@"data"];
        
        userProfile.userIdEmail = [dataDict objectForKey:@"uid"];
        userProfile.firstName = [dataDict objectForKey:@"first_name"];
        userProfile.lastName = [dataDict objectForKey:@"last_name"];

        NSData *userProfileData = [NSKeyedArchiver archivedDataWithRootObject:userProfile];
        [[NSUserDefaults standardUserDefaults] setObject:userProfileData forKey:[StaticData instance].kUserProfileData];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[StaticData instance].kLoggedIn];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:kReferringUsersArray])
        {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:kReferringUsersArray] count]>0)
            {
                [ReuseAbleFunctions addFriends];
            }
        }
        // Piwik Event (Action Login)
        [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_USERS action:ACTION_LOGIN name:[ReuseAbleFunctions convertDictionaryToJSONString:dataDict] value:nil];
        
        [self pushNextVC];
    } else {
        [Methods showAlertView:@"" message:message buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }
}

- (void)pushNextVC {
    id object = [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kSelCatsIds];
    if (!object) [Methods pushVCinNCwithName:@"SettingsVC" popTop:YES];
    else [Methods pushVCinNCwithName:@"HomeVC" popTop:YES];
}


@end
