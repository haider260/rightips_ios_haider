//
//  NotificationVC.m
//  Rightips
//
//  Created by Abdul Mannan on 12/8/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import "NotificationVC.h"

@interface NotificationVC () <BWBeaconManagerDelegate, UIWebViewDelegate>
{
    BWBeaconManager *_beaconManager;
    BWBeaconRegion *_beaconRegion;
    
    NSString *lastTrackedBeaconUMM;
    CLProximity lastTrackedZone;
    
    UIActivityIndicatorView *aiView;
}

@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *aiView;

@end

@implementation NotificationVC

@synthesize webView, aiView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc]
                                    initWithTitle:@"Home"
                                    style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(homeButtonPressed:)];
    self.navigationItem.leftBarButtonItem = leftButton;
    
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc]
                                    initWithTitle:@"Refresh"
                                    style:UIBarButtonItemStyleDone
                                    target:self
                                    action:@selector(refreshButtonPressed:)];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    _beaconManager = [[BWBeaconManager alloc] init];
    _beaconManager.delegate = self;
    
    if ([_beaconManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        [_beaconManager requestAlwaysAuthorization];
    
    NSString *uuidStr = @"5144A35F-387A-7AC0-B254-DFB1381C9DFF";
    NSString *idStr = @"com.beaconwatcher";
    NSUUID *proximityUUID = [[NSUUID alloc] initWithUUIDString:uuidStr];
    _beaconRegion = [[BWBeaconRegion alloc] initWithProximityUUID:proximityUUID identifier:idStr];
    //_beaconRegion.notifyEntryStateOnDisplay = YES;
    _beaconRegion.notifyOnEntry = YES;
    _beaconRegion.notifyOnExit = YES;

    [_beaconManager startMonitoringForRegion:_beaconRegion];
    
    [_beaconManager setBackgroundScanPeriod:3000];
    [_beaconManager updateScanPeriods];
    [_beaconManager startRangingBeaconsInRegion:_beaconRegion];
    
    lastTrackedBeaconUMM = @"";
    lastTrackedZone = CLProximityUnknown;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //Sending current screen to Piwic Tracker
    //[[PiwikTracker sharedInstance] sendView:@"Notification"];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [_beaconManager stopRangingBeaconsInRegion:_beaconRegion];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)homeButtonPressed:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)refreshButtonPressed:(id)sender
{
    [Methods fetchAllNotifications];
}

- (void)beaconManager:(BWBeaconManager *)manager didDetermineState:(CLRegionState)state forRegion:(BWBeaconRegion *)region {
    if (state == CLRegionStateInside)
    {
        UIApplicationState appState = [[UIApplication sharedApplication] applicationState];
        if (appState != UIApplicationStateActive) {
            [Methods generateNotification:NSLocalizedString(@"Touch to view content", nil)];
        }
    }
}

- (void) beaconManager:(BWBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(BWBeaconRegion *)region
{
    if ([beacons count] > 0)
    {
        [self processRangedBeacons:beacons];
    }
}

- (void)processRangedBeacons:(NSArray *)beacons
{
    BWBeacon *selBeacon;
    NSString *beaconUMM = @"";
    NotificationData *nd;
    
    NSMutableArray *validBeacons = [NSMutableArray array];
    for (unsigned int index = 0; index < [beacons count]; index++)
    {
        BWBeacon *beacon = [beacons objectAtIndex:index];
        if (beacon.accuracy >= 0.0) //exlude accuracy=-1.0
            [validBeacons addObject:beacon];
    }
        
    // Sorting is not needed as beacons are already sorted based on accuracy.
    
    for (unsigned int index = 0; index < [validBeacons count]; index++) {
        BWBeacon *beacon = [validBeacons objectAtIndex:index];
        nd = [self getNotificationDataByBeacon:beacon];
        if (nd != nil) {
            selBeacon = beacon;
            beaconUMM = [self getBeaconUMM:beacon];
            break;
        }
    }
    
    if (nd != nil) {
        if (![lastTrackedBeaconUMM isEqualToString:beaconUMM])
        {
            lastTrackedBeaconUMM = beaconUMM;
            lastTrackedZone = selBeacon.proximity;
            [self setNotificationContent:nd];
        } else { //Same beacon
            if (lastTrackedZone != selBeacon.proximity)
            {
                lastTrackedZone = selBeacon.proximity;
                [self setNotificationContent:nd];
            }
        }
    }
}

- (NSArray *)sortBeacons:(NSArray *)beacons
{
    return [beacons sortedArrayUsingComparator:^NSComparisonResult(BWBeacon *beacon1, BWBeacon *beacon2)
    {
        if (beacon1.accuracy > beacon2.accuracy)
            return (NSComparisonResult)NSOrderedDescending;
        if (beacon1.accuracy < beacon2.accuracy)
            return (NSComparisonResult)NSOrderedAscending;
        return (NSComparisonResult)NSOrderedSame;
    }];
}

- (NotificationData *)getNotificationDataByBeacon:(BWBeacon *)beacon
{
    NSMutableArray *bwns = [StaticData instance].notifications;
    for (unsigned int index = 0; index < [bwns count]; index++)
    {
        BeaconWithNotification *bwn = [bwns objectAtIndex:index];
        if ([bwn.uuid caseInsensitiveCompare:beacon.proximityUUID.UUIDString] == NSOrderedSame &&
            [bwn.major isEqualToString:beacon.major.stringValue] &&
            [bwn.minor isEqualToString:beacon.minor.stringValue]) {
            for (unsigned int j = 0; j < [bwn.notifications count]; j++) {
                NotificationData *nd = [bwn.notifications objectAtIndex:j];
                if ([nd.zone intValue] == beacon.proximity)
                    return nd;
            } break;
        }
    }
    return nil;
}

- (NSString *)getBeaconUMM:(BWBeacon *)beacon
{
    NSString *ummStr = @"";
    ummStr = [ummStr stringByAppendingString:beacon.proximityUUID.UUIDString];
    ummStr = [ummStr stringByAppendingString:@"_"];
    ummStr = [ummStr stringByAppendingString:beacon.major.stringValue];
    ummStr = [ummStr stringByAppendingString:@"_"];
    ummStr = [ummStr stringByAppendingString:beacon.minor.stringValue];
    return ummStr;
}



- (void)setNotificationContent:(NotificationData *)nd
{
    
    if (nd.notxTemplate != nil)
    {
        if (![nd.notxTemplate isEqualToString:@""])
        {
            NSURL* url = [NSURL URLWithString:nd.notxTemplate];
            NSURLRequest* request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
            [webView loadRequest:request];
            [aiView startAnimating];
            return;
        }
    }
    
    NSString *customHtml = @"";
    customHtml = [customHtml stringByAppendingString:@"<html>"];
    customHtml = [customHtml stringByAppendingString:@"<body>"];
    customHtml = [customHtml stringByAppendingString:@"<h2>"];
    customHtml = [customHtml stringByAppendingString:nd.title];
    customHtml = [customHtml stringByAppendingString:@"</h2>"];
    customHtml = [customHtml stringByAppendingString:@"<h4>"];
    customHtml = [customHtml stringByAppendingString:nd.detail];
    customHtml = [customHtml stringByAppendingString:@"</h4>"];
    customHtml = [customHtml stringByAppendingString:@"<br/><br/>"];
    customHtml = [customHtml stringByAppendingString:@"</body>"];
    customHtml = [customHtml stringByAppendingString:@"</html>"];
    
    [webView loadHTMLString:customHtml baseURL:nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [aiView stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
}

@end
