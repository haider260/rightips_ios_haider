//
//  SettingsVC.m
//  Rightips
//
//  Created by Abdul Mannan on 11/27/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import "SettingsVC.h"
#import "ImageCaptureView.h"

#define kCatFoodImg    @"category_icon_bar"
#define kCatShopsImg    @"category_icon_shop"
#define kCatServicesImg    @"category_icon_services"
#define kCatEntertainmentImg    @"category_icon_entertainment"
#define kCatTourismImg    @"category_icon_tourism"
#define kCatEventsImg    @"category_icon_events"
#define kCatEducationImg    @"category_icon_students"
#define kTickImage  @"icon_tick"
@interface SettingsVC () {
    MBProgressHUD *HUD;
    SlideMenu *slideMenu;
    
    NSUserDefaults *defaults;
    BOOL isLoggedIn;
    NSMutableArray *recvCategories;
    NSMutableArray *selCatsIds;
    NSMutableArray *catButtons;
    UserProfile *loginUserProfile;
    NSString* uniqueIdentifier;
    
    NSInteger ind;
    ImageCaptureView *view;
    
}

@property (nonatomic, retain) IBOutlet UIButton *menuToggleButton;
@property (nonatomic, retain) IBOutlet UIButton *muteButton;

@property (nonatomic, retain) IBOutlet UIView *retryContainer;
@property (nonatomic, retain) IBOutlet UILabel *retryLabel;
@property (nonatomic, retain) IBOutlet UIButton *retryButton;
@property (nonatomic, retain) IBOutlet UIView *catsContainer;
@property (nonatomic, retain) IBOutlet UIButton *catsSaveButton;

@end

@implementation SettingsVC

@synthesize menuToggleButton;
@synthesize retryContainer, retryLabel, retryButton;
@synthesize catsContainer, catsSaveButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeMuteImage:) name:@"changeMuteImage" object:nil];
    [self changeMuteImage:nil];
    
    uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    defaults = [NSUserDefaults standardUserDefaults];
    isLoggedIn = [defaults boolForKey:[StaticData instance].kLoggedIn];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    
    NSData *userProfileData = [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData];
    loginUserProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userProfileData];
    
    slideMenu = [[SlideMenu alloc] init];
    [slideMenu setHidden:NO];
    [self.view addSubview:slideMenu];
    
    UISwipeGestureRecognizer *swipeLeftMenu = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToLeftWithGestureRecognizer:)];
    swipeLeftMenu.direction = UISwipeGestureRecognizerDirectionLeft;
    [slideMenu addGestureRecognizer:swipeLeftMenu];
    
    [self.view bringSubviewToFront:menuToggleButton];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeImage) name:@"changeImage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraButtonTapped) name:@"TapOnCameraButton" object:nil];
    
    
    defaults = [NSUserDefaults standardUserDefaults];
    
    UIColor *btnColor = [UIColor colorWithRed:.03804f green:0.7686f blue:1.0f alpha:1.0f];
    [self.catsSaveButton setTitleColor:btnColor forState:UIControlStateNormal];
    [self customButton:retryButton borderColor:btnColor];
    [self customButton:catsSaveButton borderColor:btnColor];
    
    recvCategories = [NSMutableArray array];
    catButtons = [NSMutableArray array];
    
    [retryContainer setHidden:YES];
    [catsContainer setHidden:YES];
    
    
    if ([self.userProfile.userIdEmail isEqualToString:loginUserProfile.userIdEmail])
    {
//        [chooseYourPrerenceLabel setHidden:YES];
//        [catsSaveButton setHidden:YES];
        [menuToggleButton setHidden:YES];
        [backButton setHidden:NO];
        [slideMenu setHidden:YES];
        [self getPrefrences];
        
       
        
        
    }
    else if (self.userProfile)
    {
        [chooseYourPrerenceLabel setHidden:YES];
        [catsSaveButton setHidden:YES];
        [menuToggleButton setHidden:YES];
        [backButton setHidden:NO];
        [selCatsIds removeAllObjects];
        [slideMenu setHidden:YES];
        [self getPrefrences];
        
        
    }
    else if (isLoggedIn)
    {
//        self.userProfile = loginUserProfile;
        [self getPrefrences];
        
    }
    else
    {
        selCatsIds = [NSMutableArray arrayWithArray:[defaults objectForKey:[StaticData instance].kSelCatsIds]];
        if (!selCatsIds)
            selCatsIds = [NSMutableArray array];
        [self getCategories];
    }
    
    

    
    
}

- (IBAction)backButtonPressed:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navController = (UINavigationController *) appDelegate.window.rootViewController;
    [navController popViewControllerAnimated:YES];
}

-(void)getPrefrences
{
    
    NSString *apiUrl = @"http://www.rightips.com/api/index.php?action=getPreferences";
    //    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"getCategories"]];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    
    if (self.userProfile)
    {
        [postParams setValue:self.userProfile.userIdEmail forKey:@"uid"];
    }
    else
    {
        [postParams setValue:loginUserProfile.userIdEmail forKey:@"uid"];
    }
    
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:apiUrl parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
        [HUD hide:YES];
        if([[JSON objectForKey:@"status"] integerValue] == 1)
        {
            
            [self retrievePrefrenceIds:JSON];
            
            
        }
        else if([[JSON objectForKey:@"message"] isEqualToString:@"No record found"])
        {
            
            
        }
        else if([JSON objectForKey:@"message"])
        {
            
            [[[UIAlertView alloc] initWithTitle:nil message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
        }
        [self getCategories];
        
        
        
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
    [HUD hide:YES];
                                             
    [Methods showAlertView:@"" message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }];
    [operation start];
    [HUD setLabelText:NSLocalizedString(@"Please Wait.", nil)];
    [HUD setDetailsLabelText:NSLocalizedString(@"Fetching data ...", nil)];
    [HUD show:YES];
    
}
-(void)retrievePrefrenceIds:(id)json
{
    NSString *selCatIdStr;
    [selCatsIds removeAllObjects];
    NSArray *data = [json objectForKey:@"data"];
    NSDictionary *dic = [data objectAtIndex:0];
    selCatIdStr = [dic objectForKey:@"categories"];
    
    NSArray *arrComponents = [selCatIdStr componentsSeparatedByString:@","];
    selCatsIds = [NSMutableArray arrayWithArray:arrComponents];
    if (!selCatsIds)
        selCatsIds = [NSMutableArray array];
    
    
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //Sending current screen to Piwic Tracker
    //[[PiwikTracker sharedInstance] sendView:@"Settings"];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - SwipeGesture Methods
-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer
{
    [slideMenu showHideMenu];
}

#pragma mark - Change Mute Image
- (void)changeMuteImage:(NSNotification *)notification
{
    if ([[NSUserDefaults standardUserDefaults] doubleForKey:@"muteEndTime"] && ([[NSUserDefaults standardUserDefaults] doubleForKey:@"muteEndTime"] != 0))
    {
        [self.muteButton setImage:[UIImage imageNamed:@"BtnMute"] forState:UIControlStateNormal];
    }
    else
    {
        [self.muteButton setImage:[UIImage imageNamed:@"BtnUnmuted"] forState:UIControlStateNormal];
    }
}
#pragma mark - IBActions
- (IBAction)slideMenuButtonPressed:(id)sender
{
    [self.view bringSubviewToFront:slideMenu];
    [self.view bringSubviewToFront:menuToggleButton];
    [slideMenu showHideMenu];
}

- (IBAction)muteButtonPressed:(id)sender
{
    [Methods showMuteNotxActionSheet];
}

- (void)customButton:(UIButton *)button borderColor:(UIColor *)borderColor
{
    UIColor *colorNormal = [UIColor blackColor];
    UIColor *colorPressed = [[UIColor greenColor] colorWithAlphaComponent:0.4f];
    CGSize buttonSize = button.bounds.size;
    [button setBackgroundImage:[Methods imageWithColor:colorNormal size:buttonSize] forState:UIControlStateNormal];
    [button setBackgroundImage:[Methods imageWithColor:colorPressed size:buttonSize] forState:UIControlStateHighlighted];
    
    [button setClipsToBounds:YES];
    [[button layer] setCornerRadius:button.bounds.size.width/2];
    [[button layer] setBorderWidth:2.0f];
    [[button layer] setBorderColor:borderColor.CGColor];
}

- (IBAction)retryButtonPressed:(id)sender
{
    [self getCategories];
}

- (IBAction)saveButtonPressed:(id)sender
{
    [selCatsIds removeAllObjects];
    
    for (unsigned int index = 0; index < [recvCategories count]; index++)
    {
        UIButton *button = [catButtons objectAtIndex:index];
        if ([button isSelected]) {
            CategoryData *catData = [recvCategories objectAtIndex:index];
            [selCatsIds addObject:catData.catId];
        }
    }
    
//    [defaults setObject:selCatsIds forKey:[StaticData instance].kSelCatsIds];
//    [defaults synchronize];
    
    if(isLoggedIn)
    {
    
    [self savePrefrences];
    }
    else
    {
        [defaults setObject:selCatsIds forKey:[StaticData instance].kSelCatsIds];
        [defaults synchronize];
        
        [Methods fetchAllNotifications];
        
        [Methods pushVCinNCwithName:@"HomeVC" popTop:YES];
    }

    
}
-(void)savePrefrences
{
    
    NSString *catIdsStr = @"";
    if (selCatsIds)
        catIdsStr = [selCatsIds componentsJoinedByString:@","];
    
    NSString *apiUrl = @"http://www.rightips.com/api/index.php?action=savePreferences";
//    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"getCategories"]];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setValue:loginUserProfile.userIdEmail forKey:@"uid"];
    [postParams setValue:uniqueIdentifier forKey:@"deviceID"];
    [postParams setValue:@"123456789" forKey:@"number"];
    [postParams setValue:catIdsStr forKey:@"categories"];
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:apiUrl parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
{
    [HUD hide:YES];
    
    [defaults setObject:selCatsIds forKey:[StaticData instance].kSelCatsIds];
    [defaults synchronize];
    
    
    [Methods fetchAllNotifications];
    
    
    if (self.userProfile)
    {
        [self backButtonPressed:nil];
    }
    else
    {
        [Methods pushVCinNCwithName:@"HomeVC" popTop:YES];
    }
    
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
    [HUD hide:YES];
    
    [Methods showAlertView:@"" message:[error localizedDescription] buttonText:@"OK" tag:0 delegate:nil];
}];
    [operation start];
    [HUD setLabelText:NSLocalizedString(@"Save Prefrences", nil)];
////    [HUD setDetailsLabelText:NSLocalizedString(@"Fetching data ...", nil)];
    [HUD show:YES];
}

- (void)getCategories
{
    NSString *apiUrl = [NSString stringWithFormat:@"%@", @"http://api.beaconwatcher.com/index.php?action="];
    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"getCategories"]];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setValue:[StaticData instance].apiKey forKey:@"key"];
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:apiUrl parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
        [HUD hide:YES];
        [retryContainer setHidden:YES];
        [catsContainer setHidden:NO];
        [self handleCategoriesSuccess:JSON];
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        [HUD hide:YES];
        [retryContainer setHidden:NO];
        [catsContainer setHidden:YES];
        //[Methods showAlertView:@"" message:[error localizedDescription] buttonText:@"OK" tag:0 delegate:nil];
    }];
    [operation start];
//    [HUD setLabelText:NSLocalizedString(@"Please Wait.", nil)];
//    [HUD setDetailsLabelText:NSLocalizedString(@"Fetching data ...", nil)];
//    [HUD show:YES];
}

- (void)handleCategoriesSuccess:(id)JSON
{
    NSString *status = [JSON objectForKey:@"status"];
    NSString *message = [JSON objectForKey:@"message"];
    if ([status intValue] == 1)
    {
        NSArray *dataArray = [JSON objectForKey:@"data"];
        for (unsigned int index = 0; index < [dataArray count]; index++)
        {
            CategoryData *catData = [[CategoryData alloc] init];
            catData.catId = [[dataArray objectAtIndex:index] objectForKey:@"cat_id"];
            catData.catName = [[dataArray objectAtIndex:index] objectForKey:@"cat_name"];
            catData.catImgUrl = [[dataArray objectAtIndex:index] objectForKey:@"cat_icon"];
            [recvCategories addObject:catData];
        }
    } else {
        [Methods showAlertView:@"" message:message buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }
    //Radial Menu
    self.radialMenu = [[ALRadialMenu alloc] init];
    self.radialMenu.delegate = self;

    CGRect frame = CGRectMake(self.catsSaveButton.frame.origin.x, self.catsSaveButton.frame.origin.y + 70, self.catsSaveButton.frame.size.width, self.catsSaveButton.frame.size.height);

    [self.radialMenu buttonsWillAnimateFromButton:self.catsSaveButton withFrame:frame inView:self.view];
    
    [self.view bringSubviewToFront:slideMenu];
    [self.view bringSubviewToFront:menuToggleButton];
}

- (BOOL)isCatSelected:(NSString *)catId
{
    for (unsigned int index = 0; index < [selCatsIds count]; index++)
    {
        NSString *selCatId = [selCatsIds objectAtIndex:index];
        if ([selCatId isEqualToString:catId])
            return YES;
    }
    return NO;
}

- (ALRadialButton *)getCustomCatView:(NSInteger) index
{
    
    CategoryData *catData = [recvCategories objectAtIndex:index];
    
    ALRadialButton *contView = [[ALRadialButton alloc] init];
    [contView setFrame:CGRectMake((index%3) * 90, (index/3) * 100, 90, 90)];
    [contView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    [imageView setFrame:CGRectMake(10, 0, 70, 70)];
    [imageView setClipsToBounds:YES];
    [[imageView layer] setCornerRadius:imageView.bounds.size.width/2];
    [self setCategoryImageWithCatData:catData AndImageView:imageView];
    [contView addTarget:self action:@selector(catButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([self.userProfile.userIdEmail isEqualToString:loginUserProfile.userIdEmail])
    {
        [contView setUserInteractionEnabled:YES];
    }
    else if (self.userProfile)
    {
        [contView setUserInteractionEnabled:NO];
        
    }

    UILabel *label = [[UILabel alloc] init];
    [label setFrame:CGRectMake(5, 72, 80, 25)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setLineBreakMode:NSLineBreakByWordWrapping];
    [label setNumberOfLines:0];
    [label setFont:[UIFont fontWithName:@"ArialMT" size:11]];
    [label setTextColor:[UIColor whiteColor]];
    [label setText:catData.catName];
    
    float oldWidth = label.frame.size.width;
    [label sizeToFit];
    CGRect labelFrame = label.frame;
    labelFrame.size.width = oldWidth;
    label.frame = labelFrame;
    
    [contView addSubview:imageView];
    [contView addSubview:label];
    if ([self isCatSelected:catData.catId])
    {
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(50, 40, 30, 30)];
        imgView.image = [UIImage imageNamed:kTickImage];
        imgView.tag = 7;
        [contView addSubview:imgView];
        [contView setSelected:YES];
    }
    [catButtons addObject:contView];
    
    return contView;
}

- (void)catButtonPressed:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if ([button isSelected])
    {
        [[button layer] setBorderColor:[UIColor whiteColor].CGColor];
        UIColor *buttonColor = [[UIColor greenColor] colorWithAlphaComponent:0.0f];
        [button setImage:[Methods imageWithColor:buttonColor size:button.frame.size] forState:UIControlStateNormal];
        [button setSelected:NO];
        for (UIView *view in button.subviews)
        {
            if (view.tag == 7)
            {
                [view removeFromSuperview];
            }
        }
        
    } else
    {
        [[button layer] setBorderColor:[UIColor greenColor].CGColor];
        
        [button setSelected:YES];
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(50, 40, 30, 30)];
        imgView.layer.cornerRadius = imgView.frame.size.height/2;
        imgView.image = [UIImage imageNamed:kTickImage];
        imgView.tag = 7;
        [button addSubview:imgView];
    }
}

- (void) setCategoryImageWithCatData:(CategoryData*)catData AndImageView:(UIImageView*)imgView
{
    switch ([catData.catId integerValue])
    {
        case 1:
            [imgView setImage:[UIImage imageNamed:kCatFoodImg]];
            break;
        case 2:
            [imgView setImage:[UIImage imageNamed:kCatShopsImg]];
            break;
        case 3:
            [imgView setImage:[UIImage imageNamed:kCatServicesImg]];
            break;
        case 4:
            [imgView setImage:[UIImage imageNamed:kCatEntertainmentImg]];
            break;
        case 5:
            [imgView setImage:[UIImage imageNamed:kCatTourismImg]];
            break;
        case 6:
            [imgView setImage:[UIImage imageNamed:kCatEventsImg]];
            break;
        case 7:
            [imgView setImage:[UIImage imageNamed:kCatEducationImg]];
            break;
            
        default:
            break;
    }
}

#pragma mark - radial menu delegate methods
- (NSInteger) numberOfItemsInRadialMenu:(ALRadialMenu *)radialMenu
{
    return recvCategories.count;
}

- (NSInteger) arcSizeForRadialMenu:(ALRadialMenu *)radialMenu
{
    return 360;
}

- (NSInteger) arcRadiusForRadialMenu:(ALRadialMenu *)radialMenu
{
    return 120;
}

-(NSInteger) arcStartForRadialMenu:(ALRadialMenu *)radialMenu
{
    return 67;
}
- (ALRadialButton *) radialMenu:(ALRadialMenu *)radialMenu buttonForIndex:(NSInteger)index
{
     ALRadialButton *button = [self getCustomCatView:index-1];    
    return button;
}

- (void) radialMenu:(ALRadialMenu *)radialMenu didSelectItemAtIndex:(NSInteger)index
{
    
}

-(void)cameraButtonTapped
{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Edit Photo", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Change Profile Photo", nil),NSLocalizedString(@"Change Cover Photo", nil),nil];
    //    [popup setTag:3];
    [popup showInView:self.view];
    
    
    
}
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ind = buttonIndex;
    
    if (ind != 2)
    {
        
        view = [[ImageCaptureView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        [view setInd:ind];
        
        
        
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             [view setFrame:self.view.frame];
                         }
                         completion:^(BOOL finished){
                         }];
        [self.view addSubview:view];
        
    }
    
    
}
-(void)changeImage
{
    [slideMenu setupMenu];
}







@end
