//
//  LandingVC.m
//  Rightips
//
//  Created by Abdul Mannan on 11/27/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import "LandingVC.h"


@interface LandingVC () <CBCentralManagerDelegate, BWBeaconManagerDelegate,GIDSignInUIDelegate,GIDSignInDelegate>
{
    MBProgressHUD *HUD;
    
    NSUserDefaults *defaults;
    CBCentralManager *bluetoothManager;
    UserProfile *userProfile;
    UIView *toastEnableBluetooth;
    UIButton *closeButton;
    
}

@property (nonatomic, retain) IBOutlet UIButton *fbButton; //Facebook
@property (nonatomic, retain) IBOutlet UIButton *gpButton; //Google Plus
@property (nonatomic, retain) IBOutlet UIButton *emButton; //Email
@property (nonatomic, retain) IBOutlet UIButton *slButton; //Signup Later

@end

@implementation LandingVC

@synthesize fbButton, gpButton, emButton, slButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(detectBluetoothState)
                                                 name:@"BluetoothNotification"
                                               object:nil];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    
    
    defaults = [NSUserDefaults standardUserDefaults];
    userProfile = [[UserProfile alloc] init];
    
    UIColor *fbColor = [UIColor colorWithRed:0.2353f green:0.3412f blue:0.6157f alpha:1.0f];
    [self customButton:fbButton borderColor:fbColor];
    UIColor *gpColor = [UIColor colorWithRed:0.8510f green:0.3020f blue:0.1961f alpha:1.0f];
    [self customButton:gpButton borderColor:gpColor];
    UIColor *emColor = [UIColor colorWithRed:0.6510f green:0.7804f blue:0.0f alpha:1.0f];
    [self customButton:emButton borderColor:emColor];
    
    [fbButton setEnabled:NO];
    [gpButton setEnabled:NO];
    [emButton setEnabled:NO];
    [slButton setEnabled:NO];
    
    [self detectBluetoothState];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //Sending current screen to Piwic Tracker
    //[[PiwikTracker sharedInstance] sendView:@"Side Menu"];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)signupLaterButtonPressed:(id)sender
{
    
    [self pushNextVC];
}

- (void)pushNextVC
{
    id object = [defaults objectForKey:[StaticData instance].kSelCatsIds];
    if (!object) [Methods pushVCinNCwithName:@"SettingsVC" popTop:YES];
    else [Methods pushVCinNCwithName:@"HomeVC" popTop:YES];
}

- (void)customButton:(UIButton *)button borderColor:(UIColor *)borderColor
{
    UIColor *colorNormal = [UIColor clearColor];
    UIColor *colorPressed = [[UIColor redColor] colorWithAlphaComponent:0.4f];
    CGSize buttonSize = button.bounds.size;
    [button setImage:[Methods imageWithColor:colorNormal size:buttonSize] forState:UIControlStateNormal];
    [button setImage:[Methods imageWithColor:colorPressed size:buttonSize] forState:UIControlStateHighlighted];
    
    [button setClipsToBounds:YES];
    [[button layer] setCornerRadius:button.bounds.size.width/2];
    [[button layer] setBorderWidth:2.0f];
    [[button layer] setBorderColor:borderColor.CGColor];
}

- (void)detectBluetoothState
{
 //   if(!bluetoothManager)
    {
        
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], CBCentralManagerOptionShowPowerAlertKey, nil];

        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"inBackground"] == YES)
        {
            
            bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:options];
        }
        else
        {
            
            bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
            
        }
        //[self centralManagerDidUpdateState:bluetoothManager];
    }
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    NSString *stateString = nil;
    
        
    switch(bluetoothManager.state)
    {
        case CBCentralManagerStateResetting:
        {
            stateString = NSLocalizedString(@"Connection reset. Please restart the app.", nil);
            [self.view makeToast:stateString duration:300.0f position:@"bottom" title:@"Bluetooth State" Tag:1];
            break;
        }
        case CBCentralManagerStateUnsupported:
        {
            stateString = NSLocalizedString(@"This device doesn't support Bluetooth Low Energy Technology.", nil);
            [self.view makeToast:stateString duration:300.0f position:@"bottom" title:@"Bluetooth State" Tag:1];
            break;
        }
        case CBCentralManagerStateUnauthorized:
        {
            stateString = NSLocalizedString(@"The app is not authorized to use Bluetooth Low Energy.", nil);
            [self.view makeToast:stateString duration:30.0f position:@"bottom" title:@"Bluetooth State" Tag:1];
            break;
        }
        case CBCentralManagerStatePoweredOff:
        {
            stateString = NSLocalizedString(@"Please enable bluetooth service", nil);
            toastEnableBluetooth = [self.view makeToast:stateString duration:5.0f position:@"center" title:@"Bluetooh Disabled" Tag:2];

            stateString = NSLocalizedString(@"Bluetooth is currently disabled. Please enable it from Settings.", nil);
            [self.view makeToast:stateString duration:5.0f position:@"bottom" title:@"Bluetooth State" Tag:1];
            break;
        }
        case CBCentralManagerStatePoweredOn:
        {
            stateString = NSLocalizedString(@"Bluetooth Low Energy is available.", nil);
            //[self.view makeToast:stateString duration:5.0f position:@"bottom" title:@"Bluetooth State"];
            [self handleBleEnabled];
            break;
        }
        default:
        {
            stateString = NSLocalizedString(@"State unknown,  Please restart the app.", nil);
            [self.view makeToast:stateString duration:300.0f position:@"bottom" title:@"Bluetooth State" Tag:1];
            break;
        }
    }
    
    for (UIView *toast in self.view.subviews)
    {
        if (toast.tag == 2)
        {
            closeButton = [[UIButton alloc] init];
            CGRect toastFrame = toast.frame;
            float closeBtnHeight = 30.0;
            closeButton.frame = CGRectMake(toastFrame.origin.x + toastFrame.size.width - closeBtnHeight, toastFrame.origin.y - closeBtnHeight, closeBtnHeight, closeBtnHeight);
            [closeButton setBackgroundImage:[UIImage imageNamed:@"close4"] forState:UIControlStateNormal];
            closeButton.tag = 3;
//            closeButton.layer.cornerRadius = closeBtnHeight/2;
//            [closeButton setClipsToBounds:YES];
            [closeButton addTarget:self action:@selector(closeNotification:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:closeButton];
        }
    }
}

- (IBAction)closeNotification:(id)sender
{
    if (toastEnableBluetooth)
    {
        for (UIView *toast in self.view.subviews)
        {
            if (toast.tag == 2 || toast.tag == 3)
            {
                [toast removeFromSuperview];
            }
        }
        [self.view hideToast:toastEnableBluetooth];
    }
    [closeButton removeFromSuperview];
    [fbButton setEnabled:YES];
    [gpButton setEnabled:YES];
    [emButton setEnabled:YES];
    [slButton setEnabled:YES];
}
- (void)handleBleEnabled
{
    if (toastEnableBluetooth)
    {
        for (UIView *toast in self.view.subviews)
        {
            if (toast.tag == 2)
            {
                [toast removeFromSuperview];
            }
        }
        [self.view hideToast:toastEnableBluetooth];
    }
    [closeButton removeFromSuperview];
    [fbButton setEnabled:YES];
    [gpButton setEnabled:YES];
    [emButton setEnabled:YES];
    [slButton setEnabled:YES];
    
    NSDictionary *shouldLoginDict = [defaults objectForKey:@"shouldLoginDict"];
    if (shouldLoginDict)
    {
        NSNumber *shouldLogin = (NSNumber *) [shouldLoginDict objectForKey:@"shouldLogin"];
        if (shouldLogin.boolValue == YES) {
            NSNumber *provider = (NSNumber *) [shouldLoginDict objectForKey:@"provider"];
            if (provider.intValue == [StaticData instance].kProviderFacebook.intValue) {
                [self loginWithFacebookButtonPressed:nil];
            } else if (provider.intValue == [StaticData instance].kProviderGooglePlus.intValue) {
//                [self loginWithGooglePlusButtonPressed:nil];
            }
        }
        [defaults setObject:nil forKey:@"shouldLoginDict"];
        [defaults synchronize];
    }
}

- (IBAction)loginWithFacebookButtonPressed:(id)sender
{
    
//    [FBSession openActiveSessionWithReadPermissions:[StaticData instance].fbPermissions allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState state, NSError *error)
//    {
//        AppDelegate* appDelegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
//        [appDelegate sessionStateChanged:session state:state error:error];
//        
//        if(!error)
//        {
//            switch (state)
//            {
//                case FBSessionStateOpen:
//                    [FBSession setActiveSession:session];
//                    [self retrieveFbUserData];
//                    break;
//                case FBSessionStateClosedLoginFailed:
//                    NSLog(@"session:%@",session);
//                    [Methods showAlertView:NSLocalizedString(@"Oops!", nil) message:@"FBSession State Closed Login Failed" buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
//                    break;
//                default:
//                    break;
//            }
//        } else
//        {
//            NSLog(@"fbsession error: %@", [error localizedDescription]);
//            [Methods showAlertView:@"Oops!" message:[error localizedDescription] buttonText:@"OK" tag:0 delegate:nil]; // commented as error popups are being shown in sessionStateChanged method.
//        }
//    }];
    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: [StaticData instance].fbPermissions fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else
         {
             if ([result.grantedPermissions containsObject:@"email"])
             {
                 NSLog(@"result is:%@",result);
                 [self retrieveFbUserData];
             }
            
         }
     }];
}

- (void)retrieveFbUserData
{
//    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
////    [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error)
//    {
//        if (!error)
//        {
//            NSString *smId =  [result objectForKey:@"id"] != nil ? [result objectForKey:@"id"] : @"";
//            NSString *email =  [result objectForKey:@"email"] != nil ? [result objectForKey:@"email"] : @"";
//            NSString *firstName = [result objectForKey:@"first_name"] != nil ? [result objectForKey:@"first_name"] : @"";
//            NSString *lastName = [result objectForKey:@"last_name"] != nil ? [result objectForKey:@"last_name"] : @"";
//
////            userProfile.profileImgUrl = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", smId];
//
//            if ([email isEqualToString:@""])
//            {
//                [HUD hide:YES];
//                [[FBSession activeSession] closeAndClearTokenInformation];
//                [Methods showAlertView:NSLocalizedString(@"Oops!", nil) message:@"Your facebook email address is not accessible" buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
//            }
//            else
//            {
//                
//                [self performLogin:[StaticData instance].kProviderFacebook.stringValue smId:smId email:email firstName:firstName lastName:lastName];
//            }
//            
//            
//            
//        }
//        else
//        {
//            [HUD hide:YES];
//            NSLog(@"fbrequestconnection error: %@", [error localizedDescription]);
//            [Methods showAlertView:NSLocalizedString(@"Oops!", nil) message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
//        }
//    }];
    
    
    if ([FBSDKAccessToken currentAccessToken])
    {
//        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email, birthday, bio ,location ,friends ,hometown , friendlists"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 NSString *smId =  [result objectForKey:@"id"] != nil ? [result objectForKey:@"id"] : @"";
                             NSString *email =  [result objectForKey:@"email"] != nil ? [result objectForKey:@"email"] : @"";
                             NSString *firstName = [result objectForKey:@"first_name"] != nil ? [result objectForKey:@"first_name"] : @"";
                             NSString *lastName = [result objectForKey:@"last_name"] != nil ? [result objectForKey:@"last_name"] : @"";
                 
                 //            userProfile.profileImgUrl = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", smId];
                 
                             if ([email isEqualToString:@""])
                             {
                                 [HUD hide:YES];
//                                 [[FBSDKAccessToken clo];
                                 [Methods showAlertView:NSLocalizedString(@"Oops!", nil) message:@"Your facebook email address is not accessible" buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
                             }
                             else
                             {
                 
                                 [self performLogin:[StaticData instance].kProviderFacebook.stringValue smId:smId email:email firstName:firstName lastName:lastName];
                             }
                             
             
             }
             else
             {
                 [HUD hide:YES];
                 NSLog(@"fbrequestconnection error: %@", [error localizedDescription]);
                 [Methods showAlertView:NSLocalizedString(@"Oops!", nil) message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
             }
         }];
        
    }
    
    
    
    [HUD show:YES];
}

//esol_ios_dev
//P@ssw0rd

- (IBAction)loginWithEmailButtonPressed:(UIButton *)sender
{
    [Methods pushVCinNCwithName:@"LoginVC" popTop:YES];
}

- (IBAction)loginWithGooglePlusButtonPressed:(id)sender
{
    // com.beaconwatcher.rightipstest
    ///static NSString * const kClientId = @"130859959820-c1rdcm429jbshig7jebohubv56av8tnc.apps.googleusercontent.com";
    // com.beaconwatcher.rightips (from mannan.engr account)
    static NSString * const kClientId = @"462207644497-cb7ha38i5ra4lkp0c25crj4eooeda5v6.apps.googleusercontent.com";
    
    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    signIn.allowsSignInWithBrowser = NO;
    signIn.allowsSignInWithWebView = YES;
    signIn.clientID = kClientId;
//    signIn.scopes = @[ kGTLAuthScopePlusLogin ];
    signIn.scopes = @[ @"profile" ];
    signIn.uiDelegate = self;
    signIn.delegate = self;
    
    [signIn signIn];
    
    [HUD setLabelText:NSLocalizedString(@"Please Wait.", nil)];
    [HUD setDetailsLabelText:NSLocalizedString(@"Signing in ...", nil)];
    [HUD show:YES];
}
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error
{
        if (!error)
        {
            [self retrieveGpUserData:user];
        }
        else
        {
            [HUD hide:YES];
            NSLog(@"gp sign in error: %@", [error localizedDescription]);
            [Methods showAlertView:NSLocalizedString(@"Oops!", nil) message:NSLocalizedString(@"Login with google+ failed!", nil) buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
        }
    
}
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error
{
    if (!error)
    {
        [self retrieveGpUserData:signIn.currentUser];
    }
    else
    {
        [HUD hide:YES];
        NSLog(@"gp sign in error: %@", [error localizedDescription]);
        [Methods showAlertView:NSLocalizedString(@"Oops!", nil) message:NSLocalizedString(@"Login with google+ failed!", nil) buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }
}
//- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
//{
//    [self retrieveGpUserData:signIn.currentUser];
//}
//- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController
//{
//     [self retrieveGpUserData:signIn.currentUser];
//}

//- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth error:(NSError *)error
//{
//    if (!error)
//    {
//        [self retrieveGpUserData];
//    }
//    else
//    {
//        [HUD hide:YES];
//        NSLog(@"gp sign in error: %@", [error localizedDescription]);
//        [Methods showAlertView:NSLocalizedString(@"Oops!", nil) message:NSLocalizedString(@"Login with google+ failed!", nil) buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
//    }
//}

- (void)retrieveGpUserData: (GIDGoogleUser *)user
{
    if (user.authentication == nil)
    {
        return;
    }
    
//    GTLPlusPerson *person = [GPPSignIn sharedInstance].googlePlusUser;
    if (user == nil)
    {
        return;
    }
    
    NSString *smId =  user.userID;
    NSString *email =  user.profile.email;
    NSArray *name = [user.profile.name componentsSeparatedByString:@" "];
    NSString *firstName = (name.count > 0) ? name[0] : @"";
    NSString *lastName = (name.count > 1) ? name[1] : @"";
    
//    userProfile.profileImgUrl = person.image.url;
    
    // TODO: change provider to of google+.
    [self performLogin:[StaticData instance].kProviderFacebook.stringValue smId:smId email:email firstName:firstName lastName:lastName];
}


- (void)performLogin:(NSString *)provider smId:(NSString *)smId  email:(NSString *)email firstName:(NSString *)firstName lastName:(NSString *)lastName
{
    NSString *apiUrl = [NSString stringWithFormat:@"%@", [StaticData instance].baseUrl];
    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"login"]];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setValue:firstName forKey:@"fname"];
    [postParams setValue:lastName forKey:@"lname"];
    [postParams setValue:email forKey:@"email"];
    [postParams setValue:@"" forKey:@"pass"];
    [postParams setValue:smId forKey:@"sm_id"];
    [postParams setValue:provider forKey:@"provider"];
    [postParams setValue:@"0" forKey:@"signup"];
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:apiUrl parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
        [HUD hide:YES];
        userProfile.provider = provider;
        userProfile.smId = smId;
        userProfile.email = email;
//        [userProfile setFirstName:firstName];
//        [userProfile setLastName:lastName];
        [self handleLoginSuccess:JSON];
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        [HUD hide:YES];
        [Methods showAlertView:@"" message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }];
    [operation start];
    [HUD setLabelText:NSLocalizedString(@"Please Wait.", nil)];
    [HUD setDetailsLabelText:NSLocalizedString(@"Signing in ...", nil)];
    [HUD show:YES];
}

- (void)handleLoginSuccess:(id)JSON
{
    NSString *status = [JSON objectForKey:@"status"];
    NSString *message = [JSON objectForKey:@"message"];
    if ([status intValue] == 1)
    {
        NSDictionary *dataDict = [[JSON objectForKey:@"data"] objectAtIndex:0];
        
        userProfile.userIdEmail = [dataDict objectForKey:@"uid"];
        //        userProfile.firstName = [dataDict objectForKey:@"first_name"];
        //        userProfile.lastName = [dataDict objectForKey:@"last_name"];
        [userProfile setUserName:[dataDict objectForKey:@"user_name"]];
        [userProfile setProfileImgUrl:[dataDict objectForKey:@"profile_pic"]];
        [userProfile setCoverImgUrl:[dataDict objectForKey:@"cover_photo"]];
        
//        userProfile.userIdEmail = [dataDict objectForKey:@"uid"];
//        userProfile.firstName = [dataDict objectForKey:@"first_name"];
//        userProfile.lastName = [dataDict objectForKey:@"last_name"];

        NSData *userProfileData = [NSKeyedArchiver archivedDataWithRootObject:userProfile];
        [defaults setObject:userProfileData forKey:[StaticData instance].kUserProfileData];
        [defaults setBool:YES forKey:[StaticData instance].kLoggedIn];
        [defaults synchronize];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:kReferringUsersArray])
        {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:kReferringUsersArray] count]>0)
            {
                [ReuseAbleFunctions addFriends];
            }
        }

        // Piwik Event (Action Login)
        [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_USERS action:ACTION_LOGIN name:[ReuseAbleFunctions convertDictionaryToJSONString:dataDict] value:nil];

        [self pushNextVC];
    } else
    {
        [Methods showAlertView:@"" message:message buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }
}


@end
