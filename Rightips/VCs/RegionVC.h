//
//  RegionVC.h
//  Rightips
//
//  Created by Mac on 10/09/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegionVC : UIViewController <UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource>

@property(nonatomic,strong)UserLocation *userLoc;

@end
