//
//  RegionVC.m
//  Rightips
//
//  Created by Mac on 10/09/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "RegionVC.h"
#import "AsyncImageView.h"
#import "SlideShowRegionVC.h"
#import "CommentsCellTableViewCell.h"
#import "ImageCaptureView.h"

@interface RegionVC ()
{
    MBProgressHUD *HUD;
    NSMutableArray *photoUrlList;
    
    IBOutlet AsyncImageView *firstImageView;
    
    IBOutlet AsyncImageView *secondImageView;
    
    IBOutlet AsyncImageView *thirdImageView;
    
    IBOutlet AsyncImageView *fourthImageView;
    
    IBOutlet AsyncImageView *fifthImageView;
    
//    IBOutlet UIButton *photoButton;
    
    IBOutlet UIView *containerView;
    SlideMenu *slideMenu;
    
    IBOutlet UIScrollView *mainScrollView;
    UIView *photoView;
    
    NSMutableArray *userProfileList;
    NSArray *itemsList;
    UITableView *tblView;
    IBOutlet UIButton *menuToggleButton;
//    UserLocation *userLocation;
    
    IBOutlet UILabel *cityLabel;
    IBOutlet UIPageControl *pageControl;
    BOOL pageControlBeingUsed;
    UITextView *txtView;
    BOOL dataOfPhotosLoaded;
    NSString *cityString,*photoString;
    NSInteger ind;
    ImageCaptureView *view;
    NSUserDefaults *defaults;
}

@end

@implementation RegionVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dataOfPhotosLoaded = NO;
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    
    userProfileList = [[NSMutableArray alloc] init];
    [pageControl setCurrentPage:0];
    [pageControl setNumberOfPages:2];
    
    slideMenu = [[SlideMenu alloc] init];
    [self.view addSubview:slideMenu];
    [self.view bringSubviewToFront:menuToggleButton];
    
    UISwipeGestureRecognizer *swipeLeftMenu = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToLeftWithGestureRecognizer:)];
    swipeLeftMenu.direction = UISwipeGestureRecognizerDirectionLeft;
    [slideMenu addGestureRecognizer:swipeLeftMenu];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraButtonTapped) name:@"TapOnCameraButton" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeImage) name:@"changeImage" object:nil];
    
//    userLocation = [StaticData instance].userLocation;
    cityString = [NSString stringWithFormat:@"%@ - ",self.userLoc.selectedCityName];
    cityString = [cityString stringByAppendingString:NSLocalizedString(@"History", nil)];
    [cityLabel setText:cityString];
    
    photoString = [NSString stringWithFormat:@"%@ - ",self.userLoc.selectedCityName];
    photoString = [photoString stringByAppendingString:NSLocalizedString(@"Photos", nil)];
    
    
   
    
    
    [self setUpScrollView];
    [self getPlaceHistory];
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}

//- (IBAction)backButtonPressed:(UIButton *)sender
//{
//    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    UINavigationController *navController = (UINavigationController *) appDelegate.window.rootViewController;
//    [navController popViewControllerAnimated:YES];
//}

-(void)setUpScrollView
{
    for (int i = 0; i < 2; i++)
    {
      
        UIView *mainView = [[UIView alloc] initWithFrame:CGRectMake(i * mainScrollView.frame.size.width, 0, mainScrollView.bounds.size.width, self.view.frame.size.height - mainScrollView.frame.origin.y)];
        [mainView setBackgroundColor:[UIColor clearColor]];
        if (i == 0)
        {
            txtView = [[UITextView alloc] initWithFrame:CGRectMake(10, 10, mainView.frame.size.width - 20, mainView.frame.size.height - 20)];
            [txtView setEditable:NO];
            [txtView setAutocorrectionType:UITextAutocorrectionTypeNo];
            [txtView setBackgroundColor:[UIColor clearColor]];
            [txtView setTextColor:[UIColor whiteColor]];
            [txtView setFont:[UIFont fontWithName:@"Helvetica" size:20.0]];
//            [txtView setText:@"fjnefefjnfjfknfjknegjkrbgrfgjknfjkbnfkj vfkjv fjvfvjkfvnfjkv fvjkf jkf vkjfv fjv fjv fjvgbfvjfnvjkfnvjfkvf fjk vfjkv fjv fjvfjk vjkf vfjkv fjkv fjkv fjkv fjkv fv fjk vfvjfv jkf vjkf vjk fjkfvfvfgjkvbmv fvmf vfmbv fvmf bfmbfbvmfd vfkbv fb fbfjkb fbjkfbjkb fjkb fkjb fkj bfkbj fbk fkjb vkf vfkvfkf k"];
            [mainView addSubview:txtView];
        }
        if (i == 1)
        {
            photoView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, mainView.frame.size.width - 20, mainView.frame.size.height - 20)];
           
            
            
            
            [mainView addSubview:photoView];
        }
        if (i == 2)
        {
            tblView = [[UITableView alloc] initWithFrame:CGRectMake(10, 10, mainView.frame.size.width - 20, mainView.frame.size.height - 20)];
            [tblView setDataSource:self];
            [tblView setDelegate:self];
            [tblView setBackgroundColor:[UIColor clearColor]];
            [tblView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
            [mainView addSubview:tblView];
        }
        
        
        [mainScrollView addSubview:mainView];
    }
    
    [mainScrollView setPagingEnabled:YES];
    [mainScrollView setContentSize:CGSizeMake(mainScrollView.frame.size.width * 2, 1)];
}
- (IBAction)slideMenuButtonPressed:(id)sender
{
    [slideMenu showHideMenu];
}
-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer
{
    [slideMenu showHideMenu];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [itemsList count];
}



- (CommentsCellTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentsCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentCell"];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CommentsCellTableViewCell" owner:nil options:nil] objectAtIndex: 0];
    }
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    
    UserProfile *user = [userProfileList objectAtIndex:indexPath.row];
    cell.descriptionLabel.text = user.userReview;
    cell.profileImageView.imageURL = [NSURL URLWithString:user.profileImgUrl];
    cell.userNameLabel.text = [NSString stringWithFormat:@"%@ %@",user.firstName,user.lastName];
    
   
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 88.0;
}

-(void)fetchregions
{
    photoUrlList = [[NSMutableArray alloc] init];
    [HUD setLabelText:NSLocalizedString(@"Fetching Photos", nil)];
    [HUD show:YES];
//    NSString *apiUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%f,%f&radius=500&key=AIzaSyAuCgahZyspOpErsbYmlXGx0JCq3LQIz1M",self.userLoc.selectedLocation.coordinate.latitude,self.userLoc.selectedLocation.coordinate.longitude];

     NSString *apiUrl = [NSString stringWithFormat:@"http://www.panoramio.com/map/get_panoramas.php?set=public&from=0&to=30&minx=%g&miny=%f&maxx=%f&maxy=%f&size=medium&mapfilter=true",self.userLoc.selectedLocation.coordinate.longitude - 0.02,self.userLoc.selectedLocation.coordinate.latitude - 0.02,self.userLoc.selectedLocation.coordinate.longitude + 0.02,self.userLoc.selectedLocation.coordinate.latitude + 0.02];
    
    
    
    
//    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"getLocNotification"]];
    
//    CLLocation *location = [StaticData instance].userLocation.selectedLocation;
//    NSString *latStr = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
//    NSString *lngStr = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
    
//    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
//    [postParams setValue:[StaticData instance].apiKey forKey:@"key"];
//    [postParams setValue:@"1" forKey:@"isadmin"];
//    [postParams setValue:latStr forKey:@"lat"];
//    [postParams setValue:lngStr forKey:@"lng"];
//    ///[postParams setValue:@"" forKey:@"km"];
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiUrl parameters:nil];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
        [HUD hide:YES];
        [self fetchPhotoUrlFromJson:JSON];
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        [HUD hide:YES];
    }];
    [operation start];
}
-(void)fetchPhotoUrlFromJson:(id)json
{
    NSArray *resultsArray = [json objectForKey:@"photos"];
//    NSArray *itemsList = [[responceArray objectAtIndex:0] objectForKey:@"items"];
    
    for (NSDictionary *itemDic in resultsArray)
    {
        NSString *photourl = [itemDic objectForKey:@"photo_file_url"];
        if (photourl)
        {
//            NSString *photoReference = [[photosList objectAtIndex:0] objectForKey:@"photo_reference"];
            [photoUrlList addObject:photourl];
        }
        
    }
    [self loadImages];
}
-(void)loadImages
{
    
   
    
    int y = -1;
    AsyncImageView *imgView;
    
    for (int i = 0; i < [photoUrlList count]; i++)
    {
        if (i == 5)
        {
            
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake((photoView.frame.size.width / 2) + 10, y * (photoView.frame.size.height / 3) + 20 , (photoView.frame.size.width / 2) - 10, (photoView.frame.size.height / 3) - 20)];
            [view setBackgroundColor:[UIColor blackColor]];
//            [view setAlpha:0.3];
            
            imgView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
            [imgView setActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            [imgView setShowActivityIndicator:YES];
            [imgView setBackgroundColor:[UIColor blackColor]];
            [imgView setImageURL:[NSURL URLWithString:[photoUrlList objectAtIndex:i]]];
//            [imgView setTag:i];
//            [imgView setUserInteractionEnabled:YES];
//            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoButtonPressed:)];
//            [imgView addGestureRecognizer:tap];
            [view addSubview:imgView];
            
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//            [btn setFrame:CGRectMake(50, 50, 100, 100)];
            [btn setFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
            [btn setCenter:CGPointMake(view.frame.size.width / 2, view.frame.size.height / 2)];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btn setTitle:[NSString stringWithFormat:@"%lu Photos",[photoUrlList count] - 6] forState:UIControlStateNormal];
            [btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:20.0]];
            [btn setBackgroundColor:[UIColor blackColor]];
            [btn setAlpha:0.5];
            [btn addTarget:self action:@selector(photoButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:btn];
            
            [photoView addSubview:view];
            
            break;
        }
        
        
        if (i % 2)
        {
            
           imgView  = [[AsyncImageView alloc] initWithFrame:CGRectMake((photoView.frame.size.width / 2) + 10, y * (photoView.frame.size.height / 3) + 20 , (photoView.frame.size.width / 2) - 10, (photoView.frame.size.height / 3) - 20)];
            
        }
        else
        {
            y++;
            imgView  = [[AsyncImageView alloc] initWithFrame:CGRectMake(0 , y * (photoView.frame.size.height / 3) + 20, (photoView.frame.size.width / 2) - 10, (photoView.frame.size.height / 3) - 20 )];
        }
        
        [imgView setActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [imgView setShowActivityIndicator:YES];
        [imgView setBackgroundColor:[UIColor blackColor]];
        [imgView setImageURL:[NSURL URLWithString:[photoUrlList objectAtIndex:i]]];
        [imgView setTag:i];
        [imgView setUserInteractionEnabled:YES];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoButtonPressed:)];
        [imgView addGestureRecognizer:tap];
        [photoView addSubview:imgView];
        
    }
    
    
}
-(void)getPlaceHistory
{
    
    [HUD setLabelText:NSLocalizedString(@"Fetching History", nil)];
    [HUD show:YES];
    
   
    NSString *devLang = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSArray *stringList = [devLang componentsSeparatedByString:@"-"];
    devLang = [stringList objectAtIndex:0];
    
    NSString *apiUrl = [NSString stringWithFormat:@"https://%@.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exlimit=max&explaintext&exintro&titles=%@",devLang,self.userLoc.selectedCityName];
    if ([devLang isEqualToString:@"he"] || [devLang isEqualToString:@"ru"])
    {
        apiUrl = [NSString stringWithFormat:@"https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exlimit=max&explaintext&exintro&titles=%@",self.userLoc.selectedCityName];
    }

    apiUrl = [[apiUrl componentsSeparatedByString:@"-"] objectAtIndex:0];
    
    NSArray *componentsSeparatedByWhiteSpace = [apiUrl componentsSeparatedByString:@" "];
    if([componentsSeparatedByWhiteSpace count] > 1)
    {
        apiUrl = [apiUrl stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    }
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiUrl parameters:nil];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
//        [self fetchReviews];
        [HUD hide:YES];
        [self fetchDataFromJson:JSON];
        
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        [HUD hide:YES];
    }];
    [operation start];
    
}
-(void)fetchReviews
{
    
    NSString *apiUrl = [NSString stringWithFormat:@"https://api.foursquare.com/v2/venues/explore?near=%@&section=topPicks&venuePhotos=1&client_id=LHG3ZKUELWHKD44PLQRC0XWLCVPLDQW3FJUTFRR41G1AAOSR&client_secret=FJ1W0E4FW13YNTZKMBRNEIV2YC1GQ2WAVMG5UQCKKTQC5KJW&v=20120609",self.userLoc.selectedCityName];
    NSArray *stringList = [apiUrl componentsSeparatedByString:@"-"];
    apiUrl = [stringList objectAtIndex:0];
    
    NSArray *componentsSeparatedByWhiteSpace = [apiUrl componentsSeparatedByString:@" "];
    if([componentsSeparatedByWhiteSpace count] > 0)
    {
        apiUrl = [apiUrl stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    }
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiUrl parameters:nil];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
    [HUD hide:YES];
    [self fetchReviewsFromJson:JSON];
                                             
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
    [HUD hide:YES];
    }];
    [operation start];
    
}
-(void)fetchReviewsFromJson:(id)json
{
    NSDictionary *responceDic = [json objectForKey:@"response"];
    NSArray *groupList = [responceDic objectForKey:@"groups"];
    itemsList = [[groupList objectAtIndex:0] objectForKey:@"items"];
    for (NSDictionary *itemdic in itemsList)
    {
        NSArray *tipsList = [itemdic objectForKey:@"tips"];
        UserProfile *userProfileData = [[UserProfile alloc] init];
        NSDictionary *tipsDic = [tipsList objectAtIndex:0];
        userProfileData.userReview = [tipsDic objectForKey:@"text"];
        NSDictionary *imgDic = [[tipsDic objectForKey:@"user"] objectForKey:@"photo"];
        userProfileData.profileImgUrl = [NSString stringWithFormat:@"%@100x100%@",[imgDic objectForKey:@"prefix"],[imgDic objectForKey:@"suffix"]];
        userProfileData.userLikes = [[tipsDic objectForKey:@"likes"] objectForKey:@"count"];
        

        
        userProfileData.firstName = [[tipsDic objectForKey:@"user"] objectForKey:@"firstName"];
        userProfileData.lastName = [[tipsDic objectForKey:@"user"] objectForKey:@"lastName"];
        
        [userProfileList addObject:userProfileData];
    }
    
    [tblView reloadData];
}
-(void)fetchDataFromJson:(id)json
{
    NSDictionary *dic = [json objectForKey:@"query"];
    NSDictionary *pagesDic = [dic objectForKey:@"pages"];
    for (id key in pagesDic)
    {
         NSDictionary *dics = [pagesDic objectForKey:key];
         NSString *historyText = [dics objectForKey:@"extract"];
        
        
        
        if([historyText isEqualToString:@""])
        {
           [txtView setText:historyText];
        }
        else if (historyText == nil )
        {
            [txtView setText:@""];
        }
        else
        {
            NSMutableString *s = [NSMutableString stringWithString:historyText];
            NSRange start = [historyText rangeOfString:@"("];
            NSRange end = [historyText rangeOfString:@")"];
            if (start.length != 0 && end.length != 0)
            {
                 [s deleteCharactersInRange:(NSRange){ start.location, end.location - start.location + 2}];
            }
           
//            [s stringByReplacingOccurrencesOfString:@"\n" withString:@"\n\n\n"];
            NSString *newString = [[s componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@"\n\n"];
            [txtView setText:newString];
        }
        
    }
    
}
- (void)photoButtonPressed:(UITapGestureRecognizer *)recognizer
{
    
    SlideShowRegionVC  *vc = (SlideShowRegionVC *)[Methods getVCbyName:@"SlideShowRegionVC"];
    [vc setRegionImagesList:photoUrlList];
    if ([recognizer isKindOfClass:[UIButton class]])
    {
        [vc setTag:0];
    }
    else
    {
        [vc setTag:recognizer.view.tag];
    }
    
    [Methods pushVCinNCwithObj:vc popTop:NO];
}

- (IBAction)changePage
{
    CGRect frame;
    frame.origin.x = mainScrollView.frame.size.width * pageControl.currentPage;
    frame.origin.y = 0;
    frame.size = mainScrollView.frame.size;
    [mainScrollView scrollRectToVisible:frame animated:YES];
    pageControlBeingUsed = YES;
}
-(void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (!pageControlBeingUsed)
    {
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat pageWidth = mainScrollView.frame.size.width;
        int page = floor((mainScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        pageControl.currentPage = page;
    }
    if (pageControl.currentPage == 0)
    {
//        [cityLabel setText:[NSString stringWithFormat:@"%@ - History",userLocation.selectedCityName]];
        [cityLabel setText:cityString];
    }
    else if (pageControl.currentPage == 1)
    {
        [cityLabel setText:photoString];
        if (!dataOfPhotosLoaded)
        {
            dataOfPhotosLoaded = YES;
            [self fetchregions];
        }
    }
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    pageControlBeingUsed = NO;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





-(void)cameraButtonTapped
{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Edit Photo", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Change Profile Photo", nil),NSLocalizedString(@"Change Cover Photo", nil),nil];
    //    [popup setTag:3];
    [popup showInView:self.view];
    
    
    
}
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ind = buttonIndex;
    
    if (ind != 2)
    {
        
        view = [[ImageCaptureView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        [view setInd:ind];
        
        
        
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             [view setFrame:self.view.frame];
                         }
                         completion:^(BOOL finished){
                         }];
        [self.view addSubview:view];
        
    }
    
    
}
-(void)changeImage
{
    [slideMenu setupMenu];
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
