//
//  ContactListVC.h
//  Rightips
//
//  Created by Esol on 26/06/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <MessageUI/MessageUI.h>

@interface ContactListVC : UIViewController <UITableViewDelegate, UITableViewDataSource, ABPeoplePickerNavigationControllerDelegate, MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate, UISearchBarDelegate>

@property (nonatomic, retain) IBOutlet NSString *type;
@property (nonatomic, retain) IBOutlet UITableView *contactTableView;
@property (nonatomic, retain) IBOutlet UIView *redLineView;
@property (nonatomic, retain) IBOutlet UISearchBar *contactListSearchBar;

@property (nonatomic, retain) IBOutlet UIButton *doneButton;
@property (nonatomic, retain) IBOutlet UIButton *cancelButton;
@property (nonatomic, retain) IBOutlet UIButton *smsButton;
@property (nonatomic, retain) IBOutlet UIButton *whatsappButton;
@property (nonatomic, retain) IBOutlet UIButton *mail;

- (IBAction)doneButton:(id)sender;
- (IBAction)cancelButton:(id)sender;
- (IBAction)smsButton:(id)sender;
- (IBAction)whatsappButton:(id)sender;
- (IBAction)mailButton:(id)sender;
@end
