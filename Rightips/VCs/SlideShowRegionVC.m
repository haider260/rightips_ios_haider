//
//  SlideShowRegionVC.m
//  Rightips
//
//  Created by Mac on 10/09/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "SlideShowRegionVC.h"
#import "AsyncImageView.h"

@interface SlideShowRegionVC ()
{
    
    IBOutlet UIScrollView *scrlView;
    
    IBOutlet UIPageControl *pageControl;
    BOOL pageControlBeingUsed;
}

@end

@implementation SlideShowRegionVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [scrlView setFrame:CGRectMake(scrlView.frame.origin.x, scrlView.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    
    [self setScrollView];
    
    CGPoint currentOffset=scrlView.contentOffset;
    [scrlView setContentOffset:CGPointMake( scrlView.frame.size.width*self.tag, currentOffset.y)];
    [pageControl setCurrentPage:self.tag];
    [pageControl setNumberOfPages:[self.regionImagesList count]];
    
    
}
- (IBAction)backButtonPressed:(UIButton *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navController = (UINavigationController *) appDelegate.window.rootViewController;
    [navController popViewControllerAnimated:YES];
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)setScrollView
{
    
    for (int i = 0; i < [self.regionImagesList count]; i++)
    {
        AsyncImageView *imgView = [[AsyncImageView alloc] initWithFrame:CGRectMake(scrlView.frame.size.width * i, 0, scrlView.frame.size.width, scrlView.frame.size.height)];
//        [imgView setCenter:CGPointMake(self.view.frame.size.width , self.view.frame.size.height / 2)];
        [imgView setActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [imgView setShowActivityIndicator:YES];
        [imgView setContentMode:UIViewContentModeScaleAspectFit];
        [imgView setImageURL:[NSURL URLWithString:[self.regionImagesList objectAtIndex:i]]];
        [imgView setBackgroundColor:[UIColor blackColor]];
        [scrlView addSubview:imgView];
        
        
    }
    
    
    [scrlView setPagingEnabled:YES];
    [scrlView setContentSize:CGSizeMake(scrlView.frame.size.width * [self.regionImagesList count], 1)];
}

- (IBAction)changePage
{
    CGRect frame;
    frame.origin.x = scrlView.frame.size.width * pageControl.currentPage;
    frame.origin.y = 0;
    frame.size = scrlView.frame.size;
    [scrlView scrollRectToVisible:frame animated:YES];
    pageControlBeingUsed = YES;
}

-(void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (!pageControlBeingUsed)
    {
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat pageWidth = scrlView.frame.size.width;
        int page = floor((scrlView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        pageControl.currentPage = page;
    }
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    pageControlBeingUsed = NO;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
