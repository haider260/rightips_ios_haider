//
//  ReviewVC.h
//  Rightips
//
//  Created by Mac on 05/01/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate>
{
    
    IBOutlet UITableView *userReviewTableView;
    
    IBOutlet UILabel *reviewLabel;
}

@property(nonatomic,strong)UserProfile *userProfile;

@end
