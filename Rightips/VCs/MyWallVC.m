//
//  MyWallVC.m
//  Rightips
//
//  Created by Mac on 09/12/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "MyWallVC.h"
#import "HistoryCell.h"
#import "NotxCommentsVC.h"
#import "ImageCaptureView.h"

@interface MyWallVC ()
{
    SlideMenu *slideMenu;
    DbHelper *dbHelper;
    MBProgressHUD *HUD;
    NSMutableArray *allCommentsList;
    NSInteger ind;
    ImageCaptureView *view;
    NSUserDefaults *defaults;
}

@end

@implementation MyWallVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    defaults = [NSUserDefaults standardUserDefaults];
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    
    allCommentsList = [[NSMutableArray alloc] init];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,myWallTableView.frame.origin.y-7, myWallTableView.frame.size.width, 7)];
    headerView.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:0 blue:57.0/255.0 alpha:1.0f];;
    [myWallView addSubview:headerView];
    slideMenu = [[SlideMenu alloc] init];
    [self.view addSubview:slideMenu];
    [self.view bringSubviewToFront:menuToggleButton];
    
    UISwipeGestureRecognizer *swipeLeftMenu = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToLeftWithGestureRecognizer:)];
    swipeLeftMenu.direction = UISwipeGestureRecognizerDirectionLeft;
    [slideMenu addGestureRecognizer:swipeLeftMenu];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraButtonTapped) name:@"TapOnCameraButton" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeImage) name:@"changeImage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRangeBeacons:) name:@"didRangeBeacons" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeMuteImage:) name:@"changeMuteImage" object:nil];
    [self changeMuteImage:nil];
    
   
    
}
-(void)viewWillAppear:(BOOL)animated
{
     [self getComments];
}
-(void)getComments
{
    
    NSData *userProfileData = [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData];
    UserProfile *userProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userProfileData];
//    [HUD setLabelText:NSLocalizedString(@"Fetching Comments", nil)];
    [HUD show:YES];
    
    //    NSString *apiUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%f,%f&radius=500&key=AIzaSyAuCgahZyspOpErsbYmlXGx0JCq3LQIz1M",self.userLoc.selectedLocation.coordinate.latitude,self.userLoc.selectedLocation.coordinate.longitude];
    
    NSString *apiUrl = [NSString stringWithFormat:@"http://www.rightips.com/api/index.php?action=my_wall&uid=%@",userProfile.userIdEmail];
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiUrl parameters:nil];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             [HUD hide:YES];
                                             
                                             if([[JSON objectForKey:@"status"] integerValue] == 1)
                                             {
                                                 
                                                 [self fetchComments:JSON];
                                             }
                                             else if([JSON objectForKey:@"message"])
                                             {
                                                 
                                                 [[[UIAlertView alloc] initWithTitle:@"Error" message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
                                             }
                                             
                                         }
                                                                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             
                                             [HUD hide:YES];
                                         }];
    [operation start];
}
-(void)fetchComments:(NSDictionary *)json
{
    [allCommentsList removeAllObjects];
    NSArray *commentsArray = [json objectForKey:@"data"];
    for (NSDictionary *dic in commentsArray)
    {
        UserProfile *user = [[UserProfile alloc] init];
        user.userName = [dic objectForKey:@"username"];
        
//        user.userLikes = [[dic objectForKey:@"like_count"] integerValue];
//        user.createdAt = [NSString stringWithFormat:@"%@000",[dic objectForKey:@"created"]];
        user.createdAt = [dic objectForKey:@"createdDate"];
        user.userReview = [dic objectForKey:@"comment"];
        user.commentImage = [dic objectForKey:@"com_img"];
        user.profileImgUrl = [dic objectForKey:@"profile_pic"];
        user.userIdEmail = [dic objectForKey:@"uid_fk"];
        user.isLiked = [dic objectForKey:@"is_liked"];
        user.commentId = [dic objectForKey:@"com_id"];
        user.messageId = [dic objectForKey:@"msg_id_fk"];
        user.userName = [dic objectForKey:@"name"];
        user.title = [dic objectForKey:@"message"];
        
        
        [allCommentsList addObject:user];
        
        
    }
    
    
    [myWallTableView reloadData];
    
}
//-(NSString*)timeLeftSinceTime:(double)timesTamp
//{
//    NSString *timeLeft;
//    
//    double currentTimeInMiliSeconds = (double)([[NSDate date] timeIntervalSince1970] * 1000.0);
//    
//    
//    double miliSeconds = currentTimeInMiliSeconds - timesTamp;
//    double seconds = miliSeconds / 1000.0;
//    
//    
//    NSInteger days = (int) (floor(seconds / (3600 * 24)));
//    if(days) seconds -= days * 3600 * 24;
//    
//    NSInteger hours = (int) (floor(seconds / 3600));
//    if(hours) seconds -= hours * 3600;
//    
//    NSInteger minutes = (int) (floor(seconds / 60));
//    if(minutes) seconds -= minutes * 60;
//    
//    if(days) {
//        timeLeft = [NSString stringWithFormat:@"%ld days ago", (long)days];
//    }
//    else if(hours) { timeLeft = [NSString stringWithFormat: @"%ld hours ago", (long)hours];
//    }
//    else if(minutes) { timeLeft = [NSString stringWithFormat: @"%ld minutes ago", (long)minutes];
//    }
//    else if(seconds)
//        timeLeft = [NSString stringWithFormat: @"%ld seconds ago", (long)seconds];
//    
//    return timeLeft;
//}

- (void)didRangeBeacons:(NSNotification *)notification
{
    NotificationData *nd = [BwMethods getNotificationDataFromRangedBeacons:[notification object]];
    if (nd != nil)
    {
        [dbHelper insertNotxToDB:nd];
        
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [allCommentsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UserProfile *user = [allCommentsList objectAtIndex:indexPath.row];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HistoryCell_iPhone" owner:self options:nil];
    HistoryCell *customCell = [nib objectAtIndex:1];
    if  ([user.userReview isEqualToString:@""] && user.commentImage)
    {
        
        [customCell.imageIcon setHidden:NO];
        [customCell.descLabel setHidden:YES];
        [customCell.imageIcon setTitle:NSLocalizedString(@"Image", nil) forState:UIControlStateNormal];
        
    }
    else
    {
        [customCell.imageIcon setHidden:YES];
//        [customCell.descLabel setHidden:NO];
        
    }
    if (![user.profileImgUrl isEqualToString:@""] && ![user.profileImgUrl isEqual:[NSNull null]])
    {
        [customCell.profileImageView setImageURL:[NSURL URLWithString:user.profileImgUrl]];
    }
    else
    {
        [customCell.profileImageView setImage:[UIImage imageNamed:@"NoProfileImage"]];
    }
    
//    NSString *timeLeft = [self timeLeftSinceTime:user.createdAt.doubleValue];
    
    //Getting date from string
//    NSString *dateString = user.createdAt;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
   [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *date = [[NSDate alloc] init];
    date = [dateFormatter dateFromString:user.createdAt];
    // converting into our required date format
    [dateFormatter setDateFormat:@"MMM dd, yyyy"];
    NSString *reqDateString = [dateFormatter stringFromDate:date];
    [customCell.dateTimeLabel setText:reqDateString];
    
    [customCell.nameLabel setText:user.title];
    [customCell.descLabel setText:user.userReview];
    [customCell.descLabel sizeToFit];
    customCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    return customCell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UserProfile *user = [allCommentsList objectAtIndex:indexPath.row];
    
    NotificationData *notxData = [[NotificationData alloc] init];
    [notxData setMsgId:user.messageId];
    [notxData setTitle:user.title];
    
    NotxCommentsVC *vc = (NotxCommentsVC *)[Methods getVCbyName:@"NotxCommentsVC"];
    [vc setSelNotxData:notxData];
    [Methods pushVCinNCwithObj:vc popTop:NO];
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    
//    UserProfile *user = [allCommentsList objectAtIndex:indexPath.row];
//    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HistoryCell_iPhone" owner:self options:nil];
//    HistoryCell *customCell = [nib objectAtIndex:1];
////    if  (![user.commentImage isEqual:[NSNull null]] && ![user.commentImage isEqualToString:@""])
////    {
////        
////        [customCell.imageIcon setHidden:NO];
////        
////    }
////    else
////    {
////        [customCell.imageIcon setHidden:YES];
////        
////    }
////    if (user.profileImgUrl && ![user.profileImgUrl isEqual:[NSNull null]])
////    {
////        [customCell.profileImageView setImageURL:[NSURL URLWithString:user.profileImgUrl]];
////    }
////    else
////    {
////        [customCell.profileImageView setImage:[UIImage imageNamed:@"NoProfileImage"]];
////    }
////
////    NSString *timeLeft = [self timeLeftSinceTime:user.createdAt.doubleValue];
////    [customCell.dateTimeLabel setText:timeLeft];
//    
//    [customCell.nameLabel setText:user.userName];
//    [customCell.descLabel setText:user.userReview];
//    [customCell.descLabel sizeToFit];
//   
//    
//    
//    return customCell.frame.size.height + customCell.descLabel.frame.size.height;
//    
//}
- (IBAction)menuButtonTapped:(UIButton *)sender
{
    [slideMenu showHideMenu];
}
- (IBAction)muteButtonTapped:(UIButton *)sender
{
     [Methods showMuteNotxActionSheet];
}
-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer
{
    [slideMenu showHideMenu];
}


#pragma mark - Change Mute Image
- (void)changeMuteImage:(NSNotification *)notification
{
    if ([[NSUserDefaults standardUserDefaults] doubleForKey:@"muteEndTime"] && ([[NSUserDefaults standardUserDefaults] doubleForKey:@"muteEndTime"] != 0))
    {
        [muteButton setImage:[UIImage imageNamed:@"BtnMute"] forState:UIControlStateNormal];
    }
    else
    {
        [muteButton setImage:[UIImage imageNamed:@"BtnUnmuted"] forState:UIControlStateNormal];
    }
}


-(void)cameraButtonTapped
{
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Edit Photo", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Change Profile Photo", nil),NSLocalizedString(@"Change Cover Photo", nil),nil];
    //    [popup setTag:3];
    [popup showInView:self.view];
    
    
    
}
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ind = buttonIndex;
    
    if (ind != 2)
    {
        
        view = [[ImageCaptureView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        [view setInd:ind];
        
        
        
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             [view setFrame:self.view.frame];
                         }
                         completion:^(BOOL finished){
                         }];
        [self.view addSubview:view];
        
    }
    
    
}
-(void)changeImage
{
    [slideMenu setupMenu];
}







/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
