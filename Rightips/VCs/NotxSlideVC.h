//
//  NotxSlideVC.h
//  Rightips
//
//  Created by Abdul Mannan on 1/1/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotxSlideVC : UIViewController <UIActionSheetDelegate>

@property (nonatomic, retain) NotificationData *selNotxData;
@property (nonatomic, retain) NSArray *arrayOfNotifications;

@end
