//
//  NavController.m
//  WaveClock
//
//  Created by Abdul Mannan on 11/19/14.
//  Copyright (c) 2014 Curiologix. All rights reserved.
//

#import "NavController.h"

@interface NavController ()
{
    NSUserDefaults *defaults;
    CBCentralManager *bluetoothmanager;
}

@end

@implementation NavController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self detectBluetoothState];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void) checkIfAlreadyrun
{
    defaults = [NSUserDefaults standardUserDefaults];
    
    BOOL hasRunAlready = [defaults boolForKey:[StaticData instance].kHasRunAlready];
    if (hasRunAlready)
    {
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
        [self pushViewController:vc animated:YES];
    } else
    {
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LandingVC"];
        [self pushViewController:vc animated:YES];
        
        hasRunAlready = YES;
        [defaults setBool:hasRunAlready forKey:[StaticData instance].kHasRunAlready];
        [defaults synchronize];
    }
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Bluetooth
- (void)detectBluetoothState
{
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], CBCentralManagerOptionShowPowerAlertKey, nil];
    bluetoothmanager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:options];
}


- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    
    defaults = [NSUserDefaults standardUserDefaults];
    
    BOOL hasRunAlready = [defaults boolForKey:[StaticData instance].kHasRunAlready];
    if (bluetoothmanager.state == CBCentralManagerStatePoweredOn)
    {
        if (hasRunAlready)
        {
            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
            [self pushViewController:vc animated:YES];
        }
        else
        {
            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LandingVC"];
            [self pushViewController:vc animated:YES];
            
            hasRunAlready = YES;
            [defaults setBool:hasRunAlready forKey:[StaticData instance].kHasRunAlready];
            [defaults synchronize];
        }
    }
    else
    {
        if (hasRunAlready)
        {
            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
            [self pushViewController:vc animated:YES];
        }
        else
        {
            
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LandingVC"];
        [self pushViewController:vc animated:YES];
        
        hasRunAlready = YES;
        [defaults setBool:hasRunAlready forKey:[StaticData instance].kHasRunAlready];
        [defaults synchronize];
        }
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
