//
//  SearchPlaceVC.h
//  Rightips
//
//  Created by Abdul Mannan on 1/18/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchPlaceVC : UIViewController

@property (nonatomic,strong) NSString *searchText;

@end
