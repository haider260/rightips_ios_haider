//
//  PhotoVC.m
//  Rightips
//
//  Created by Mac on 04/01/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import "PhotoVC.h"
#import "SlideShowRegionVC.h"
#import "PhotoCell.h"
#import "GridPhotoCell.h"
#import "NotxCommentImageVC.h"

@interface PhotoVC ()
{
    
    MBProgressHUD *HUD;
    NSMutableArray *commentList;
    UIView *photoView;
    SlideMenu *slideMenu;
    UIButton *delButton;
    UserProfile *currentuserProfile,*specificUser;
    BOOL isLiked;
    
}

@end

@implementation PhotoVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    [gridButton.layer setBorderWidth:0.5];
//    [gridButton.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//    
//    [verticalButton.layer setBorderWidth:0.5];
//    [verticalButton.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//    
//    [locationButton.layer setBorderWidth:0.5];
//    [locationButton.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//    
//    [groupButton.layer setBorderWidth:0.5];
//    [groupButton.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    
    currentuserProfile=[NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData]];
    
    commentList =[[NSMutableArray alloc] init];
    [recentActivityLabel setText:[NSString stringWithFormat:@"%@'s Recent Activities",[[self.userProfile.userName componentsSeparatedByString:@" "] objectAtIndex:0]]];
    
    
    
    [gridButton setImage:[UIImage imageNamed:@"SelectedGridIcon"] forState:UIControlStateSelected | UIControlStateHighlighted];

    [verticalButton setImage:[UIImage imageNamed:@"SelectedListViewIcon"] forState:UIControlStateSelected | UIControlStateHighlighted];

    
//    NSData *userProfileData = [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData];
//    userProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userProfileData];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    
    [gridButton setSelected:YES];
    [self getPhotos];
}

-(void)getPhotos
{
    [HUD show:YES];
    
    NSString *apiUrl =@"http://www.rightips.com/api/index.php?action=userPhotos";
    
        NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    
        [postParams setValue:self.userProfile.userIdEmail forKey:@"uid"];
        ///[postParams setValue:@"" forKey:@"km"];
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiUrl parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
    [HUD hide:YES];
        if([[JSON objectForKey:@"status"] integerValue] == 1)
        {
            [self fetchPhotoUrlFromJson:JSON];
            
        }
        else if([[JSON objectForKey:@"status"] integerValue] == 2)
        {
            
        [[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"There are currently no pictures", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
        }
        else if([JSON objectForKey:@"message"])
        {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
        }
        
    
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
    [HUD hide:YES];
    [[[UIAlertView alloc] initWithTitle:@"Error" message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
    }];
    [operation start];
}
-(void)fetchPhotoUrlFromJson:(id)json
{
    [commentList removeAllObjects];
    NSArray *commentsArray = [json objectForKey:@"data"];
    for (NSDictionary *dic in commentsArray)
    {
        UserProfile *user = [[UserProfile alloc] init];
//        user.userName = [dic objectForKey:@"username"];
        
        user.userLikes = [[dic objectForKey:@"like_count"] integerValue];
        user.createdAt = [NSString stringWithFormat:@"%@000",[dic objectForKey:@"created"]];
        //        user.createdAt = [dic objectForKey:@"createdDate"];
        user.userReview = [dic objectForKey:@"comment"];
        user.commentImage = [dic objectForKey:@"com_img"];
        user.profileImgUrl = [dic objectForKey:@"profile_pic"];
        user.userIdEmail = [dic objectForKey:@"uid_fk"];
        user.isLiked = [dic objectForKey:@"is_liked"];
        user.commentId = [dic objectForKey:@"com_id"];
//        user.userName = [dic objectForKey:@"name"];
        
        
        [commentList addObject:user];
        
        
    }
    [commentTableView reloadData];
    [collectionVu reloadData];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [commentList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserProfile *user = [commentList objectAtIndex:indexPath.row];
    
    PhotoCell *customCell;
    
    customCell = [[[NSBundle mainBundle] loadNibNamed:@"PhotoCell" owner:nil options:nil] objectAtIndex: 0];
    [customCell.commentImageView setImageURL:[NSURL URLWithString:user.commentImage]];
        
   
//     [Methods setImageInImageViewFromUrl:customCell.commentImageView urlStr:user.commentImage];
//    if (user.img)
//    {
//        [customCell.commentImageView setImage:user.img];
//    }
//    else
//    {
////        [customCell.activityView startAnimating];
//    [self downloadImageWithURL:[NSURL URLWithString:user.commentImage] completionBlock:^(BOOL succeeded, UIImage *image)
//    {
//        if (succeeded)
//        {
////            [customCell.activityView stopAnimating];
//            
//            // change the image in the cell
//            customCell.commentImageView.image = image;
//            
//            
//            NSInteger newHeight = (int)image.size.height;
//            // If image is wider than our imageView, calculate the max height possible
//            if (image.size.width > CGRectGetWidth(customCell.commentImageView.bounds)) {
//                CGFloat ratio = image.size.height / image.size.width;
//                newHeight = CGRectGetWidth(self.view.bounds) * ratio;
//            }
//            user.height = [NSNumber numberWithInteger:newHeight];
//            
//            // cache the image for use later (when scrolling up)
//            user.img = image;
//            [commentList replaceObjectAtIndex:indexPath.row withObject:user];
//            [commentTableView beginUpdates];
//            [commentTableView endUpdates];
//        }
//    }];
//    }
    [customCell.likeButton setTitle:[NSString stringWithFormat:@"%ld",(long)user.userLikes] forState:UIControlStateNormal];
    [customCell.likeButton setImage:[UIImage imageNamed:@"UnSelectLikeIcon"] forState:UIControlStateNormal];
    [customCell.likeButton setImage:[UIImage imageNamed:@"SelectLikeIcon"] forState:UIControlStateSelected];
    [customCell.likeButton setTag:indexPath.row];
    
    
    if ([user.isLiked isEqualToString:@"0"])
    {
        [customCell.likeButton setSelected:NO];
    
    }
    else
    {
        [customCell.likeButton setSelected:YES];
        
    }
    
    
    [customCell.likeButton addTarget:self action:@selector(likeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [customCell.delButton addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [customCell.contentView setBackgroundColor:[UIColor clearColor]];

    if (self.userProfile.profileImgUrl && ![self.userProfile.profileImgUrl isEqual:[NSNull null]])
    {
        [customCell.profileImageView setImageURL:[NSURL URLWithString:self.userProfile.profileImgUrl]];
//        [Methods setImageInImageViewFromUrl:customCell.profileImageView urlStr:user.profileImgUrl];

    }
    else
    {
        [customCell.profileImageView setImage:[UIImage imageNamed:@"NoProfileImage"]];
    }
    
//    [customCell.likeCountLabel setText:[NSString stringWithFormat:@"%ld Likes",(long)user.userLikes]];
    [customCell.userTitleLabel setText:self.userProfile.userName];
//    customCell.commentLabel.text = user.userReview;
//    [customCell.commentLabel sizeToFit];
    
    
    [customCell sizeToFit];
    customCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return customCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 466.0;

//    UserProfile *user = [commentList objectAtIndex:indexPath.row];
//    PhotoCell *customCell;
//    
//        customCell = [[[NSBundle mainBundle] loadNibNamed:@"PhotoCell" owner:nil options:nil] objectAtIndex: 0];
//        [customCell.commentImageView setImageURL:[NSURL URLWithString:user.commentImage]];
//
////    if (user.height)
////    {
////        [HUD hide:YES];
////    }
//    
//    
//    
////    [customCell.userNameLabel setText:user.userName];
//    customCell.commentLabel.text = user.userReview;
//    [customCell.commentLabel sizeToFit];
//    //    [customCell sizeToFit];
//    
//    //    return [self calculateHeightForConfiguredSizingCell:customCell];
//    return  customCell.frame.size.height + customCell.commentLabel.frame.size.height + user.height.doubleValue - 81;
//    return 130 + customCell.commentImageView.frame.size.height;
    
}
- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}
-(void)likeButtonPressed:(UIButton *)sender
{
    
    [self likeComment:sender];
    //    if ([sender isSelected])
    //    {
    //
    //        [self unlikeComment:sender];
    //    }
    //    else
    //    {
    //
    //
    //        [self likeComment:sender];
    //    }
}
-(void)likeComment:(UIButton *)btn
{
    [HUD setLabelText:@""];
    [HUD show:YES];
    specificUser = [commentList objectAtIndex:btn.tag];
    
    NSString *apiURL=@"http://www.rightips.com/api/index.php?action=like";
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setObject:currentuserProfile.userIdEmail forKey:@"uid"];
    [postParams setObject:[NSString stringWithFormat:@"%ld",specificUser.commentId.integerValue] forKey:@"id"];
    [postParams setObject:@"comment" forKey:@"type"];
    if ([specificUser.isLiked isEqualToString:@"0"])
    {
        [postParams setObject:@"Like" forKey:@"rel"];
        isLiked = YES;
    }
    else
    {
        [postParams setObject:@"Unlike" forKey:@"rel"];
        isLiked = NO;
    }
    
    NSURL *url = [NSURL URLWithString:apiURL];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiURL parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [HUD hide:YES];
        if([[JSON objectForKey:@"status"] integerValue] == 1)
        {
            if (isLiked)
            {
                [btn setSelected:YES];
                specificUser.userLikes++;
                specificUser.isLiked = @"1";
                [commentList replaceObjectAtIndex:btn.tag withObject:specificUser];
                [btn setTitle:[NSString stringWithFormat:@"%ld",(long)specificUser.userLikes] forState:UIControlStateSelected];
            }
            else
            {
                [btn setSelected:NO];
                specificUser.userLikes--;
                specificUser.isLiked = @"0";
                [commentList replaceObjectAtIndex:btn.tag withObject:specificUser];
                
                [btn setTitle:[NSString stringWithFormat:@"%ld",(long)specificUser.userLikes] forState:UIControlStateNormal];
                
            }
        }
        else if([JSON objectForKey:@"message"])
        {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
        }
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             [HUD hide:YES];
                                             [Methods showAlertView:@"" message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
                                         }];
    [operation start];
}
-(void)deleteButtonPressed:(UIButton *)sender
{
    UserProfile *user = [commentList objectAtIndex:sender.tag];
    [Methods showReportActionSheet:user.commentId.integerValue];

}

- (IBAction)backButtonPressed:(UIButton *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navController = (UINavigationController *) appDelegate.window.rootViewController;
    [navController popViewControllerAnimated:YES];
}
-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [commentList count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UserProfile *user = [commentList objectAtIndex:indexPath.row];
    
    
    NSString *identifier = @"GridPhotoCell";
    
     BOOL nibMyCellloaded = NO;
    
    if(!nibMyCellloaded)
    {
        UINib *nib = [UINib nibWithNibName:@"GridPhotoCell" bundle: nil];
        [collectionView registerNib:nib forCellWithReuseIdentifier:identifier];
        nibMyCellloaded = YES;
    }
    
    GridPhotoCell *cell = (GridPhotoCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"GridPhotoCell" forIndexPath:indexPath];
    [cell.imgview setImageURL:[NSURL URLWithString:user.commentImage]];
    [user setImg:cell.imgview.image];
    
    [commentList replaceObjectAtIndex:indexPath.row withObject:user];
    
    

    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UserProfile *user = [commentList objectAtIndex:indexPath.row];
    NotxCommentImageVC *vc = (NotxCommentImageVC *)[Methods getVCbyName:@"NotxCommentImageVC"];
    [vc setImg:user.img];
    [Methods pushVCinNCwithObj:vc popTop:NO];
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSLog(@"SETTING SIZE FOR ITEM AT INDEX %d", indexPath.row);
    CGSize mElementSize = CGSizeMake(106, 106);
    return mElementSize;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}
- (IBAction)verticalButtonSelected:(UIButton *)sender
{
    [collectionVu setHidden:YES];
    [commentTableView setHidden:NO];
    [gridButton setSelected:NO];
    [verticalButton setSelected:YES];
}

- (IBAction)gridButtonPressed:(id)sender
{
    [collectionVu setHidden:NO];
    [commentTableView setHidden:YES];
    [verticalButton setSelected:NO];
    [gridButton setSelected:YES];
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
