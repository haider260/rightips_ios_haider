//
//  HomeVC.m
//  Rightips
//
//  Created by Abdul Mannan on 12/30/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import "HomeVC.h"
#import "HomeNotx.h"
#import "NotxDetailsVC.h"
#import "NotxSlideVC.h"
#import "HomeFullListCell.h"
#import "APTimeZones.h"
#import "MapVC.h"
#import "RegionVC.h"
#import "SearchResultVC.h"
#import "ImageCaptureView.h"
#import "PhotoView.h"
#import "SearchPlaceVC.h"


@interface HomeVC () <SwipeViewDelegate, SwipeViewDataSource>
{
    MBProgressHUD *HUD;
    SlideMenu *slideMenu;
   SearchMenu *searchMenu;
    
    
    DbHelper *dbHelper;
    NSMutableArray *homeNotxs,*commentList;
    BOOL sendLocationToPiwik;
    BOOL beaconsInRange;
    BOOL pageControlBeingUsed;
    UserProfile *loginUserProfile;
    
    UIImageView *imgView;
    
    
    NSInteger ind;
    ImageCaptureView *view;

    
    IBOutlet UIButton *thingsToDoButton;
    
    IBOutlet UIButton *foodButton;
    
    IBOutlet UIButton *shoppingButton;
    
    IBOutlet UIButton *latestEventsButton;
    
    IBOutlet UIButton *mapButton;
    
    IBOutlet UIButton *regionButton;
    
    
    
}

@property (nonatomic, retain) IBOutlet UIButton *menuToggleButton;
@property (nonatomic, retain) IBOutlet UIButton *muteButton;
@property (nonatomic, retain) IBOutlet UIView *miniSliderContView;
@property (nonatomic, retain) IBOutlet UILabel *locationLabel;
@property (nonatomic, retain) IBOutlet SwipeView *swipeView;
@property (nonatomic, retain) IBOutlet UIPageControl *notxPageControl;
@property (strong, nonatomic) IBOutlet UILabel *localTimeLabel;
@property (nonatomic, retain) IBOutlet UIView *locationNaContView;
@property (nonatomic, retain) IBOutlet UILabel *locationNaLabel;
@property (nonatomic, retain) IBOutlet UITextView *locationNaTextView;
@property (weak, nonatomic) IBOutlet UILabel *currentLocationLabel;
@end

@implementation HomeVC

@synthesize myWeatherView;
@synthesize menuToggleButton;
@synthesize miniSliderContView, locationLabel, notxPageControl;
@synthesize locationNaContView, locationNaLabel, locationNaTextView;
#pragma mark - View Life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [scrlVu setFrame:CGRectMake(0, 0, scrlVu.frame.size.width, scrlVu.frame.size.height)];
    
    
    
    NSData *userProfileData = [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData];
    loginUserProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userProfileData];
    
    commentList = [[NSMutableArray alloc] init];

    [self.myWeatherView.layer setBorderWidth:1.0];
    [self.myWeatherView.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    
//    [self.mapsButton setBackgroundColor:[UIColor blueColor]];
    [mapButton.layer setBorderWidth:1.0];
    [mapButton.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [regionButton.layer setBorderWidth:1.0];
    [regionButton.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [thingsToDoButton.layer setBorderWidth:1.0];
    [thingsToDoButton.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [foodButton.layer setBorderWidth:1.0];
    [foodButton.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [shoppingButton.layer setBorderWidth:1.0];
    [shoppingButton.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [latestEventsButton.layer setBorderWidth:1.0];
    [latestEventsButton.layer setBorderColor:[UIColor whiteColor].CGColor];
    

    sendLocationToPiwik = YES;

    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isHomeLoaded"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    slideMenu = [[SlideMenu alloc] init];
    [self.view addSubview:slideMenu];
    [self.view bringSubviewToFront:menuToggleButton];
    
    imgView= [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [imgView setImage:[UIImage imageNamed:@"TransparentImage"]];
    [imgView setHidden:YES];
    [imgView setUserInteractionEnabled:YES];
    [self.view addSubview:imgView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideToUpperWithGestureRecognizer:)];
    [tap setDelegate:self];
    [imgView addGestureRecognizer:tap];
    
    searchMenu = [[SearchMenu alloc] init];
    [self.view addSubview:searchMenu];
    [searchMenu setUpMenu];

    UISwipeGestureRecognizer *swipeLeftMenu = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToLeftWithGestureRecognizer:)];
    swipeLeftMenu.direction = UISwipeGestureRecognizerDirectionLeft;
    [slideMenu addGestureRecognizer:swipeLeftMenu];
    
    _swipeView.alignment = SwipeViewAlignmentCenter;
    _swipeView.pagingEnabled = YES;
    _swipeView.itemsPerPage = 1;
    _swipeView.truncateFinalPage = YES;
    _swipeView.bounces = NO;
    
    dbHelper = [[DbHelper alloc] init];
    homeNotxs = [NSMutableArray array];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noResult) name:@"noResult" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchHide) name:@"SearchHide" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeImage) name:@"changeImage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cameraButtonTapped) name:@"TapOnCameraButton" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateLocations:) name:@"didUpdateLocations" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRangeBeacons:) name:@"didRangeBeacons" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeMuteImage:) name:@"changeMuteImage" object:nil];
    
    [self changeMuteImage:nil];
    
   
    
    if ([CLLocationManager locationServicesEnabled])
    {
        if ([CLLocationManager authorizationStatus] >= kCLAuthorizationStatusAuthorizedAlways)
        {
//            [miniSliderContView setHidden:NO];
//            [locationNaContView setHidden:YES];
//            [myWeatherView setHidden:NO];
//            [self.swipeView setHidden:NO];
        }
        else
        {
//            [noNotificationLabel setHidden:YES];
//            [locationNaLabel setText:NSLocalizedString(@"Location Access is DISABLED", nil)];
//            [locationNaTextView setText:NSLocalizedString(@"Location Access for Rightips app is disabled. Please enable it by going to Settings -> Privacy -> Location Services -> Rightips and pressing the toggle button.", nil)];
            
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location Access for Rightips app is disabled. Please enable it by going to Settings", nil)
//                                                            message:nil
//                                                           delegate:self
//                                                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
//                                                  otherButtonTitles:NSLocalizedString(@"Settings", nil),nil];
//            [alert show];
        }
    }
    else
    {
//        [noNotificationLabel setHidden:YES];
//        [locationNaLabel setText:NSLocalizedString(@"Location Services is OFF", nil)];
//        [locationNaTextView setText:NSLocalizedString(@"Please enable location services by going to Settings -> Privacy -> Location Services and pressing the toggle button. You can use the search bar above to find the best tips in the place of your choice.", nil)];
        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Turn on Location Services to allow Rightips to Determine your Current Location", nil)
//                                                        message:nil
//                                                       delegate:self
//                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
//                                              otherButtonTitles:NSLocalizedString(@"Settings", nil),nil];
//        [alert show];
//        [self uploadStaticNotifications];
    }
    
    
    
    [self didUpdateLocations:nil];
    [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
}
- (void)scrollingTimer
{
    
         CGFloat contentOffset = self.swipeView.scrollView.contentOffset.x;
         // calculate next page to display
         int nextPage = (int)(contentOffset/self.swipeView.scrollView.frame.size.width) + 1 ;
         if( nextPage!= 5)
         {
             [self.swipeView.scrollView scrollRectToVisible:CGRectMake(nextPage*self.swipeView.scrollView.frame.size.width, 0, self.swipeView.scrollView.frame.size.width, self.swipeView.scrollView.frame.size.height) animated:YES];
             notxPageControl.currentPage=nextPage;
             // else start sliding form 1 :)
         } else
         {
             [self.swipeView.scrollView scrollRectToVisible:CGRectMake(0, 0, self.swipeView.scrollView.frame.size.width, self.swipeView.scrollView.frame.size.height) animated:YES];
             notxPageControl.currentPage=0;
         }
     }
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (buttonIndex == 1)
//    {
//        
//    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
//    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: UIApplicationOpenSettingsURLString]];
//    }
//}
-(void)slideToUpperWithGestureRecognizer:(UITapGestureRecognizer *)gestureRecognizer
{
//    UIImageView *imgView =  (UIImageView *)gestureRecognizer.view;
    [imgView setHidden:YES];
    [searchMenu showHideMenu];
    
}
-(void)uploadStaticNotifications
{
    [homeNotxs removeAllObjects];
    for (int i = 0; i < 2; i++)
    {
    NotificationData *nd = [[NotificationData alloc] init];
        if (i == 0)
        {
            
    [nd setTitle:@"Food and Drinks"];
    [nd.images addObject:@"http://weissenstein-pub.ch/wp-content/uploads/2016/01/Drinks-Food.png"];
        }
        if (i == 1)
        {
            [nd setTitle:@"Events"];
            [nd.images addObject:@"http://petprofessionalguild.com/Resources/Pictures/Event%20Planning.png"];
        }
    [homeNotxs addObject:nd];
    }
    [self.swipeView setHidden:NO];
    [self.swipeView reloadData];
    
}
-(void)searchHide
{
    [imgView setHidden:YES];
    SearchResultVC *vc = (SearchResultVC *)[Methods getVCbyName:@"SearchResultVC"];
    NSLog(@"%@",searchMenu.searchResults);
    [vc setSearchResultsList:searchMenu.searchResults];
//    SearchMenu *searchMenue = [[SearchMenu alloc] init];
    
    [Methods pushVCinNCwithObj:vc popTop:YES];
}
-(void)noResult
{
    [imgView setHidden:YES];
}
- (IBAction)changePage
{
    CGRect frame;
    frame.origin.x = self.swipeView.scrollView.frame.size.width * notxPageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.swipeView.scrollView.frame.size;
    [self.swipeView.scrollView scrollRectToVisible:frame animated:YES];
    pageControlBeingUsed = YES;
}
- (void)tapOnSpecificEvent:(NSString *)title
{
    [searchMenu.searchTextField setText:title];
    UserLocation *userLocation = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"userLocationSelectedData"]];
    if (!userLocation.selectedLocation)
    {
        userLocation = [StaticData instance].userLocation;
        if (!userLocation.selectedLocation)
        {
            [self searchButtonPressed:searchBtn];
            [searchMenu locationButtonPressed:searchMenu.locationbutton];
            [searchMenu.searchLocationTextField becomeFirstResponder];
            return;
            
        }
    }
    
    [searchMenu getSearchResults];
    
//    SearchResultVC *vc = (SearchResultVC *)[Methods getVCbyName:@"SearchResultVC"];
////    [vc setSearchString:title];
//    [Methods pushVCinNCwithObj:vc popTop:YES];
}
- (IBAction)thingsToDoButtonPressed:(UIButton *)sender
{
    [self tapOnSpecificEvent:@"Entertainment  tourism  service"];
}
- (IBAction)foodDrinksButtonPressed:(UIButton *)sender
{
    [self tapOnSpecificEvent:@"Food & Drinks"];
}
- (IBAction)shoppingButtonPressed:(UIButton *)sender
{
    [self tapOnSpecificEvent:@"Retailers & Shops"];
}
- (IBAction)latestEventsButtonPressed:(UIButton *)sender
{
    [self tapOnSpecificEvent:@"Events"];
}


- (IBAction)mapButtonPressed:(UIButton *)sender
{
//    [self.mapsButton setBackgroundColor:[UIColor whiteColor]];
//    [self.mapsButton setAlpha:0.5];
//    [self.mapsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
//    if (selNotxData.catGroupId != nil && ![selNotxData.catGroupId isEqualToString:@""])
//    {
    UserLocation *userLocation = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"userLocationSelectedData"]];
    if (!userLocation.selectedLocation)
    {
        userLocation = [StaticData instance].userLocation;
    }
    MapVC *vc = (MapVC *)[Methods getVCbyName:@"MapVC"];
   
        [vc setUserLoc:userLocation];
        [Methods pushVCinNCwithObj:vc popTop:YES];
//    }
//    else
//    {
//        [[[UIAlertView alloc] initWithTitle:@"Map data missing" message:@"Please check your internet connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
//    }
}
- (IBAction)regionButtonPressed:(UIButton *)sender
{

    UserLocation *userLocation = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"userLocationSelectedData"]];
    if (!userLocation.selectedLocation)
    {
        userLocation = [StaticData instance].userLocation;
    }
    RegionVC  *vc = (RegionVC *)[Methods getVCbyName:@"RegionVC"];
    
    [vc setUserLoc:userLocation];
    [Methods pushVCinNCwithObj:vc popTop:NO];
}


- (IBAction)takePhotoButtonPressed:(UIButton *)sender
{
//    [self.takePhotoButton setBackgroundColor:[UIColor whiteColor]];
//    [self.takePhotoButton setAlpha:0.5];
//    [self.takePhotoButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
}

- (IBAction)photosButtonPressed:(UIButton *)sender
{
//    [self.photosButton setBackgroundColor:[UIColor whiteColor]];
//    [self.photosButton setAlpha:0.5];
//    [self.photosButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
}
- (IBAction)searchButtonPressed:(UIButton *)sender
{
    
    [imgView setHidden:NO];
    [searchMenu showHideMenu];

    
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
}
- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //Sending current screen to Piwic Tracker
    //[[PiwikTracker sharedInstance] sendView:@"Home"];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didRangeBeacons" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SearchHide" object:nil];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - SwipeGesture Methods
-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer
{
    [slideMenu showHideMenu];
}

#pragma mark - Swipe View Methods
- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
//    if ([homeNotxs count] < 1)
//    {
//        [noNotificationLabel setHidden:NO];
//        [noNotificationLabel setText:NSLocalizedString(@"Oops! we didn’t find anything that matches your location", nil)];
//    }
//    else
//    {
//        [noNotificationLabel setHidden:YES];
//    }
    
    
    notxPageControl.numberOfPages = [homeNotxs count] > 4 ? 4+1 : [homeNotxs count];
    [notxPageControl setCurrentPage:0];
    return [homeNotxs count] > 4 ? 4+1 : [homeNotxs count];
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view {
    
    if (index == 4)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeFullListCell" owner:self options:nil];
        HomeFullListCell *fullListCell = [nib objectAtIndex:0];
        return fullListCell;
    }
    else
    {
        NotificationData *nd = [homeNotxs objectAtIndex:index];
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeNotx_iPhone" owner:self options:nil];
        HomeNotx *notxView = [nib objectAtIndex:0];
        [notxView setupView];
        if ([nd.images count] > 0)
            [notxView.iconBigIV setImageURL:[NSURL URLWithString:[nd.images objectAtIndex:0]]];
//            [Methods setImageInImageViewFromUrl:notxView.iconBigIV urlStr:[nd.images objectAtIndex:0]];
        else
            notxView.iconBigIV.image = [UIImage imageNamed:@"ImageNA"];
        
        notxView.nameLabel.text = nd.siteName;
        notxView.descLabel.text = nd.title;
        
        if ([nd.cats count] > 0)
        {
            if ([nd.cats count] > 1)
            {
                CategoryData *cd1 = [nd.cats objectAtIndex:0];
                [Methods setImageInImageViewFromUrl:notxView.cat1IconIV urlStr:cd1.catImgUrl];
                notxView.cat1Label.text = cd1.catName;
                CategoryData *cd2 = [nd.cats objectAtIndex:0];
                [Methods setImageInImageViewFromUrl:notxView.cat2IconIV urlStr:cd2.catImgUrl];
                notxView.cat2Label.text = cd2.catName;
            } else
            {
                CategoryData *cd1 = [nd.cats objectAtIndex:0];
                [Methods setImageInImageViewFromUrl:notxView.cat1IconIV urlStr:cd1.catImgUrl];
                notxView.cat1Label.text = cd1.catName;
                notxView.cat2IconIV.hidden = YES;
                notxView.cat2Label.hidden = YES;
            }
        }
        else
        {
            notxView.cat1IconIV.hidden = YES;
            notxView.cat1Label.hidden = YES;
            notxView.cat2IconIV.hidden = YES;
            notxView.cat2Label.hidden = YES;
        }
        
        return notxView;
        
    }
}

- (void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView
{
    notxPageControl.currentPage = swipeView.currentItemIndex;
}

- (void)swipeView:(SwipeView *)swipeView didSelectItemAtIndex:(NSInteger)index
{
    
    NotificationData *nd = [homeNotxs objectAtIndex:index];
    if ([nd.siteName isEqualToString:@""])
    {
        if ([nd.title isEqualToString:@"Food and Drinks"])
        [searchMenu.searchTextField setText:@"Food & Drinks"];
        else
            [searchMenu.searchTextField setText:nd.title];
        UserLocation *userLocation = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"userLocationSelectedData"]];
        if (!userLocation.selectedLocation)
        {
            userLocation = [StaticData instance].userLocation;
            if (!userLocation.selectedLocation)
            {
                [self searchButtonPressed:searchBtn];
                [searchMenu locationButtonPressed:searchMenu.locationbutton];
                [searchMenu.searchLocationTextField becomeFirstResponder];
                return;
                
            }
            [searchMenu getSearchResults];
            return;
        }
        
        [searchMenu getSearchResults];
        return;
    }
//    NotxSlideVC *vc = (NotxSlideVC *)[Methods getVCbyName:@"NotxSlideVC"];
//    [vc setSelNotxData:[homeNotxs objectAtIndex:index]];
//    [vc setArrayOfNotifications:homeNotxs];
//    
////    [dbHelper insertNotxToDB:[homeNotxs objectAtIndex:index]];
//    [Methods pushVCinNCwithObj:vc popTop:YES];
    if (index == 4)
    {
//        SearchResultVC *vc = (SearchResultVC *)[Methods getVCbyName:@"SearchResultVC"];
//        [vc setSearchResultsList:homeNotxs];
//        [Methods pushVCinNCwithObj:vc popTop:YES];
        [searchMenu.searchTextField setText:@""];
        [searchMenu getSearchResults];
        return;
    }
    
    NotxDetailsVC *vc = (NotxDetailsVC *)[Methods getVCbyName:@"NotxDetailsVC"];
    [vc setSelNotxData:[homeNotxs objectAtIndex:index]];
    [dbHelper insertNotxToDB:[homeNotxs objectAtIndex:index]];
    [Methods pushVCinNCwithObj:vc popTop:YES];
}

- (IBAction)rightButtonTapped:(UIButton *)sender
{
    notxPageControl.currentPage++;
    CGRect frame;
    frame.origin.x = self.swipeView.scrollView.frame.size.width * notxPageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.swipeView.scrollView.frame.size;
    [self.swipeView.scrollView scrollRectToVisible:frame animated:YES];

    
}
- (IBAction)leftButtonTapped:(UIButton *)sender
{
    notxPageControl.currentPage--;
    CGRect frame;
    frame.origin.x = self.swipeView.scrollView.frame.size.width * notxPageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.swipeView.scrollView.frame.size;
    [self.swipeView.scrollView scrollRectToVisible:frame animated:YES];
    
    
}

- (void)swipeViewDidScroll:(SwipeView *)swipeView
{
    
    if (!pageControlBeingUsed)
    {
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat pageWidth = self.swipeView.scrollView.frame.size.width;
        int page = floor((self.swipeView.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        notxPageControl.currentPage = page;
    }

}
- (void)swipeViewWillBeginDragging:(SwipeView *)swipeView
{
    pageControlBeingUsed = NO;
}
- (void)swipeViewDidEndDecelerating:(SwipeView *)swipeView
{
     pageControlBeingUsed = NO;
}

#pragma mark - Location Methods
- (void)didUpdateLocations:(NSNotification *)notification
{
    
//    UserLocation *userLocation = [StaticData instance].userLocation;
    UserLocation *userLocation = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"userLocationSelectedData"]];
    if (!userLocation.selectedLocation)
    {
        userLocation = [StaticData instance].userLocation;
    }
    
//    if (beaconsInRange == NO)
//    {
        if (userLocation.selectedLocation && ![userLocation.selectedCityName isEqualToString:@""])
        {
            [myWeatherView setHidden:NO];
            [myWeatherView updateCurrentWeather];
            locationLabel.text = userLocation.selectedCityName;
            [self fetchLocNotxs:userLocation.selectedLocation];
        }
        else
        {
            locationLabel.text = @"";
            [myWeatherView setHidden:YES];
            [self handleLocNotxsFailure];
        }
        if (userLocation.userLocationMode == UserLocationModeCurrent)
        {
            self.currentLocationLabel.hidden = NO;
        }
        else
        {
            self.currentLocationLabel.hidden = YES;
        }
//    }
    // Piwik Event When Location Update
    CLLocation *location = userLocation.selectedLocation;
    [self.localTimeLabel setText:@""];
    if(location)
    {
    
    NSTimeZone *timeZone = [[APTimeZones sharedInstance] timeZoneWithLocation:location];
    NSDate *now = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:timeZone.name]];
    [self.localTimeLabel setText:[formatter stringFromDate:now]];
    }

        if (location!=nil && sendLocationToPiwik == YES)
    {
        NSString *latStr = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
        NSString *lngStr = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
//        float lat = latStr.floatValue;
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
        [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (!(error))
             {
            if(userLocation.selectedCityName)
            {
                
                 CLPlacemark *placemark = [placemarks objectAtIndex:0];
                 NSString *address = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                 
                 NSMutableDictionary *datadictForPiwik = [[NSMutableDictionary alloc] init];
                 [datadictForPiwik setObject:address forKey:kLocationAddress];
                 [datadictForPiwik setObject:userLocation.selectedCityName forKey:kLocationCityName];
                 [datadictForPiwik setObject:latStr forKey:kLocationLatitude];
                 [datadictForPiwik setObject:lngStr forKey:kLocationLongitude];
                 [datadictForPiwik setObject:@"iPhone" forKey:kLocationSource];
                 if (userLocation.userLocationMode == UserLocationModeCurrent)
                 {
                     [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_LOCATION action:ACTION_CURRENT_LOCATION name:[ReuseAbleFunctions convertDictionaryToJSONString:datadictForPiwik] value:nil];
                 }
                 else
                 {
                     [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_LOCATION action:ACTION_LOCATION_SEARCHED name:[ReuseAbleFunctions convertDictionaryToJSONString:datadictForPiwik] value:nil];
                 }
            }
             }
             sendLocationToPiwik = NO;
         }];
    }
}
//-(void)updateCurrentWeather
//{
//
//}

#pragma mark - Change Mute Image
- (void)changeMuteImage:(NSNotification *)notification
{
    if ([[NSUserDefaults standardUserDefaults] doubleForKey:@"muteEndTime"] && ([[NSUserDefaults standardUserDefaults] doubleForKey:@"muteEndTime"] != 0))
    {
        [self.muteButton setImage:[UIImage imageNamed:@"BtnMute"] forState:UIControlStateNormal];
    }
    else
    {
        [self.muteButton setImage:[UIImage imageNamed:@"BtnUnmuted"] forState:UIControlStateNormal];
    }
}
#pragma mark - Did Range Beacons
- (void)didRangeBeacons:(NSNotification *)notification
{
    UserLocation *userLocation = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"userLocationSelectedData"]];
    if (!userLocation.selectedLocation)
    {
        userLocation = [StaticData instance].userLocation;
    }
    NotificationData *nd = [BwMethods getNotificationDataFromRangedBeacons:[notification object]];
    if (nd != nil)
    {
        [dbHelper insertNotxToDB:nd];
//        NotxSlideVC *vc = (NotxSlideVC *)[Methods getVCbyName:@"NotxSlideVC"];
//        [vc setArrayOfNotifications:[dbHelper getAllNotxFromDB]];
//        [Methods pushVCinNCwithObj:vc popTop:YES];
        
        NotxDetailsVC *vc = (NotxDetailsVC *)[Methods getVCbyName:@"NotxDetailsVC"];
        [vc setSelNotxData:nd];
        [Methods pushVCinNCwithObj:vc popTop:YES];
        //Piwik Event (Notification Opened)
        NSMutableDictionary *datadictForPiwik = [[NSMutableDictionary alloc] init];
        [datadictForPiwik setObject:nd.notxId forKey:kNotificationId];
        [datadictForPiwik setObject:nd.siteId forKey:kNotificationSiteId];
        [datadictForPiwik setObject:nd.siteName forKey:kNotificationSiteName];
        [datadictForPiwik setObject:nd.title forKey:kNotificationTitle];
        if (nd.zone)
        {
            [datadictForPiwik setObject:nd.zone forKey:kNotificationZone];
        }
        [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_NOTIFICATION action:ACTION_OPENED name:[ReuseAbleFunctions convertDictionaryToJSONString:datadictForPiwik] value:nil];
    }
    NSMutableArray *beaconsArrayWithData = [BwMethods getNotificationsFromRangedBeacons:[notification object]];
   
    
    
    if ([beaconsArrayWithData count] != 0)
    {
        beaconsInRange = YES;
        
//        self.currentLocationLabel.hidden = NO;
//        if (userLocation.selectedLocation && ![userLocation.selectedCityName isEqualToString:@""])
//        {
//            locationLabel.text = userLocation.currentCityName;
//        }
//        else
//        {
//            locationLabel.text = @"N/A";
//        }
//        [homeNotxs removeAllObjects];
//        homeNotxs = beaconsArrayWithData;
//        [_swipeView reloadData];
        
    }
    else
    {
        beaconsInRange = NO;
        if (userLocation.selectedLocation && ![userLocation.selectedCityName isEqualToString:@""])
        {
            locationLabel.text =
            userLocation.selectedCityName;
            [self fetchLocNotxs:userLocation.selectedLocation];
        } else
        {
            locationLabel.text = @"N/A";
            [self handleLocNotxsFailure];
        }
        if (userLocation.userLocationMode == UserLocationModeCurrent)
        {
            self.currentLocationLabel.hidden = NO;
        }
        else
        {
            self.currentLocationLabel.hidden = YES;
        }
    }
}

#pragma mark - Button Actions
- (IBAction)slideMenuButtonPressed:(id)sender
{
    [slideMenu showHideMenu];
   
}

- (IBAction)muteButtonPressed:(id)sender
{
    [Methods showMuteNotxActionSheet];
}


#pragma mark - Fetching And Handling Location Notifications
- (void)fetchLocNotxs:(CLLocation *)loc
{
    NSString *apiUrl = [NSString stringWithFormat:@"%@", @"http://api.beaconwatcher.com/index.php?action="];
    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"getLocNotification"]];
    
//    CLLocation *location = [StaticData instance].userLocation.selectedLocation;
    NSString *latStr = [NSString stringWithFormat:@"%f", loc.coordinate.latitude];
    NSString *lngStr = [NSString stringWithFormat:@"%f", loc.coordinate.longitude];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setValue:[StaticData instance].apiKey forKey:@"key"];
    [postParams setValue:@"1" forKey:@"isadmin"];
    [postParams setValue:latStr forKey:@"lat"];
    [postParams setValue:lngStr forKey:@"lng"];
    ///[postParams setValue:@"" forKey:@"km"];
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:apiUrl parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        [self handleLocNotxsSuccess:JSON];
    }failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        [self handleLocNotxsFailure];
    }];
    [operation start];
}

- (void)handleLocNotxsSuccess:(id)JSON
{
    [homeNotxs removeAllObjects];
    
    //NSString *status = [JSON objectForKey:@"status"];
    //if ([status intValue] == 1) {
    id notxArray = [JSON objectForKey:@"data"];
    for (unsigned int i = 0; i < [notxArray count]; i++)
    {
        id notxObj = [notxArray objectAtIndex:i];
        NotificationData *nd = [Methods parseNotificationData:notxObj];
        [homeNotxs addObject:nd];
    }
    //}
    if ([homeNotxs count] > 0)
    {
        
        [self.swipeView setHidden:NO];
        [self.swipeView reloadData];
    } else
    {
        
        [self handleLocNotxsFailure];
    }
}

- (void)handleLocNotxsFailure
{
//     if ([CLLocationManager locationServicesEnabled])
//     {
//         if ([CLLocationManager authorizationStatus] >= kCLAuthorizationStatusAuthorized)
//             [self.swipeView setHidden:YES];
//         else
//         {
//             [self.swipeView setHidden:NO];
//             [self uploadStaticNotifications];
//         }
//     
//     }
//    else
//    {
//        [self uploadStaticNotifications];
//    }
   
    
////    dbHelper = [[DbHelper alloc] init];
////    homeNotxs = [dbHelper getAllNotxFromDB];
//    [_swipeView reloadData];
//    if ([locationNaLabel isHidden])
//    {
//        [noNotificationLabel setHidden:NO];
//    }
    [self uploadStaticNotifications];
    
//    [noNotificationLabel setHidden:NO];
//    [noNotificationLabel setText:NSLocalizedString(@"Oops! we didn’t find anything that matches your location", nil)];
    
    
    
}
- (NotificationData *)parseNotificationData:(id)notxObj
{
    NotificationData *nd = [[NotificationData alloc] init];
//    NSString  *sourceString = @"location";
    [nd setSource:@"location"];
    nd.msgId=[notxObj objectForKey:@"msg_id"];
    nd.zone = [notxObj objectForKey:@"zone"];
    nd.notxId = [notxObj objectForKey:@"nt_id"];
    nd.title = [notxObj objectForKey:@"nt_title"];
    nd.detail = [notxObj objectForKey:@"nt_details"];
    nd.notxTemplate = [notxObj objectForKey:@"template"];
    nd.statusId = [notxObj objectForKey:@"status_id"];
    nd.siteId = [notxObj objectForKey:@"site_id"];
    nd.siteName = [notxObj objectForKey:@"site_name"];
    nd.siteAddress = [notxObj objectForKey:@"site_address"];
    nd.sitePhone = [notxObj objectForKey:@"site_phone"];
    nd.notxStartDate = [notxObj objectForKey:@"nt_startdate"];
    nd.notxEndDate = [notxObj objectForKey:@"nt_enddate"];
    nd.discount = [notxObj objectForKey:@"nt_discount"];
    nd.dealStartDate = [notxObj objectForKey:@"nt_deal_start"];
    nd.dealEndDate = [notxObj objectForKey:@"nt_deal_end"];
    if([notxObj objectForKey:@"nt_url"])
    {
        nd.notxUrl = [notxObj objectForKey:@"nt_url"];
    }
    if ([notxObj objectForKey:@"cat_id"])
    {
        nd.catId = [notxObj objectForKey:@"cat_id"];
    }
    //nd.creationDate = [[notObj objectForKey:@""] longValue];
    if ([notxObj objectForKey:@"site_long"])
    {
        nd.longitude = [notxObj objectForKey:@"site_long"];
    }
    if ([notxObj objectForKey:@"site_lat"])
    {
        nd.latitude = [notxObj objectForKey:@"site_lat"];
    }
    NSArray *imgsArrObj = [notxObj objectForKey:@"images"];
    for (id imgObj in imgsArrObj)
    {
        [nd.images addObject:[imgObj objectForKey:@"url"]];
    }
    
    NSArray *catsArrObj = [notxObj objectForKey:@"category"];
    for (id catObj in catsArrObj) {
        CategoryData *cd = [[CategoryData alloc] init];
        cd.catId = [catObj objectForKey:@"cat_id"];
        cd.catName = [catObj objectForKey:@"cat_name"];
        cd.catImgUrl = [catObj objectForKey:@"cat_image"];
        [nd.cats addObject:cd];
    }
    
    if([notxObj objectForKey:@"nt_url"])
    {
        nd.notxUrl = [notxObj objectForKey:@"nt_url"];
    }
    NSArray *socialLinksArray = [notxObj objectForKey:@"social_links"];
    for (NSDictionary *linkDict in socialLinksArray)
    {
        if ([[linkDict objectForKey:@"name"] isEqualToString:@"facebook"])
        {
            nd.fbUrl = [linkDict objectForKey:@"url"];
        }
        else if ([[linkDict objectForKey:@"name"] isEqualToString:@"instagram"]) {
            nd.instagramUrl = [linkDict objectForKey:@"url"];
        }
        else if ([[linkDict objectForKey:@"name"] isEqualToString:@"googleplus"]) {
            nd.googlePlusUrl = [linkDict objectForKey:@"url"];
        }
        else if ([[linkDict objectForKey:@"name"] isEqualToString:@"mail"]) {
            nd.mailUrl = [linkDict objectForKey:@"url"];
        }
    }
    if ([notxObj objectForKey:@"nt_website_url"])
    {
        nd.catLink = [notxObj objectForKey:@"nt_website_url"];
    }
    if ([notxObj objectForKey:@"site_total_notifications"])
    {
        nd.totalSiteNotifications = [notxObj objectForKey:@"site_total_notifications"];
    }
    return nd;
}

-(void)cameraButtonTapped
{

    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Edit Photo", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Change Profile Photo", nil),NSLocalizedString(@"Change Cover Photo", nil),nil];
    //    [popup setTag:3];
    [popup showInView:self.view];
    
    
    
}
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ind = buttonIndex;
    
    if (ind != 2)
    {
        
    view = [[ImageCaptureView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    [view setInd:ind];
    
    
    
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         [view setFrame:self.view.frame];
                     }
                     completion:^(BOOL finished){
                     }];
    [self.view addSubview:view];
    
    }
    
    
}
-(void)changeImage
{
    [slideMenu setupMenu];
}
@end
