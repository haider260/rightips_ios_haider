//
//  NotxDetailsVC.h
//  Rightips
//
//  Created by Abdul Mannan on 1/5/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBActivity.h"
#import "RatingView.h"
#import "AddReviewVC.h"
@interface NotxDetailsVC : UIViewController<UIActionSheetDelegate, FacBookShareDelegate,UIScrollViewDelegate,RatingViewDelegate,UITableViewDataSource,UITableViewDelegate,FBSDKSharingDelegate>
{
    
    IBOutlet UIView *rateInfoView;
    
    
    IBOutlet UITableView *notificationReviewTableView;
    
    IBOutlet RatingView *rateView;
    
}

- (void) tempShareForFacebook;
@property (nonatomic, retain) NotificationData *selNotxData;

@property (nonatomic, retain) IBOutlet UILabel *lblSiteName;

@property (strong, nonatomic) IBOutlet RatingView *rateview;


//@property (nonatomic, retain) IBOutlet UIButton *btnFacebook;
//@property (nonatomic, retain) IBOutlet UIButton *btnInstagram;
//@property (nonatomic, retain) IBOutlet UIButton *btnGooglePlus;
//@property (nonatomic, retain) IBOutlet UIButton *btnEmail;
//@property (nonatomic, retain) IBOutlet UIButton *btnCatLink;


- (IBAction)likeButtonTapped:(id)sender;
- (IBAction)shareButtonTapped:(id)sender;

- (IBAction)btnFacebookPressed:(id)sender;
- (IBAction)btnInstgramPressed:(id)sender;
- (IBAction)btnGooglePlusPressed:(id)sender;
- (IBAction)btnEmailPressed:(id)sender;
- (IBAction)btnCatLink:(id)sender;
- (IBAction)btnComments:(id)sender;

@end
