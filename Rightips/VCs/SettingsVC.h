//
//  SettingsVC.h
//  Rightips
//
//  Created by Abdul Mannan on 11/27/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALRadialMenu.h"

@interface SettingsVC : UIViewController<ALRadialMenuDelegate>
{
    
    IBOutlet UILabel *chooseYourPrerenceLabel;
    
    IBOutlet UIButton *backButton;
}

@property (strong, nonatomic) ALRadialMenu *radialMenu;
@property (strong, nonatomic) UserProfile *userProfile;

@end
