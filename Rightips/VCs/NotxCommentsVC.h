//
//  NotxCommentsVC.h
//  Rightips
//
//  Created by Shahbaz Ali on 8/4/15.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "ProfileVC.h"



//@interface NotxCommentsVC : JSQMessagesViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@interface NotxCommentsVC : UIViewController <UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,UIImagePickerControllerDelegate,UITextViewDelegate,UIAlertViewDelegate,UIActionSheetDelegate>
{
    
    
    UITableView *tableVu;
    IBOutlet UILabel *notificationName;
    
    IBOutlet UILabel *siteName;
    
    
    UIView *txtFieldView;
    
    IBOutlet UIView *borderView;
   
    UITextView *txtView;
    IBOutlet UIButton *likeButton;
    
    IBOutlet UIButton *shareButton;
}

@property (nonatomic, retain) NotificationData *selNotxData;



@end
