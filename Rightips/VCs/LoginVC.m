//
//  LoginVC.m
//  Rightips
//
//  Created by Abdul Mannan on 11/27/14.
//  Copyright (c) 2014 Beaconwatcher. All rights reserved.
//

#import "LoginVC.h"
#import "SettingsVC.h"

#import <QuartzCore/QuartzCore.h>

@interface LoginVC () <UITextFieldDelegate>
{
    MBProgressHUD *HUD;
    NSUserDefaults *defaults;
    UITextField *focusedTF;
    
    UserProfile *userProfile;
}

@property (nonatomic, retain) IBOutlet UITextField *emailTF;
@property (nonatomic, retain) IBOutlet UITextField *passwordTF;
@property (nonatomic, retain) IBOutlet UIButton *loginButton;
@property (nonatomic, retain) IBOutlet UIButton *signupButton;

@end

@implementation LoginVC

@synthesize emailTF, passwordTF;
@synthesize loginButton, signupButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    
    defaults = [NSUserDefaults standardUserDefaults];
    
    userProfile = [[UserProfile alloc] init];
    
    loginButton.layer.cornerRadius = 5.0f;
    loginButton.clipsToBounds = YES;
    signupButton.layer.cornerRadius = 5.0f;
    signupButton.clipsToBounds = YES;
    
    // TODO: Test account
    //[emailTF setText:@"email4shahbaz@gmail.com"];
    //[passwordTF setText:@"test"];
}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //Sending current screen to Piwic Tracker
    //[[PiwikTracker sharedInstance] sendView:@"Login"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    focusedTF = textField;
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    if (textField == emailTF)
    {
        [passwordTF becomeFirstResponder];
    } else if (textField == passwordTF) {
        [textField resignFirstResponder];
        [self loginButtonPressed:loginButton];
    }
    
    return NO;
}



- (IBAction)loginButtonPressed:(id)sender
{
    if ([self validateInputData] == YES)
    {
        [self performLogin:emailTF.text password:passwordTF.text];
//        [self verifyEmail:emailTF.text];
    }
}

- (IBAction)skipLoginButtonPressed:(id)sender
{
    [self pushNextVC];
}

- (void)pushNextVC
{
    id object = [defaults objectForKey:[StaticData instance].kSelCatsIds];
    if (!object)
        [Methods pushVCinNCwithName:@"SettingsVC" popTop:YES];
    else
        [Methods pushVCinNCwithName:@"HomeVC" popTop:YES];
}

- (BOOL)validateInputData
{
    if ([Methods validateTextFieldData:self tf:emailTF tfName:@"Email" shouldTrim:YES showMsg:YES]) {
        if([Methods validateTextFieldData:self tf:passwordTF tfName:@"Password" shouldTrim:YES showMsg:YES]) {
            [focusedTF resignFirstResponder];
            return YES;
        }
        else
            return NO;
    }
    
    return NO;
}
//-(void)verifyEmail:(NSString *)email
//{
//    NSString *apiUrl = @"https://bpi.briteverify.com/emails.json?";
//    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"login"]];
//    
//    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
//    [postParams setValue:email forKey:@"address"];
//    [postParams setValue:@"031b8dbb-2485-4916-923f-e348b9075ca2" forKey:@"apikey"];
//    
//    
//    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
//    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
//    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiUrl parameters:postParams];
//    [request setTimeoutInterval:[StaticData instance].serverTimeout];
//    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
//    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
//                                         {
//                                             [HUD hide:YES];
//                                             if ([[JSON objectForKey:@"status"] isEqualToString:@"invalid"])
//                                             {
//                                                 [Methods showAlertView:@"" message:[JSON objectForKey:@"error"] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
//                                             }
//                                             else
//                                             {
//                                                 [self performLogin:emailTF.text password:passwordTF.text];
//                                             }
//                                             
//                                         }
//                                                                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
//                                         {
//                                             [HUD hide:YES];
//                                             [Methods showAlertView:@"" message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
//                                         }];
//    [operation start];
//    [HUD setLabelText:NSLocalizedString(@"Please Wait.", nil)];
//    [HUD setDetailsLabelText:NSLocalizedString(@"Verifying the Email", nil)];
//    [HUD show:YES];
//}

- (void)performLogin:(NSString *)username password:(NSString *)password
{
    NSString *apiUrl = [NSString stringWithFormat:@"%@", [StaticData instance].baseUrl];
    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"login"]];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setValue:username forKey:@"email"];
    [postParams setValue:password forKey:@"pass"];
    [postParams setValue:@"0" forKey:@"sm_id"];
    [postParams setValue:@"0" forKey:@"provider"];
    [postParams setValue:@"0" forKey:@"signup"];
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:apiUrl parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
        [HUD hide:YES];
        [self handleLoginSuccess:JSON];
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        [HUD hide:YES];
        [Methods showAlertView:@"" message:[error localizedDescription] buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }];
    [operation start];
    [HUD setLabelText:NSLocalizedString(@"Please Wait.", nil)];
    [HUD setDetailsLabelText:NSLocalizedString(@"Signing in ...", nil)];
    [HUD show:YES];
}

- (void)handleLoginSuccess:(id)JSON
{
    NSString *status = [JSON objectForKey:@"status"];
    NSString *message = [JSON objectForKey:@"message"];
    if ([status intValue] == 1)
    {
        NSArray *dataArray = [JSON objectForKey:@"data"];
        NSDictionary *dataDict = [dataArray objectAtIndex:0];
        
        userProfile.userIdEmail = [dataDict objectForKey:@"uid"];
        userProfile.userName = [dataDict objectForKey:@"user_name"];
//        userProfile.lastName = [dataDict objectForKey:@"last_name"];
        userProfile.profileImgUrl = [dataDict objectForKey:@"profile_pic"];
        userProfile.coverImgUrl = [dataDict objectForKey:@"cover_photo"];
        userProfile.email = [emailTF.text copy];
        userProfile.smId = @"0";
        userProfile.provider = @"0";
        
        NSData *userProfileData = [NSKeyedArchiver archivedDataWithRootObject:userProfile];
        [defaults setObject:userProfileData forKey:[StaticData instance].kUserProfileData];
        [defaults setBool:YES forKey:[StaticData instance].kLoggedIn];
        [defaults synchronize];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:kReferringUsersArray])
        {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:kReferringUsersArray] count]>0)
            {
                [ReuseAbleFunctions addFriends];
            }
        }
        // Piwik Event (Action Login)
        [PiwikTracker sharedInstance].userID = [dataDict objectForKey:@"uid"];
        [[PiwikTracker sharedInstance] sendEventWithCategory:CATEGORY_USERS action:ACTION_LOGIN name:[ReuseAbleFunctions convertDictionaryToJSONString:dataDict] value:nil];
        [self pushNextVC];
    } else {
        [Methods showAlertView:@"" message:message buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }
}

@end
