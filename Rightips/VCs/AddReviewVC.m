//
//  AddReviewVC.m
//  Rightips
//
//  Created by Mac on 05/01/2016.
//  Copyright (c) 2016 Beaconwatcher. All rights reserved.
//

#import "AddReviewVC.h"
#import "ImageCaptureView.h"

@interface AddReviewVC ()
{
    UIImageView *transparentImageView;
    UIButton *sendButton,*photoLibraryButton,*takePhotoButton,*selectedBtn;
    UIView *selectedPhotoView;
    UIImage *chosenImage;
    MBProgressHUD *HUD;
    CGSize keyboardSize;
}

@end

@implementation AddReviewVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [postItbutton.layer setCornerRadius:5.0];
    [postItbutton setClipsToBounds:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeImage) name:@"changeImage" object:nil];
    
//    UIView *redView = [[UIView alloc] initWithFrame:CGRectMake(0, 400, self.view.frame.size.width, 50)];
//    [redView setBackgroundColor:[UIColor redColor]];
//    [scrlView addSubview:redView];
    
    [[cameraButton imageView] setContentMode:UIViewContentModeScaleAspectFit];
    
    HUD  = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    
    UserProfile *currentUserProfile = [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData] ];
    NSString *str = NSLocalizedString(@"what do you think of", nil);
    [reviewLabel setText:[NSString stringWithFormat:@"%@, %@ %@",currentUserProfile.userName,str,self.notificationdata.siteName]];
    
    ratingView = [[RatingView alloc] initWithFrame:ratingView.frame
                                    selectedImageName:@"RedStarIcon"
                                      unSelectedImage:@"GreyStarIcon"
                                             minValue:0
                                             maxValue:5
                                        intervalValue:1
                                           stepByStep:NO];
    ratingView.value = self.ratingValue;
    [ratingView setDelegate:self];
    [scrlView addSubview:ratingView];
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tap];
    
    [self ratingUpdate:ratingView.value];
    
}
-(void)viewDidLayoutSubviews
{
    [scrlView setContentSize:CGSizeMake(self.view.frame.size.width, 450.0)];
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.tag == 1)
    {
        [reviewTxtView resignFirstResponder];
    }
    
}
-(void)handleTap:(UITapGestureRecognizer *)recognizer
{
    [reviewTxtView resignFirstResponder];
}
- (IBAction)tapOnBackButton:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)cameraButtonPressed:(UIButton *)sender
{
    [reviewTxtView resignFirstResponder];
    [self setupView];
   
    
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         [transparentImageView setFrame:self.view.frame];
                     }
                     completion:^(BOOL finished){
                     }];
    [self.view addSubview:transparentImageView];
    
    
    
}
- (IBAction)postReview:(UIButton *)sender
{
    
    if ([reviewTxtView.text isEqualToString:@"I spend a great time with my friends,in this wonderful place...-(optional)"])
    {
        [Methods showAlertView:@"" message:@"Please enter some review" buttonText:@"OK" tag:0 delegate:nil];
        
        return;
    }
    
    NSData *userProfileData = [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData];
    UserProfile *userProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userProfileData];
    
//    NSString *catIdsStr = @"";
//    if (selCatsIds)
//        catIdsStr = [selCatsIds componentsJoinedByString:@","];
    
    CGRect rect = CGRectMake(0,0,500,500);
    UIGraphicsBeginImageContext( rect.size );
    [chosenImage drawInRect:rect];
    UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSData *imageData = UIImagePNGRepresentation(picture1);
    
    NSString *apiUrl = @"http://www.rightips.com/api/index.php?action=saveReview";
    //    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"getCategories"]];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setValue:userProfile.userIdEmail forKey:@"uid"];
    [postParams setValue:self.notificationdata.siteId forKey:@"site_id"];
    [postParams setValue:reviewTxtView.text forKey:@"rev_text"];
    [postParams setValue:[NSString stringWithFormat:@"%f",ratingView.value] forKey:@"rev_rate"];
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request;
    if (!chosenImage)
    {
        request = [client requestWithMethod:@"POST" path:apiUrl parameters:postParams];
    }
    else
    {
        request = [client multipartFormRequestWithMethod:@"POST" path:apiUrl parameters:postParams constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
            [formData appendPartWithFileData: imageData name:@"file" fileName:@"file.png" mimeType:@"image/png"];
        }];
    }
    
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
        [HUD hide:YES];
    
        [reviewTxtView setText:NSLocalizedString(@"I spend a great time with my friends,in this wonderful place...-(optional)", nil)];
        [self tapOnBackButton:nil];
        
        
    }
        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        [HUD hide:YES];
                                             
        [Methods showAlertView:@"" message:[error localizedDescription] buttonText:@"OK" tag:0 delegate:nil];
    }];
    [operation start];
    [HUD setLabelText:NSLocalizedString(@"Save Prefrences", nil)];
    ////    [HUD setDetailsLabelText:NSLocalizedString(@"Fetching data ...", nil)];
    [HUD show:YES];
    
}
- (void)rateChanged:(RatingView *)sender
{
    [self ratingUpdate:sender.value];
}
-(void)ratingUpdate:(CGFloat )value
{
    if (value == 1)
    {
        [ratingLabel setText:NSLocalizedString(@"Didn't like it", nil)];
    }
    else if (value == 2)
    {
        [ratingLabel setText:NSLocalizedString(@"Would not recommend", nil)];
    }
    else if (value == 3)
    {
        [ratingLabel setText:NSLocalizedString(@"It was okay", nil)];
    }
    else if (value == 4)
    {
        [ratingLabel setText:NSLocalizedString(@"Really liked it", nil)];
    }
    else if (value == 5)
    {
        [ratingLabel setText:NSLocalizedString(@"Loved it!", nil)];
    }
}
-(void)setupView
{
    transparentImageView = [[UIImageView alloc] init];
    [transparentImageView setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    [transparentImageView setImage:[UIImage imageNamed:@"TransparentImage"]];
    [transparentImageView setUserInteractionEnabled:YES];
    //    [transparentImgView setBackgroundColor:[UIColor blackColor]];
    //    [transparentImgView setAlpha:0.5];
    
    
    
    UIView *cancelView = [[UIView alloc] init];
    [cancelView setFrame:CGRectMake(20, self.view.frame.size.height - 100, self.view.frame.size.width - 40, 50)];
    //    [cancelView setCenter:CGPointMake(self.view.frame.size.width / 2, cancelView.frame.origin.y)];
    [cancelView setBackgroundColor:[UIColor whiteColor]];
    [cancelView setAlpha:1.0];
    [cancelView.layer setCornerRadius:5.0];
    [cancelView setClipsToBounds:YES];
    [transparentImageView addSubview:cancelView];
    //    UIView *overlay = [[UIView alloc] initWithFrame:cancelView.frame];
    //    overlay.backgroundColor = [UIColor blackColor];
    //    overlay.alpha = 0.6;
    //    [cancelView addSubview:overlay];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btn setFrame:cancelView.bounds];
    [btn setTitle:@"Cancel" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [btn setBackgroundColor:[UIColor whiteColor]];
    [btn addTarget:self action:@selector(cancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelView addSubview:btn];
    
    UIView *libraryORTakePhotoView = [[UIView alloc] init];
    [libraryORTakePhotoView setFrame:CGRectMake(20, cancelView.frame.origin.y - 60, self.view.frame.size.width - 40, 50)];
    //    [libraryORTakePhotoView setCenter:CGPointMake(self.view.frame.size.width / 2, libraryORTakePhotoView.frame.origin.y)];
    [libraryORTakePhotoView setBackgroundColor:[UIColor whiteColor]];
    [libraryORTakePhotoView setAlpha:1.0];
    [libraryORTakePhotoView.layer setCornerRadius:5.0];
    [libraryORTakePhotoView setClipsToBounds:YES];
    [transparentImageView addSubview:libraryORTakePhotoView];
    
    //    [overlay setFrame:libraryORTakePhotoView.frame];
    //    overlay.backgroundColor = [UIColor blackColor];
    //    overlay.alpha = 0.6;
    //    [libraryORTakePhotoView addSubview:overlay];
    
    sendButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [sendButton setFrame:CGRectMake(0, 0, libraryORTakePhotoView.frame.size.width, libraryORTakePhotoView.frame.size.height)];
    [sendButton setTitle:NSLocalizedString(@"Use Selected Image", nil) forState:UIControlStateNormal];
    [sendButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [sendButton setBackgroundColor:[UIColor whiteColor]];
    [sendButton setHidden:YES];
    [sendButton setAlpha:1.0];
    [sendButton addTarget:self action:@selector(selectedImage) forControlEvents:UIControlEventTouchUpInside];
    [libraryORTakePhotoView addSubview:sendButton];
    
    photoLibraryButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [photoLibraryButton setFrame:CGRectMake(0, 0, libraryORTakePhotoView.frame.size.width / 2, libraryORTakePhotoView.frame.size.height)];
    [photoLibraryButton setTitle:NSLocalizedString(@"Photo Library", nil) forState:UIControlStateNormal];
    [photoLibraryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [photoLibraryButton setBackgroundColor:[UIColor whiteColor]];
    [photoLibraryButton addTarget:self action:@selector(libraryPhotoButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [photoLibraryButton.layer setBorderColor:[UIColor blackColor].CGColor];
    [photoLibraryButton.layer setBorderWidth:0.5];
    [photoLibraryButton setAlpha:1.0];
    [libraryORTakePhotoView addSubview:photoLibraryButton];
    
    takePhotoButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [takePhotoButton setFrame:CGRectMake(libraryORTakePhotoView.frame.size.width / 2, 0, libraryORTakePhotoView.frame.size.width / 2, libraryORTakePhotoView.frame.size.height)];
    [takePhotoButton setTitle:NSLocalizedString(@"Take Photo", nil) forState:UIControlStateNormal];
    [takePhotoButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [takePhotoButton setBackgroundColor:[UIColor whiteColor]];
    [takePhotoButton addTarget:self action:@selector(takePhotoButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [takePhotoButton.layer setBorderColor:[UIColor blackColor].CGColor];
    [takePhotoButton.layer setBorderWidth:0.5];
    [takePhotoButton setAlpha:1.0];
    [libraryORTakePhotoView addSubview:takePhotoButton];
    
    selectedPhotoView = [[UIView alloc] init];
    [selectedPhotoView setFrame:CGRectMake(20, libraryORTakePhotoView.frame.origin.y - 130, self.view.frame.size.width - 40, 120)];
    //    [selectedPhotoView setCenter:CGPointMake(self.view.frame.size.width / 2, selectedPhotoView.frame.origin.y)];
    [selectedPhotoView setBackgroundColor:[UIColor whiteColor]];
    [selectedPhotoView setHidden:YES];
    [selectedPhotoView.layer setCornerRadius:5.0];
    [selectedPhotoView setClipsToBounds:YES];
    [selectedPhotoView setAlpha:1.0];
    [transparentImageView addSubview:selectedPhotoView];
    
    //    selectedImageView = [[UIImageView alloc] init];
    selectedBtn = [[UIButton alloc] init];
    [selectedBtn setFrame:CGRectMake(10, 10, 100, 100)];
    [selectedBtn setCenter:CGPointMake(selectedPhotoView.frame.size.width / 2, selectedPhotoView.frame.size.height / 2)];
    [selectedBtn setHidden:YES];
    //    [selectedBtn setBackgroundImage:[self imageWithColor:[UIColor clearColor]] forState:UIControlStateHighlighted];
    [selectedBtn addTarget:self action:@selector(imageButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [selectedPhotoView addSubview:selectedBtn];
    
    
}
-(void)selectedImage
{
    [cameraButton setImage:chosenImage forState:UIControlStateNormal];
    [cameraButton setBackgroundColor:[UIColor clearColor]];
    
    
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         [transparentImageView setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
                     }
                     completion:^(BOOL finished){
                         if (finished)
                             [transparentImageView removeFromSuperview];
                     }];
}
-(void)cancelButtonPressed
{
    chosenImage = nil;
    
    
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         [transparentImageView setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
                     }
                     completion:^(BOOL finished){
                         if (finished)
                             [transparentImageView removeFromSuperview];
                     }];
}
-(void)libraryPhotoButtonPressed
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}
-(void)takePhotoButtonPressed
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:NULL];
}
- (void)imageButtonPressed:(UIButton *)sender
{
    
    if ([sender isSelected])
    {
        //        [[sender layer] setBorderColor:[UIColor whiteColor].CGColor];
        //        UIColor *buttonColor = [[UIColor greenColor] colorWithAlphaComponent:0.0f];;
        //        [sender setImage:[Methods imageWithColor:buttonColor size:sender.frame.size] forState:UIControlStateNormal];
        [sender setSelected:NO];
        [takePhotoButton setHidden:NO];
        [photoLibraryButton setHidden:NO];
        [sendButton setHidden:YES];
        
        for (UIView *tickView in sender.subviews)
        {
            if (tickView.tag == 7)
            {
                [tickView removeFromSuperview];
            }
        }
        
    } else
    {
        [[sender layer] setBorderColor:[UIColor greenColor].CGColor];
        
        [sender setSelected:YES];
        [takePhotoButton setHidden:YES];
        [photoLibraryButton setHidden:YES];
        [sendButton setHidden:NO];
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(70, selectedBtn.frame.size.height - 30, 30, 30)];
        imgView.layer.cornerRadius = imgView.frame.size.height/2;
        imgView.image = [UIImage imageNamed: @"icon_tick"];
        imgView.tag = 7;
        [sender addSubview:imgView];
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    chosenImage = info[UIImagePickerControllerEditedImage];
    
    if (chosenImage)
    {
        
        [selectedPhotoView setHidden:NO];
        [selectedBtn setHidden:NO];
        [selectedBtn setSelected:YES];
        //        [selectedBtn setBackgroundImage:chosenImage forState:UIControlStateNormal];
        [selectedBtn setImage:chosenImage forState:UIControlStateNormal];
        //        [selectedBtn setBackgroundImage:chosenImage forState:UIControlStateHighlighted];
        [takePhotoButton setHidden:YES];
        [photoLibraryButton setHidden:YES];
        [sendButton setHidden:NO];
        
        UIImageView *tickImageView = [[UIImageView alloc] initWithFrame:CGRectMake(70, selectedBtn.frame.size.height - 30, 30, 30)];
        tickImageView.layer.cornerRadius = tickImageView.frame.size.height/2;
        tickImageView.image = [UIImage imageNamed:@"icon_tick"];
        [tickImageView setTag:7];
        [selectedBtn addSubview:tickImageView];
    }
    
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (void)keyboardWillShow:(NSNotification *)notification
{
//    isKeyboardHide = NO;
     keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    
}

-(void)keyboardWillHide:(NSNotification *)notification
{
//    isKeyboardHide = YES;
    if ([reviewTxtView.text isEqualToString:@""])
    {
        [reviewTxtView setText:@"I spend a great time with my friends,in this wonderful place...-(optional)"];
    }
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:@"I spend a great time with my friends,in this wonderful place...-(optional)"])
    {
        [textView setText:@""];
    }
}
//- (void)textViewDidEndEditing:(UITextView *)textView
//{
//    if ([textView.text isEqualToString:@""])
//    {
//        statements
//    }
//}
//- (void)textViewDidChange:(UITextView *)textView
//{
//    
//    CGFloat height = textView.contentSize.height  + keyboardSize.height;
//    [self resizeTextView:height];
//
//}
//-(void)resizeTextView:(CGFloat)height
//{
//    CGRect frame = reviewTxtView.frame;
//    frame.size.height = height;
//    reviewTxtView.frame = frame;
//    
//    
//}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
