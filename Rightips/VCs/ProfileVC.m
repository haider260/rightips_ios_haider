//
//  ProfileVC.m
//  Rightips
//
//  Created by Mac on 30/12/2015.
//  Copyright (c) 2015 Beaconwatcher. All rights reserved.
//

#import "ProfileVC.h"
#import "PhotoVC.h"
#import "SettingsVC.h"
#import "ReviewVC.h"
#import "LikeVC.h"
#import "ImageCaptureView.h"

#define kCatFoodImg    @"category_icon_bar"
#define kCatShopsImg    @"category_icon_shop"
#define kCatServicesImg    @"category_icon_services"
#define kCatEntertainmentImg    @"category_icon_entertainment"
#define kCatTourismImg    @"category_icon_tourism"
#define kCatEventsImg    @"category_icon_events"
#define kCatEducationImg    @"category_icon_students"
#define kTickImage  @"icon_tick"

@interface ProfileVC ()
{
    MBProgressHUD *HUD;
    UIImageView *transparentImageView;
    UIButton *selectImageButton,*photoLibraryButton,*takePhotoButton,*selectedBtn;
    UIImage *chosenImage;
    UIView *selectedPhotoView;
    NSInteger ind;
    UserProfile *loginUserProfile;
    NSMutableArray *receivedCategories,*selCatsIds;
    ImageCaptureView *view;
}

@end

@implementation ProfileVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [profileImageView.layer setCornerRadius:profileImageView.frame.size.width / 2];
    [profileImageView setClipsToBounds:YES];
    
    receivedCategories = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeImage) name:@"changeImage" object:nil];
    
    
    NSData *userProfileData = [[NSUserDefaults standardUserDefaults] objectForKey:[StaticData instance].kUserProfileData];
    loginUserProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userProfileData];
    
    [userNameLabel setText:self.userProfile.userName];

    
    [followerView.layer setBorderWidth:0.5];
    [followerView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    
    [reviewsView.layer setBorderWidth:0.5];
    [reviewsView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    
    
    [likesView.layer setBorderWidth:0.5];
    [likesView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    
    
    [photosView.layer setBorderWidth:0.5];
    [photosView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
   
    
//    [categoryView.layer setBorderWidth:1.0];
//    [categoryView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    
    [settingsButton.layer setBorderWidth:1.0];
    [settingsButton.layer setBorderColor:[UIColor redColor].CGColor];
    
    [followButton.layer setBorderWidth:1.0];
    [followButton.layer setBorderColor:[UIColor redColor].CGColor];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.dimBackground = YES;
    
    
//    [self getProfile];
    if ([self.userProfile.userIdEmail isEqualToString:loginUserProfile.userIdEmail])
    {
        [cameraButton setHidden:NO];
        [settingsButton setHidden:NO];
        [followButton setHidden:YES];
    }
    else
    {
        [cameraButton setHidden:YES];
        [settingsButton setHidden:YES];
        [followButton setHidden:NO];
    }
        
        

    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self getProfile];
}

- (void)getCategories
{
    NSString *apiUrl = [NSString stringWithFormat:@"%@", @"http://api.beaconwatcher.com/index.php?action="];
    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"getCategories"]];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setValue:[StaticData instance].apiKey forKey:@"key"];
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:apiUrl parameters:postParams];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
    [HUD hide:YES];
    [self handleCategoriesSuccess:JSON];
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
    [HUD hide:YES];
                                             
    //[Methods showAlertView:@"" message:[error localizedDescription] buttonText:@"OK" tag:0 delegate:nil];
    }];
    [operation start];
    //    [HUD setLabelText:NSLocalizedString(@"Please Wait.", nil)];
    //    [HUD setDetailsLabelText:NSLocalizedString(@"Fetching data ...", nil)];
    //    [HUD show:YES];
}

- (void)handleCategoriesSuccess:(id)JSON
{
    [receivedCategories removeAllObjects];
    NSString *status = [JSON objectForKey:@"status"];
    NSString *message = [JSON objectForKey:@"message"];
    if ([status intValue] == 1)
    {
        NSArray *dataArray = [JSON objectForKey:@"data"];
        for (unsigned int index = 0; index < [dataArray count]; index++)
        {
            CategoryData *catData = [[CategoryData alloc] init];
            catData.catId = [[dataArray objectAtIndex:index] objectForKey:@"cat_id"];
            catData.catName = [[dataArray objectAtIndex:index] objectForKey:@"cat_name"];
            catData.catImgUrl = [[dataArray objectAtIndex:index] objectForKey:@"cat_icon"];
            [receivedCategories addObject:catData];
        }
        [self getCategoryIcons];
    } else {
        [Methods showAlertView:@"" message:message buttonText:NSLocalizedString(@"OK", nil) tag:0 delegate:nil];
    }
    
}
-(void)getCategoryIcons
{
    NSArray *viewsToRemove = [categoryView subviews];
    for (UIView *v in viewsToRemove)
    {
        [v removeFromSuperview];
    }

    for (int i = 0; i < [receivedCategories count]; i++)
    {
        
    
    CategoryData *catData = [receivedCategories objectAtIndex:i];
    ALRadialButton *contView = [[ALRadialButton alloc] init];
    [contView setBackgroundColor:[UIColor clearColor]];
        
    if (i % 2)
    {
       [contView setFrame:CGRectMake((40 * i) - 10, 80, 60, 60)];
        
    }
    else
    {
        [contView setFrame:CGRectMake((40 * i) - 10, 10, 60, 60)];
    }
    
        
    
    UIImageView *imageView = [[UIImageView alloc] init];
    [imageView setFrame:CGRectMake(0, 0, 60, 60)];
    [imageView setClipsToBounds:YES];
    [[imageView layer] setCornerRadius:imageView.bounds.size.width/2];
    [self setCategoryImageWithCatData:catData AndImageView:imageView];
    [contView addTarget:self action:@selector(catButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [contView setUserInteractionEnabled:NO];
    
//    if ([self.userProfile.userIdEmail isEqualToString:loginUserProfile.userIdEmail])
//    {
//        [contView setUserInteractionEnabled:YES];
//    }
//    else if (self.userProfile)
//    {
//        [contView setUserInteractionEnabled:NO];
//        
//    }
    
//    UILabel *label = [[UILabel alloc] init];
//    [label setFrame:CGRectMake(0, 40, 80, 20)];
//    [label setBackgroundColor:[UIColor clearColor]];
//    [label setTextAlignment:NSTextAlignmentCenter];
//    [label setLineBreakMode:NSLineBreakByWordWrapping];
//    [label setNumberOfLines:0];
//    [label setFont:[UIFont fontWithName:@"ArialMT" size:11]];
//    [label setTextColor:[UIColor blackColor]];
//    [label setText:catData.catName];
    
//    float oldWidth = label.frame.size.width;
//    [label sizeToFit];
//    CGRect labelFrame = label.frame;
//    labelFrame.size.width = oldWidth;
//    label.frame = labelFrame;
    
    [contView addSubview:imageView];
//    [contView addSubview:label];
    if ([self isCatSelected:catData.catId])
    {
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(30, 30, 30, 30)];
        imgView.image = [UIImage imageNamed:kTickImage];
        imgView.tag = 7;
        [contView addSubview:imgView];
        [contView setSelected:YES];
    }
    else
    {
        [contView setAlpha:0.5];
        [contView setSelected:NO];
            
    }
    [categoryView addSubview:contView];
    }
    
    
}
- (void)catButtonPressed:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if ([button isSelected])
    {
        [[button layer] setBorderColor:[UIColor whiteColor].CGColor];
        UIColor *buttonColor = [[UIColor greenColor] colorWithAlphaComponent:0.0f];
        [button setImage:[Methods imageWithColor:buttonColor size:button.frame.size] forState:UIControlStateNormal];
        [button setSelected:NO];
        for (UIView *view in button.subviews)
        {
            if (view.tag == 7)
            {
                [view removeFromSuperview];
                [view setAlpha:0.5];
            }
        }
        
    } else
    {
        [[button layer] setBorderColor:[UIColor greenColor].CGColor];
        
        [button setSelected:YES];
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(30, 30, 30, 30)];
        imgView.layer.cornerRadius = imgView.frame.size.height/2;
        imgView.image = [UIImage imageNamed:kTickImage];
        imgView.tag = 7;
        [button addSubview:imgView];
    }
}
- (BOOL)isCatSelected:(NSString *)catId
{
    for (unsigned int index = 0; index < [selCatsIds count]; index++)
    {
        NSString *selCatId = [selCatsIds objectAtIndex:index];
        if ([selCatId isEqualToString:catId])
            return YES;
    }
    return NO;
}
- (void) setCategoryImageWithCatData:(CategoryData*)catData AndImageView:(UIImageView*)imgView
{
    switch ([catData.catId integerValue])
    {
        case 1:
            [imgView setImage:[UIImage imageNamed:kCatFoodImg]];
            break;
        case 2:
            [imgView setImage:[UIImage imageNamed:kCatShopsImg]];
            break;
        case 3:
            [imgView setImage:[UIImage imageNamed:kCatServicesImg]];
            break;
        case 4:
            [imgView setImage:[UIImage imageNamed:kCatEntertainmentImg]];
            break;
        case 5:
            [imgView setImage:[UIImage imageNamed:kCatTourismImg]];
            break;
        case 6:
            [imgView setImage:[UIImage imageNamed:kCatEventsImg]];
            break;
        case 7:
            [imgView setImage:[UIImage imageNamed:kCatEducationImg]];
            break;
            
        default:
            break;
    }
}
-(void)getProfile
{
    [HUD setLabelText:NSLocalizedString(@"Fetching Profile", nil)];
    [HUD show:YES];
    
    NSString *apiUrl;
    
    apiUrl = [NSString stringWithFormat:@"http://www.rightips.com/api/index.php?action=getProfile&uid=%@&request_uid=%@",self.userProfile.userIdEmail,loginUserProfile.userIdEmail];

    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"GET" path:apiUrl parameters:nil];
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
    [HUD hide:YES];
                                             
    if([[JSON objectForKey:@"status"] integerValue] == 1)
    {
     
    [self fetchProfile:JSON];
    [self getCategories];
    
    }
    else if([JSON objectForKey:@"message"])
    {
                                                 
    [[[UIAlertView alloc] initWithTitle:nil message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
    }
                                             
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
                                             
    [HUD hide:YES];
    }];
    [operation start];
}
-(void)fetchProfile:(id)json
{
    
    NSArray *commentsArray = [json objectForKey:@"data"];
    
    NSDictionary *dic = [commentsArray objectAtIndex:0];
    
    [likesLabel setText:[dic objectForKey:@"totalLikes"]];
    [reviewsLabel setText:[dic objectForKey:@"totalReviews"]];
    [photosLabel setText:[dic objectForKey:@"totalPhotos"]];
    [followerLabel setText:[dic objectForKey:@"totalFollowers"]];
    NSString *timeLeft = [self timeLeftSinceTime:[NSString stringWithFormat:@"%@000",[dic objectForKey:@"created"]].doubleValue];
    [subscribeDateLabel setText:timeLeft];
    
    if (![self.userProfile.userIdEmail isEqualToString:loginUserProfile.userIdEmail])
    {
        
    if ([[dic objectForKey:@"is_followed"] isEqualToString:@"0"])
    {
        [followButton setHidden:NO];
        [unFollowButton setHidden:YES];
    }
    else
    {
        [followButton setHidden:YES];
        [unFollowButton setHidden:NO];
    }
    }

    [self.userProfile setProfileImgUrl:[dic objectForKey:@"profile_pic"]];
    [self.userProfile setCoverImgUrl:[dic objectForKey:@"cover_photo"]];
    if (self.userProfile.profileImgUrl && ![self.userProfile.profileImgUrl isEqual:[NSNull null]] && ![self.userProfile.profileImgUrl isEqualToString:@""])
    {
        [Methods setImageInImageViewFromUrl:profileImageView urlStr:self.userProfile.profileImgUrl];
    }
    else
    {
        [profileImageView setImage:[UIImage imageNamed:@"NoProfileImage"]];
    }
    
    if (self.userProfile.coverImgUrl && ![self.userProfile.coverImgUrl isEqual:[NSNull null]] && ![self.userProfile.coverImgUrl isEqualToString:@""])
    {
        [Methods setImageInImageViewFromUrl:coverImageView urlStr:self.userProfile.coverImgUrl];
    }
    else
    {
        [coverImageView setImage:nil];
    }
    NSString *selCatIdStr;

    selCatIdStr = [dic objectForKey:@"tribes"];
    
    if (![selCatIdStr isEqual:[NSNull null]])
    {
         NSArray *arrComponents = [selCatIdStr componentsSeparatedByString:@","];
        selCatsIds = [NSMutableArray arrayWithArray:arrComponents];
    }
   
    
    if (!selCatsIds)
        selCatsIds = [NSMutableArray array];
    
}

-(void)changeImage
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *userProfileData = [defaults objectForKey:[StaticData instance].kUserProfileData];
    UserProfile *userProfile = [NSKeyedUnarchiver unarchiveObjectWithData:userProfileData];
    
    if (ind == 0)
    {
        //                 [profileImageView setImageURL:[NSURL URLWithString:imgUrl]];
        [Methods setImageInImageViewFromUrl:profileImageView urlStr:userProfile.profileImgUrl];
        
    }
    else if (ind == 1)
    {
        //                [coverImageView setImageURL:[NSURL URLWithString:imgUrl]];
        [Methods setImageInImageViewFromUrl:coverImageView urlStr:userProfile.coverImgUrl];
        
        
    }

}

- (IBAction)backbuttonPressed:(UIButton *)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navController = (UINavigationController *) appDelegate.window.rootViewController;
    [navController popViewControllerAnimated:YES];
}

- (IBAction)photosButtonPressed:(id)sender
{
    PhotoVC *vc = (PhotoVC *)[Methods getVCbyName:@"PhotoVC"];
    [vc setUserProfile:self.userProfile];
    [Methods pushVCinNCwithObj:vc popTop:NO];
}
- (IBAction)settingButtonPressed:(UIButton *)sender
{
    SettingsVC *vc = (SettingsVC *)[Methods getVCbyName:@"SettingsVC"];
    [vc setUserProfile:self.userProfile];
    [Methods pushVCinNCwithObj:vc popTop:NO];
    
}
- (IBAction)reviewButtonPressed:(UIButton *)sender
{
    ReviewVC *vc = (ReviewVC *)[Methods getVCbyName:@"ReviewVC"];
    [vc setUserProfile:self.userProfile];
    [Methods pushVCinNCwithObj:vc popTop:NO];
}
- (IBAction)likeButtonPressed:(UIButton *)sender
{
    LikeVC *vc = (LikeVC *)[Methods getVCbyName:@"LikeVC"];
    [vc setUserProfile:self.userProfile];
    [Methods pushVCinNCwithObj:vc popTop:NO];
}
- (IBAction)cameraButtonPressed:(UIButton *)sender
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Edit Photo", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Change Profile Photo", nil),NSLocalizedString(@"Change Cover Photo", nil),nil];
//    [popup setTag:3];
    [popup showInView:self.view];
    
}
-(void)cancelButtonPressed
{
    //    chosenImage = nil;
    
    
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationCurveEaseIn
                     animations:^{
                         [transparentImageView setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
                     }
                     completion:^(BOOL finished)
    {
                         if (finished)
                             [transparentImageView removeFromSuperview];
                     }];
}
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ind = buttonIndex;
    
    if (ind != 2)
    {

        view = [[ImageCaptureView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        [view setInd:ind];
        
        
        
        
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             [view setFrame:self.view.frame];
                         }
                         completion:^(BOOL finished){
                         }];
        [self.view addSubview:view];
    }
   
    
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    chosenImage = info[UIImagePickerControllerEditedImage];
    
    if (chosenImage)
    {
        
        [selectedPhotoView setHidden:NO];
        [selectedBtn setHidden:NO];
        [selectedBtn setSelected:YES];
        //        [selectedBtn setBackgroundImage:chosenImage forState:UIControlStateNormal];
        [selectedBtn setImage:chosenImage forState:UIControlStateNormal];
        //        [selectedBtn setBackgroundImage:chosenImage forState:UIControlStateHighlighted];
        [takePhotoButton setHidden:YES];
        [photoLibraryButton setHidden:YES];
        [selectImageButton setHidden:NO];
        
        UIImageView *tickImageView = [[UIImageView alloc] initWithFrame:CGRectMake(70, selectedBtn.frame.size.height - 30, 30, 30)];
        tickImageView.layer.cornerRadius = tickImageView.frame.size.height/2;
        tickImageView.image = [UIImage imageNamed:@"icon_tick"];
        [tickImageView setTag:7];
        [selectedBtn addSubview:tickImageView];
    }
    
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (IBAction)followButtonPressed:(UIButton *)sender
{
    
    NSString *apiUrl = @"http://www.rightips.com/api/index.php?action=follow";
    //    apiUrl = [apiUrl stringByAppendingString:[NSString stringWithFormat:@"getCategories"]];
    
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    [postParams setValue:loginUserProfile.userIdEmail forKey:@"uid"];
    [postParams setValue:self.userProfile.userIdEmail forKey:@"uid_follow"];
    
    
    NSURL *url = [[NSURL alloc] initWithString:apiUrl];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:apiUrl parameters:postParams];
    
    
    [request setTimeoutInterval:[StaticData instance].serverTimeout];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
     {
         [HUD hide:YES];
         if([[JSON objectForKey:@"status"] integerValue] == 1)
         {
             [followButton setHidden:YES];
             [unFollowButton setHidden:NO];
             
         }
         else if([JSON objectForKey:@"message"])
         {
             
             [[[UIAlertView alloc] initWithTitle:nil message:[JSON objectForKey:@"message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] show];
         }
         
         
         
         
     }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
     {
         [HUD hide:YES];
         
         [Methods showAlertView:@"" message:[error localizedDescription] buttonText:@"OK" tag:0 delegate:nil];
     }];
    [operation start];
    
    [HUD show:YES];
}

- (IBAction)unfollowButtonPressed:(UIButton *)sender
{
    
}

-(NSString*)timeLeftSinceTime:(double)timesTamp
{
    NSString *timeLeft;
    
    double currentTimeInMiliSeconds = (double)([[NSDate date] timeIntervalSince1970] * 1000.0);
    
    
    double miliSeconds = currentTimeInMiliSeconds - timesTamp;
    double seconds = miliSeconds / 1000.0;
    
    
    NSInteger days = (int) (floor(seconds / (3600 * 24)));
    if(days) seconds -= days * 3600 * 24;
    
    NSInteger hours = (int) (floor(seconds / 3600));
    if(hours) seconds -= hours * 3600;
    
    NSInteger minutes = (int) (floor(seconds / 60));
    if(minutes) seconds -= minutes * 60;
    
    if(days) {
        timeLeft = [NSString stringWithFormat:@"%ld days ago", (long)days];
    }
    else if(hours) { timeLeft = [NSString stringWithFormat: @"%ld hours ago", (long)hours];
    }
    else if(minutes) { timeLeft = [NSString stringWithFormat: @"%ld minutes ago", (long)minutes];
    }
    else if(seconds)
        timeLeft = [NSString stringWithFormat: @"%ld seconds ago", (long)seconds];
    
    return timeLeft;
}



-(BOOL)prefersStatusBarHidden
{
    return YES;
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
